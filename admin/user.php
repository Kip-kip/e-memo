<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
//include_once '../config.php';

class DB_connect{
    
    private $fname;
    private $sname;
    private $memno;
    private $idnum;
    private $prinloan;
    private $duration;
    private $loanbal;
    private $weekleypay;
    private $loanpaid;
    private $savings;
    private $tsavings;
    private $tinterest;
    
    public function __construct(){
        
    }
    //fname
    public function getFname(){
        return $this->fname;
    }
    public function setFname($fName){
        $this->fname=$fName;
    }
    //sname
    public function getSname(){
        return $this->sname;
    }
    public function setSname($sName){
        $this->sname=$sName;
    }
    
    // a function which inserts admin form info into the database 
    public function insertAdmin($mysqli,$fname,$mname,$lname,$phone,$position,$acting,$department,$sector,$directorate,$password){
        //encrypt the password
        $salt='#!@*%';
	   $pepper='*-#$QW';
	   $password=hash('sha512',$salt.$password.$pepper);
      $stmt = $mysqli->prepare("SELECT phone from ememo_users WHERE phone = ?");
      $stmt->bind_param("s", $phone);
      $stmt->execute();
       $stmt->store_result();
        if ($stmt->num_rows > 0) {
 
            echo"<p style=\"color:#FC4A1A\">There already exists a user with that phone number</p>";
        }
        else{
             //get the department 
            $query1="SELECT department FROM departments_list WHERE department_id='$department'";
            if ($mysqli->real_query($query1)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $departmentname=$row['department'];
         }}
             //in the event that sector isnt selected
            if($department==''){
                $departmentname="";
            }
          
            
              //get the sectorname
           $query2="SELECT sector FROM sectors_list WHERE sector_id='$sector'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $sectorname=$row['sector'];
         }
            }
            //in the event that sector isnt selected
            if($sector==''){
                $sectorname="";
            }
               
            //determine the hierarchical_level
            if($position=='GO'){
                $hierarchical_level=9;
            }
            else if($position=='D.G'){
                $hierarchical_level=8;
            }
            else if($position=='C.S'){
                $hierarchical_level=7;
            }
            else if($position=='D.C.S'){
                $hierarchical_level=6;
            }
            else if($position=='C.O.F'){
                $hierarchical_level=5;
            }
            else if($position=='C.E.C.M'){
                $hierarchical_level=4;
            }
            else if($position=='C.O'){
                $hierarchical_level=3;
            }
            else if($position=='DIR'){
                $hierarchical_level=2;
            }
            else if($position=='D.DIR'){
                $hierarchical_level=1;
            }
            else{
                  $hierarchical_level=0;
            }
            
            
            
             if($stmnt=$mysqli->prepare("INSERT INTO ememo_users(fname,mname,lname,phone,position,acting,department,sector,directorate,hierarchical_level,password)VALUES(?,?,?,?,?,?,?,?,?,?,?)")){
            $stmnt->bind_param('sssssssssss',$fname,$mname,$lname,$phone,$position,$acting,$departmentname,$sectorname,$directorate,$hierarchical_level,$password);
            $stmnt->execute();
            $message="<p style=\"color:#FC4A1A\">Sucessful Registration</p>";
                echo $message;
    

                                }
            

                 
            
        else{
            $tii="oops couldnt perform the desired operation";
                echo $tii;
        }
       
            
        }
    }
    
    
    
}
?>