<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:../login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:../login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">    
   <link rel="stylesheet" href="../vendors/bower_components/select2/dist/css/select2.min.css">
   <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="../css/app.min.css">
    
    
<script src="jquery-3.0.0.js"></script>
   <script type="text/javascript">
                $(document).ready(function(){
       
            $('#departmentgroup').hide();
            $('#sectorgroup').hide();
            $('#directorategroup').hide();
    
                });
            </script>
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>


            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="../img/nanditrans.png">E-MEMO NCG</h1>
                </div>

               
 <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>
              

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                         <?php         
                                require("../_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $position=$row["position"];
         $department=$row["department"];
    
    
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>        
                        <div class="user__info" data-toggle="dropdown">
                          <?php
                            
                             echo "<img class=\"user__img\" src=\"img/admin.jpg\" alt=\"\">";
                            ?>
                            <div>
                       
                                <div class="user__name"> <?php
echo $position.'.&nbsp;'.$department;
?></div>
                                <div class="user__email"> <?php
echo $department;
?></div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                             <form action="index.php" method="post">
                             <button type="submit" class="btn btn-outline-danger" name="out">Log Out</button>
                               </form>
                             <?php
      if(isset($_POST['out'])){
          session_destroy();
   header("Location:../login.php");
      }    
    ?>
                        </div>
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                        
                        

                         </ul>
                </div>
            </aside>

            <section class="content">
                <header class="content__title">
                    <h1 id="dash">ADMINISTRATOR PAGE</h1>
                   
                </header>

                
                  
                
                  <div class="row quick-stats">
                        <!-------------------------------------------------ADD USER--------------------------------------> 
                                     <div class="col-sm-6 col-md-6">
       <p id="profiletext">Add User</p>
                       <div class="card">
                           
                            <div class="card-body"  id="cbmemcreation">
                                 <p>Register new users into the system.</p>
                                   <div class="login__block active" id="l-login">
                <h id="title">E-Memo</h>
                <div class="login__block__header">
                    <i id="mee" class="zmdi zmdi-account-circle"></i>
                    Create an account

                    <div class="actions actions--inverse login__block__actions">
                        <div class="dropdown">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="../login.php" class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-register" ></a>
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-forget-password" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login__block__body">
                    <form action="index.php" enctype="multipart/form-data" method="post">
                 
                        <div class="form-group">
                        <input type="text" name="fname" class="form-control text-center" placeholder="First Name" required>
                    </div>
                        <div class="form-group">
                        <input type="text" name="mname" class="form-control text-center" placeholder="Middle Name" required>
                    </div>
                        <div class="form-group">
                        <input type="text" name="lname" class="form-control text-center" placeholder="Last Name" required>
                    </div>
                    
                        <div class="form-group">
                        <input type="text" name="phone" class="form-control text-center" placeholder="Phone Number" required>
                    </div>
                    <div class="form-group">  
                        <select name="position"class="select2" onChange="hideFields(this.value);" required>
                                <option value="null">Select Position</option>
                                <option value="GO">Governor</option>
                                <option value="D.G">Deputy Governor</option>
                                <option value="C.S">County Secretary</option>
                                <option value="D.C.S">Deputy County Secretary</option>
                                <option value="C.O.F">C.O.F</option>
                                <option value="C.E.C.M">CEC</option>
                                <option value="C.O">Chief Officer</option>
                               
                     </select>

                    </div>
                        
                        <div class="form-group" id="actinggroup"> 
                         <label class="custom-control custom-checkbox">
                                <input type="checkbox" name="acting" class="custom-control-input" value="Ag">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Acting</span>
                            </label>
                          
                                <input type="checkbox" name="acting" value="A" hidden="true">
                                
                            
                         </div>
                    <div class="form-group" id="departmentgroup">  
                        <select name="department"class="select2" onChange="getSector(this.value);">
                                <option value="">Select Department</option>
                               	<?php
                            include_once '../connectdb.php';
                            
                            $sql1="SELECT * FROM departments_list";
                            $results=$dbhandle->query($sql1); 
                            while($rs=$results->fetch_assoc()) { 
                            ?>
                            <option value="<?php echo $rs["department_id"]; ?>"><?php echo $rs["department"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>

                    </div>
                        <div class="form-group" id="sectorgroup">  
                        <select name="sector" id="sector" class="select2" onChange="getDirectorate(this.value);">
                            <option value="">Select Sector</option>
                            
                        </select>

                    </div>
                        <div class="form-group" id="directorategroup">  
                        <select name="directorate" id="directorate" class="select2">
                                <option value="">Select Directorate</option>
                               
                            </select>

                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control text-center" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name ="confirmpass" class="form-control text-center" placeholder="Confirm Password" required>
                    </div>
                        
                 <!--div class="form-group">
                     <label>Upload User Signature</label>
                      <input type="file" name="photo" required/>
                    </div-->
                        
                     <button class="btn btn-dark btn--icon" name="submit" ><i class="zmdi zmdi-account-add"></i></button>
        
                        </form>
                    
<?php
include_once 'user.php';
require("../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
//create an instance of DB_con class, an object
$con= new DB_connect();
//insert data
if(isset($_POST['submit'])){
    $fname=$_POST['fname'];
    $mname=$_POST['mname'];
    $lname=$_POST['lname'];
    $phone=$_POST['phone'];
    $position=$_POST['position']; 
    $department=$_POST['department'];
    $sector=$_POST['sector'];
    $directorate=$_POST['directorate'];
    $password=$_POST['password'];
    $confirmpass=$_POST['confirmpass'];
    
   if (isset($_POST["acting"])) {
       $acting=$_POST['acting'];
}else{  
    $acting="";
}
    //checking if passwords match
    if($password==$confirmpass){
        
       
        //making sure info is saved 
        if($con->insertAdmin($mysqli,$fname,$mname,$lname,$phone,$position,$acting,$department,$sector,$directorate,$password)== true){
            
               $con->set_sign($mysqli);
        }
        
   
        
    }
    else{
        echo"please make sure you match the passwords, try again!!";
    }
    
    
}

    

?>
                    
                </div>
            </div>
                         
                            </div>

                        </div>
                    </div> 
                      
                      
                      
                      
               <!------------------------------------------------------ADD DEPARTMENT--------------------------------------> 
               <div class="col-sm-6 col-md-6">
       <p id="profiletext">Add Department</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="index.php">
             </br>
             
        <input type="text" name="department" class="form-control text-center" placeholder="Department Name" size=20  required/></br></br>
      <input type="text" name="department_id" class="form-control text-center" placeholder="Enter Code(in the format DXYZ, xyz being initials of departments name)" size=30  required/></br></br>
        
    
             
            <input type="submit" name="adddept"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
    
        </form>

 <?php
     if(isset($_POST['adddept'])){
        $department=$_POST['department'];
        $department_id=$_POST['department_id'];
      
    $sqlogs="insert into departments_list(department,department_id)VALUES(?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
   $stmnt->bind_param('ss',$department,$department_id);
    $stmnt->execute();
    echo "<p style=\"color:#FC4A1A\">Department successfully added</p>";
   
}
     }
    ?>
    
                         
                            </div>

                        </div>
                    </div>  

 <!-----------------------------------------------------------ADD SECTOR----------------------------------------------> 
              <div class="col-sm-6 col-md-6">
       <p id="profiletext">Add Sector</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="index.php">
             </br>
               <select name="department" class="select2" id="department">
                                           <option value="none">Select Department</option>
                                         <?php
                                            require("../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
 $query="SELECT * from departments_list ORDER BY id DESC";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $department=$row["department"];
        $department_id=$row["department_id"];
       
        
        
                               echo     "<option value=\"$department_id\">$department&nbsp;&nbsp;</option>";
    }
}

$db->close();
?>                                      
                                
    
</select>
    </br></br>
    
        <input type="text" name="sector" class="form-control text-center" placeholder="Sector Name" size=20  required/></br></br>
      <input type="text" name="sector_id" class="form-control text-center" placeholder="Enter Code(in the format SXYZ, xyz being initials of sectors name)" size=30  required/></br></br>
        
    
             
            <input type="submit" name="addsect"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
    
        </form>

 <?php
 require("../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
  if(isset($_POST['addsect'])){
        $department=$_POST['department'];
        $sector=$_POST['sector'];
        $sector_id=$_POST['sector_id'];
      
    $sqlogs="insert into sectors_list(sector,department_id,sector_id)VALUES(?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
   $stmnt->bind_param('sss',$sector,$department,$sector_id);
    $stmnt->execute();
    echo "<p style=\"color:#FC4A1A\">Sector successfully added</p>";
   
}
     }
    ?>
    
                         
                            </div>

                        </div>
                    </div> 


 <!---------------------------------------------------REMOVE DEPARTMENT--------------------------------------> 
                <div class="col-sm-6 col-md-6">
       <p id="profiletext">Remove Department</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="user_datadept" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                    </div> 

 <!-------------------------------------------------------Remove Sector----------------------------------------------------> 
               <div class="col-sm-6 col-md-6">
       <p id="profiletext">Remove Sector</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="user_datasect" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                    </div>  
</div>




 

        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
            </section>



        <!-- Javascript -->
          
            <script>            
             function hideFields(val){
                if(val=='C.E.C.M'){
                    $('#departmentgroup').show();
                    //hide the rest
                      $('#sectorgroup').hide();
                       $('#directorategroup').hide();
                       //$('#actinggroup').hide();
            
                }
                else if(val=='C.O'){
                       $('#departmentgroup').show();
                       $('#sectorgroup').show();
                     //hide
                      $('#directorategroup').hide();
                        //$('#actinggroup').hide();
                 }
                else if(val=='DIR'){
                       $('#departmentgroup').show();
                       $('#sectorgroup').show();
                       $('#directorategroup').show();
                  
                    // $('#actinggroup').show();
                 }
                 else if(val=="D.DIR"){
                       $('#departmentgroup').show();
                       $('#sectorgroup').show();
                       $('#directorategroup').show();
                  
                    // $('#actinggroup').show();
                 }
                 else{
                         $('#departmentgroup').hide();
                  
                       $('#sectorgroup').hide();
                       $('#directorategroup').hide();
                       //$('#actinggroup').hide();
                 }
             }
                
function getSector(val) {
	$.ajax({
	type: "POST",
	url: "get_sector.php",
	data:'department_id='+val,
	success: function(data){
		$("#sector").html(data);
	}
	});
}
            
function getDirectorate(val){
  	$.ajax({
	type: "POST",
	url: "get_directorate.php",
	data:'sector_id='+val,
	success: function(data){
		$("#directorate").html(data);
	}
	});  
}                

</script>

<!--delete-->
<script type="text/javascript" language="javascript">
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datadept').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"display_deletestations/fetchdept.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var iddept = $(this).attr("iddept");

    $.ajax({
     url:"display_deletestations/deletedept.php",
     method:"POST",
     data:{iddept:iddept},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datadept').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
     
    $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datasect').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"display_deletestations/fetchsect.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var idsect = $(this).attr("idsect");
 
    $.ajax({
     url:"display_deletestations/deletesect.php",
     method:"POST",
     data:{idsect:idsect},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datasect').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
     
</script>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>



        <!-- Vendors -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="../vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="../vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="../vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="../vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="../vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="../vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="../vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="../vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="../vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="../vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="../vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- Charts and maps-->
        <script src="../demo/js/flot-charts/curved-line.js"></script>
        <script src="../demo/js/flot-charts/line.js"></script>
        <script src="../demo/js/flot-charts/dynamic.js"></script>
        <script src="../demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="../demo/js/other-charts.js"></script>
        <script src="../demo/js/jqvmap.js"></script>

 <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>




        <!-- App functions and actions -->
        <script src="../js/app.min.js"></script>

        <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
        <script src="../js/rChat.js"></script>
        <script src="../js/rChatSent.js"></script>
        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>