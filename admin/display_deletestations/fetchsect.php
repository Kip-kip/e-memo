<?php

session_start();
if(!isset($_SESSION['karibu'])){
    header("Location:login.php");
}

//fetch.php
include_once '../../config.php';
$ses=$_SESSION['karibu'];
$columns = array('sector');

$query = "SELECT * FROM sectors_list";


$query .= '  ';

if(isset($_POST["order"])){
/* $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';*/
}
else{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1){
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($mysqli, $query));

$result = mysqli_query($mysqli, $query . $query1);

$data = array();
$i=1;
while($row = mysqli_fetch_array($result)){
 $sub_array = array();
 $sub_array[] = '<div  data-id="'.$row["id"].'" data-column="sector">'.$i++.'. '. $row["sector"] . '</div>';
 $sub_array[] = '<button type="button" name="delete" class="btn btn-danger btn--icon delete" idsect="'.$row["id"].'"><i class="zmdi zmdi-delete"></i></button>';
 $data[] = $sub_array;
}

function get_all_data($mysqli){
    
 $query =  "SELECT * FROM sectors_list";
 $result = mysqli_query($mysqli, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($mysqli),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
