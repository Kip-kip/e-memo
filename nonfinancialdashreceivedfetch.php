<!DOCTYPE html>
<html lang="en">

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

    </head>
<body>

                                <div class="table-responsive">
<table  id="data-table" class="table">
   
<tbody>
    
 
    	
<?php
 
require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    if(isset($_POST["reftracking"])){
        $reftrackingno = trim(mysqli_real_escape_string($db,$_POST["reftracking"]));
$query="SELECT * FROM logs where referenceno='$reftrackingno'";
//execute query
        $i=1;
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        $referenceno=$row["referenceno"];
         $actor_recepient=$row["actor_recepient"];
        $action=$row["action"];
        $subject=$row['subject'];
        $at=$row["at"];
        $cdate=date_create($row["cdate"]);
        
         if($action=='Commented'){
            $on="on";
        }
        else{
            $on="";
        }
        
        
        echo" 
   <div class=\"col-sm-6 col-md-12\">
				<div class=\"col-md-12 col-md-offset-0\">
					<ul class=\"timeline animate-box\">
						<p style=\"font-size:14px;color:#00b19d;\">Memo Activity Alert!</p>
						<li class=\"timeline-inverted animate-box\" style=\"font-size:13px; color:#505458;font-weight:500;\">
							".$i++.".<div class=\"timeline-badge\" style=\"background-color:#0d5247;opacity:1;\"><p style=\"font-size:15px; color:#fff;\">$action</p></div>
							<div class=\"timeline-panel\" id=\"box\">
								
								<div class=\"timeline-body\" style=\"color:#505458;font-weight:700;\">
									<p>$actor_recepient $action $on $subject $at ".date_format($cdate, 'g:ia \o\n l jS F Y')."</br></br>
                                                           
        
            </p>
								</div>
							</div>
						</li> 
						
			    	</ul>
				</div>
			</div>
    ";    
                
          
    }
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        
            
    }
    
    
    
    if(isset($_POST["refparticipants"])){
         echo'<thead>
       <tr>
                                                <th>Recipient</th>
                                                <th>Nature</th>
                                                <th>Status</th>
                                                 <th>Progress</th>
                                                    
                                                
                                                
                                            </tr>
    </thead>';
    
        $refparticipantsno = trim(mysqli_real_escape_string($db,$_POST["refparticipants"])); 
     // $query="SELECT * FROM nonfinancialmemos where referenceno='$refparticipantsno'";
        $query="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE referenceno='$refparticipantsno'";
//execute query
        $i=1;
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $user_id=$row['user_id'];
       
         $dp_file_ext=$row['dp_file_ext'];
        
         $id=$row["id"];
        $subject=$row["subject"];
        $referenceno=$row["referenceno"];
        $datecreated=$row["datecreated"];
        $duedate=$row["duedate"];
        $requestor=$row["requestor"];
        $progress=$row["progress"];
        $generalprogress=$row["generalprogress"];
        $status=$row["status"];
        $generalstatus=$row["generalstatus"];
        $introduction=$row['introduction'];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $position=$row['position'];
        $department=$row['department'];
        $sector=$row['sector'];
        $directorate=$row['directorate'];
        $section=$row["section"];
        $unit=$row["unit"];
             $urgency=$row['urgency'];
         $memotype=$row['memotype'];
        $nature=$row['nature'];
         /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       
        
         /*if condition for gen status buttons*/
        if($status=="Pending"){
            $color="info";
        }
        else if($status=="Approved"){
            $color="success";
        }
        else if($status=="Rejected") {
           $color="danger"; 
        }
        else if($status=="Closed"){
            $color="danger";
        }
         else if($status=="Noted"){
              $color="warning";
         }
        else if($status=="Forwarded"){
              $color="secondary";
         }
        else if($status=="Escalated"){
              $color="secondary";
         }
        else if($status=="Inactive"){
            $color="light";
        }
		else if($status=="Not Approved"){
			$color="danger";
		}
		else if($status=="Recommended"){
			$color="success";
		}
		else if($status=="Not Recommended"){
			$color="danger";
		}
        
        /**nature**/
         $hide="";
        if($nature=="Cc"){
            $hide="hidden";
        }
        
        /**prevent image flicker**/
        if($dp_file_ext==""){
            $steady="nandi.png";
        }
        else{
            $steady=$user_id.$dp_file_ext;
        }
      
         echo  "<tr>";
         echo"<td>".$i++.".    <img class=\"user__img\" src=\"img/profilepics/$steady\" alt=\"\">$position $station</td>";
         echo"<td>$nature recipient</td>";
         echo  "<td $hide><button type=\"button\" class=\"btn btn-$color btn-sm\">$status</button></td>";
         echo "<td $hide><button class=\"btn btn-dark btn--icon \"><p style=\"color:white; font-size:12px;\">$progress%</p></button></td>";
            
                echo "</tr>";
           
     
    }
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
    }
    
    
    /*****FETCH PROGRESS AND STATUS****/
    
    if(isset($_POST["refprogress"])){
        $refprogressno = trim(mysqli_real_escape_string($db,$_POST["refprogress"])); 
        $query="SELECT * FROM nonfinancialmemos where referenceno='$refprogressno' LIMIT 1";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        $generalstatus=$row["generalstatus"];
        $generalprogress=$row["generalprogress"];
        
         /*if condition for gen status buttons*/
        if($generalstatus=="Pending"){
            $color="info";
        }
        else if($generalstatus=="Approved"){
            $color="success";
        }
        else if($generalstatus=="Rejected") {
           $color="danger"; 
        }
        else if($generalstatus=="Closed"){
            $color="danger";
        }
         else if($generalstatus=="Noted"){
              $color="warning";
         }
        else if($generalstatus=="Forwarded"){
              $color="secondary";
         }
        else if($generalstatus=="Escalated"){
              $color="secondary";
         }
        else if($generalstatus=="Inactive"){
            $color="light";
        }
		else if($generalstatus=="Not Approved"){
			$color="danger";
		}
        
        
        
       echo"<div class=\"card-body\" id=\"cbmemcreation\">
       <small style=\"font-size:18px;\">General Status</h2></br><h2 id=\"hech2\"> <button type=\"button\" class=\"btn btn-$color btn-sm\">$generalstatus</button></small></br></br>
                    <h2 style=\"font-size:18px;\">General Progress</h2></br> <div class=\"progress mt-1\">
                          
                                            <div id=\"progbar\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: $generalprogress%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div></br>
                                            
                                        </div></br>
           <small style=\"font-size:14px;\">Memo approval is <button class=\"btn btn-dark btn--icon \"><p style=\"color:white; font-size:12px;\">$generalprogress%</p></button> completed&nbsp&nbsp&nbsp</small>
           </div>";
     
    }
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        
    }
$db->close();
?>
    </tbody>
    </table>
        </div>
        </body>
</html>
