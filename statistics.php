<?php
session_start();
if(!isset($_SESSION['karibu'])){
    header("Location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
 
     <script src="jquery-3.0.0.js"></script>
   
        <!-- App styles -->
      
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>

        
    <?php
    
require("./_connect.php");

$ses=$_SESSION['karibu'];
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}


//-----------------------------------NUMBER OF COMMUNICATION MEMOS-----------------------------------------------// 
    if(isset($_POST["commnum"])){
        $stationcommnum =trim(mysqli_real_escape_string($db,$_POST["stationcommnum"]));
        $periodcommnum=trim(mysqli_real_escape_string($db,$_POST["periodcommnum"]));
        $scnindicator=trim(mysqli_real_escape_string($db,$_POST["scnindicator"]));
        
        //if EXECUTIVE
        if($scnindicator=="exec"){
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodcommnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
    
                                    }     

                                }
                                echo $cocomm;
    }
          //if CECM
        else if($scnindicator=="cecm"){
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationcommnum' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodcommnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
    
                                    }     

                                }
                                echo  $cocomm;
        }
          //if CO
        else if($scnindicator=="co"){
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationcommnum' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodcommnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
   
                                    }     

                                }
                                echo $cocomm;
        }
    }
    
//-----------------------------------------------------NUMBER OF FINANCIAL MEMOS--------------------------------------------// 
   if(isset($_POST["finnum"])){
        $stationfinnum =trim(mysqli_real_escape_string($db,$_POST["stationfinnum"]));
        $periodfinnum=trim(mysqli_real_escape_string($db,$_POST["periodfinnum"]));
        $sfnindicator=trim(mysqli_real_escape_string($db,$_POST["sfnindicator"]));
         //if EXECUTIVE
        if($sfnindicator=="exec"){
          $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodfinnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
    
                                    }     

                                }
                                echo $co_fin;
        }
         //if CECM
        else if($sfnindicator=="cecm"){
              $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationfinnum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodfinnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
    
                                    }     

                                }
                                echo $co_fin;
        }
         //if CO
        else if($sfnindicator=="co"){
              $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationfinnum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodfinnum' DAY AND NOW()";
                                $result2=mysqli_query($db,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
    
                                    }     

                                }
                                echo $co_fin;
        }
    }

//-------------------------------------------------SUM OF REQUESTED MEMOS----------------------------------------------// 
      if(isset($_POST["reqsum"])){
        $stationreqsum=trim(mysqli_real_escape_string($db,$_POST["stationreqsum"]));
        $periodreqsum=trim(mysqli_real_escape_string($db,$_POST["periodreqsum"]));
            $srsindicator=trim(mysqli_real_escape_string($db,$_POST["srsindicator"]));
         //if EXECUTIVE
        if($srsindicator=="exec"){
          $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE  nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $corequested=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($corequested);
        }
            //if CECM
        else if($srsindicator=="cecm"){
               $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationreqsum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $corequested=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($corequested);
        }
            //if CO
        else if($srsindicator=="co"){
               $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationreqsum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $corequested=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($corequested);
        }
          
    }

//---------------------------------------------------SUM OF APPROVED MEMOS-------------------------------------------------// 
    if(isset($_POST["appsum"])){
        $stationappsum =trim(mysqli_real_escape_string($db,$_POST["stationappsum"]));
        $periodappsum=trim(mysqli_real_escape_string($db,$_POST["periodappsum"]));
         $sasindicator=trim(mysqli_real_escape_string($db,$_POST["sasindicator"]));
         //if EXECUTIVE
        if($sasindicator=="exec"){
           $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE  nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $coapproved=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($coapproved);
        }
           //if CECM
        else if($sasindicator=="cecm"){
                $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationappsum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $coapproved=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($coapproved);
        }
           //if CO
        else if($sasindicator=="co"){
                $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationappsum' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappsum' DAY AND NOW()";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                            $coapproved=$row['c'];
                                                }     
                                       }
                                    echo "Kshs: ".number_format($coapproved);
        }
        
    }

    
    
    
    
    
    
    
    
    
//---------------------------------CONTRIBUTION TO REQUESTED AMOUNT(GREEN GRAPH)-------------------------//
   if(isset($_POST["reqgraph"])){
        $stationreqgraph =trim(mysqli_real_escape_string($db,$_POST["stationreqgraph"]));
        $periodreqgraph=trim(mysqli_real_escape_string($db,$_POST["periodreqgraph"]));
        $srgindicator=trim(mysqli_real_escape_string($db,$_POST["srgindicator"]));
       //if EXECUTIVE
        if($srgindicator=="exec"){
         
       $abc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE  nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
                                            }
                                  
            
                   $coabc="SELECT * FROM departments_list";
                         $result1=mysqli_query($db,$coabc);
                                  if($result1) {
                                      while($row=mysqli_fetch_assoc($result1)) {
                                      $department=$row['department'];
                 $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$department' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                  if($result1) {
                                   while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                      }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $department</div>
                        </div>";
                                                }     

                                            }
        }
       else if($srgindicator=="cecm"){
           
            $abc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationreqgraph' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
                                            }
        //get sector id
       $juke="SELECT * FROM departments_list WHERE department='$stationreqgraph'";
                                    $result1=mysqli_query($db,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {                                    
                                                   $department_id=$row['department_id'];
                                                }}
      $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector=$row['sector'];
                                       $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$sector' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $sector</div>
                        </div>
                          ";
                                     }     

                                            }
            //THE ONE CONTRIBUTED BY THE CECM HERSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";
           $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationreqgraph' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>CEC's Contribution</div>
                        </div>
                          ";
           
       }
       else if($srgindicator=="co"){
         $abc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationreqgraph' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
                                            }
        //get sector id
       $juke="SELECT * FROM sectors_list WHERE sector='$stationreqgraph'";
                                    $result1=mysqli_query($db,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
      $coabc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $co_directorate=$row['directorate'];
                                       $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE directorate='$co_directorate' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $co_directorate</div>
                        </div>
                          ";
                                     }     

                                            } 
             //THE ONE CONTRIBUTED BY THE CO HIMSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";
           $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationreqgraph' AND nature='direct' AND position='C.O' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodreqgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> C.O's Contribution</div>
                        </div>
                          ";
           
       }
                            
    }

//---------------------------------CONTRIBUTION TO APPROVED AMOUNT(GREEN GRAPH)-------------------------//
   if(isset($_POST["appgraph"])){
       $stationappgraph =trim(mysqli_real_escape_string($db,$_POST["stationappgraph"]));
        $periodappgraph=trim(mysqli_real_escape_string($db,$_POST["periodappgraph"]));
         $sagindicator=trim(mysqli_real_escape_string($db,$_POST["sagindicator"]));
        //get co requested amount
         //if EXECUTIVE
        if($sagindicator=="exec"){
         
        $abc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE  nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved=$row['c'];
                                                }     
                                            }
                                  
            
                   $coabc="SELECT * FROM departments_list";
                         $result1=mysqli_query($db,$coabc);
                                  if($result1) {
                                      while($row=mysqli_fetch_assoc($result1)) {
                                      $department=$row['department'];
                 $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$department' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                  if($result1) {
                                   while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                      }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $department</div>
                        </div>";
                                                }     

                                            }
        }
       else if($sagindicator=="cecm"){
           
            
            $abc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationappgraph' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved=$row['c'];
                                                }     
                                            }
        //get sector id
       $juke="SELECT * FROM departments_list WHERE department='$stationappgraph'";
                                    $result1=mysqli_query($db,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {                                    
                                                   $department_id=$row['department_id'];
                                                }}
      $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector=$row['sector'];
                                       $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$sector' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $sector</div>
                        </div>
                          ";
                                     }     

                                            }
           //THE ONE CONTRIBUTED BY CECM HERSELF
            //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";
            $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationappgraph' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>CEC's Contribution</div>
                        </div>
                          ";
           
       }
       else if($sagindicator=="co"){
         $abc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationappgraph' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result1=mysqli_query($db,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved=$row['c'];
                                                }     
                                            }
        //get sector id
       $juke="SELECT * FROM sectors_list WHERE sector='$stationappgraph'";
                                    $result1=mysqli_query($db,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
      $coabc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($db,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $directorate=$row['directorate'];
                                       $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $directorate</div>
                        </div>
                          ";
                                     }     

                                            }   
           //THE ONE CONTRIBUTED BYCO HIMSELF
            //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";
           $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationappgraph' AND nature='direct' AND position='C.O' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$periodappgraph' DAY AND NOW()";
                                    $result=mysqli_query($db,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
         
                                                }     

                                            }
                                                      //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo " 
                         <div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>C.O's Contribution</div>
                        </div>
                          ";
       }
                            
    }



    
$db->close();
?>

 


        <!-- Vendors -->
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>
        
      