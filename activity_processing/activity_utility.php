<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
include_once './config.php';

class DB_connect{

    
public function updateProg(){
  
}
    
    
    
    public function setPersonnalStatus($mysqli,$ses,$referenceno){
         //SELECT ALL FOR SINGLE RECEPIENT ROW TO UPDATE STATUS
  $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && recepient='$ses'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $progress=$row["progress"];   
        $status=$row["status"];
    }
}
       if($progress>=100){
            //ensure that if it is already rejected it remains rejected   
            if(($status!=='Rejected')&&($status!=='Approved')&&($status!=='Closed')&&($status!=='Escalated')&&($status!=='Forwarded')&&($status!=='Noted')&&($status!=='Not Approved')&&($status!=='Not Recommended')&&($status!=='Recommended')){
            $sql3="UPDATE nonfinancialmemos SET status='Approved' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql3)){
   
        $stmnt->execute();      
           }  
            }
        }
         else if($progress<=0){
             $sql4="UPDATE nonfinancialmemos SET status='Inactive' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql4)){
   
        $stmnt->execute();      
     }  
        }
        else{
            $sql5="UPDATE nonfinancialmemos SET status='Pending' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql5)){
   
        $stmnt->execute();      
}  
        }
    
    
    }
    
    public function generateGeneralProgress($mysqli,$referenceno){
        
                  
$abc="SELECT sum(progress) as c FROM nonfinancialmemos where referenceno='$referenceno' AND nature!='Cc'";
  $result1=mysqli_query($mysqli,$abc);
    if($result1) {
 while($row=mysqli_fetch_assoc($result1)) {
        $totalprogress=$row['c'];
    
      }     

}
          
          //ACQUIRE TOTAL NUMBER OF ROWS CONTAINING REFERENCE NO
          
          $def="SELECT count(*) as f FROM nonfinancialmemos where referenceno='$referenceno' AND nature!='Cc'";

          $result2=mysqli_query($mysqli,$def);
        if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
            $totalcolumns=$row['f'];
    
      }     

}
          
          
 //DO THE CALCULATION TO HAVE A MUTUAL GENERAL PROGRESS         
          
          $percentage=100;
          
          $calcprogress=($totalprogress/($totalcolumns*$percentage)*$percentage);
        //  echo $calcprogress;
          
          
      
                    //UPDATE GENERAL PROGRESS
      $sql8="UPDATE nonfinancialmemos SET generalprogress='$calcprogress' WHERE referenceno='$referenceno'";
     if($stmnt=$mysqli->prepare($sql8)){
   
        $stmnt->execute();      
}  
    
    
    }
    
    
    public function setGeneralStatus($mysqli,$referenceno){
    
     //SELECT ALL FOR ALL RECEPIENTS ROW TO UPDATE GENERAL STATUS
  $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $generalprogress= $row["generalprogress"];   
    }
    }
        
        //find out what main recepient said and set as general status
         $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && nature='direct'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
    while ($row = $res->fetch_assoc()) {
         $status= $row["status"];   
    }
    }
        
        if($generalprogress>=100){
            
            
            $sql3="UPDATE nonfinancialmemos SET generalstatus='$status' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql3)){
   
        $stmnt->execute();      
}  
        }
        else if($generalprogress<=0){
             $sql4="UPDATE nonfinancialmemos SET generalstatus='Inactive' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql4)){
   
        $stmnt->execute();      
     }  
        }
        else{
             $sql5="UPDATE nonfinancialmemos SET generalstatus='Pending' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql5)){
   
        $stmnt->execute();      
}  
        }
    }

   
    public function setViewLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                 $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                  $memotype=$row['memotype'];
                 
                  /*get station*/
            if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Viewed';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        /********make sure memotype is not broadcast to prevent disturbance*********/
        if($memotype!='broadcast'){
 //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
 while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Viewed memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n"; 
//$headers .= "Reply-To:";	
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}

        }else{}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Viewed memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n"; 
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
    
    
    public function setCommentLogs($mysqli,$referenceno,$subject,$ses){
        
        /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                  $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
         
   

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Commented';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
/********make sure memotype is not broadcast to prevent disturbance*********/
        if($memotype!='broadcast'){
            
 //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Commented on memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n"; 
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        }else{}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Commented on memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        

    }
    
    
    public function setApproveLogs($mysqli,$referenceno,$subject,$ses){
        
        /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                      $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
        //indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Approved';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Approved';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
    //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Approved memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Approved memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
    
    
    public function setRejectLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                     $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Rejected';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Rejected';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


    //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Rejected memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Rejected memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
    
    
    
    public function setCloseLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                       $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Closed';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}


   //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Closed memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Closed memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}

    
    }
    
    
    
    public function setNoteLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                        $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
     
//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Noted';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
/********make sure memotype is not broadcast to prevent disturbance*********/
        if($memotype!='broadcast'){

  //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Noted memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        }else{}
         //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Noted memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
    
    
    
    public function setEscalateLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                       $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Escalated';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Escalated';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


   //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Escalated memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
         //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Escalated memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}



    
    }
    
    

    public function setForwardLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                        $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Forwarded';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Forwarded';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }

 //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Forwarded memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Forwarded memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}

    
    }
    
	
	 public function setDisapproveTravelLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                     $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Disapproved';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Disapproved';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


    //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Disapproved memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Disapproved memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
	
	 public function setRecommendLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                     $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Recommended';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Recommended';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


    //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Recommended memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Recommended memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
	
	
	public function setNotRecommendLogs($mysqli,$referenceno,$subject,$ses){
        
         /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                     $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Advised against';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Advised against';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


    //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Advised against memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Advised against memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}


    
    }
	
	
	
	
	
	
	
	
    /*-----------------------------------------------------------ACTIONS--------------------------------------------------------------*/
  
    
    /********************VOTE HEAD*********************/
  public function assignVotehead($mysqli,$referenceno,$ses,$filledvotehead){
   //get everything about this memo
       $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && recepient='$ses'";
if ($mysqli->real_query($query)) {
    $res = $mysqli->use_result();
 while ($row = $res->fetch_assoc()) {
          $votehead=$row["votehead"];
          $amount=$row['amount'];
          $approvedamount=$row['approvedamount'];
          $vh_deduct_indicator=$row['vh_deduct_indicator'];    
        }
}
      //check if votehead isnt there
      if($votehead=='none'){
          //assign votehead
          //insert vote head in the record
     $sql1="UPDATE nonfinancialmemos SET votehead='$filledvotehead' WHERE referenceno='$referenceno' ";
  if($stmnt=$mysqli->prepare($sql1)){
      $stmnt->execute();      
     }  
           ?>
<script>
alert("Vote head successfully assigned to memo.");
</script>
<?php
          
      }
      else{//if votehead is already assigned
        ?>
<script>
alert("Vote head is already assigned. If you would like to reassign pick the 'reassign' option");
</script>
<?php 
      
      }
  }
    
    
  /********************RE-ASSIGNING VOVE*******************/
     public function reAssignVotehead($mysqli,$referenceno,$ses,$filledvotehead){
         
       //get everything about this memo
       $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && recepient='$ses'";
if ($mysqli->real_query($query)) {
    $res = $mysqli->use_result();
 while ($row = $res->fetch_assoc()) {
          $votehead=$row["votehead"];
          $amount=$row['amount'];
          $approvedamount=$row['approvedamount'];
          $vh_deduct_indicator=$row['vh_deduct_indicator'];    
        }
}
      //check if votehead isnt there
      if($votehead=='none'){
      ?>
<script>
alert("Use the 'assign' option for first time vote head assignment");
</script>
<?php
          
      }
      else{//if votehead is already assigned
      //check if memo is first time
          if($vh_deduct_indicator==0){
               //insert vote head in the record--prolly a mistake
     $sql1="UPDATE nonfinancialmemos SET votehead='$filledvotehead' WHERE referenceno='$referenceno' ";
  if($stmnt=$mysqli->prepare($sql1)){
      $stmnt->execute();      
     }  
         
                     ?>
<script>
alert("Vote head successfully assigned to memo.");
</script>
<?php
              
      }
          else{//if it is not first time
              //return money to votehead balance and remove money from used
            
                $sql1="UPDATE vote_heads SET Balance=Balance+'$approvedamount', Used=Used-'$approvedamount' WHERE Item_Source='$votehead'";
                  if($stmnt=$mysqli->prepare($sql1)){
                      $stmnt->execute();      
                     }  
              //asssign new votehead
                $sql1="UPDATE nonfinancialmemos SET votehead='$filledvotehead' WHERE referenceno='$referenceno'";
                  if($stmnt=$mysqli->prepare($sql1)){
                      $stmnt->execute();      
                     }  
              //set vh_deduct to 0
               $sql1="UPDATE nonfinancialmemos SET vh_deduct_indicator=0 WHERE referenceno='$referenceno'";
                  if($stmnt=$mysqli->prepare($sql1)){
                      $stmnt->execute();      
                     }  
      
                     ?>
<script>
alert("Vote head successfully assigned to memo.");
</script>
<?php
              
      }   
         
     }
     }
    
 /********************AMOUNT APPROVAL*********************/    
    
    
    public function approveAmount($mysqli,$referenceno,$approvedamount,$ses){
         //get memo contents
       $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && recepient='$ses'";
if ($mysqli->real_query($query)) {
    $res = $mysqli->use_result();
 while ($row = $res->fetch_assoc()) {
          $votehead=$row["votehead"];
          $amount=$row['amount'];
          $serverappamount=$row['approvedamount'];
          $vh_deduct_indicator=$row['vh_deduct_indicator'];
         
        }
}
        //Get votehead info
         $query="SELECT * FROM vote_heads WHERE Item_Source='$votehead'";
    if ($mysqli->real_query($query)) {
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
          $Balance=$row["Balance"];
        
        }
}
        
        
        if($votehead=='none'){ //DIR
            //check if user alters the amount
            if($approvedamount==''){//means user is okay with the amount
                //set approved amount
                    $sql="UPDATE nonfinancialmemos SET approvedamount='$amount' WHERE referenceno='$referenceno'";
                  if($stmnt=$mysqli->prepare($sql)){
                   $stmnt->execute(); 
                  }        
                
                                               ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                
                
            }
            else{//user enters an alternative value
                //set app amount
                 $sql="UPDATE nonfinancialmemos SET approvedamount='$approvedamount' WHERE referenceno='$referenceno'";
                  if($stmnt=$mysqli->prepare($sql)){
                   $stmnt->execute(); 
                  }    
                
                                              ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                
                
                
            }
            
        }
        else{ //actor is a CO. Vote is assigned (deductions begin)
            //check if it is first time
            if( $vh_deduct_indicator==0){ //FIRST TIME
                
                //check if CO is altering or not
                
                if($approvedamount==''){ //means he not altering
                    
                    //check if serverappamount is available
                    if($serverappamount==0){ //not available
                        //use amount but first ensure it is not exceeding balance
                        $difference=$Balance-$amount;
                        if($difference<0){
                            ?>
                        <script> 
                            alert("The amount requested exceeds the vote head balance. Reduce the amount or pick a different vote head");
                        </script>
                        <?php
                        }
                        else{ //if amount is allowed
                              
                            //deduct the amount from balance
                             $sql="UPDATE vote_heads SET Balance=(Balance-'$amount'), Used=(Used+'$amount') WHERE Item_Source='$votehead'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                            //set approved amount
                               $sql="UPDATE nonfinancialmemos SET approvedamount='$amount' WHERE referenceno='$referenceno'";
                                  if($stmnt=$mysqli->prepare($sql)){
                                   $stmnt->execute(); 
                                  }  
                            
                            //set vh_indicator to 1
                              $sql="UPDATE nonfinancialmemos SET vh_deduct_indicator=1 WHERE referenceno='$referenceno'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                            
                               ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                            
                        }
                        
                    }
                    else{//if there is a server amount
                        //use serverappamount but first ensure it is not exceeding balance
                        $difference=$Balance-$serverappamount;
                        if($difference<0){
                         ?>
                        <script> 
                            alert("The amount requested exceeds the vote head balance. Reduce the amount or pick a different vote head");
                        </script>
                        <?php
                        
                        }
                        else{ //amount is allowed
                             
                            //deduct the amount from balance
                             $sql="UPDATE vote_heads SET Balance=(Balance-'$serverappamount'), Used=(Used+'$serverappamount') WHERE Item_Source='$votehead'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                             //set approved amount
                               $sql="UPDATE nonfinancialmemos SET approvedamount='$serverappamount' WHERE referenceno='$referenceno'";
                                  if($stmnt=$mysqli->prepare($sql)){
                                   $stmnt->execute(); 
                                  }    
                            
                            //set vh_indicator to 1
                              $sql="UPDATE nonfinancialmemos SET vh_deduct_indicator=1 WHERE referenceno='$referenceno'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                            
                                            ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                            
                            
                        }
                    }
                    
                }
                else{ //if user enters alternative amount
                       //use approved amount but first ensure it is not exceeding balance
                        $difference=$Balance-$approvedamount;//entered amount
                        if($difference<0){
                         ?>
                        <script> 
                            alert("The amount requested exceeds the vote head balance. Reduce the amount or pick a different vote head");
                        </script>
                        <?php
                        
                        }
                        else{ //amount is allowed
                                //deduct the amount from balance
                             $sql="UPDATE vote_heads SET Balance=(Balance-'$approvedamount'), Used=(Used+'$approvedamount') WHERE Item_Source='$votehead'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                            
                              //set approved amount
                               $sql="UPDATE nonfinancialmemos SET approvedamount='$approvedamount' WHERE referenceno='$referenceno'";
                                  if($stmnt=$mysqli->prepare($sql)){
                                   $stmnt->execute(); 
                                  }    
                        
                            //set vh_indicator to 1
                              $sql="UPDATE nonfinancialmemos SET vh_deduct_indicator=1 WHERE referenceno='$referenceno'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          }
                            
                                            ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                            
                            
                            
                            
                        }
                }
                
                
            }
            else{ //NOT FIRST TIME
                //check if user is altering
                if($approvedamount==''){//not altering
                    //do nothing
                       ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                }
                else{// if he alters
                       //use serverappamount but first ensure it is not exceeding balance
                        $difference=$Balance-$approvedamount;
                        if($difference<0){
                         ?>
                        <script> 
                            alert("The amount requested exceeds the vote head balance. Reduce the amount or pick a different vote head");
                        </script>
                        <?php
                        
                        }
                        else{ //amount is allowed
                            
                             //deduct the amount from balance
   
                             $sql="UPDATE vote_heads SET Balance=((Balance+'$serverappamount')-'$approvedamount'), Used=((Used-'$serverappamount')+'$approvedamount') WHERE Item_Source='$votehead'";
                          if($stmnt=$mysqli->prepare($sql)){
                           $stmnt->execute(); 
                          
                        }
                            //set approved amount
                               $sql="UPDATE nonfinancialmemos SET approvedamount='$approvedamount' WHERE referenceno='$referenceno'";
                                  if($stmnt=$mysqli->prepare($sql)){
                                   $stmnt->execute(); 
                                  }    
                
                                                   ?>
                                            <script>
                                            alert("Memo Approved.");
                                            </script>
                                            <?php
                        
                        
                        }
                
            }
        
        
        }
        
    }
    
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

 
    
    
    
?>