<?php
session_start();
if(!isset($_SESSION['welcome'])){
    header("Location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
 <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
    
    
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    </head>

    <body data-sa-theme="1">
       
          <div class="row quick-stats" style="background-color:#ecebec;">
              

      <div class="col-lg-4">
          
                    </div>
                <div class="col-lg-4">
         
                                </br>  </br>  </br>  </br>  </br> 
                        <div class="card" >
                            <div class="card-body" id="cbmemcreation">
                                  <div class="login__block__header">
                    <i id="mee" class="zmdi zmdi-account-circle"></i>
                    Choose account to log into

                    <div class="actions actions--inverse login__block__actions">
                        <div class="dropdown">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-register" href="#"></a>
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-forget-password" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                                  <div class="container box">
   <?php
                      $ses=$_SESSION['welcome'];
                                       //set session last_time
                    $_SESSION['last_time'] = time();
                      $newses=$ses+1;
                                       require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
         $query="SELECT * FROM ememo_users where user_id='$newses'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $position=$row["position"];
                                         $stationdepartment=$row["department"];
                                        $stationsector=$row["sector"];
                                        $stationdirectorate=$row["directorate"];

                                        if($position=='C.E.C.M'){
                                            $station=$stationdepartment;
                                        }
                                        else if($position=='C.O'){
                                             $station=$stationsector;
                                        }
                                         else if($position=='DIR'){
                                             $station=$stationdirectorate;
                                        }
                                         else if($position=='D.DIR'){
                                             $station=$stationdirectorate;
                                        }
                                        else{
                                             $station='County Government of Nandi';
                                        }
                                    
                                      $owner=$position.", ".$station."( My account)";
                                    
                                      
                                }
                                }
                              
    ?>
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="user_data" class="table">
     <thead>
      <tr>
       <th><?php echo $owner; ?></th>
       <th>
           <form action="login_choose_acc.php" method="post">
               <button type="submit" name="mine" class="btn btn-success btn-sm access"><i class="zmdi zmdi-key"></i> access</button>
               <?php
                if(isset($_POST['mine'])){
                    //set session karibu
               $_SESSION['karibu']=$newses;
                  
    
                      //unset session welcome
                    unset($_SESSION['welcome']);
    
                                //redirect to home page
                                        ?>
                                    <script type="text/javascript">
                                  window.location.href="index.php";
                                    </script>
                                   <?php
                }
               ?>
           </form>
          </th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                                
                             </br></br></br></br></br>
                            </div>
                        </div>
                    </div>
              </div>
</div>
        <!-- Javascript -->
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
                
   <script src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>
     
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
</html>
        <script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"choose_acc/fetch.php",
     type:"POST"
    }
   });
  }
  
 
  
  $(document).on('click', '.access', function(){
   var id = $(this).attr("id");
 
    $.ajax({
     url:"choose_acc/access.php",
     method:"POST",
     data:{id:id},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
</script>
<script>
    window.alert=function(){}
</script>