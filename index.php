<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">   
        <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
 
       <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="jquery-3.0.0.js"></script>
 <script type="text/javascript">
     $(document).ready(function(){
           $('#backarrow').hide();     
     });
    </script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>


            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>
              
                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner" id="dontouch">

                    <div class="user">
                        <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>        
                        <p id="stationdepartment" hidden><?php echo $stationdepartment; ?></p>
                        <p id="stationsector" hidden><?php echo $stationsector; ?></p>
                        <p id="stationdirectorate" hidden><?php echo $stationdirectorate; ?></p>
                        <div class="user__info" data-toggle="dropdown">
                          <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                              
                                
                                <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                            
                                 <form action="index.php" method="post">
                             <button type="submit" class="btn btn-outline-danger" name="out">Log Out</button>
                               </form>
                              <?php
      if(isset($_POST['out'])){
         // session_destroy();
          unset($_SESSION['infilref']);
          unset($_SESSION['dirtyses']);
          unset($_SESSION['karibu']);
      ?>
                                    <script type="text/javascript">
                                         window.location.href="login.php";
                                    </script>
                                   <?php
      }    
    ?>
                           
                        </div>
                        
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                       
                        
                        <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->
                        <li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>

          
                            

                         </ul>
                </div>
            </aside>

            <section class="content">
                
                <!--SESSION BUSINESS-->
             
<?php
                          
        if(isset($_POST['sesbiz'])){   
            
         require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name);  
        if($position=='C.O'){
           $sesinfil=$_SESSION['infilref'];
             $def="SELECT * FROM  infiltration WHERE infilref='$sesinfil'";
          $result2=mysqli_query($conn,$def);
           if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
                $status=$row["status"];
          $cecm=$row["cecm_co"];
                              }   
               $status=$status;
           $cecm=$cecm;
           }
            //set session
             $_SESSION['karibu']=$cecm;
            
         /**SELECT PATH TO HOME from infiltration_enhanced*/
             $def="SELECT * FROM  infiltration_enhanced WHERE infilref='$sesinfil'";
          $result2=mysqli_query($conn,$def);
           if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
          $homepath=$row["homepath"];
                              }
           $homepath=$homepath;
           }
            //ensure homepath isnt empty
            if($homepath!==""){
                $_SESSION['infilref']=$homepath;
            }
            else{
                 
            }
            
            //check status to know whether to unset dirty ses or not
            if($status==0){
            $_SESSION['dirtyses']="Clean";
            }
            else{
                
            }

       }
            else if($position=='C.E.C.M'){
               $sesinfil=$_SESSION['infilref'];
             $def="SELECT * FROM  infiltration WHERE infilref='$sesinfil'";
          $result2=mysqli_query($conn,$def);
           if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
          $exec=$row["exec_cecm"];
                              }
           $exec=$exec;
           }
            //set session
             $_SESSION['karibu']=$exec;
            //unset session infilref
            unset($_SESSION['infilref']);
            $_SESSION['dirtyses']="Clean";     
            }
     
?>
                <script type="text/javascript">
                                         window.location.href="index.php";
                                    </script>
                <?php
        }
?>
                
                <header class="content__title">
                     <form method="post" action='index.php'>
                <button type='submit' name='sesbiz' class="btn btn-dark btn--icon" id="backarrow"><i class="zmdi zmdi-arrow-back"></i></button>
                  </form>
                    <h1 id="dash">Dashboard</h1>
                    <small id="profiletext">Welcome to the memo management system.(E-Memo)</small>
                </header>

                          <div class="row quick-stats">
                              <?php
                              
                $ses=$_SESSION['karibu'];                                   
          require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
                              
          if($ses!==71){
          $def="SELECT count(*) as f FROM nonfinancialmemos where recepient='$ses'";
          $result2=mysqli_query($conn,$def);
           if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
            $received=$row['f'];
    
      }     

}
                              }
                              else{
          $def="SELECT count(*) as f FROM nonfinancialmemos where nature='direct' AND availability!='forwarded_memo' AND availability!='escalated_memo'";
          $result2=mysqli_query($conn,$def);
           if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
            $received=$row['f'];
                              }
           }
                              }
               
                              //SENT MEMOS
                                 if($ses!==71){
                              
        $sen="SELECT count(*) as n FROM nonfinancialmemos where requestor='$ses' AND nature='direct' AND availability!='forwarded_memo' AND availability!='escalated_memo' AND memotype!='broadcast'";
          $result3=mysqli_query($conn,$sen);
           if($result3) {
            while($row=mysqli_fetch_assoc($result3)) {
            $sent=$row['n'];
    
      }     

}
                                 }
                              else{
                                  $sent=0;
                              }
                              //DRAFTS
                                  if($ses!==71){
                             $dra="SELECT count(*) as a FROM nonfinancialdrafts where requestor='$ses' AND nature='direct'";
          $result4=mysqli_query($conn,$dra);
           if($result4) {
            while($row=mysqli_fetch_assoc($result4)) {
            $drafts=$row['a'];
    
      }     

} }
                              else{
                                $drafts=0;  
                              }
                                  
                              
                              ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="quick-stats__item">
                            <div id="kati" class="quick-stats__info">
                                <?php echo  "<a href=\"settings.php\"><img style=\"margin-left:7px\"  id=\"userimg\" class=\"user__img\"  src=\"img/profilepics/$ses$dp_file_ext\"/></a>"  ?>
                            </br>
                                <small id="profiletext"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></small>
                         <small id="profiletext"><?php
echo $position;
?></small>
                               <small id="profiletext"><?php
echo $station;
?></small></br>
                        
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                       <a href="indexnonfinancialreceived.php"> <div class="quick-stats__item">
                            <div id="kati" class="quick-stats__info">
                                <p><i id="me" class="zmdi zmdi-arrow-left-bottom zmdi-hc-5x"></i> <h1 id="basicstats" ><?php echo $received;?></h1>Received Memos</p>
                            </div>

                        </div></a>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <a href="indexnonfinancialsent.php"><div class="quick-stats__item">
                            <div id="kati" class="quick-stats__info">
                                
                                <p><i id="me" class="zmdi zmdi-arrow-right-top zmdi-hc-5x"></i> <h1 id="basicstats"><?php echo $sent;?></h1>Sent Memos</p>
                            </div>

                            </div></a>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <a href="indexnonfinancialdrafts.php"><div class="quick-stats__item">
                            <div id="kati" class="quick-stats__info">
                     
                                <p><i id="me" class="zmdi zmdi-edit zmdi-hc-5x"></i> <h1 id="basicstats"><?php echo $drafts;?></h1>Memos in Draft</p>
                            </div>
                            </div></a>
                    </div>
                </div>

<!--FILTER RESULTS-->









<!-----------------------------------------------------------------DASHBOARD---------------------------------------------------------->
<div id="stats">
    <!--------------------------FILTER THE RESULTS---------------------------------------->
      <div class="row quick-stats">
                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="loner">
                        
                         <select name="filter" class="select2" id="filter" onchange="add_filterco(this.value)">
                                <option value="general">General Memo Statistics</option>
                                <option value="1"> Memo Statistics for the Past 24 Hours</option>
                                <option value="7">Memo Statistics for the Past 7 Days</option>
                                <option value="30">Memo Statistics for the Past 30 Days</option>
                        </select></br></br></br></br>
                                            
                                      
                        
                        </div>
                    </div>

<!-----------------------------------------SELECTING DASHBOARD TO VIEW FROM------------------------------------------->
                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="loner">
                           
                                <select name="department"class="select2" onchange="drillvotehead(this.value)">
                                    <option value="">Select Dashboard to View</option>
                                      <?php
                            include_once 'connectdb.php';
                                     if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                    $sql1="SELECT * FROM departments_list";
                                    $results=$dbhandle->query($sql1); 
                                    while($rs=$results->fetch_assoc()) { 
                                    ?>
                                    <option value="<?php echo $rs["department_id"]; ?>"><?php echo $rs["department"]; ?></option>
                                    <?php
                                    }
                                     }
                                     else if($position=='C.E.C.M'){
                                       $abc="SELECT * FROM departments_list where department='$stationdepartment'";
                                    $result1=mysqli_query($dbhandle,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department_id=$row['department_id'];
                                                }}
                                    
                            $sql1="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                            $results=$dbhandle->query($sql1); 
                            while($rs=$results->fetch_assoc()) { 
                            ?>
                            <option value="<?php echo $rs["sector_id"]; ?>"><?php echo $rs["sector"]; ?></option>
                            <?php
                            }
                            }
                                    else if($position=='C.O'){
                                            ?>
                            <option value=""><?php echo "There are no other dashboards for your office to view" ?></option>
                            <?php
                                    }
                            ?>
                            
                              </select>
                                             </br></br></br></br>
                     
                        </div>
                    </div>

                </div>
 


<!-----------------------------------------------------NUMERICAL MEMO COUNTS---------------------------------------------------->

<!----COMMUNICATION MEMOS---->
            <div class="row quick-stats">
                    <div class="col-sm-6 col-md-3" >
                        <div class="quick-stats__item" id="cbmemcreation">
                            <div class="quick-stats__info" >
                                 <h2 id="cocomm" style="color:white;"><?php
            if($position=='C.E.C.M'){
                                 $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationdepartment' AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
                                    }     
                                }
            }
                                     else if($position=='C.O'){
                                          $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationsector' AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
                                    }     
                                } 
                                     }
                                     else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication'  AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $cocomm=$row['f'];
                                    }     
                                }
                                     }
                                echo $cocomm;
                                ?>
                               </h2>
                                <small style="color:white;">Communication Memos</small>
                            </div>

                            <div class="quick-stats__chart peity-bar" id="bars">6,4,8,6,5,6,7,8,3,5,9</div>
                        </div>
                    </div>

                
                <!----FINANCIAL MEMOS---->
                    <div class="col-sm-6 col-md-3">
                        <div class="quick-stats__item" id="cbmemcreation">
                            <div class="quick-stats__info">
                               <h2 id="co_fin" style="color:white;"><?php
                                   if($position=='C.E.C.M'){
                                 $codef="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationdepartment' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$codef);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
                                    }     
                                }
                                   }
                                   else if($position=='C.O'){
                                         $codef="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationsector' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$codef);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
                                    }      
                                   }
                                   }
                                   else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                         $codef="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$codef);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $co_fin=$row['f'];
                                    }      
                                   }
                                   }
                                echo $co_fin;
                                ?>
                               </h2>
                                <small style="color:white;">Financial Memos</small>
                            </div>

                            <div class="quick-stats__chart peity-bar">4,7,6,2,5,3,8,6,6,4,8</div>
                        </div>
                    </div>

            
                <!-----------------------------------------------NUMERICAL AMOUNT  COUNTS---------------------------------------------->
                
                <!----REQUESTED AMOUNT---->
                
                    <div class="col-sm-6 col-md-3">
                        <div class="quick-stats__item">
                            <div class="quick-stats__info">
                                <h2 id="corequested"><?php
                                    if($position=='C.E.C.M'){
                                    $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationdepartment' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
      }
                                    }
                                    else if($position=='C.O'){
                                          $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationsector' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
      }
                                    }
                                    else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                          $coabc="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $corequested=$row['c'];
                                                }     
      }
                                    }
                                    echo "Kshs: ".number_format($corequested);
                                    ?></h2>
                                <small id="profiletext">Requested Funds</small>
                            </div>

                           
                        </div>
                    </div>

                <!-----APPROVED AMOUNT----->
                
                    <div class="col-sm-6 col-md-3">
                        <div class="quick-stats__item">
                            <div class="quick-stats__info">
                                <h2 id="coapproved"><?php
                                      if($position=='C.E.C.M'){
                                    $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationdepartment' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved= $row['c'];
                                                }     
                                            }
                                      }
                                    else if($position=='C.O'){
                                          $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationsector' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved= $row['c'];
                                                }     
                                            }
                                    }
                                    else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                         $coabc="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE  nature='direct' AND availability!='forwarded_memo'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                        $coapproved= $row['c'];
                                                }     
                                            } 
                                    }
                                    echo "Kshs: ".number_format($coapproved);
                                    ?></h2>
                                <small id="profiletext">Approved funds</small>
                            </div>

                           
                        </div>
                    </div>
                </div>

<!--------------------------------------------GRAPHICAL(CHARTS) MEMO COUNTS---------------------------------------------->



  <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            
                            <!------COMMUNICATION MEMOS(DOUGHNUT CHART)------>
                            
                            <div class="card-body">
                                <h4 class="card-title" id="basicstats">Communication Memo Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The doughnut chart below shows the number of communication memos initiated from each <?php 
                                    //determine from
                                    if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                        $from='Department';
                                    }
                                    else if($position=='C.E.C.M'){
                                         $from='Sector';
                                    }
                                    else if($position=='C.O'){
                                         $from='Directorate';
                                    }
                                    echo $from;
                                        ?></h6>
                                <div id="execdoughnutcomm">
                                 
                                  </div>
                                <div id="cecmdoughnutcomm">
                                     
                                  </div>
                                <div id="codoughnutcomm">
                                    
                                </div>
                                 
                            </div>
                             <!------FINANCIAL MEMOS(PIE  CHART)------>
                            
                              <div  class="card-body">
                                <h4 class="card-title" id="basicstats">Financial Memo Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The pie chart below shows the number of financial memos initiated from each <?php echo $from;?></h6>
                                  <div id="execpiefin">
                                     
                                  </div>
                                   <div id="cecmpiefin">
                                   
                                  </div>
                                   <div id="copiefin">
                                    
                                  </div>
                                 
                            </div>
                                </div>
                    </div>

      <!--------------------------------------------GRAPHICAL(GREEN CHARTS) MEMO AMOUNT---------------------------------------->
      
      
                    <div class="col-lg-6">
                        <div class="card">
                            
                            <!---------------------------------------GREEN GRAPH REQUESTED AMOUNT------------------------------------->
                            
                            <div class="card-body">
                                <h4 class="card-title"   id="profiletext"><?php echo $from."'s";?> contribution to the total requested amount </h4>
                                <h6 class="card-subtitle" id="profiletext"></h6>
                              
                                  <div id ="coreqgraph" class="card widget-pie">
                                    
                                   <?php
                                        if($position=='C.E.C.M'){
                                   
                                        $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
                                            $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                       $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$sector' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                     //ensure there is no division by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\" id=\"soft\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $sector</div>
                        </div>";
                                                }   

                                            }
                                              //THE ONE CONTRIBUTED BY THE CECM HERSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";

                                          $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationdepartment' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                     }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>CEC's Contribution</div>
                        </div>";
                                        }
                                      else if($position=='C.O'){
                                              $juke="SELECT * FROM sectors_list WHERE sector='$stationsector'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
                                          
                                            $coabc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                       $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $directorate</div>
                        </div>";
                                                }     

                                            }
                                          //THE ONE CONTRIBUTED BY THE CO HIMSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";

                                          $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationsector' AND nature='direct' AND position='C.O' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                     }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>C.O's Contribution</div>
                        </div>";
                                          
                                          
                                      }
                                      else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                          
                                            $coabc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                       $coab="SELECT sum(amount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$department' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                     //ensure there is no division by 0
                                                    if($corequested==0){
                                                        $corequestedsafe=1;
                                                    }
                                                    else{
                                                        $corequestedsafe=$corequested;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$corequestedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $department</div>
                        </div>";
                                                }     

                                            }
                                          
                                      }
                            
                            ?>
                                     
                    </div>

                            </div>
                            
                             <!---------------------------------------GREEN GRAPH APPROVED AMOUNT------------------------------------->
                            
                                 <div class="card-body">
                                <h4 class="card-title" id="profiletext"><?php echo $from."'s";?> contribution to the total Approved amount </h4>
                                <h6 class="card-subtitle"></h6>
                              
                                  <div id="coappgraph" class="card widget-pie">
                                      
                               <?php
                                        if($position=='C.E.C.M'){
                                   
                                        $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
                                            $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                       $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$sector' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                     //ensure there is no division by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $sector</div>
                        </div>";
                                                }     

                                            }
                                              //THE ONE CONTRIBUTED BY THE CECM HERSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";

                                          $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$stationdepartment' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                     }     
                                   }
                                                    //ensure there is no division by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>CEC's Contribution</div>
                        </div>";
                                        }
                                      else if($position=='C.O'){
                                              $juke="SELECT * FROM sectors_list WHERE sector='$stationsector'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
                                            $coabc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                       $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                     //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $directorate</div>
                        </div>";
                                                }     

                                            }
                                          //THE ONE CONTRIBUTED BY THE CO HIMSELF
                                          //place a div to separate from the rest
                                            echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                            <div class=\"widget-pie__title\"><br><br></div>
                                        </div>";

                                          $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE sector='$stationsector' AND nature='direct' AND position='C.O' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
                                     }     
                                   }
                                                  //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br>C.O's Contribution</div>
                        </div>";
                                      }
                                      else if(($position=='GO') OR ($position=='D.G') OR ($position=='C.S') OR($position=='D.C.S')){ 
                                          
                                            $coabc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                       $coab="SELECT sum(approvedamount) as c FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE department='$department' AND nature='direct' AND availability!='forwarded_memo'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $cototal= $row['c'];
             }     
                                   }
                                                    //ensure there is no dividion by 0
                                                    if($coapproved==0){
                                                        $coapprovedsafe=1;
                                                    }
                                                    else{
                                                        $coapprovedsafe=$coapproved;
                                                    }
                                                      //get percentage
                                        $cototalperc=round(($cototal/$coapprovedsafe)*100);
                                                    
                         echo "<div class=\"col-6 col-sm-4 col-md-6 col-lg-4 widget-pie__item\">
                                           
                             <div class=\"easy-pie-chart\" data-percent=\" $cototalperc\" data-size=\"80\" data-track-color=\"rgba(0,0,0,0.35)\" data-bar-color=\"#fff\">
                                <span class=\"easy-pie-chart__value\">$cototalperc</span>
                            </div>
                            <div class=\"widget-pie__title\"> Kshs: ".number_format($cototal)."<br><br> $department</div>
                        </div>";
                                                }     

                                            }
                                      }
                            
                            ?>
                                     
                    </div>

                            </div>
                        </div>
                    </div>
      
    <!--------------------------------------------------VOTE HEAD USAGE STATS-------------------------------------------------->
      
      <!--Development against Recurrent-->
            <div style="background-color:#ecebec;" class="quick-stats__item">
        <div class="col-lg-6">
                        <div class="card">
                           <div  class="card-body">
                               <h4 class="card-title" id="basicstats">Development Vs Recurrent (Used) Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The pie chart compares the development vote head used amount against the recurrent's vote head.</h6>
                              
                                   <div id="draw-charts-cousedcomp">
                               
                                    </div><br/>
                                     <div id="draw-charts-cecmusedcomp">
                                        
                               </div><br/>
                                   <div id="draw-charts-execusedcomp"></div><br/>
                            
                            </div>
                            
                        </div>
                    </div>
      <div class="col-lg-6">
                        <div class="card">
                           <div  class="card-body">
                                <h4 class="card-title" id="basicstats">Development Vs Recurrent (Balance) Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The pie chart compares the development vote head balance against the recurrent vote head balance.</h6>
                              
                                   <div id="draw-charts-cobalcomp">
                               
                                    </div><br/>
                                     <div id="draw-charts-cecmbalcomp">
                                        
                               </div><br/>
                                   <div id="draw-charts-execbalcomp"></div><br/>
                            
                            </div>
                            
                        </div>
                    </div>
</div>
     
      <!--Development and Recurrent separately-->
      <div style="background-color:#ecebec;" class="quick-stats__item">
        <div class="col-lg-6">
                        <div class="card">
                           <div  class="card-body">
                                <h4 class="card-title" id="basicstats">Development Vote Head Usage Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The charts below show the vote head usage in the station. The pie chart shows the total and the bar chart thereafter breaks it down.</h6>
                              
                                   <div id="draw-charts-co">
                               
                                    </div><br/>
                                     <div id="draw-charts-cecm">
                                        
                               </div><br/>
                                   <div id="draw-charts-exec"></div><br/>
                            
                            </div>
                            
                        </div>
                    </div>
      <div class="col-lg-6">
                        <div class="card">
                           <div  class="card-body">
                                <h4 class="card-title" id="basicstats">Recurrent Vote Head Usage Statistics</h4>
                                <h6 class="card-subtitle" id="profiletext">The charts below show the vote head usage in the station. The pie chart shows the total and the bar chart thereafter breaks it down.</h6>
                              
                                   <div id="draw-charts-corec">
                               
                                    </div><br/>
                                     <div id="draw-charts-cecmrec">
                                        
                               </div><br/>
                                   <div id="draw-charts-execrec"></div><br/>
                            
                            </div>
                            
                        </div>
                    </div>
</div>
     
      <p id="refno"></p>
      <div id="result"></div>
                </div>


</div></br></br></br></br></br>
        <footer class="footer hidden-xs-down">
            <p id="profiletext">©County Government of Nandi 2018. All rights reserved.</p>
        </footer></br></br></br></br></br>
</section>

<script>
    
/*-----------------------------------------WHEN FILTER IS CLICKED(FILTERED RESULTS)------------------------------------------------*/
 function add_filterco(val) {  
    
	 
  //number of communication memos  
     $(function() { 
           var commnum="commnum";
         //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationcommnum = $("#stationdepartment").html();
                       var scnindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationcommnum = $("#stationsector").html();
                        var scnindicator="co";
                    }
                     else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationcommnum = "all" ;
                         var scnindicator="exec";
                    }
        
          var periodcommnum=val;
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        commnum:commnum,
     stationcommnum:stationcommnum,
        scnindicator:scnindicator,
        periodcommnum:periodcommnum
    },
    cache: false
  }).done(function(data){
    $("#cocomm").html(data);
  });
 

});
     
     
     
     
      //number of financial memos  
     $(function() { 
          var finnum="finnum";
        //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationfinnum = $("#stationdepartment").html();
                       var sfnindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationfinnum = $("#stationsector").html();
                         var sfnindicator="co";
                    }
                    else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationfinnum = "all" ;
                         var sfnindicator="exec";
                    }
         var periodfinnum=val;
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        finnum:finnum,
       stationfinnum: stationfinnum,
          sfnindicator:sfnindicator,
        periodfinnum:periodfinnum
    },
    cache: false
  }).done(function(data){
    $("#co_fin").html(data);
  });
 

});
    
     
     
     
     //sum of requested amount
    $(function() { 
     var reqsum="reqsum";
     //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationreqsum = $("#stationdepartment").html();
                    var srsindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationreqsum = $("#stationsector").html();
                         var srsindicator="co";
                    }
                     else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationreqsum = "all" ;
                         var srsindicator="exec";
                    }
     var periodreqsum=val;
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        reqsum:reqsum,
      stationreqsum:stationreqsum,
            srsindicator:srsindicator,
        periodreqsum:periodreqsum
    },
    cache: false
  }).done(function(data){
    $("#corequested").html(data);
  });
 

});
     
     
     
     
      //sum of approved amount
    $(function() { 
        var appsum="appsum";
       //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationappsum = $("#stationdepartment").html();
                     var sasindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationappsum = $("#stationsector").html();
                          var sasindicator="co";
                    }
                     else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationappsum = "all" ;
                          var sasindicator="exec";
                    }
     var periodappsum=val;
    
    
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        appsum:appsum,
      stationappsum:stationappsum,
          sasindicator:sasindicator,
        periodappsum:periodappsum
    },
    cache: false
  }).done(function(data){
    $("#coapproved").html(data);
  });
 

});
     
     
     
       //graph of requested amount
    $(function() { 
        var reqgraph="reqgraph";
    //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationreqgraph = $("#stationdepartment").html();
                     var srgindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationreqgraph = $("#stationsector").html();
                          var srgindicator="co";
                    }
                    else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationreqgraph = "all" ;
                          var srgindicator="exec";
                    }
     var periodreqgraph=val;
    
    
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        reqgraph:reqgraph,
      stationreqgraph:stationreqgraph,
           srgindicator:srgindicator,
        periodreqgraph:periodreqgraph
    },
    cache: false
  }).done(function(data){
    $("#coreqgraph").html(data);
  });
 

});
     
     
     
       //graph of approved amount
    $(function() { 
        var appgraph="appgraph";
    //DISTINGUISH WHICH STATION TO PARSE DEPENDING ON THE POSITION
           var position="<?php echo $position?>";
               if(position=='C.E.C.M'){
                       var stationappgraph = $("#stationdepartment").html();
                    var sagindicator="cecm";
                    }
                    else if(position=='C.O'){
                       var stationappgraph = $("#stationsector").html();
                         var sagindicator="co";
                    }
                    else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
                       var stationappgraph = "all" ;
                         var sagindicator="exec";
                    }
     var periodappgraph=val;
    
    
  $.ajax({
    method: "POST",
    url: "statistics.php",
    data: {
        appgraph:appgraph,
       stationappgraph: stationappgraph,
           sagindicator:sagindicator,
        periodappgraph:periodappgraph
    },
    cache: false
  }).done(function(data){
    $("#coappgraph").html(data);
  });
 

});
     
     
     
     
     
     
     
     
     /*---------------------------------------------------------CHARTS AFTER FILTER IS CHOSEN-----------------------------*/
     
     
     
     
       //-------------------------------------------COMMUNICATION DOUGHNUT CHART-----------------------------------// 
    $(function() { 
        
        //When 24 hours is selected
     if(val==1){
     <?php $period=1;?>
          
           var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$sector' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$comm],"; 
                                                }
                                            }
           //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$comm],"; 
           
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmdoughnutcomm'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND directorate='$directorate' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$comm],"; 
                                                }
                                            }
   //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$comm],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('codoughnutcomm'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$department' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$comm],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('execdoughnutcomm'));
  chart.draw(data, options);
      }
         }

     }
        
        
        
        
        
        else if(val==7){
            <?php $period=7;?>
          
          
           var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$sector' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$comm],"; 
                                                }
                                            }
              //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$comm],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmdoughnutcomm'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND directorate='$directorate' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$comm],"; 
                                                }
                                            }
            //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$comm],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('codoughnutcomm'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$department' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$comm],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('execdoughnutcomm'));
  chart.draw(data, options);
      }
         }

        }
        else if(val==30){
                <?php $period=30;?>
    
            var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$sector' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$comm],"; 
                                                }
                                            }
            //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$comm],"; 
            
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmdoughnutcomm'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND directorate='$directorate' AND nature='direct' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$comm],"; 
                                                }
                                            }
             //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='escalated_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$comm],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('codoughnutcomm'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$department' AND nature='direct' AND availability!='escalated_memo'  AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$comm],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
  var chart = new google.visualization.PieChart(document.getElementById('execdoughnutcomm'));
  chart.draw(data, options);
      }
         }

        }
});
    
     
     
       //--------------------------------------------FINANCIAL PIE CHART------------------------------------------------// 
    $(function() { 
        
        //When 24 hours is selected
     if(val==1){
     <?php $period=1;?>
          
           var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$sector' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$fin],"; 
                                                }
                                            }
              //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$fin],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmpiefin'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$fin],"; 
                                                }
                                            }
             //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$fin],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
          is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('copiefin'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$department' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$fin],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
            is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('execpiefin'));
  chart.draw(data, options);
      }
         }

     }
        
        
        
        
        
        else if(val==7){
            <?php $period=7;?>
          
          
           var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$sector' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$fin],"; 
                                                }
                                            }
         //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$fin],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmpiefin'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$fin],"; 
                                                }
                                            }
             //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$fin],"; 
            
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
               is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('copiefin'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$department' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$fin],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
              is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('execpiefin'));
  chart.draw(data, options);
      }
         }

        }
        else if(val==30){
                <?php $period=30;?>
    
            var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
           google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    if($stationdepartment==""){
       $stationdepartmentuse="Administration, Public Service and e-Government";
    }
           else{
                $stationdepartmentuse=$stationdepartment;
           }
            
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartmentuse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
            
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$sector' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$sector'    ,$fin],"; 
                                                }
                                            }
             //THE ONE CEC CONTRIBUTED
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationdepartmentuse' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
       echo "['CEC'    ,$fin],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
             is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('cecmpiefin'));
  chart.draw(data, options);
      }
      }
         //if position is CO
         else if(position=='C.O'){
             google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
              if($stationsector==""){
       $stationsectoruse="Administration";
    }
           else{
                $stationsectoruse=$stationsector;
           }
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsectoruse'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
            
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
                                                      echo "['$directorate'    ,$fin],"; 
                                                }
                                            }
           //CO's CONTRIBUTION         
             $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationsectoruse'  AND nature='direct' AND position='C.O' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin= $row['f'];
                                    }     
  }
            echo "['C.O'    ,$fin],"; 
      
            ?>
        ]);
  var options = {
          title: '',
            height:250,
              is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('copiefin'));
  chart.draw(data, options);
      } 
         }
         
         //if executive
        else if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
              google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
 function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
           $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$department' AND nature='direct' AND availability!='forwarded_memo' AND created_at BETWEEN NOW() - INTERVAL '$period' DAY AND NOW()";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$fin],"; 
                                                }
                                            }
            ?>
        ]);
  var options = {
          title: '',
            height:250,
            is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
  var chart = new google.visualization.PieChart(document.getElementById('execpiefin'));
  chart.draw(data, options);
      }
         }

        }
});
     
     

     
 }
     

</script>











<!------------------------------------------INITIAL PIE CHART AND DOUGHNUT CHART----------------------------------------------->

<!--DOUGHNUT FOR COMMUNICATION MEMO-->
  <script type="text/javascript">
          var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$sector' AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$sector'    ,$comm],"; 
                                                    
                                                }
                                            }
         /**THE ONE CONTRIBUTED BY CECM HERSELF**/
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$stationdepartment' AND nature='direct' AND position='C.E.C.M' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                    echo "['CEC',$comm],"; 
          
          
            ?>
        ]);
        var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
        var chart = new google.visualization.PieChart(document.getElementById('cecmdoughnutcomm'));
        chart.draw(data, options);
      }
      }
      
       //If position is C.O
      else if(position=='C.O'){
        google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsector'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND directorate='$directorate' AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$directorate'    ,$comm],"; 
                                                }
                                            }
              /**THE ONE CONTRIBUTED BY CO HimSELF**/
           $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND sector='$stationsector' AND nature='direct' AND position='C.O' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                    echo "['C.O',$comm],"; 
            
            
            ?>
        ]);
        var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
        var chart = new google.visualization.PieChart(document.getElementById('codoughnutcomm'));
        chart.draw(data, options);
      }  
          
    
      }
     
    </script>
<script type="text/javascript">
    //if EXECUTIVE
      var position="<?php echo $position?>";
      if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
        
      $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='communication' AND department='$department' AND nature='direct' AND availability!='escalated_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$comm],"; 
                                                }
                                            }
            ?>
        ]);
        var options = {
          title: '',
            height:250,
             pieHole: 0.4,
        };
        var chart = new google.visualization.PieChart(document.getElementById('execdoughnutcomm'));
        chart.draw(data, options);
      } 
      }
</script>
<!--PIE CHART FOR FINANCIAL MEMO COUNT-->
  <script type="text/javascript">
       var position="<?php echo $position?>";
         //If position is CECM
      if(position=='C.E.C.M'){
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
             $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
      $abc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$sector' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                                                      echo "['$sector'    ,$fin],"; 
                                                }
                                            }
       //CONTRIBUTED BY CECM HERSELF
         $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$stationdepartment' AND nature='direct' AND position='C.E.C.M' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                            echo "['CEC'    ,$fin],"; 
       
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
        var chart = new google.visualization.PieChart(document.getElementById('cecmpiefin'));
        chart.draw(data, options);
      }
      }
      
       //If position is C.O
      else if(position=='C.O'){
        google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
             $juke="SELECT * FROM sectors_list WHERE sector='$stationsector'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $sector_id=$row['sector_id'];
                                                }}
      $abc="SELECT * FROM directorates_list WHERE sector_id='$sector_id'";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $directorate=$row['directorate'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND directorate='$directorate' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                                                      echo "['$directorate'    ,$fin],"; 
                                                }
                                            }
             //CONTRIBUTED BY CO HIMSELF
         $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND sector='$stationsector' AND nature='direct' AND position='C.O' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $fin=$row['f'];
                                    }     
                                }
                            echo "['C.O'    ,$fin],"; 
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  1: {offset: 0.2},
                     },
        };
        var chart = new google.visualization.PieChart(document.getElementById('copiefin'));
        chart.draw(data, options);
      }  
      }
    </script>
<script type="text/javascript">
    //if EXECUTIVE
      var position="<?php echo $position?>";
      if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
        
      $abc="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$abc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $department=$row['department'];
                                                      $def="SELECT count(*) as f FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id WHERE memotype='financial' AND department='$department' AND nature='direct' AND availability!='forwarded_memo'";
                                $result2=mysqli_query($conn,$def);
                                if($result2) {
                                    while($row=mysqli_fetch_assoc($result2)) {
                                        $comm=$row['f'];
                                    }     
                                }
                                                      echo "['$department'    ,$comm],"; 
                                                }
                                            }
            ?>
        ]);
        var options = {
          title: '',
            height:250,
              slices: {  1: {offset: 0.2},
                     },
        };
        var chart = new google.visualization.PieChart(document.getElementById('execpiefin'));
        chart.draw(data, options);
      } 
      }
</script>






<!----------------VOT HEADS ---------------------------VOTE HEADS------VOT HEADS---------VOTE HEADS--------------------->

<!--------------------DEVELOPMENT VS RECURRENT(USED) ------------------>
<?php
 require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
if($position=='C.O'){
//get department and sector id
       $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                       
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
/****GET dev vs rec Used****/

       ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $useddevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedrectotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Development'    ,$useddevtotal],"; 
                                                      echo "['Used Recurrent'    ,$usedrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#183951'},1:{color:'#917022'},
                     },
        };
      
           var container = document.getElementById('draw-charts-cousedcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
    
}
 else if($position=='C.E.C.M'){
      $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
     
     /****GET dev vs rec used THE DEPARTMENT*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $useddevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedrectotal= $row['c'];
                                      }     
                                   }
                                                      echo "['Used Development'    ,$useddevtotal],"; 
                                                      echo "['Used Recurrent'    ,$usedrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2},
                     },
        };
      
           var container = document.getElementById('draw-charts-cecmusedcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
}
 else if($position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $useddevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Used) as c FROM vote_heads WHERE Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedrectotal= $row['c'];
                                      }     
                                   }
                                                            
                                                       echo "['Used Development'    ,$useddevtotal],"; 
                                                      echo "['Used Recurrent'    ,$usedrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#183951'},1:{color:'#917022'},
                     },
        };
      
           var container = document.getElementById('draw-charts-execusedcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
 
 }
?>

<!--------------------DEVELOPMENT VS RECURRENT(BALANCE) ------------------>
<?php
 require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
if($position=='C.O'){
//get department and sector id
       $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                       
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
/****GET dev vs rec Used****/

       ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baldevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $balrectotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Development Balance'    ,$baldevtotal],"; 
                                                      echo "['Recurrent Balance'    ,$balrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#183951'},1:{color:'#917022'},
                     },
        };
      
           var container = document.getElementById('draw-charts-cobalcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
    
}
 else if($position=='C.E.C.M'){
      $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
     
     /****GET dev vs rec used THE DEPARTMENT*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baldevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $balrectotal= $row['c'];
                                      }     
                                   }
                                                      echo "['Development Balance'    ,$baldevtotal],"; 
                                                      echo "['Recurrent Balance'    ,$balrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2},
                     },
        };
      
           var container = document.getElementById('draw-charts-cecmbalcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
}
 else if($position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baldevtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $balrectotal= $row['c'];
                                      }     
                                   }
                                                            
                                                       echo "['Development Balance'    ,$baldevtotal],"; 
                                                      echo "['Recurrent Balance'    ,$balrectotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#183951'},1:{color:'#917022'},
                     },
        };
      
           var container = document.getElementById('draw-charts-execbalcomp').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
 
 }
?>





<!--------------------DEVELOPMENT VOTE HEAD ------------------>
<?php
require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
if($position=='C.O'){
//get department and sector id
       $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                       
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
/****GET TOTAL UNDER THIS SECTOR****/

       ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-co').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
    
    
 /****FETCH ALL THE VOTE HEADS UNDER THIS SECTOR****/
 $juke="SELECT * FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
         $result1=mysqli_query($conn,$juke);
        if($result1) {
 while($row=mysqli_fetch_assoc($result1)) {
     $id=$row["id"];
     $used=$row["Used"];
     $balance=$row["Balance"];
     $title=$row["Title_and_details"];
     

?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
        ['Used', <?php echo $used;?>,'#A2210D'],
        ['Balance', <?php echo $balance;?>,'#0C4223' ],
      
      ]);

      var options = {
        title: '<?php echo $title;?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
         
        }
      };

    var container = document.getElementById('draw-charts-co').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php
 }}
    
}
 else if($position=='C.E.C.M'){
    
     
     
      $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
     
     /****GET TOTAL USED AND BALANCE OF THE DEPARTMENT*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-cecm').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
     /******GET FOR SPECIFIC SECTORS*****/
     
                                         //get the sectors in this dept
                                            $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                     $sector_id=$row['sector_id'];
                                                    //get total of used votehead
                                       $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                               ?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
       ['Used', <?php if($usedtotal==''){ $usedtotalsafe=0;}else{$usedtotalsafe=$usedtotal;}echo $usedtotalsafe; ?>,'#A2210D'],
        ['Balance', <?php if($baltotal==''){ $baltotalsafe=0;}else{$baltotalsafe=$baltotal;}echo $baltotalsafe; ?>,'#0C4223' ],
      ]);

      var options = {
        title: '<?php echo $sector; ?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
        
        }
      };

    var container = document.getElementById('draw-charts-cecm').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php     
                                                    
                                                   
                                                }   

                                            }
    
          
    
}
 else if($position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
     
      /****GET TOTAL USED AND BALANCE OF THE COUNTY*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-exec').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
     /******GET FOR SPECIFIC DEPARTMENTS*****/
     
     
   $juke="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    $department=$row['department'];
                                                   $department_id=$row['department_id'];
                                          
                                                    //get total of used votehead
                                       $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='development'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                               ?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
        ['Used', <?php if($usedtotal==''){ $usedtotalsafe=0;}else{$usedtotalsafe=$usedtotal;}echo $usedtotalsafe; ?>,'#A2210D'],
        ['Balance', <?php if($baltotal==''){ $baltotalsafe=0;}else{$baltotalsafe=$baltotal;}echo $baltotalsafe; ?>,'#0C4223' ],
      
      ]);

      var options = {
        title: '<?php echo $department; ?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
        
        }
      };

    var container = document.getElementById('draw-charts-exec').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php     
                                                    
                                                   
                                                }   

                                            }
    
          
    
 
 }
?>



<!--------------------RECURRENT----------------VOTE HEAD ------------------>
<?php
require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name);  
if($position=='C.O'){
//get department and sector id
       $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                       
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
/****GET TOTAL UNDER THIS SECTOR****/

       ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-corec').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
    
    
    
 /****FETCH ALL THE VOTE HEADS UNDER THIS SECTOR****/
 $juke="SELECT * FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
         $result1=mysqli_query($conn,$juke);
        if($result1) {
 while($row=mysqli_fetch_assoc($result1)) {
     $id=$row["id"];
     $used=$row["Used"];
     $balance=$row["Balance"];
     $title=$row["Title_and_details"];
     

?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
        ['Used', <?php echo $used;?>,'#A2210D'],
        ['Balance', <?php echo $balance;?>,'#0C4223' ],
      
      ]);

      var options = {
        title: '<?php echo $title;?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
         
        }
      };

    var container = document.getElementById('draw-charts-corec').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php
 }}
    
}
 else if($position=='C.E.C.M'){
    
     
     
      $juke="SELECT * FROM departments_list WHERE department='$stationdepartment'";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $department_id=$row['department_id'];
                                                }}
     
     /****GET TOTAL USED AND BALANCE OF THE DEPARTMENT*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-cecmrec').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
     /******GET FOR SPECIFIC SECTORS*****/
     
                                         //get the sectors in this dept
                                            $coabc="SELECT * FROM sectors_list WHERE department_id='$department_id'";
                                    $result1=mysqli_query($conn,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $sector=$row['sector'];
                                                     $sector_id=$row['sector_id'];
                                                    //get total of used votehead
                                       $coab="SELECT sum(Used) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE sector_id='$sector_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                               ?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
       ['Used', <?php if($usedtotal==''){ $usedtotalsafe=0;}else{$usedtotalsafe=$usedtotal;}echo $usedtotalsafe; ?>,'#A2210D'],
        ['Balance', <?php if($baltotal==''){ $baltotalsafe=0;}else{$baltotalsafe=$baltotal;}echo $baltotalsafe; ?>,'#0C4223' ],
      ]);

      var options = {
        title: '<?php echo $sector; ?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
        
        }
      };

    var container = document.getElementById('draw-charts-cecmrec').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php     
                                                    
                                                   
                                                }   

                                            }
    
          
    
}
 else if($position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
     
      /****GET TOTAL USED AND BALANCE OF THE COUNTY*****/
     
        ?>
<script>
         google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Action', 'Number'],
         <?php
    
    $coab="SELECT sum(Used) as c FROM vote_heads WHERE Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                                            
                                                      echo "['Used Vote Head Amount'    ,$usedtotal],"; 
                                                      echo "['Vote Head Balance'    ,$baltotal],"; 
                                                     
                                             
            ?>
        ]);
        var options = {
          title: '',
            height:250,
           is3D: true,
           slices: {  0: {offset: 0.2,color:'#A2210D'},1:{color:'#0C4223'},
                     },
        };
      
           var container = document.getElementById('draw-charts-execrec').appendChild(document.createElement('div'));
      var chart = new google.visualization.PieChart(container);
      chart.draw(data, options);
      }  
    </script>
    
    <?php
    
     /******GET FOR SPECIFIC DEPARTMENTS*****/
     
     
   $juke="SELECT * FROM departments_list";
                                    $result1=mysqli_query($conn,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    $department=$row['department'];
                                                   $department_id=$row['department_id'];
                                          
                                                    //get total of used votehead
                                       $coab="SELECT sum(Used) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $usedtotal= $row['c'];
                                      }     
                                   }
                                                    
                                                    //get total of balance
                                                    $coab="SELECT sum(Balance) as c FROM vote_heads WHERE department_id='$department_id' AND Type='recurrent'";
                                    $result=mysqli_query($conn,$coab);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result)) {
                                        $baltotal= $row['c'];
                                      }     
                                   }
                                               ?>
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['', '',{ role: 'style' }],
        ['Used', <?php if($usedtotal==''){ $usedtotalsafe=0;}else{$usedtotalsafe=$usedtotal;}echo $usedtotalsafe; ?>,'#A2210D'],
        ['Balance', <?php if($baltotal==''){ $baltotalsafe=0;}else{$baltotalsafe=$baltotal;}echo $baltotalsafe; ?>,'#0C4223' ],
      
      ]);

      var options = {
        title: '<?php echo $department; ?>',
        chartArea: {width: '50%'},
        hAxis: {
         /* title: 'Total Population',
          minValue: 0*/
        },
        vAxis: {
         // title: 'City'
           
        
        }
      };

    var container = document.getElementById('draw-charts-execrec').appendChild(document.createElement('div'));
      var chart = new google.visualization.BarChart(container);
      chart.draw(data, options);
    }

</script>


<?php     
                                                    
                                                   
                                                }   

                                            }
    
          
    
 
 }
?>






<!---------------------VOTE HEAD WHEN DRILLED DEEPER--------VOTE HEAD DRILLED------------------->
<?php
if($_SESSION['infilref']==""){
    $infilses="null";
    
}
else{
    $infilses=$_SESSION['infilref'];
   
}


?>
<script>
function drillvotehead(val){
     var position="<?php echo $position?>";
    
     if(position=='GO' || 'D.G' || 'C.S' || 'D.C.S'){
    $(function() {
   $.ajax({
	type: "POST",
	url: "get_departmentinfil.php",
	data:'dept='+val,
	success: function(data){
		 document.location.reload()
	}
	});
  });
     }
     if(position=='C.E.C.M'){
        var ses='<?php echo $ses?>';
         var infilses='<?php echo $infilses?>';
        $(function() {
   $.ajax({
	type: "POST",
	url: "get_sectorinfil.php",
        data: {
        sect:val,
        infilses:infilses,
        ses:ses 
    },
	success: function(data){
		 document.location.reload()
	}
	});
  });
    }
    
}
</script>

<!--WHEN USER EXITS DASH PAGE-->



<script type="text/javascript">
     $(document).ready(function(){
        var dirtyses='<?php echo $_SESSION['dirtyses']?>';
         if(dirtyses=="Dirty"){
         $('#backarrow').show();
             $('#dontouch').hide();
         }
         else{
             
         }
     });
</script>



        <!-- Javascript -->
<!--NOTIFICATIONS-->
  <script>
                $(document).ready(function(){
                    var position="<?php echo $position?>";
                    if(position=='C.E.C.M'){
                       $('#execdoughnutcomm').hide();
                     $('#codoughnutcomm').hide();
                         $('#execpiefin').hide();
                        $('#copiefin').hide();
                        $('#draw-charts-co').hide();
                        $('#draw-charts-exec').hide();
                    }
                    else if(position=='C.O'){
                          $('#execdoughnutcomm').hide();
                     $('#cecmdoughnutcomm').hide();
                         $('#execpiefin').hide();
                        $('#cecmpiefin').hide();
                        $('#draw-charts-exec').hide();
                        $('#draw-charts-cecm').hide();
                    }
                    else if(position=='GO'){
                       $('#cecmdoughnutcomm').hide();
                     $('#codoughnutcomm').hide();
                         $('#cecmpiefin').hide();
                        $('#copiefin').hide();
                        $('#draw-charts-co').hide();
                        $('#draw-charts-cecm').hide();
                    }
                     else if(position=='D.G'){
                       $('#cecmdoughnutcomm').hide();
                     $('#codoughnutcomm').hide();
                         $('#cecmpiefin').hide();
                        $('#copiefin').hide();   
                         $('#draw-charts-co').hide();
                        $('#draw-charts-cecm').hide();
                    }
                    else if(position=='C.S'){
                       $('#cecmdoughnutcomm').hide();
                     $('#codoughnutcomm').hide();
                         $('#cecmpiefin').hide();
                        $('#copiefin').hide();      
                        $('#draw-charts-co').hide();
                        $('#draw-charts-cecm').hide();
                    }
                    else if(position=='D.C.S'){
                       $('#cecmdoughnutcomm').hide();
                     $('#codoughnutcomm').hide();
                         $('#cecmpiefin').hide();
                        $('#copiefin').hide();    
                        $('#draw-charts-co').hide();
                        $('#draw-charts-cecm').hide();
                    }
                    else{
                         $('#stats').hide();
                    }
                    
                   

                });
            </script>





<script>
$(document).ready(function(){
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"notifications/fetch.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('#dropdownotif').html(data.notification);
    if(data.unseen_notification > 0)
    {
        
     $('.countnotif').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $(document).on('click', '.nav__notify', function(){
  $('.countnotif').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 200);
 
});
</script>

<!--CHATS-->
<script>
$(document).ready(function(){
 // $('#dash').hide();
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"notifications/fetchmessages.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('#dropdownmes').html(data.notification);
    if(data.unseen_notification > 0)
    {
        
      
     $('.countmes').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $(document).on('click', '.nav__mes', function(){
  $('.countmes').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 20000);
 
});
</script>






        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

        <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
        <script src="js/rChat.js"></script>
        <script src="js/rChatSent.js"></script>

<script type="text/javascript" src="chartjs/js/Chart.min.js"></script>
    <script type="text/javascript" src="chartjs/js/Chart.min.js"></script>
    <script type="text/javascript" src="chartjs/js/linegraph.js"></script>
        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>