<?php

session_start();
if(!isset($_SESSION['karibu'])){
    header("Location:login.php");
}

//fetch.php
$ses=$_SESSION['karibu'];
include_once '../../config.php';
$columns = array('delegated_office');

$query = "SELECT * FROM delegates";


$query .= ' WHERE  delegated_by LIKE "%'.$ses.'%" ';

if(isset($_POST["order"])){
/* $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';*/
}
else{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1){
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($mysqli, $query));

$result = mysqli_query($mysqli, $query . $query1);

$data = array();
$i=1;
while($row = mysqli_fetch_array($result)){
 $sub_array = array();
 $sub_array[] = '<div  data-id="'.$row["id"].'" data-column="delegated_office">'.$i++.'. '. $row["delegated_office"] . '</div>';
 $sub_array[] = '<button type="button" name="delete" class="btn btn-danger btn--icon delete" iddeleg="'.$row["id"].'"><i class="zmdi zmdi-lock"></i></button>';
 $data[] = $sub_array;
}

function get_all_data($mysqli){
    
 $query =  "SELECT * FROM delegates";
 $result = mysqli_query($mysqli, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($mysqli),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
