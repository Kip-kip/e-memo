<?php
error_reporting(E_ALL);
ini_set('display_errors',1);


class DB_connect{
    
    private $fname;
    private $sname;
    private $memno;
    private $idnum;
    private $prinloan;
    private $duration;
    private $loanbal;
    private $weekleypay;
    private $loanpaid;
    private $savings;
    private $tsavings;
    private $tinterest;
    
    public function __construct(){
        
    }
    //fname
    public function getFname(){
        return $this->fname;
    }
    public function setFname($fName){
        $this->fname=$fName;
    }
    //sname
    public function getSname(){
        return $this->sname;
    }
    public function setSname($sName){
        $this->sname=$sName;
    }
    
    // a function which inserts admin form info into the database 
    public function insertAdmin($mysqli,$fname,$mname,$lname,$phone,$pfno,$idnum,$gender,$designation,$empstatus,$jobdesc,$dob,$email,$subcounty,$ward,$workstatus,$position,$acting,$department,$sector,$directoratename,$sectionname,$unit,$password){
        //encrypt the password
        $salt='#!@*%';
	   $pepper='*-#$QW';
	   $password=hash('sha512',$salt.$password.$pepper);
      $stmt = $mysqli->prepare("SELECT phone from ememo_users WHERE phone = ?");
      $stmt->bind_param("s", $phone);
      $stmt->execute();
       $stmt->store_result();
        if ($stmt->num_rows > 0) {
 ?>
<script>
alert("There already exists a user with that phone number");
</script>
<?php
            
        }
        else{
             
           
            
            
              
               
            //determine the hierarchical_level
            if($position=='GO'){
                $hierarchical_level=9;
            }
            else if($position=='D.G'){
                $hierarchical_level=8;
            }
            else if($position=='C.S'){
                $hierarchical_level=7;
            }
            else if($position=='D.C.S'){
                $hierarchical_level=6;
            }
            else if($position=='C.O.F'){
                $hierarchical_level=5;
            }
            else if($position=='C.E.C.M'){
                $hierarchical_level=4;
            }
            else if($position=='C.O'){
                $hierarchical_level=3;
            }
            else if($position=='DIR'){
                $hierarchical_level=2;
            }
            else if($position=='D.DIR'){
                $hierarchical_level=1;
            }
            else{
                  $hierarchical_level=0;
            }
            
            $dp_file_ext="";
            $sig_file_ext="";
			$staffposition="";
            
             if($stmnt=$mysqli->prepare("INSERT INTO ememo_users(fname,mname,lname,phone,pfno,idnum,gender,designation,empstatus,jobdesc,dob,email,subcounty,ward,workstatus,position,staffposition,acting,department,sector,directorate,section,unit,hierarchical_level,password,dp_file_ext,sig_file_ext)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")){
            $stmnt->bind_param('sssssssssssssssssssssssssss',$fname,$mname,$lname,$phone,$pfno,$idnum,$gender,$designation,$empstatus,$jobdesc,$dob,$email,$subcounty,$ward,$workstatus,$position,$staffposition,$acting,$department,$sector,$directoratename,$sectionname,$unit,$hierarchical_level,$password,$dp_file_ext,$sig_file_ext);
            $stmnt->execute();
          
     ?>
<script>
  window.location.href="settings.php"; 
    alert("Sucessful Registration");
</script>
<?php

                                }
            
   
         
                 
            
        else{
            $tii="oops couldnt perform the desired operation";
                echo $tii;
        }
       
            
        }
    }
    
    
    
}
?>