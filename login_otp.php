<?php
session_start();
if(!isset($_SESSION['welcome'])){
    header("Location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
 <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    </head>

    <body data-sa-theme="1">
        
        <div class="login" style="background-color:#ecebec;">
            <div class="login__block active">
<img width="80" height="80" src="img/nanditransold.png"/>
            <!-- Login -->
            
            <div class="login__block active" id="cbmemcreation">
                <h id="title">E-Memo</h>
                <div class="login__block__header">
                    <i id="mee" class="zmdi zmdi-account-circle"></i>
                    OTP Verification

                    <div class="actions actions--inverse login__block__actions">
                        <div class="dropdown">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-register" href="#"></a>
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-forget-password" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login__block__body">
                    <form action="login_otp.php" method="post">
                    <div class="form-group">  
                       
                        <input type="text" name="otp" class="form-control text-center" placeholder="Please enter OTP">
                    </div>

                  <p id="car"></p>
                     <button class="btn btn-dark btn--icon" name="submit" ><i class="zmdi zmdi-key"></i></button>
                    
                        </form>
                    
<?php
                    include_once 'config.php';
                     $ses=$_SESSION['welcome']; 
                      $newses=$ses+1;
     if(isset($_POST['submit'])){
        $otp=$_POST['otp'];
    
         
 
           $query ="SELECT * FROM otp_pass WHERE otp=? AND is_expired!=0 LIMIT 1";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $otp);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                   
                    //check if the user has delegated accounts
                $query2 ="SELECT * FROM delegates WHERE delegate_id=?";  
                 if($stmt = $mysqli->prepare($query2)){
                $stmt->bind_param('s', $newses);
                $stmt->execute();
                $stmt->store_result();
                     //if there are delegated duties
                      if($stmt->num_rows>=1){
                          //redirect to choose account pages
                           //Expire the OTP
                     $sql="UPDATE otp_pass SET is_expired=1 WHERE otp='$otp'";
                    if($stmnt=$mysqli->prepare($sql)){
                    $stmnt->execute();      
                    }  
                           ?>
                                    <script type="text/javascript">
                                         window.location.href="login_choose_acc.php";
                                    </script>
                                   <?php
                      } 
                     else{//if there are no delegated duties
                           //Expire the OTP
                     $sql="UPDATE otp_pass SET is_expired=1 WHERE otp='$otp'";
                    if($stmnt=$mysqli->prepare($sql)){
                    $stmnt->execute();      
                    }  
                    
                    //ASSIGN KARIBU SESSION
                    $_SESSION['karibu']=$newses;
                         
                          //set session last_time
                    $_SESSION['last_time'] = time();
                         
                             //unset session welcome
                    unset($_SESSION['welcome']);
                            //give access to admin page if admin
                            if($newses==2){
                        
                                   ?>
                                    <script type="text/javascript">
                                         window.location.href="admin/index.php";
                                    </script>
                                   <?php
                        }
                         else{//give access to home page
                            ?>
                                    <script type="text/javascript">
                                         window.location.href="index.php";
                                    </script>
                                   <?php
                        }
                         
                     }
                
                
                    
                }
                }
         else{
                        echo "<p style=\"color:#FC4A1A\">The OTP you entered is Invalid !</p>";
                    
                    }
                }
        
        }
     
        
     
    ?>
                </div>
            </div>
</row>
        </div></br>
        <script>
     
            </script>
        <!-- Javascript -->
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>
        <script src="js/event-controller.js"></script>
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
</html>