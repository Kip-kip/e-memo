<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}

/****** GENERATE PDF*******/
 if(isset($_POST['pdf'])){
       
  
              function fetch_data()  
 {  
                  
      $ses=$_SESSION['karibu'];
      $referenceno=$_GET['referenceno'];
      $subject=$_GET['subject'];
      $output = '';  
      require("./_connect.php");
//connect to db
$connect = new mysqli($db_host,$db_user, $db_password, $db_name);  
                     
    /*FIRST QUERY TO SELECT AND FILL IN THE SENDER/REQUESTOR*/
      $queryonepdf="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.requestor = ememo_users.user_id WHERE referenceno='$referenceno'";    
                                //execute query
if ($connect->real_query($queryonepdf)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
         $reqmemotypepdf=$row["memotype"];
         $reqnaturepdf=$row["nature"];
        $reqfnamepdf=$row["fname"];
         $reqmnamepdf=$row["mname"];
         $reqlnamepdf=$row["lname"];
         $reqpositionpdf=$row["position"];
         $reqdepartmentpdf=$row["department"];
         //$reqdepartmentpdfstbn=strtoupper($reqdepartmentpdf);
         $reqsectorpdf=$row["sector"];
         $reqdirectoratepdf=$row["directorate"];
         $reqsectionpdf=$row["section"];
         $requnitpdf=$row["unit"];
         $requser_idpdf=$row["user_id"];
         $reqsig_file_extpdf=$row['sig_file_ext'];
         $requrgencypdf=$row["urgency"];
        if($requrgencypdf=='very high'){
            $urgencypdf='Icon_red';
        }
        else if($requrgencypdf=='high'){
              $urgencypdf='Icon_orange';
        }
        else{
              $urgencypdf='Icon_blue';
        }
        
         if($reqpositionpdf=='C.E.C.M'){
            $reqstationpdf=$reqdepartmentpdf;
              $prefixpdf="DEPARTMENT OF";
             $suffixpdf=$reqstationpdf;
        }
        else if($reqpositionpdf=='C.O'){
             $reqstationpdf=$reqsectorpdf;
            $prefixpdf=$reqstationpdf;
             $suffixpdf="SECTOR";
        }
         else if($reqpositionpdf=='DIR'){
             $reqstationpdf=$reqdirectoratepdf;
             $prefixpdf=$reqstationpdf;
             $suffixpdf="DIRECTORATE";
        }
         else if($reqpositionpdf=='D.DIR'){
             $reqstationpdf=$reqsectionpdf;
              $prefixpdf= $reqstationpdf;
             $suffixpdf="SECTION";
        }
        else if($reqpositionpdf=='HOU'){
             $reqstationpdf=$requnitpdf;
              $prefixpdf= $reqstationpdf;
             $suffixpdf="UNIT";
        }
        else if($reqpositionpdf=='STAFF'){
             $reqstationpdf=$requnitpdf;
             $prefixpdf= $reqstationpdf;
             $suffixpdf="UNIT";
            
        }
        else{
             $reqstationpdf='County Government of Nandi';
        }

   
    }
     $output .="<p style=\"margin-left:800px;margin-top:-10px;\"><b></p> "; 
}
                     
    /*SECOND QUERY TO SELECT AND FILL IN THE RECEIVER*/
                     //check if it is broadcast
                if($reqmemotypepdf=='broadcast'){
                    $recpositionpdf='';
                    $recstationpdf=$reqnaturepdf;
                }
                //if not
                else{
      $querytwopdf="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='direct'"; 
                                //execute query
if ($connect->real_query($querytwopdf)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
        
        $recpositionpdf=$row["position"];
         $recdepartmentpdf=$row["department"];
          $recsectorpdf=$row["sector"];
          $recdirectoratepdf=$row["directorate"];
        $recsectionpdf=$row["section"];
         $recunitpdf=$row["unit"];
        $recnaturepdf=$row["nature"];
        
        if($recpositionpdf=='C.E.C.M'){
            $recstationpdf=$recdepartmentpdf;
        }
        else if($recpositionpdf=='C.O'){
             $recstationpdf=$recsectorpdf;
        }
         else if($recpositionpdf=='DIR'){
             $recstationpdf=$recdirectoratepdf;
        }
          else if($recpositionpdf=='D.DIR'){
             $recstationpdf=$recsectionpdf;
        }
        else if($recpositionpdf=='HOU'){
             $recstationpdf=$recunitpdf;
        }
        else if($recpositionpdf=='STAFF'){
             $recstationpdf=$recunitpdf;
        }
        else{
             $recstationpdf='County Government of Nandi';
        }
    
    }
}                     
                }
                     /***MAIN QUERY FOR MEMO BODY***/
     $sql="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' LIMIT 1";                      
//execute query
if ($connect->real_query($sql)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
         $referenceno=$row["referenceno"];
        $dpdf=$row["datecreated"];
        $introduction=$row["introduction"];
        $genstat=$row["generalstatus"];
        $prevmemo=$row["prevmemo"];
		$memotype=$row["memotype"];
        $subject=$row["subject"];
        $datecreatedpdf=date("jS M, Y", strtotime($dpdf));
        /*stamping approval status*/
    if($genstat=="Approved"){
        $stamp="approved";
    }
        else if($genstat=="Rejected"){
            $stamp="rejected";
        }
        else{
            $stamp="null";
        }
        
        
    }
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
                     /***********************QR GENERATION************************/   
                                
$querywmpdf="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($connect->real_query($querywmpdf)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $positionwmpdf=$row["position"];
         $stationdepartmentwmpdf=$row["department"];
        $stationsectorwmpdf=$row["sector"];
        $stationdirectoratewmpdf=$row["directorate"];
         $stationsectionwmpdf=$row["section"];
         $stationunitwmpdf=$row["unit"];
         $fnamewmpdf=$row['fname'];
         $mnamewmpdf=$row['mname'];
         $lnamewmpdf=$row['lname'];
        $dp_file_extwmpdf=$row['dp_file_ext'];
        
        if($positionwmpdf=='C.E.C.M'){
            $stationwmpdf=$stationdepartmentwmpdf;
        }
        else if($positionwmpdf=='C.O'){
             $stationwmpdf=$stationsectorwmpdf;
        }
         else if($positionwmpdf=='DIR'){
             $stationwmpdf=$stationdirectoratewmpdf;
        }
         else if($positionwmpdf=='D.DIR'){
             $stationwmpdf=$stationsectionwmpdf;
        }
        else if($positionwmpdf=='HOU'){
             $stationwmpdf=$stationunitwmpdf;
        }
        else if($positionwmpdf=='STAFF'){
             $stationwmpdf=$stationunitwmpdf;
        }
        else{
             $stationwmpdf='County Government of Nandi';
        }
       
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $connect->errno;
}
                    /**PLACE ACTIONS ON TOP OF PAGE**/
                  //pick the most recent activity on the logs table
    $query="SELECT * FROM logs  inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND actor_nature='direct'";
//execute query
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
        $cdate=date_create($row["cdate"]);
        $actor_id=$row['actor_id'];
        $actor_recepient=$row['actor_recepient'];
        $action=$row['action'];
        
        $finaluser_id=$row['user_id'];
        $finalsig_file_ext=$row['sig_file_ext'];
        
        //if the action is approved, forwarded escalated or rejected display it
        if(($action=='Approved')OR($action=='Rejected')OR($action=='Escalated')OR($action=='Forwarded')){
            
            $finalaction=$action;
            $finaldate=$cdate;
            $finalofficer=$actor_recepient;
             $visibility="";
            if($action=='Approved'){
               $rangi="#0d5247;";
            }
            else if($action=='Rejected'){
                $rangi="#A2210D";
            }
            else if($action=='Forwarded'){
               $rangi="#505458;";
            }
            else if($action=='Escalated'){
                 $rangi="#505458;";
            }
        }
        else{
           $visibility="none";
             $rangi="#505458;";
             $finaldate="";
             $finalofficer="";
             $finalaction="";
            $finaldate=date_create("");
            
        }
    }
}
           
                  
                  
                    
                   //remove white spaces for the sake of qr code
              $stripped = str_replace(' ', '_', $stationwmpdf);
                  //NOW OUTPUT EVERYTHING
                 
                  
          $output .= "<div class=\"table-responsive\" style=\"margin-left:20px;font-size:9.5px;margin-right:20px;margin-top:-15px;\">
     <table border=1 class=\"table\">
  
    <tr>
    <th style=\"width:170px;margin-top:100px;margin-left:20px;\"><img width=\"100\" src=\"http://localhost/e-memo/qr_generator.php?code=PDF_printed_or_generated_by_$positionwmpdf$stripped\"/></th>
    <th style=\"width:155px;text-align:center;\"><img  width=\"15\" height=\"15\" src=\"img/$urgencypdf.png\"/></th>
    <th style=\"width:170px;text-align:center;padding-top:50px;margin-left:200px;\">
    <p style=\"text-align:left;display:$visibility;color:$rangi;\"><b>Status: $finalaction</b></p>
    <p style=\"text-align:left;display:$visibility;\"><b>Date:".date_format($finaldate, ' l jS F Y')."</b></p>
    <p style=\"text-align:left;display:$visibility;\"><b>Officer: $finalofficer</b></p>
    <p style=\"text-align:left;display:$visibility;\"><b>Signature:<br><img  style=\"display:$visibility;\"width=\"50\" height=\"50\" src=\"img/signatures/$finaluser_id$finalsig_file_ext\"/></b></p>
    </th>
    </tr>
    </table>
    </div>";        
                  
    $output .= "<div class=\"table-responsive\" style=\"margin-left:20px;margin-right:20px;\">
     <table border=1 class=\"table\">
    <tr>
    <td style=\"width:500px;text-align:center;font-size:14px;font face:\"times new roman\"\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
    $output .= "<div class=\"table-responsive\" style=\"margin-left:20px;font-size:9.5px;margin-right:20px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
    <th style=\"width:170px;-top:100px;margin-left:20px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:155px;text-align:center;\"><img width=\"80\" height=\"80\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:170px;text-align:center;padding-top:50px;margin-left:200px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </table>
    </div>";   
				  
				  /**--------------------------------------IF MEMO IS A LOCAL TRAVEL REQUEST------------------------------------------**/
					if($memotype=='local'){
						
						
							//get Travel details from the travel table 	and inner join with ememo users to get applicants name and signature
				
$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.officer_id=ememo_users.user_id WHERE referenceno='$referenceno'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
	$fname=$row['fname'];
	$mname=$row['mname'];
	$lname=$row['lname'];
	$officer_id=$row['officer_id'];
	$sig_file_ext=$row['sig_file_ext'];
    $personalno=$row['personal_number'];	
	$designation=$row['designation'];	
	$telephone=$row['telephone'];	
	$duration=$row['duration'];	
	$startdate=$row['start_date'];	
	$enddate=$row['end_date'];	
	$destination=$row['destination'];	
	$purpose=$row['purpose'];
	$activities=$row['activities'];	
	$delegatedofficer=$row['delegated_officer'];	
	$delegatedofficerdesign=$row['delegated_officer_design'];	
	$components=$row['components'];	
}}
				
				//get the delegated officer name and signature
$query="SELECT * FROM ememo_users WHERE user_id='$delegatedofficer'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
$fnamedelegated=$row['fname'];	
	$mnamedelegated=$row['mname'];	
	$lnamedelegated=$row['lname'];
	$sig_file_extdelegated=$row['sig_file_ext'];
}}
	
						//incase CECM doesnt exist
					$cecmremarks="";
						
						
						//get CECM's Remarks and Signature
	$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.E.C.M'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {

	$cecmremarks=$row['remarks'];	
    $cecmsig=$row['sig_file_ext'];
	$cecmuser_id=$row['user_id'];
	
}}
						
						//to ensure an empty record isnt encountered			
	if($cecmremarks==''){
		$cecmaction="";
		$cecmdate="";
		$cecmsig=".png";
		$cecmuser_id="white";
						
	}
						
						  //get CECM's Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.E.C.M'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
	
	$cecmaction=$row['action'];
	$cecmdate=$row['cdate'];
	
}}
						
						
		    //get CS' Remarks and Signature
		$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.S'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
	
	$csremarks=$row['remarks'];	
    $cssig=$row['sig_file_ext'];
	$csuser_id=$row['user_id'];
	
}}
						//to ensure an empty record isnt encountered			
	if($csremarks==''){
		$csaction="";
		$csdate="";
		$csuser_id="white";
		$cssig=".png";
	}
						
						  //get CS' Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.S'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
	
	$csaction=$row['action'];
	$csdate=$row['cdate'];
	
}}
						
						
						
		   //get DG's Remarks, Action and Signature
$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='D.G'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {

	$dgremarks=$row['remarks'];	
    $dgsig=$row['sig_file_ext'];
	$dguser_id=$row['user_id'];
		
}}
	                  //to ensure an empty record isnt encountered			
	if($dgremarks==''){
		$dgaction="";
		$dgdate="";
		$dguser_id="white";
		$dgsig=".png";
	}
						
						  //get DG's Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='D.G'";    
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
	
	$dgaction=$row['action'];
	$dgdate=$row['cdate'];

}}
							
						
							
						
						
						
						 $output .= "<div class=\"table-responsive\" style=\"margin-left:100px;margin-right:20px;margin-top:-10px;\">
     <table border=1 cellpadding=\"0\">
    <tr>
    <td style=\"width:500px;text-transform:uppercase;text-align:center;font-size:13px;margin-left:400px;\"><b>OFFICE OF THE DEPUTY GOVERNOR</b></td>
   </tr>
    </table>
    </div>";  
						                          
     $output .="<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-weight:1000;text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
				
				
						
                      $output .= "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">To be completed in Duplicate: Original to be retained by applicant and Duplicate by HRM</div>"; 
					
					 $output .= "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>PART I</b></div>"; 
					
					 $output .= "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">1. Name of Officer: <b> $fname $mname $lname</b></div>"; 
					
					 $output .= "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">2. Personal Number:<b> $personalno</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">3. Designation:  <b>$designation</b>  Tel:  <b>$telephone</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">4. Duration:  <b>$duration</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">5. Start Date:  <b>$startdate</b>  End Date:  <b>$enddate</b></div>"; 
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">6. Country/City of Destination:  <b>$destination</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">Purpose/Reason for request of travel:</div>";

					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>$purpose</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Signature of Applicant: <img width=\"50\" height=\"50\" src=\"img/signatures/$officer_id$sig_file_ext\"/></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>PART II-HANDING OVER</b></div>"; 
					
					 $output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">7. The following assignments/duties/responsibilities will be handled by: </div>"; 
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>ACTIVITIES/DUTIES AND RESPONSIBILITIES</b></div>"; 
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>$activities</b></div>"; 
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">Name of the Officer:  <b>$fnamedelegated $mnamedelegated $lnamedelegated</b> </div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">Designation:  <b>$delegatedofficerdesign</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\">Component:  <b>$components</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Signature: <img width=\"50\" height=\"50\" src=\"img/signatures/$delegatedofficer$sig_file_extdelegated\"/></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>8. SUPERVISOR/HEAD OF DEPARTMENT (Where applicable)</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Remarks:<b> $cecmremarks</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Signature: <img width=\"50\" height=\"50\" src=\"img/signatures/$cecmuser_id$cecmsig\"/>  Date:<b> $cecmdate</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>9. COUNTY SECRETARY</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Status: <b>$csaction</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Remarks (If any): <b>$csremarks</b></div>";
						
						$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Signature: <img width=\"50\" height=\"50\" src=\"img/signatures/$csuser_id$cssig\"/>  Date: <b>$csdate</b></div>";
						
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;\"><b>10. H.E. THE DEPUTY GOVERNOR</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Status: <b>$dgaction</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Remarks (If any): <b>$dgremarks</b></div>";
					
					$output .="<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:10px;font-weight:200;\">Signature: <img width=\"50\" height=\"50\" src=\"img/signatures/$dguser_id$dgsig\"/>  Date:<b> $dgdate</b></div>";
					
						 $output .= "<p></p>";     
                        $output .= "<p style=\"color:white;\">.</p>"; 
                        $output .= "<p style=\"color:;text-align:center;\">.</p>";
                         $output .= "<p style=\"color:white;\">.</p>"; 
                
                         $output .= "<p style=\"text-align:center;font-size:9px;\"><i>#TRANSFORMING NANDI</i></p>";   
                  
                  $output .= "<p style=\"text-align:center;font-size:9px;\"><i>Innovations for Effective and Efficient Service Delivery for a Truly e-Government</i></p>"; 
						
					 	
					}
					else{
				  
				  
				  
    $output .= "<div class=\"table-responsive\" style=\"margin-left:100px;margin-right:20px;margin-top:-10px;\">
     <table border=1 cellpadding=\"0\">
    <tr>
    <td style=\"width:500px;text-transform:uppercase;text-align:center;font-size:13px;margin-left:400px;\"><b>".strtoupper($prefixpdf)." ".strtoupper($suffixpdf)."</b></td>
   </tr>
   <tr>
    <td style=\"width:500px;text-transform:uppercase;text-align:center;font-size:12px;margin-left:400px;\"><b>MEMO</b></td>
   </tr>
    </table>
    </div>";        
                  
         $output .="<table style=\"font-size:9px\">
         <tr>
         <td style=\"width:500px;text-transform:uppercase;text-align:center;margin-left:150px;font-size:20px;\"><hr style=\"height:1px;border:none;color:#000;background-color:#000;\"></td>
         </tr>
          
         </table>";               
                    
    $output .="<div class=\"table-responsive\" style=\"font-size:9px;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"270px;\" ><b>TO: ".strtoupper($recpositionpdf)." ".strtoupper($recstationpdf)."</b></th>
    <th style=\"width:470px;text-align:left;\"><b>".strtoupper($referenceno)."</b></th>
    </tr>
    </table>
     </div>";
                                
    $output .= "<div class=\"table-responsive\" style=\"margin:20px;margin-right:20px;font-size:9px;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"270px;\"><b>FROM: ".strtoupper($reqpositionpdf)." ".strtoupper($reqstationpdf)."</b></th>
    <th style=\"width:470px;text-align:left;\"><b>DATE: ".strtoupper($datecreatedpdf)."</b></th>
    </tr>
    </table>
     </div>";
                  
                     /**determine whether through will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Through' AND referenceno=?";   
        if($stmt = $connect->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hidethpdf=""; 
                  
                }
            else{
                $hidethpdf="none";   
            }
        }
                     
                  
                  
                  /****FILL IN Through below from****/           
  $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Through'";    
                                //execute query
if ($connect->real_query($querytwo)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionthrpdf=$row["position"];
         $recdepartmentthrpdf=$row["department"];
        $recsectorthrpdf=$row["sector"];
        $recdirectoratethrpdf=$row["directorate"];
        $recsectionthrpdf=$row["section"];
         $recunitthrpdf=$row["unit"];
        
        
        if($recpositionthrpdf=='C.E.C.M'){
            $recstationthrpdf=$recdepartmentthrpdf;
        }
        else if($recpositionthrpdf=='C.O'){
             $recstationthrpdf=$recsectorthrpdf;
        }
         else if($recpositionthrpdf=='DIR'){
             $recstationthrpdf=$recdirectoratethrpdf;
        }
         else if($recpositionthrpdf=='D.DIR'){
             $recstationthrpdf=$recsectionthrpdf;
        }
        else if($recpositionthrpdf=='HOU'){
             $recstationthrpdf=$recunitthrpdf;
        }
         else if($recpositionthrpdf=='STAFF'){
             $recstationthrpdf=$recunitthrpdf;
        }
        else{
             $recstationthrpdf='County Government of Nandi';
        }
   
           $output .= "<div class=\"table-responsive\" style=\"margin:20px;margin-right:20px;font-size:9px;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"220px;\" style=\"font-size:9px;display:$hidethpdf;\"><b>THROUGH: ".strtoupper($recpositionthrpdf)." ".strtoupper($recstationthrpdf)."</b></th>
    </tr>
    </table>
     </div>";
        
    }
 
}  
                  
                  
                  
                  $output .= "<div class=\"table-responsive\" style=\"margin:20px;margin-right:20px;\">
    <table border=0 class=\"table\">
    <tr>
    <td style=\"font-size:9px;text-decoration:underline;\"><b>RE: ".strtoupper($subject)."</b></td>
    </tr>
    </table>
     </div>";
                  
                  
         $output .="<table style=\"font-size:10px\">
         <tr>
         <td>$introduction</td>
         </tr>
         </table>"; 

                                      
  
      $output .= "
     <table style=\"font-size:9px\">
     <tr>
     <th><img  width=\"50\" height=\"50\" src=\"img/signatures/$requser_idpdf$reqsig_file_extpdf\"/></th>
     </tr>
     <tr>
     <th style=\"margin-top:15px;\"><b>".strtoupper($reqfnamepdf)." ".strtoupper($reqmnamepdf)." ".strtoupper($reqlnamepdf)."</b></th>
     </tr>
     </table>";
                  
     $output .= "<div class=\"table-responsive\" style=\"font-size:9px;\">
    <table border=0 class=\"table\">
     <tr>
     <th>".strtoupper($reqpositionpdf)." ".strtoupper($reqstationpdf)."</th>
     </tr>
    </table>
     </div>";
       
          
               
            
                  
                                /**determine whether cc will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Cc' AND referenceno=?";   
        if($stmt = $connect->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hideccpdf=""; 
                  
                }
            else{
                $hideccpdf="white";   
            }
        }
                  
                  
         $output .= "
    <table border=0 class=\"table\" style=\"font-size:10px;color:$hideccpdf;\">
    <tr>
    <th>Cc</th>
     </tr>             
    </table>
    ";                  
                                        
         /****FILL IN CCs at the bottom****/           
   $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Cc'";    
                                //execute query
if ($connect->real_query($querytwo)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $recpositionccpdf=$row["position"];
         $recdepartmentccpdf=$row["department"];
        $recsectorccpdf=$row["sector"];
        $recdirectorateccpdf=$row["directorate"];
        $recsectionccpdf=$row["section"];
         $recunitccpdf=$row["unit"];
        
        if($recpositionccpdf=='C.E.C.M'){
            $recstationccpdf=$recdepartmentccpdf;
        }
        else if($recpositionccpdf=='C.O'){
             $recstationccpdf=$recsectorccpdf;
        }
         else if($recpositionccpdf=='DIR'){
             $recstationccpdf=$recdirectorateccpdf;
        }
         else if($recpositionccpdf=='D.DIR'){
             $recstationccpdf=$recsectionccpdf;
        }
        else if($recpositionccpdf=='HOU'){
             $recstationccpdf=$recunitccpdf;
        }
        else if($recpositionccpdf=='STAFF'){
             $recstationccpdf=$recunitccpdf;
        }
        else{
             $recstationccpdf='County Government of Nandi';
        }
     
     
      
       $output .= "<div style=\"margin:20px;margin-right:20px;\">
    <table border=0 cellpadding=\"-2\">
    <tr>
    <td style=\"font-size:9px;width:15px;\"></td>
    <td style=\"font-size:9px;width:500px;text-transform:uppercase;\"> ".strtoupper($recpositionccpdf)." ".strtoupper($recstationccpdf)."</td>
    </tr>
    </table>
     </div>";     
        
        
    }
}       
                  
                   $output .= "<p></p>";     
                        $output .= "<p style=\"color:white;\">.</p>"; 
                        $output .= "<p style=\"color:white;\">.</p>";
                         $output .= "<p style=\"color:white;\">.</p>"; 
                  
                  //APPEND COMMENTS ON MEMO
                  $query="SELECT comments FROM nonfinancialmemos where referenceno='$referenceno' ORDER by id ASC LIMIT 1";
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
        $comments=$row["comments"];
       $cleancomments=str_replace('&nbsp;', '', $comments);
       $cleanercomments=str_replace('>&', '', $cleancomments);
       $cleanestcomments=str_replace('hr', '', $cleanercomments);
      $cleanesttcomments=str_replace('50', '40', $cleanestcomments);
    
      $output .= "<div class=\"table-responsive\" style=\"background-color:#0d5247;\">
     <table border=1>
    <tr>
    <td style=\"width:500px;color:white;text-align:center;font-size:10px;\"><b><p style=\"text-align:center;\"><img width=\"35\" height=\"35\" src=\"img/pin.jpg\" style=\"text-align:center;\"/></p></b><u>Comments made on this memo</u></td>
   </tr>
   <tr>
    <td style=\"width:500px;background-color:white;text-align:left;font-size:9px;\">$cleanesttcomments</td>
   </tr>
    </table>

    </div>";    
    
    
    }
}
                  
                  $output .="<div style=\"background-color:#0d5247;\">
                                       <p style=\"color:white;font-size:10px;text-align:center;\"><u>Actions taken on this memo</u></p>
                                         
                                       
                                       
                                      
                                        
                                        
                                      </div>"; 
                            //APPEND ACTIONS ON MEMO
                $query="SELECT * FROM logs Inner Join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno'";
                   $i=1;
if ($connect->real_query($query)) {
    //If the query was successful
    $res = $connect->use_result();
while ($row = $res->fetch_assoc()) {
         $referenceno=$row["referenceno"];
         $actor_recepient=$row["actor_recepient"];
        $action=$row["action"];
        $subject=$row['subject'];
        $at=$row["at"];
        $cdate=date_create($row["cdate"]);
    
     $user_id=$row["user_id"];
    $sig_file_ext=$row["sig_file_ext"];
        
        if($action=='Commented'){
            $on="on";
        }
        else{
            $on="";
        }
 
    
     $output .="<div style=\"background-color:white;font-size:9px;\"><p style=\"background-color:;;color:;margin-left:50px;\">".$i++. " $actor_recepient $action $on $subject $at ".date_format($cdate, 'g:ia \o\n l jS F Y')." <img width=\"40\" height=\"40\" src=\"img/signatures/$user_id$sig_file_ext\"></p></div>";

        }
}
                  
                  
                  
                         $output .= "<p></p>";     
                        $output .= "<p style=\"color:white;\">.</p>"; 
                        $output .= "<p style=\"color:white;\">.</p>";
                         $output .= "<p style=\"color:white;\">.</p>"; 
                
                         $output .= "<p style=\"text-align:center;font-size:9px;\"><i>#TRANSFORMING NANDI</i></p>"; 
                  
                   $output .= "<p style=\"text-align:center;font-size:9px;\"><i>Innovations for Effective and Efficient Service Delivery for a Truly e-Government</i></p>"; 
						
					}
return $output;  
                               
   
 }  
   


// set javascript

 $subject=$_GET['subject'];

      require_once('tcpdf/tcpdf.php'); 

     class MYPDF extends TCPDF {
    //Page header
    public function Header() {
         $ses=$_SESSION['karibu'];
      require("./_connect.php");
//connect to db
$connect = new mysqli($db_host,$db_user, $db_password, $db_name);
        /**get user**/
        $querywmpdf="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($connect->real_query($querywmpdf)) {
    //If the query was successful
    $res = $connect->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $positionwmpdf=$row["position"];
         $stationdepartmentwmpdf=$row["department"];
        $stationsectorwmpdf=$row["sector"];
        $stationdirectoratewmpdf=$row["directorate"];
         $stationsectionwmpdf=$row["section"];
         $stationunitwmpdf=$row["unit"];
         $fnamewmpdf=$row['fname'];
         $mnamewmpdf=$row['mname'];
         $lnamewmpdf=$row['lname'];
        $dp_file_extwmpdf=$row['dp_file_ext'];
        
        if($positionwmpdf=='C.E.C.M'){
            $stationwmpdf=$stationdepartmentwmpdf;
        }
        else if($positionwmpdf=='C.O'){
             $stationwmpdf=$stationsectorwmpdf;
        }
         else if($positionwmpdf=='DIR'){
             $stationwmpdf=$stationdirectoratewmpdf;
        }
         else if($positionwmpdf=='D.DIR'){
             $stationwmpdf=$stationsectionwmpdf;
        }
        else if($positionwmpdf=='HOU'){
             $stationwmpdf=$stationunitwmpdf;
        }
        else if($positionwmpdf=='STAFF'){
             $stationwmpdf=$stationunitwmpdf;
        }
        else{
             $stationwmpdf='County Government of Nandi';
        }
       
      
              
        }
     
}
             $bMargin = $this->getBreakMargin();
            $auto_page_break = $this->AutoPageBreak;
            $this->SetAutoPageBreak(false, 0);
            $this->StartTransform();
           $this->SetAlpha(0.6);
           $this->Rotate(20, 70, 110);
           $this->Image("http://localhost/e-memo/phptext-image/text-image.php?fname=$fnamewmpdf&&mname=$mnamewmpdf&&lname=$lnamewmpdf", 30, 150, 200, 100, '', '', '', false, 100, '', false, false, 0);
            $this->StopTransform();
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            $this->setPageMark();
            }
}
     
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle("Print The Memo Here");  
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, '2', PDF_MARGIN_RIGHT);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE, 10);
$pdf->SetFont('helvetica', '', 8.0);
$pdf->AddPage();
$content = ''; 
$content .= ''; 
$content .= fetch_data();  
$content .= ''; 
$pdf->writeHTML($content, true, false, true, false, '');
$pdf->Output($subject.".badmojo", 'I');  
   

            }    
                    
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
  <script src="ckeditor/adapters/jquery.js"></script>
     <script src="ckeditor/ckeditor.js"></script>
    
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="push.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
     <script src="js/jquery-3.0.0.js"></script>
    
    <!--hide some elements-->
    <script>
                $(document).ready(function(){
                
                    $('#report').hide();
                    $('#btnfro').hide();
    
                });
            </script>
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                   <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>&nbsp;&nbsp;&nbsp;&nbsp;</h1>
                    <a href ="nonfinancialdashsent.php?subject=<?php echo $_GET['subject'];?>&&referenceno=<?php echo $_GET['referenceno'];?>&&requestor=<?php echo $_GET['requestor'];?>"><button class="btn btn-dark btn--icon"><i class="zmdi zmdi-arrow-back"></i></button></a>
                        
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                              <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                       
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                        
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                         <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
  
         while ($row = $res->fetch_assoc()) {
             
       $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
         $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        $sig_file_ext=$row['sig_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
             else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>        
                        <div class="user__info" data-toggle="dropdown">
                             <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
            
                                
                                <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                        
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                       
                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->

                        <li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>


                         </ul>
                </div>
            </aside>

            <section class="content">
                <header class="content__title">
                    <p id="refno" hidden><?php echo $_GET['referenceno'];?></p>
                    <h1 id="dashtitle">Reference. No:  <?php   $referenceno=$_GET['referenceno'];  echo $referenceno; ?>  </h1>
                    <small id="dashtitle">Title of Memo:  <?php   $subject=$_GET['subject'];  echo $subject; ?></small>
                    
                </header>
                
                <div class="row">
                    <div class="col-lg-12">
                                <div class="card" id="cbmemcreation">
                                    <div id="whi" class="card-body">
                                       
                                    <?php 
                               $referenceno=$_GET['referenceno'];        
                                     
                                        
                                        require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    $ses=$_SESSION['karibu'];
                                
                   
                                /*FIRST QUERY TO SELECT AND FILL IN THE SENDER/REQUESTOR*/
      $queryone="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.requestor = ememo_users.user_id WHERE referenceno='$referenceno'";    
                                //execute query
if ($db->real_query($queryone)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $reqmemotype=$row["memotype"];
         $reqnature=$row["nature"];
         $reqfname=$row["fname"];
         $reqmname=$row["mname"];
         $reqlname=$row["lname"];
         $reqposition=$row["position"];
         $reqdepartment=$row["department"];
         $reqsector=$row["sector"];
         $reqdirectorate=$row["directorate"];
        $reqsection=$row["section"];
        $requnit=$row["unit"];
         $requser_id=$row["user_id"];
         $reqsig_file_ext=$row['sig_file_ext'];
         $requrgency=$row["urgency"];
        if($requrgency=='very high'){
            $urgency='Icon_red';
        }
        else if($requrgency=='high'){
              $urgency='Icon_orange';
        }
        else{
              $urgency='Icon_blue';
        }
        
        if($reqposition=='C.E.C.M'){
            $reqstation=$reqdepartment;
             $prefix="DEPARTMENT OF";
             $suffix=$reqstation;
        }
        else if($reqposition=='C.O'){
             $reqstation=$reqsector;
              $prefix=$reqstation;
             $suffix="SECTOR";
        }
         else if($reqposition=='DIR'){
             $reqstation=$reqdirectorate;
              $prefix= $reqstation;
             $suffix="DIRECTORATE";
        }
         else if($reqposition=='D.DIR'){
             $reqstation=$reqsection;
              $prefix= $reqstation;
             $suffix="SECTION";
        }
        else if($reqposition=='HOU'){
             $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
         else if($reqposition=='STAFF'){
            $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
        else{
             $reqstation='County Government of Nandi';
        }
   
    }
  
    echo "<p style=\"margin-left:800px;margin-top:-10px;\"><b></p>";
    
}
                                
                                    
                                /*SECOND QUERY TO SELECT AND FILL IN THE RECEIVER/RECIPIENT*/
                                         //check if it is broadcast
                if($reqmemotype=='broadcast'){
                    $recposition='';
                    $recstation=$reqnature;
                }
                //if not
                else{
      $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='direct'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recposition=$row["position"];
         $recdepartment=$row["department"];
        $recsector=$row["sector"];
        $recdirectorate=$row["directorate"];
         $recsection=$row["section"];
        $recunit=$row["unit"];
        $recnature=$row["nature"];
      
         if($recposition=='C.E.C.M'){
            $recstation=$recdepartment;
        }
        else if($recposition=='C.O'){
             $recstation=$recsector;
        }
         else if($recposition=='DIR'){
             $recstation=$recdirectorate;
        }
         else if($recposition=='D.DIR'){
             $recstation=$recsection;
        }
        else if($recposition=='HOU'){
             $recstation=$recunit;
        }
        else if($recposition=='STAFF'){
             $recstation=$recunit;
        }
        else{
             $recstation='County Government of Nandi';
        }
    }
}
                }
                                
 /***MAIN QUERY FOR THE MEMOBODY***/                               
$query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' LIMIT 1";                      
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $referenceno=$row["referenceno"];
		$memotype=$row["memotype"];
        $d=$row["datecreated"];
        $introduction=$row["introduction"];
        $genstat=$row["generalstatus"];
        $prevmemo=$row["prevmemo"];
        $subject=$row["subject"];
        $datecreated=date("jS M, Y", strtotime($d));
        /*stamping approval status*/
    if($genstat=="Approved"){
        $stamp="approved";
    }
        else if($genstat=="Rejected"){
            $stamp="rejected";
        }
        else{
            $stamp="null";
        }
        
        
    }
}
                                     
                                   
else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
                                               /***********************USER WATER MARK************************/  
                                
$querywm="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($querywm)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $positionwm=$row["position"];
         $stationdepartmentwm=$row["department"];
        $stationsectorwm=$row["sector"];
        $stationdirectoratewm=$row["directorate"];
        $stationsectionwm=$row["section"];
         $stationunitwm=$row["unit"];
         $fnamewm=$row['fname'];
         $mnamewm=$row['mname'];
         $lnamewm=$row['lname'];
        $dp_file_extwm=$row['dp_file_ext'];
        
        if($positionwm=='C.E.C.M'){
            $stationwm=$stationdepartmentwm;
        }
        else if($positionwm=='C.O'){
             $stationwm=$stationsectorwm;
        }
         else if($positionwm=='DIR'){
             $stationwm=$stationdirectoratewm;
        }
         else if($positionwm=='D.DIR'){
             $stationwm=$stationsectionwm;
        }
         else if($positionwm=='HOU'){
             $stationwm=$stationunitwm;
        }
        else if($positionwm=='STAFF'){
             $stationwm=$stationunitwm;
        }
        else{
             $stationwm='County Government of Nandi';
        }
       
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        echo "<div style=\" font-size:50px;margin-top:500px;text-transform:uppercase;margin-left:130px;position:absolute;font-family:'times';color:green;opacity:0.2;transform: rotate(-20deg);-ms-transform: rotate(-20deg);-webkit-transform: rotate(-20deg);\">$fnamewm $mnamewm $lnamewm</div>";   
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgency.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-size:18px;font-family:'times';padding:15;\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-family:'times';font-size:15px;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;padding-top:50px;font-family:'times';font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
               
										
         /**--------------------------------------IF MEMO IS A LOCAL TRAVEL REQUEST-------------------------------------**/
					if($memotype=='local'){
						
						
							//get Travel details from the travel table 	and inner join with ememo users to get applicants name and signature
				
$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.officer_id=ememo_users.user_id WHERE referenceno='$referenceno'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	$fname=$row['fname'];
	$mname=$row['mname'];
	$lname=$row['lname'];
	$officer_id=$row['officer_id'];
	$sig_file_ext=$row['sig_file_ext'];
    $personalno=$row['personal_number'];	
	$designation=$row['designation'];	
	$telephone=$row['telephone'];	
	$duration=$row['duration'];	
	$startdate=$row['start_date'];	
	$enddate=$row['end_date'];	
	$destination=$row['destination'];	
	$purpose=$row['purpose'];
	$activities=$row['activities'];	
	$delegatedofficer=$row['delegated_officer'];	
	$delegatedofficerdesign=$row['delegated_officer_design'];	
	$components=$row['components'];	
}}
				
				//get the delegated officer name and signature
$query="SELECT * FROM ememo_users WHERE user_id='$delegatedofficer'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
$fnamedelegated=$row['fname'];	
	$mnamedelegated=$row['mname'];	
	$lnamedelegated=$row['lname'];
	$sig_file_extdelegated=$row['sig_file_ext'];
}}
						
						//incase CECM doesnt exist
					$cecmremarks="";
						
			//get CECM's Remarks and Signature
	$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.E.C.M'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {

	$cecmremarks=$row['remarks'];	
    $cecmsig=$row['sig_file_ext'];
	$cecmuser_id=$row['user_id'];
	
}}
						//to ensure an empty record isnt encountered			
	if($cecmremarks==''){
		$cecmaction="";
		$cecmdate="";
		$cecmsig=".png";
		$cecmuser_id="white";
						
	}
						
						  //get CECM's Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.E.C.M'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	
	$cecmaction=$row['action'];
	$cecmdate=$row['cdate'];
	
}}
				
		    //get CS' Remarks and Signature
		$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.S'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	
	$csremarks=$row['remarks'];	
    $cssig=$row['sig_file_ext'];
	$csuser_id=$row['user_id'];
	
}}
	//to ensure an empty record isnt encountered			
	if($csremarks==''){
		$csaction="";
		$csdate="";
		$csuser_id="white";
		$cssig=".png";
	}					
						  //get CS' Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='C.S'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	
	$csaction=$row['action'];
	$csdate=$row['cdate'];
	
}}
						
						
						
						
		   //get DG's Remarks and Signature
$query="SELECT * FROM localtravelmemos inner join ememo_users on localtravelmemos.recipient=ememo_users.user_id WHERE referenceno='$referenceno' AND position='D.G'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {

	$dgremarks=$row['remarks'];	
    $dgsig=$row['sig_file_ext'];
	$dguser_id=$row['user_id'];
		
}}
			//to ensure an empty record isnt encountered			
	if($dgremarks==''){
		$dgaction="";
		$dgdate="";
		$dguser_id="white";
		$dgsig=".png";
	}
		  //get DG's Action and Date
	$query="SELECT * FROM logs inner join ememo_users on logs.actor_id=ememo_users.user_id WHERE referenceno='$referenceno' AND position='D.G'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	
	$dgaction=$row['action'];
	$dgdate=$row['cdate'];

}}
						
						
	echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;text-align:center;font-family:times;font-size:18px;margin-left:60px;\"><b>OFFICE OF THE DEPUTY GOVERNOR</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-weight:1000;text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
				
				
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">To be completed in Duplicate: Original to be retained by applicant and Duplicate by HRM</div>"; 
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\"><b>PART I</b></div>"; 
						
						
					 echo "<div class=\"table-respnsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">1. Name of Officer: <b style=\"font-weight:800;\"> $fname $mname $lname</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">2. Personal Number:<b style=\"font-weight:800;\"> $personalno</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">3. Designation:  <b style=\"font-weight:800;\">$designation</b>  Tel:  <b style=\"font-weight:800;\">$telephone</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">4. Duration:  <b style=\"font-weight:800;\">$duration</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">5. Start Date:  <b style=\"font-weight:800;\">$startdate</b>  End Date:  <b style=\"font-weight:800;\">$enddate</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">6. Country/City of Destination:  <b style=\"font-weight:800;\">$destination</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Purpose/Reason for request of travel:</div>";

					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">$purpose</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature of Applicant: <img height='60' width='60' src='img/signatures/$officer_id$sig_file_ext'/></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART II-HANDING OVER</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">7. The following assignments/duties/responsibilities will be handled by: </div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">ACTIVITIES/DUTIES AND RESPONSIBILITIES</div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\"><b style=\"font-weight:800;\">$activities</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Name of the Officer:  <b style=\"font-weight:800;\">$fnamedelegated $mnamedelegated $lnamedelegated</b> </div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Designation:  <b style=\"font-weight:800;\">$delegatedofficerdesign</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Component:  <b style=\"font-weight:800;\">$components</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature:  <img height='60' width='60' src='img/signatures/$delegatedofficer$sig_file_extdelegated'/></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">8. SUPERVISOR/HEAD OF DEPARTMENT (Where applicable)</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks: $cecmremarks </div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature: <img height='60' width='60' src='img/signatures/$cecmuser_id$cecmsig'/>  Date:$cecmdate</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">9. COUNTY SECRETARY</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Status: <b style=\"font-weight:800;\"> $csaction</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks (If any): <b style=\"font-weight:800;\">$csremarks </b></div>";
					
						echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature: <img height='60' width='60' src='img/signatures/$csuser_id$cssig'/>    Date:<b style=\"font-weight:800;\">$csdate</b></div>";
						
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">10. H.E. THE DEPUTY GOVERNOR</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Status: <b style=\"font-weight:800;\">$dgaction</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks (If any): <b style=\"font-weight:800;\">$dgremarks</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature: <img height='60' width='60' src='img/signatures/$dguser_id$dgsig'/>  Date:<b style=\"font-weight:800;\">$dgdate</b></div>";
								
									 echo "<p style=\"color:white;\">.</p>"; 
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
                
      echo "<p style=\"text-align:center;\"><i>#TRANSFORMING NANDI</i></p>";  
									
								}
								else{
										
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;font-family:'times';text-align:center;font-size:18px;margin-left:60px;\"><b>$prefix $suffix</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-family:'times';text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-20px;text-transform:uppercase;\">
     <table border=0 class=\"table\">
  
    <tr>
    <th width=\"470px;\"><b>TO: $recposition $recstation</b></th>
    <th style=\"width:470px;text-align:left;\"><b>$referenceno</b></th>
    </tr>
    </table>
    </div>";
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\"><b>FROM: $reqposition $reqstation</b></th>
    <th style=\"width:370px;text-align:left;\"><b>DATE: $datecreated</b></th>
    </tr>
    </table>
     </div>";
                                        
                                        /**determine whether thr will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Through' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hideth=""; 
                  
                }
            else{
                $hideth="hidden";   
            }
        }
                                        
                   /****FILL IN Through below from****/           
  $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Through'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionthr=$row["position"];
         $recdepartmentthr=$row["department"];
        $recsectorthr=$row["sector"];
        $recdirectoratethr=$row["directorate"];
        $recsectionthr=$row["section"];
        $recunitthr=$row["unit"];
        
        if($recpositionthr=='C.E.C.M'){
            $recstationthr=$recdepartmentthr;
        }
        else if($recpositionthr=='C.O'){
             $recstationthr=$recsectorthr;
        }
         else if($recpositionthr=='DIR'){
             $recstationthr=$recdirectoratethr;
        }
        else if($recpositionthr=='D.DIR'){
             $recstationthr=$recsectionthr;
        }
        else if($recpositionthr=='HOU'){
             $recstationthr=$recunitthr;
        }
         else if($recpositionthr=='STAFF'){
             $recstationthr=$recunitthr;
        }
        else{
             $recstationthr='County Government of Nandi';
        }
          echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\" $hideth><b>THROUGH: $recpositionthr $recstationthr</b></th>
    </tr>
    </table>
     </div>";
        
    }
 
}      
                                        
                                        
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
    <table border=0 class=\"table\">
    <tr>
    <td style=\"text-transform: uppercase;font-weight:500;font-family:Segoe UI;\"><b><u>RE: $subject</u></b></td>
    </tr>
    </table>
     </div>";
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">$introduction</div>";     
                                        
                     
                                              
                                        
                                        
     echo "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:55px;\">
    <table border=0 cellpadding=\"10\">
     <tr>
     <th><img  width=\"60\" height=\"60\" src=\"img/signatures/$requser_id$reqsig_file_ext\"/></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqfname $reqmname $reqlname</b></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqposition $reqstation</b></th>
     </tr>
     </table>
      </div>";
                                
            
                       
                                        /**determine whether cc will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Cc' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hidecc=""; 
                  
                }
            else{
                $hidecc="hidden";   
            }
        }
         
                                       
                
       echo  "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:75px;\">
     <table border=0 cellpadding=\"10\">
     <tr>
     <th $hidecc>Cc</th>
     </tr>
     
     </table>
      </div>";                         
                                        
         /****FILL IN CCs at the bottom****/           
   $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Cc'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $recpositioncc=$row["position"];
         $recdepartmentcc=$row["department"];
        $recsectorcc=$row["sector"];
        $recdirectoratecc=$row["directorate"];
        $recsectioncc=$row["section"];
        $recunitcc=$row["unit"];
        
        if($recpositioncc=='C.E.C.M'){
            $recstationcc=$recdepartmentcc;
        }
        else if($recpositioncc=='C.O'){
             $recstationcc=$recsectorcc;
        }
         else if($recpositioncc=='DIR'){
             $recstationcc=$recdirectoratecc;
        }
         else if($recpositioncc=='D.DIR'){
             $recstationcc=$recsectioncc;
        }
        else if($recpositioncc=='HOU'){
             $recstationcc=$recunitcc;
        }
        else if($recpositioncc=='STAFF'){
             $recstationcc=$recunitcc;
        }
        else{
             $recstationcc='County Government of Nandi';
        }
     
     
      
        echo "<p style=\"text-transform:uppercase;margin:-5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$recpositioncc .", ".$recstationcc."</p>"."<br>";
    }
}                    
                                
                             echo "<p></p>";     
                         echo "<p style=\"color:white;\">.</p>"; 
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
                
      echo "<p style=\"text-align:center;\"><i>#TRANSFORMING NANDI</i></p>";  
                                        
								}
                                
                                
                                
$db->close();

                                        
                                        ?>


                                

                            </div>
                        </div>
                    </div>
                </div>
                
                <!--previoius memo body -->
                            <div class="modal fade" id="modal-xl" tabindex="-1">
                                <div class="modal-dialog modal-xl">
                                    <div style="margin-left:0rem;background-color:white;color:black;" class="modal-content">
                                        <div class="modal-header">
                                            <h5 style="color:black;"class="modal-title pull-left"> <?php    echo "Reference. No: &nbsp;".$prevmemo; "&nbsp;&nbsp;&nbsp;&nbsp;Subject:&nbsp;";?></h5>
                                        </div>
                                        <div style="padding-top:5rem;" class="modal-body">
                                          <?php    
                                           require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
                                     if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    $ses=$_SESSION['karibu'];
                                
            if($prevmemo=="empty"){
         echo "Sorry, no previous memo was attached to this memo. You can check for other file attachments";
         }      
                 else{       
                                /*FIRST QUERY TO SELECT AND FILL IN THE SENDER/REQUESTOR*/
      $queryone="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.requestor = ememo_users.user_id WHERE referenceno='$prevmemo'";    
                                //execute query
if ($db->real_query($queryone)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        $reqmemotypemodal=$row["memotype"];
         $reqnaturemodal=$row["nature"];
         $reqfnamemodal=$row["fname"];
         $reqmnamemodal=$row["mname"];
         $reqlnamemodal=$row["lname"];
         $reqpositionmodal=$row["position"];
         $reqdepartmentmodal=$row["department"];
         $reqsectormodal=$row["sector"];
         $reqdirectoratemodal=$row["directorate"];
        $reqsectionmodal=$row["section"];
        $requnitmodal=$row["unit"];
         $requser_idmodal=$row["user_id"];
        $reqsig_file_extmodal=$row['sig_file_ext'];
         $requrgencymodal=$row["urgency"];
        if($requrgencymodal=='very high'){
            $urgencymodal='Icon_red';
        }
        else if($requrgencymodal=='high'){
              $urgencymodal='Icon_orange';
        }
        else{
              $urgencymodal='Icon_blue';
        }
        
         if($reqpositionmodal=='C.E.C.M'){
            $reqstationmodal=$reqdepartmentmodal;
              $prefixmodal="DEPARTMENT OF";
             $suffixmodal=$reqstationmodal;
        }
        else if($reqpositionmodal=='C.O'){
             $reqstationmodal=$reqsectormodal;
             $prefixmodal=$reqstationmodal;
             $suffixmodal="SECTOR";
            
        }
         else if($reqpositionmodal=='DIR'){
             $reqstationmodal=$reqdirectoratemodal;
             $prefixmodal=$reqstationmodal;
             $suffixmodal="DIRECTORATE";
        }
         else if($reqpositionmodal=='D.DIR'){
             $reqstationmodal=$reqsectionmodal;
              $prefixmodal= $reqstationmodal;
             $suffixmodal="SECTION";
        }
        else if($reqpositionmodal=='HOU'){
             $reqstationmodal=$requnitmodal;
             $prefixmodal=$reqstationmodal;
              $suffixmodal="UNIT";
        }
          else if($reqpositionmodal=='STAFF'){
             $reqstationmodal=$requnitmodal;
               $prefixmodal=$reqstationmodal;
              $suffixmodal="UNIT";
        }
        else{
             $reqstationmodal='County Government of Nandi';
        }
   
    }
  
    echo "<p style=\"margin-left:800px;margin-top:-10px;\"><b></p>";
    
}
                                
                                    
                                /*SECOND QUERY TO SELECT AND FILL IN THE RECEIVER/RECIPIENT*/
                                            //check if it is broadcast
                if($reqmemotypemodal=='broadcast'){
                    $recpositionmodal='';
                    $recstationmodal=$reqnaturemodal;
                }
                //if not
                else{
      $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$prevmemo' AND nature='direct'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionmodal=$row["position"];
         $recdepartmentmodal=$row["department"];
        $recsectormodal=$row["sector"];
        $recdirectoratemodal=$row["directorate"];
         $recsectionmodal=$row["section"];
        $recunitmodal=$row["unit"];
        $recnaturemodal=$row["nature"];
      
         if($recpositionmodal=='C.E.C.M'){
            $recstationmodal=$recdepartmentmodal;
        }
        else if($recpositionmodal=='C.O'){
             $recstationmodal=$recsectormodal;
        }
         else if($recpositionmodal=='DIR'){
             $recstationmodal=$recdirectoratemodal;
        }
         else if($recpositionmodal=='D.DIR'){
             $recstationmodal=$recsectionmodal;
        }
         else if($recpositionmodal=='HOU'){
             $recstationmodal=$recunitmodal;
        }
          else if($recpositionmodal=='STAFF'){
             $recstationmodal=$recunitmodal;
        }
        else{
             $recstationmodal='County Government of Nandi';
        }
    }
}
                }
                                
 /***MAIN QUERY FOR THE MEMOBODY***/                               
$query="SELECT * FROM nonfinancialmemos WHERE referenceno='$prevmemo' LIMIT 1";                      
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $referencenomodal=$row["referenceno"];
        $dmodal=$row["datecreated"];
        $introductionmodal=$row["introduction"];
        $genstatmodal=$row["generalstatus"];
        $prevmemomodal=$row["prevmemo"];
        $subjectmodal=$row["subject"];
        $datecreatedmodal=date("jS M, Y", strtotime($dmodal));
        /*stamping approval status*/
    if($genstatmodal=="Approved"){
        $stampmodal="approved";
    }
        else if($genstatmodal=="Rejected"){
            $stampmodal="rejected";
        }
        else{
            $stampmodal="null";
        }
        
        
    }
}
                                     
                                   
else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
          
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgencymodal.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-size:18px;font-family:'times';padding:15;\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-family:'times';font-size:15px;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;padding-top:50px;font-family:'times';font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
                             
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;font-family:'times';text-align:center;font-size:18px;margin-left:60px;\"><b>$prefixmodal $suffixmodal</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-family:'times';text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-20px;text-transform:uppercase;\">
     <table border=0 class=\"table\">
  
    <tr>
    <th width=\"470px;\"><b>TO: $recpositionmodal $recstationmodal</b></th>
    <th style=\"width:470px;text-align:left;\"><b>$referencenomodal</b></th>
    </tr>
    </table>
    </div>";
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\"><b>FROM: $reqpositionmodal $reqstationmodal</b></th>
    <th style=\"width:370px;text-align:left;\"><b>DATE: $datecreatedmodal</b></th>
    </tr>
    </table>
     </div>";
                           /**determine whether thr will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Through' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $prevmemo);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hidethmodal=""; 
                  
                }
            else{
                $hidethmodal="hidden";   
            }
        }
                            /****FILL IN Through below from****/           
  $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$prevmemo' AND nature='Through'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionthrmodal=$row["position"];
         $recdepartmentthrmodal=$row["department"];
        $recsectorthrmodal=$row["sector"];
        $recdirectoratethrmodal=$row["directorate"];
         $recsectionthrmodal=$row["section"];
         $recunitthrmodal=$row["unit"];
        
        if($recpositionthrmodal=='C.E.C.M'){
            $recstationthrmodal=$recdepartmentthrmodal;
        }
        else if($recpositionthrmodal=='C.O'){
             $recstationthrmodal=$recsectorthrmodal;
        }
         else if($recpositionthrmodal=='DIR'){
             $recstationthrmodal=$recdirectoratethrmodal;
        }
        else if($recpositionthrmodal=='D.DIR'){
             $recstationthrmodal=$recsectionthrmodal;
        }
        else if($recpositionthrmodal=='HOU'){
             $recstationthrmodal=$recunitthrmodal;
        }
        else if($recpositionthrmodal=='STAFF'){
             $recstationthrmodal=$recunitthrmodal;
        }
        else{
             $recstationthrmodal='County Government of Nandi';
        }
   
        echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\" $hideth><b>THROUGH: $recpositionthrmodal $recstationthrmodal</b></th>
    </tr>
    </table>
     </div>";
             
           
    }
 
}                         
                     
                     
                     
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
    <table border=0 class=\"table\">
    <tr>
    <td style=\"text-transform: uppercase;font-weight:500;font-family:Segoe UI;\"><b><u>RE: $subjectmodal</u></b></td>
    </tr>
    </table>
     </div>";
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">$introductionmodal</div>";     
                                        
                     
                                               
                                        
     echo "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:55px;\">
    <table border=0 cellpadding=\"10\">
     <tr>
     <th><img  width=\"60\" height=\"60\" src=\"img/signatures/$requser_idmodal$reqsig_file_extmodal\"/></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqfnamemodal $reqmnamemodal $reqlnamemodal</b></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqpositionmodal $reqstationmodal</b></th>
     </tr>
     </table>
      </div>";
                                
                  
                       
                                        /**determine whether cc will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Cc' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $prevmemo);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hideccmodal=""; 
                  
                }
            else{
                $hideccmodal="hidden";   
            }
        }
         
                                       
                
       echo  "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:75px;\">
     <table border=0 cellpadding=\"10\">
     <tr>
     <th $hideccmodal>Cc</th>
     </tr>
     
     </table>
      </div>";                         
                                        
         /****FILL IN CCs at the bottom****/           
   $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$prevmemo' AND nature='Cc'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $recpositionccmodal=$row["position"];
         $recdepartmentccmodal=$row["department"];
        $recsectorccmodal=$row["sector"];
        $recdirectorateccmodal=$row["directorate"];
         $recsectionccmodal=$row["section"];
         $recunitccmodal=$row["unit"];
        
        if($recpositionccmodal=='C.E.C.M'){
            $recstationccmodal=$recdepartmentccmodal;
        }
        else if($recpositionccmodal=='C.O'){
             $recstationccmodal=$recsectorccmodal;
        }
         else if($recpositionccmodal=='DIR'){
             $recstationccmodal=$recdirectorateccmodal;
        }
        else if($recpositionccmodal=='D.DIR'){
             $recstationccmodal=$recsectionccmodal;
        }
        else if($recpositionccmodal=='HOU'){
             $recstationccmodal=$recunitccmodal;
        }
        else if($recpositionccmodal=='STAFF'){
             $recstationccmodal=$recunitccmodal;
        }
        else{
             $recstationccmodal='County Government of Nandi';
        }
     
     
      
        echo "<p style=\"text-transform:uppercase;margin:-5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$recpositionccmodal .", ".$recstationccmodal."</p>"."<br>";
    }
}                    
                                
                                
                 
                            echo "<p></p>";     
                         echo "<p style=\"color:white;\">.</p>"; 
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
                
      echo "<p style=\"text-align:center;\"><i>#TRANSFORMING NANDI</i></p>";            }     
                                
$db->close();



                                            ?>
                                        </div>
                                     
                                        <div class="modal-footer">
                                           
                                            <button style="color:black;" type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<!--ATTACHED DOCUMENTS-->
<input type="text" name="refr_no" id="refr_no" value="<?php echo $_GET['referenceno']; ?>" hidden>

 <div class="row" id="hide1">
                    <div class="col-lg-12">
                        
                        <div class="card">
                            <div class="card-body" id="cbmemcreation">
                                <p>Previous memos and Documents attached to this memo</p></br>
                                <div class="card">       
                                    <div class="card-body"  id="cbmemcreation">
                                        <div class="btn-demo">
      <button class="btn btn-light" data-toggle="modal" data-target="#modal-xl">view previous memo attachment</button>
                </div>
                                        <div id="load_documents">
                    
                    
                    
                    
                    
                    
                             
                             
                    
                    </div>    
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
                
                
                <form action="nonfinancialmemoviewsent.php?referenceno=<?php echo $_GET['referenceno'];?>&&subject=<?php echo $_GET['subject'];?>&&requestor=<?php echo $_GET['requestor'];?>" method="post">
                <div class="row" id="hide2">
                    <div class="col-lg-12">
                        <div class="card">
                               
                                    <div class="card-body" id="cbmemcreation">
                                          
                                         <textarea name="remarks" id="comments" class="ckeditor">         <?php echo "<p>&nbsp</p>";  echo "Commented by " .$position.'.&nbsp;'.$station;    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
                                            
                                             echo "<img  width=\"50\" height=\"50\" src=\"img/signatures/$ses$sig_file_ext\"/>";?><hr/> </textarea>
                  <script>
                           CKEDITOR.replace( 'comments' );
                          </script>

                                       
                                </div>

                            </div>
                            
                        
                    </div>
                    
        </div>
 
                        <div id="exp" class="card">
            <div class="card-body" id="cbmemcreation">
                 <div class="row">
                           
                   
                    <div class="col-sm-6 col-md-12">
                         
                        <button type="submit" class="btn btn-info" name="append"><i class="zmdi zmdi-comment"></i>&nbsp;Comment on the memo</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </form></br><br/>
                     <form target="_blank" method="post">
                         <button type="submit" class="btn btn-info" name="pdf"><i class="zmdi zmdi-file"></i>&nbsp;Generate PDF</button>
                     </form>
                     </div>
                     


                </div>

            </div>
        </div>
                    <button type="submit" class="btn btn-danger" name="report" onclick="viewreport()" id="btnto"><i class="zmdi zmdi-eye"></i>&nbsp;View Report</button><p></p>
<button type="submit" class="btn btn-danger" name="report" onclick="goback()" id="btnfro"><i class="zmdi zmdi-arrow-back"></i>&nbsp;Back</button><p></p>
                   <div class="row" id="hide3">
                    <div class="col-lg-12">
                        
                        <div class="card">
                            <div class="card-body" id="cbmemcreation">
                                <p>Comments made by participants regarding this memo. You can add yours above</p></br>
                                <div class="card" id="whi">       
                                    <div class="card-body">
                                        <div id="displaycomments">
                    
                    
                    
                    
                    
                    
                             
                             
                    
                    </div>    
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

 <div class="row" id="report">
                    <div class="col-lg-6">
                        
                        <div class="card">
                            <div class="card-body" id="cbmemcreation">
                                <p>Actions taken by participants on this memo.</p></br>
                                <div class="card" id="whi">       
                                    <div class="card-body">
                                        <div id="displayactions">
                    
                    
                    
                    
                    
                    
                             
                             
                    
                    </div>    
                </div>
            </div>
        </div>
    </div>
  </div>
<div class="col-lg-6">
                        
                        <div class="card">
                            <div class="card-body" id="cbmemcreation">
                                <p>Comments made by participants regarding this memo.</p></br>
                                <div class="card" id="whi">       
                                    <div class="card-body">
                                        <div id="displaycommentsecond">
                    
                    
                    
                    
                    
                    
                             
                             
                    
                    </div>    
                </div>
            </div>
        </div>
    </div>
  </div>
  

</div>
<p id="test" hidden>test</p>
                   
                 <?php
                       
//include_once 'config.php';
require("./_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
                
                
/*-------------------------------------WHEN COMMENT IS CLICKED------------------------------------------------------*/
if(isset($_POST['append'])){
        //ensure user has signature before anything else
    if($sig_file_ext==''){
    //tell them to upload    
        ?>
<script>alert("Upload  a signature before commenting");</script>
<?php
    }
    else{
include_once 'activity_processing/utility_sent.php';
//create an instance of DB_con class, an object
$con= new DB_connect();
$referenceno=$_GET["referenceno"];
    $remarks=$_POST['remarks'];

    //Append remarks on the comment field  
$sql10="UPDATE nonfinancialmemos SET comments =CONCAT(comments,'$remarks') WHERE referenceno='$referenceno'";
     if($stmnt=$mysqli->prepare($sql10)){
        $stmnt->execute(); 
} 
    /**SAVE LOGS ACTIVITY**/
     $fulltitle=$position.",".$station;
    
    /*get the memotype*/
     $query1="SELECT * FROM nonfinancialmemos  WHERE referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
       $memotype=$row["memotype"];   
     }}
    
    if($con->setCommentLogs($mysqli,$fulltitle,$memotype,$referenceno,$subject,$ses)){
        }
}
}

    ?>               
        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
            </section>
        </main>

    <script>
          //fetching comments
 $(function() {       
  var ref = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialmemoviewreceivedfetch.php",
    data: {
      ref:ref
    },
    cache: false
  }).done(function(data){
    $("#displaycomments").html(data);
  });
 },1000);

});
        
        //fetching comments for secondpage
 $(function() {       
  var refsecond = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialmemoviewreceivedfetch.php",
    data: {
      refsecond:refsecond
    },
    cache: false
  }).done(function(data){
    $("#displaycommentsecond").html(data);
  });
 },1000);

});
     //fetching actions for secondpage
 $(function() {       
  var reftracking = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialmemoviewreceivedfetch.php",
    data: {
      reftracking:reftracking
    },
    cache: false
  }).done(function(data){
    $("#displayactions").html(data);
  });
 },1000);

});
    
    //view report
    function viewreport(){
        $('#report').show();
        $('#btnfro').show();
        $('#btnto').hide();
       $('#hide1').hide();
        $('#hide2').hide();
        $('#hide3').hide();
        $('#exp').hide();
    }
    //back
     function goback(){
        $('#report').hide();
         $('#btnfro').hide();
         $('#btnto').show();
       $('#hide1').show();
        $('#hide2').show();
        $('#hide3').show();
        $('#exp').show();
    }
    
    
    
</script>

        <!-- Javascript -->
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<script type="text/javascript">
             var ref_no=document.getElementById('refr_no').value;

             console.log("ref="+ref_no);
            var url="./controller/apis/documents/documents.php?ref_no="+ref_no;
            $.ajax({
                type:"POST",
                url:url,
                dataType:"text",

                success:function(response){
                    
                    $('#load_documents').html(response);
                }
            });
    
        </script>
<script language="JavaScript">
  /**
    * Disable right-click of mouse, F12 key, and save key combinations on page
    * By Arthur Gareginyan (arthurgareginyan@gmail.com)
    * For full source code, visit http://www.mycyberuniverse.com
    */
  window.onload = function() {
    document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
    //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  };
</script>

<script>
function copyToClipboard(elementId) {

  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value", document.getElementById(elementId).innerHTML);

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);

}
$(document).ready(function(){
    $(window).keyup(function(e){
      if(e.keyCode == 44){
        copyToClipboard('test');
      };
    });
});


</script>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>


<!--------LOG USER OUT AFTER 5 MINUTES OF INACTIVITY----------->
<script>


</script>




        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

   <script type="text/javascript">
            
            function open_document(link){

                window.open(link);
            }
        </script>        

    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>