<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
  <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="push.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    	<link rel="stylesheet" href="trackstyling/style.css">
    <script src="js/jquery-3.0.0.js"></script>
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>


            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>
                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>
                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                                      <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
       
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>      
                        <div class="user__info" data-toggle="dropdown">
                             <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                       
                                
                                <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                       
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                        
                        
                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->

                        <li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>


                         </ul>
                </div>
            </aside>
            <section  class="content">
                <header class="content__title">
                     <p id="refno" hidden><?php echo $_GET['referenceno'];?></p>
                    <h1 id="dashtitle">Reference. No:   <?php $referenceno=$_GET['referenceno']; echo $referenceno; ?></h1>
                    <small></small>
                </header>

                          <div class="row quick-stats">
                    <div class="col-sm-6 col-md-12">
                        <div id="qstatspia" class="quick-stats__item">
                            
                               <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body" id="reener">
                                <h6>Subject: <?php $subject=$_GET['subject']; echo strtoupper($subject); ?></h6><br/>
                                <h6> <?php                      
                                     require("./_connect.php");

                                //connect to db
                                    $db = new mysqli($db_host,$db_user, $db_password, $db_name); 
                                      $requestor=$_GET['requestor'];
                                    $query="SELECT * FROM ememo_users where user_id='$requestor'";
                                    //execute query
                                    if ($db->real_query($query)) {
                                        //If the query was successful
                                        $res = $db->use_result();

                                        while ($row = $res->fetch_assoc()) {
                                            $requser_id=$row['user_id'];
                                             $reqposition=$row["position"];
                                             $reqdepartment=$row["department"];
                                            $reqsector=$row["sector"];
                                            $reqdirectorate=$row["directorate"];
                                             $reqfname=$row['fname'];
                                             $reqmname=$row['mname'];
                                             $reqlname=$row['lname'];
                                             $reqdp_file_ext=$row['dp_file_ext'];

                                            if($reqposition=='C.E.C.M'){
                                                $reqstation=$reqdepartment;
                                            }
                                            else if($reqposition=='C.O'){
                                                 $reqstation=$reqsector;
                                            }
                                             else if($reqposition=='DIR'){
                                                 $reqstation=$reqdirectorate;
                                            }
                                             else if($reqposition=='D.DIR'){
                                                 $reqstation=$reqdirectorate;
                                            }
                                            else{
                                                 $reqstation='County Government of Nandi';
                                            }

      
              
                                            }}
                                      echo "<img class=\"user__img\" src=\"img/profilepics/$requser_id$reqdp_file_ext\" alt=\"\">";
                                     echo $reqposition.', '; echo $reqstation; 
                                    
                                    ?></h6>
                            </div>
                        </div>
                    </div>
                               <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body" id="reener">
                                <?php                      
                                     require("./_connect.php");

                                 //connect to db
                                    $db = new mysqli($db_host,$db_user, $db_password, $db_name); 
                                      $requestor=$_GET['requestor'];
                                    $query="SELECT * FROM nonfinancialmemos where referenceno='$referenceno' LIMIT 1";
                                    //execute query
                                    if ($db->real_query($query)) {
                                        //If the query was successful
                                        $res = $db->use_result();

                                        while ($row = $res->fetch_assoc()) {
                                            $memotype=$row["memotype"];
                                             $financial_year=$row["financial_year"];
                                            $votehead=$row["votehead"];
                                            $amount=$row["amount"];
                                             $approvedamount=$row["approvedamount"];
                                            
                                            
                                            }}
                                if($votehead=="none"){
                                $voteheadtitle="Unassigned";
                                }
                                else{
                                 $query="SELECT * FROM vote_heads where Item_source='$votehead'";
                                    //execute query
                                    if ($db->real_query($query)) {
                                        //If the query was successful
                                            $res = $db->use_result();
                                            while ($row = $res->fetch_assoc()) {
                                            $voteheadtitle=$row["Title_and_details"];
                                            }}
                                }
                                    ?>
                                
                              <h6 id="fichaview1">Memo Type:<?php echo "&nbsp;".strtoupper($memotype)?></h6>
                                 <h6 id="fichaview2">Financial Year:<?php echo "&nbsp;".$financial_year?></h6>
                                <h6 id="fichaviewextra">Vote Head:<?php echo "&nbsp;".$voteheadtitle ?></h6><br/>
                                 <button id="fichabutton1" type="submit" class="btn btn-info" name="submit"><i class="zmdi zmdi-money"></i>&nbsp;Total Funds Requested:<br/><br/><h style="font-size:17px;"><?php echo "Kshs:".number_format($amount);?></h4></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <button id="fichabutton2" type="submit" class="btn btn-success" name="submit"><i class="zmdi zmdi-money"></i>&nbsp;Total Funds Approved:<br/><br/><h style="font-size:17px;"><?php echo "Kshs:".number_format($approvedamount);?></h></button>
                            </div>
                        </div>
                    </div>
                           
                        </div>
                    </div>

                </div>
                <form action="nonfinancialmemoviewsent.php?referenceno=<?php echo $_GET['referenceno'];?>&&subject=<?php echo $_GET['subject'];?>&&requestor=<?php echo $_GET['requestor'];?>" method="post">
                            <button id="fichaview" type="submit" class="btn btn-info" name="submit"><i class="zmdi zmdi-eye"></i>&nbsp;View Memo Commentary</button></br><br/>
                    </form>

              <div class="row quick-stats">
                  <div class="col-sm-6 col-md-8">
                        <div id="qstatspia" class="quick-stats__item">
                            <div id="qstats" class="quick-stats__info">
                                <h2 id="dash">All Memo Participants</h2></br></br>
                         <div id="displayparticipants">
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      </div>
          
          </div>      
                </div>
                    </div>
  <div class="col-sm-6 col-md-4">
                        <div id="qstatspia" class="quick-stats__item">
                            <div id="qstats" class="quick-stats__info">
                                <div id="displayprogress"></div>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                    </div>

                        </div>
      
      <form action="nonfinancialdashsent.php?referenceno=<?php echo $_GET['referenceno'];?>&&subject=<?php echo $_GET['subject'];?>&&requestor=<?php echo $_GET['requestor'];?>" method="post">
          <!--button id="fichaterminate" type="submit" class="btn btn-outline-danger" name="terminate"><i class="zmdi zmdi-close"></i>&nbsp;Terminate the Memo</button></br><br/-->
          <!--?php 
          if(isset($_POST['terminate'])){
               $mysqli = new mysqli("localhost","root", "", "ememo"); 
              
           $sql="UPDATE nonfinancialmemos SET availability='terminated' WHERE referenceno='$referenceno'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                
                echo "This memo has been successfully terminated";
        } 
          }
          ?-->
          
      </form>
                    </div>
                  
                </div>
           <div class="row quick-stats">
                    <div class="col-sm-6 col-md-12">
                        
                    </div>
 <div class="col-sm-6 col-md-12">
                        <div id="qstatspia" class="quick-stats__item">
                            <div id="qstats" class="quick-stats__info">
                                <h2 id="dash">Memo Tracking</h2></br>
                            <h2 style="font-size:14px;color:#505458;" id="basictext">Keep track of events and actions taking place on the memo towards approval. </h2></br></br></br>
                                <div id="displaytracking">
                            
                            
                            
                            
                            
                                    
                                    
                                    
                            
                            
                            </div>
                           </div>

                        </div>
                    </div>

                </div>
                </div>

               
        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
                 </section>
        </main>

           <script>
          //fetching tracking
 $(function() {       
  var reftracking = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialdashreceivedfetch.php",
    data: {
      reftracking:reftracking
    },
    cache: false
  }).done(function(data){
    $("#displaytracking").html(data);
  });
 },1000);

});
          //fetching participants
          $(function() {       
  var refparticipants = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialdashreceivedfetch.php",
    data: {
      refparticipants:refparticipants
    },
    cache: false
  }).done(function(data){
    $("#displayparticipants").html(data);
  });
 },1000);

});
            //fetching progress
          $(function() {       
  var refprogress = $("#refno").html();

  setTimeout(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialdashreceivedfetch.php",
    data: {
      refprogress:refprogress
    },
    cache: false
  }).done(function(data){
    $("#displayprogress").html(data);
  });
 },1000);

});
</script>

<script>
    
var memotype = "<?php 
  echo $memotype ?>";
var pos = "<?php 
  echo $position ?>";
  
  if(memotype!=='financial'){
     
      $('#fichaview2').hide();
      $('#fichabutton1').hide();
       $('#fichabutton2').hide();
      $('#fichaviewextra').hide();
     
  }
    else if(memotype=='financial'){
       if((pos=='DIR')&&(directorate=='Budget and Planning')){
          $('#fichaviewextra').show();
      }
        else if((pos=='DIR')&&(directorate=='Finance')){
             $('#fichaviewextra').show();
        }
        else if(pos=='DIR'){
             $('#fichaviewextra').hide();
        }
        else if(pos=='D.DIR'){
           $('#fichaviewextra').hide();
        }
         else if(pos=='HOU'){
           $('#fichaviewextra').hide();
        }
       else if(pos=='STAFF'){
           $('#fichaviewextra').hide();
        }
    }
</script>


<!--------LOG USER OUT AFTER 5 MINUTES OF INACTIVITY----------->
<script>

</script>



        <!-- Javascript -->
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="js/rChat.js"></script>
        <script src="js/rChatSent.js"></script>
        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>