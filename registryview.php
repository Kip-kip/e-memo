<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E-Memo</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/dropzone/dist/dropzone.css">
    <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
       <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    
  <script src="ckeditor/adapters/jquery.js"></script>
     <script src="ckeditor/ckeditor.js"></script>
    
    <!-- App styles -->
    <link rel="stylesheet" href="css/app.min.css">
</head>

<body data-sa-theme="1">
<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>

            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                    <h1><a href="index.html">E-Memo</a></h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="to-nav__notify"><i id="" class="label label-pill label-danger count"></i></br><i id="mee" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="messages.html" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownlow" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <a href="#" class="view-more">View all notifications</a>
                            </div>
                        </div>
                    </li>

                    <li class="dropdown message">
                        <a href="#" data-toggle="dropdownmessage" class="to-nav__notify"><i id="" class="label label-pill label-danger count"></i></br><i id="mee" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="messages.html" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownlow" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <a href="#" class="view-more">View all messages</a>
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        <?php       
                          require("./_connect.php");
                           //connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
      
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>  
                        <div class="user__info" data-toggle="dropdown">
                           
                            <?php
                            
                            echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                      
                                
                             <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>
                       
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                      
                        
                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->
<li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>
                        


                         </ul>
                </div>
            </aside>

    <section class="content">
        <header class="content__title">
         <h1>Reference. No:  <?php   $referenceno=$_GET['referenceno'];  echo $referenceno; ?>  </h1>
                    <small>Title of Memo: <?php   $subject=$_GET['subject'];  echo $subject; ?></small>
        </header>
        
        <div class="card">
            <div id="whi" class="card-body">
                
                <!--DISPLAYING THE MEMO-->
                 <?php 
                               $referenceno=$_GET['referenceno'];        
                                     
                                        
                                        require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    $ses=$_SESSION['karibu'];
                                
                   
                                /*FIRST QUERY TO SELECT AND FILL IN THE SENDER/REQUESTOR*/
      $queryone="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.requestor = ememo_users.user_id WHERE referenceno='$referenceno'";    
                                //execute query
if ($db->real_query($queryone)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $reqfname=$row["fname"];
         $reqmname=$row["mname"];
         $reqlname=$row["lname"];
         $reqposition=$row["position"];
         $reqdepartment=$row["department"];
         $reqsector=$row["sector"];
         $reqdirectorate=$row["directorate"];
        $reqsection=$row["section"];
        $requnit=$row["unit"];
         $requser_id=$row["user_id"];
         $reqsig_file_ext=$row['sig_file_ext'];
         $requrgency=$row["urgency"];
        if($requrgency=='very high'){
            $urgency='Icon_red';
        }
        else if($requrgency=='high'){
              $urgency='Icon_orange';
        }
        else{
              $urgency='Icon_blue';
        }
        
        if($reqposition=='C.E.C.M'){
            $reqstation=$reqdepartment;
             $prefix="DEPARTMENT OF";
             $suffix=$reqstation;
        }
        else if($reqposition=='C.O'){
             $reqstation=$reqsector;
              $prefix="SECTOR OF";
             $suffix=$reqstation;
        }
         else if($reqposition=='DIR'){
             $reqstation=$reqdirectorate;
              $prefix="DIRECTORATE OF";
             $suffix=$reqstation;
        }
         else if($reqposition=='D.DIR'){
             $reqstation=$reqsection;
              $prefix= $reqstation;
             $suffix="SECTION";
        }
        else if($reqposition=='HOU'){
             $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
         else if($reqposition=='STAFF'){
            $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
        else{
             $reqstation='County Government of Nandi';
        }
   
    }
  
    echo "<p style=\"margin-left:800px;margin-top:-10px;\"><b></p>";
    
}
                                
                                    
                                /*SECOND QUERY TO SELECT AND FILL IN THE RECEIVER/RECIPIENT*/
      $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='direct'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recposition=$row["position"];
         $recdepartment=$row["department"];
        $recsector=$row["sector"];
        $recdirectorate=$row["directorate"];
        $recsection=$row["section"];
        $recunit=$row["unit"];
        $recnature=$row["nature"];
      
         if($recposition=='C.E.C.M'){
            $recstation=$recdepartment;
              $reqprefix="DEPARTMENT OF";
        }
        else if($recposition=='C.O'){
             $recstation=$recsector;
            $reqprefix="SECTOR OF";
        }
         else if($recposition=='DIR'){
             $recstation=$recdirectorate;
             $reqprefix="DIRECTORATE OF";
        }
         else if($recposition=='D.DIR'){
             $recstation=$recsection;
        }
        else if($recposition=='HOU'){
             $recstation=$recunit;
        }
        else if($recposition=='STAFF'){
             $recstation=$recunit;
        }
        else{
             $recstation='County Government of Nandi';
        }
    }
}
                                   
                                
 /***MAIN QUERY FOR THE MEMOBODY***/                               
$query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' LIMIT 1";                      
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $referenceno=$row["referenceno"];
        $d=$row["datecreated"];
        $introduction=$row["introduction"];
        $genstat=$row["generalstatus"];
        $prevmemo=$row["prevmemo"];
        $subject=$row["subject"];
         $datecreated=date("jS M, Y", strtotime($d));
        /*stamping approval status*/
    if($genstat=="Approved"){
        $stamp="approved";
    }
        else if($genstat=="Rejected"){
            $stamp="rejected";
        }
        else{
            $stamp="null";
        }
        
        
    }
}
                                     
                                   
else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgency.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-size:18px;font-family:'times';padding:15;\"><b>THE COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-family:'times';font-size:15px;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;padding-top:50px;font-family:'times';font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
                             
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;font-family:'times';text-align:center;font-size:18px;margin-left:60px;\"><b>$prefix $suffix</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-family:'times';text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-20px;text-transform:uppercase;\">
     <table border=0 class=\"table\">
  
    <tr>
    <th width=\"470px;\"><b>TO: $recposition $recstation</b></th>
    <th style=\"width:470px;text-align:left;\"><b>$referenceno</b></th>
    </tr>
    </table>
    </div>";
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\"><b>FROM: $reqposition $reqstation</b></th>
    <th style=\"width:370px;text-align:left;\"><b>DATE: $datecreated</b></th>
    </tr>
    </table>
     </div>";
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
    <table border=0 class=\"table\">
    <tr>
    <td style=\"text-transform: uppercase;\"><b><u>RE: $subject</u></b></td>
    </tr>
    </table>
     </div>";
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">$introduction</div>";     
                                        
                     
                                              /**determine whether thr will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Through' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hideth=""; 
                  
                }
            else{
                $hideth="hidden";   
            }
        }
                                        
                                        
     echo "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:55px;\">
    <table border=0 cellpadding=\"10\">
     <tr>
     <th><img  width=\"60\" height=\"60\" src=\"img/signatures/$requser_id$reqsig_file_ext\"/></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqfname $reqmname $reqlname</b></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqposition $reqstation</b></th>
     </tr>
     <tr>
     <th $hideth>Through</th>
     </tr>
     </table>
      </div>";
                                
                    /****FILL IN Through below from****/           
  $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Through'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionthr=$row["position"];
         $recdepartmentthr=$row["department"];
        $recsectorthr=$row["sector"];
        $recdirectoratethr=$row["directorate"];
         $recsectionthr=$row["section"];
        $recunitthr=$row["unit"];
        
        if($recpositionthr=='C.E.C.M'){
            $recstationthr=$recdepartmentthr;
        }
        else if($recpositionthr=='C.O'){
             $recstationthr=$recsectorthr;
        }
         else if($recpositionthr=='DIR'){
             $recstationthr=$recdirectoratethr;
        }
         else if($recpositionthr=='D.DIR'){
             $recstationthr=$recsectionthr;
        }
        else if($recpositionthr=='HOU'){
             $recstationthr=$recunitthr;
        }
        else if($recpositionthr=='STAFF'){
             $recstationthr=$recunitthr;
        }
        else{
             $recstationthr='County Government of Nandi';
        }
   
             
             echo "<p style=\"text-transform:uppercase;margin:-5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$recpositionthr .", ".$recstationthr."</p>"."<br>";
    }
 
}               
                       
                                        /**determine whether cc will be displayed**/
            $query ="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE nature='Cc' AND referenceno=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $referenceno);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hidecc=""; 
                  
                }
            else{
                $hidecc="hidden";   
            }
        }
         
                                       
                
       echo  "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:75px;\">
     <table border=0 cellpadding=\"10\">
     <tr>
     <th $hidecc>Cc</th>
     </tr>
     
     </table>
      </div>";                         
                                        
         /****FILL IN CCs at the bottom****/           
   $querytwo="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient = ememo_users.user_id WHERE referenceno='$referenceno' AND nature='Cc'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $recpositioncc=$row["position"];
         $recdepartmentcc=$row["department"];
        $recsectorcc=$row["sector"];
        $recdirectoratecc=$row["directorate"];
        $recsectioncc=$row["section"];
        $recunitcc=$row["unit"];
        
        if($recpositioncc=='C.E.C.M'){
            $recstationcc=$recdepartmentcc;
        }
        else if($recpositioncc=='C.O'){
             $recstationcc=$recsectorcc;
        }
         else if($recpositioncc=='DIR'){
             $recstationcc=$recdirectoratecc;
        }
         else if($recpositioncc=='D.DIR'){
             $recstationcc=$recsectioncc;
        }
        else if($recpositioncc=='HOU'){
             $recstationcc=$recunitcc;
        }
        else if($recpositioncc=='STAFF'){
             $recstationcc=$recunitcc;
        }
        else{
             $recstationcc='County Government of Nandi';
        }
     
     
      
        echo "<p style=\"text-transform:uppercase;margin:-5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$recpositioncc .", ".$recstationcc."</p>"."<br>";
    }
}                    
                                
                                
                                
                                
                                
$db->close();

                                        
                                        ?>

                       
                        </div>
                      </div>
 

                <br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        
        
        

      
        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
    </section>
</main>

<!-- Javascript -->
<!-- Vendors -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>
<script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
    
        <script src="vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>

<!-- Charts and maps-->
<script src="demo/js/flot-charts/curved-line.js"></script>
<script src="demo/js/flot-charts/line.js"></script>
<script src="demo/js/flot-charts/dynamic.js"></script>
<script src="demo/js/flot-charts/chart-tooltips.js"></script>
<script src="demo/js/other-charts.js"></script>
<script src="demo/js/jqvmap.js"></script>

<!-- App functions and actions -->
<script src="js/app.min.js"></script>
<script src="js/event-controller.js"></script>

    <!-- Demo -->

                              
        <script type="text/javascript">
        
            
            /*lost*/
        
            

            /*--------------------------------------
                Bootstrap Notify Notifications
            ---------------------------------------*/
            function notify(from, align, icon, type, animIn, animOut){
                $.notify({
                    icon: icon,
                    title: 'Bootstrap Notify',
                    message: 'Turning standard Bootstrap alerts into awesome notifications',
                    url: ''
                },{
                    element: 'body',
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: from,
                        align: align
                    },
                    offset: {
                        x: 20,
                        y: 20
                    },
                    spacing: 10,
                    z_index: 1031,
                    delay: 2500,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: false,
                    animate: {
                        enter: animIn,
                        exit: animOut
                    },
                    template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                                    '<span data-notify="icon"></span> ' +
                                    '<span data-notify="title">{1}</span> ' +
                                    '<span data-notify="message">{2}</span>' +
                                    '<div class="progress" data-notify="progressbar">' +
                                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                    '</div>' +
                                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                    '<button type="button" aria-hidden="true" data-notify="dismiss" class="close"><span>×</span></button>' +
                                '</div>'
                });
            }

            $('.notifications-demo > .btn').click(function(e){
                e.preventDefault();
                var nFrom = $(this).attr('data-from');
                var nAlign = $(this).attr('data-align');
                var nIcons = $(this).attr('data-icon');
                var nType = $(this).attr('data-type');
                var nAnimIn = $(this).attr('data-animation-in');
                var nAnimOut = $(this).attr('data-animation-out');

                notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
            });


            /*--------------------------------------
                Sweet Alert Dialogs
            ---------------------------------------*/

            // Basic
            $('#sa-basic').click(function(){
                swal({
                    title: "Here's a message!",
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Success Message
            $('#sa-success').click(function(){
                swal({
                    title: 'Good job!',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'success',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Success Message
            $('#sa-info').click(function(){
                swal({
                    title: 'Information!',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'info',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Warning Message
            $('#sa-warning').click(function(){
                swal({
                    title: 'Not a good sign...',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Question Message
            $('#sa-question').click(function(){
                swal({
                    title: 'Hmm.. what did you say?',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'question',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Warning Message with function
            $('#sa-funtion').click(function(){
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover this imaginary file!',
                    type: 'warning',
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonClass: 'btn btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                }).then(function(){
                    swal({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover this imaginary file!',
                        type: 'success',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-light',
                        background: 'rgba(0, 0, 0, 0.96)'
                    });
                });
            });

            // Custom Image
            $('#sa-image').click(function(){
                swal({
                    title: 'Sweet!',
                    text: "Here's a custom image.",
                    imageUrl: 'demo/img/thumbs-up.png',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-light',
                    confirmButtonText: 'Super!',
                    background: 'rgba(0, 0, 0, 0.96)'
                });
            });

            // Auto Close Timer
            $('#sa-timer').click(function(){
                swal({
                    title: 'Auto close alert!',
                    text: 'I will close in 2 seconds.',
                    timer: 2000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                });
            });
        </script>

</body>
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>