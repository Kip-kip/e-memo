<?php
session_start();
if(!isset($_SESSION['karibu'])){
    header("Location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">    
 <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
 
        <!-- App styles --> 
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="push.js"></script>
        <link rel="stylesheet" href="css/app.min.css">
     <script src="js/jquery-3.0.0.js"></script>
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>



            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                  <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="to-nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="to-nav__notify"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        
                               <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    

//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
       $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
 
              
$db->close();                  
                 ?>  
                        
                        <div class="user__info" data-toggle="dropdown">
                            <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                      
                                <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                        
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <!--ul-->
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php"><i id="me" class="zmdi zmdi-arrow-left-bottom zmdi-hc-5x"></i>Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php"><i id="me" class="zmdi zmdi-arrow-right-top zmdi-hc-5x"></i>Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php"><i id="me" class="zmdi zmdi-edit zmdi-hc-5x"></i>Memos in Draft</a><!--/ul-->
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                        <li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li>

                        


                         </ul>
                </div>
            </aside>
           
            <section class="content">
                <div class="content__inner">
                    <header class="content__title">
                        <h1 id="dash">Messages</h1>

                       
                    </header>

                    
                    <p id="otherp" hidden><?php echo $_GET['user_id'];?></p>
                <form action="chats.php?user_id=<?php echo $_GET['user_id'];?>&&station=<?php echo $_GET['station'];?>&&position=<?php echo $_GET['position'];?>" method="post">
                    <div class="messages">
                        <div class="messages__sidebar" id="cbmemcreation">
        

                            <div class="messages__reply">
                                <textarea class="messages__reply__text" name="message" placeholder="Type a message..."></textarea>
                            </div>      </br>
                            
                            <button type="submit" class="btn btn-info" name="submit"><i class="zmdi zmdi-mail-send"></i>Send Message</button></br><br/>
                             <?php
     //open a connection
                    $user_id=$_GET['user_id'];
                    $ses=$_SESSION['karibu'];
$mysqli= new mysqli('localhost','root','','ememo');
if(!$mysqli){
    die('connection to the database failed'.$mysqli->error);
}
                    
                    if(isset($_POST['submit'])){
//create and execute a query
$sqlogs="insert into chats(sender,receiver,message,time)VALUES(?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $sender=$ses;
        $receiver=$user_id;
        $message=$_POST['message'];
        $time=date("Y-m-d H:i:s");
$stmnt->bind_param('ssss',$sender,$receiver,$message,$time);
    $stmnt->execute();
    $message="<p style=\"color:#bb421a\">Message sent</p>";
   
}else{
    die("could not query the database".$mysqli->error);
}
    
echo $message;
                    }
                
   
    ?>
                            
                        </div>
                    

                        <div class="messages__body" id="cbmemcreation">
                            <div class="messages__header">
                                <div class="toolbar toolbar--inner mb-0">
                                    <div class="toolbar__label">You are chatting with: <?php echo $_GET['position'].'.&nbsp;'.$_GET['station'];?></div>

                                    <div class="toolbar__search">
                                        <input type="text" placeholder="Search...">
                                        <i class="toolbar__search__close zmdi zmdi-long-arrow-left" data-sa-action="toolbar-search-close"></i>
                                    </div>
                                </div>
                            </div>

                            <div id="displaychat"class="messages__content">
                                
                                
                                
                                
                                
                                
                                
                            </div>

                        </div>
                        
                        
                        
                        
                    </div>
                        
                                               
  
                        
                        
                        
                        </form>
                </div>
            </section></main>


        <!-- Javascript -->
<!--NOTIFICATIONS-->
<script>
 //fetching chats
         $(function() {       
  var otherperson = $("#otherp").html();

  setInterval(function(){
  $.ajax({
    method: "POST",
    url: "nonfinancialreadchats.php",
    data: {
      otherperson:otherperson
    },
    cache: false
  }).done(function(data){
    $("#displaychat").html(data)
  });
   
 },250);
            

});
    
    
</script>
<!--NOTIFICATIONS-->
<script>
$(document).ready(function(){
 // $('#dash').hide();
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"notifications/fetch.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('#dropdownotif').html(data.notification);
    if(data.unseen_notification > 0)
    {
        //send push
 
          Push.create("Memo Activity Alert!",{
            body: "Follow up from the dashboard",
            icon: 'img/message.png',
            onClick: function () {
                window.focus();
                this.close();
                 window.location.href = "http://localhost/ememo_update/index.php"; 
              
                Push.clear();
            }         
        });
        
        
        
        /*var notify;
        notify=new Notification('An activity has taken place on your memo',
                                {
            body:'Follow up from the dashboard',
            icon:'img/message.png'
        });*/
        
     $('.countnotif').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $(document).on('click', '#meenotif', function(){
  $('.countnotif').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 20000);
 
});
</script>

<!--CHATS-->
<script>
$(document).ready(function(){
 // $('#dash').hide();
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"notifications/fetchmessages.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('#dropdownmes').html(data.notification);
    if(data.unseen_notification > 0)
    {
        //send push
 
          Push.create("You have a new message",{
            body: "Reply to the sender",
            icon: 'img/message.png',
            onClick: function () {
                window.focus();
                this.close();
                 window.location.href = "http://localhost/ememo_update/indexchats.php"; 
              
                Push.clear();
            }         
        });
        
        
        
      
     $('.countmes').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
 $(document).on('click', '#meemes', function(){
  $('.countmes').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 20000);
 
});
</script>
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
       
        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>