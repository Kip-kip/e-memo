function add_email(){
	var email=document.getElementById('recipient_emails').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_data").append(response);
        
			}
		});
}

function add_station(){
	var email=document.getElementById('recipient_stations').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-station.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_station").append(response);
        
			}
		});
}
function add_financial(){
	var email=document.getElementById('recipient_financials').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-financial.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_financial").append(response);
			}
		});
}

function add_loctravel(){
	var email=document.getElementById('recipient_loctravels').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-loctravel.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_loctravel").append(response);
			}
		});
}

function add_inttravel(){
	var email=document.getElementById('recipient_inttravels').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-inttravel.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_inttravel").append(response);
			}
		});
}





//CC EMAIL
function add_cc_email(){
	var email=document.getElementById('recipient_cc_emails').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-cc-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_cc_data").append(response);
				 
			}
		});
}
//for forward financial
function add_ccf_email(){
	var email=document.getElementById('recipient_ccf_emails').value;
	variables="email="+email;

	var url="./controller/apis/add-temp-ccf-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_ccf_data").append(response);
				 
			}
		});
}


function onchange_taster(value){
	alert(value);
}

function delete_single_data(email){

	variables="email="+email;

	var url="./controller/apis/delete-single-temp-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
				
				
			}
		});
}
function delete_singlethr_data(email){

	variables="email="+email;

	var url="./controller/apis/delete-singlethr-temp-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
				
				
			}
		});
}
function add_data(){
     //////////////////////////    VALIDATING   ///////////////////////////
    //communication
if (document.getElementById('communication').checked == true) {
     if(document.getElementById('recipient_emails').value=="null"){
         alert('Kindly fill the recipient  before proceeding');
    }
    else if(document.getElementById('subject').value==""){
        alert('Kindly fill the subject before proceeding');
        } 
    else if(document.getElementById('datecreated').value==""){
       alert('Kindly fill the date created before proceeding');
    }
     else if((document.getElementById('urgencyvery').checked==false)&&(document.getElementById('urgencyhigh').checked==false)&&(document.getElementById('urgencymedium').checked==false)){
        alert('Kindly choose urgency before proceeding ');
        } 
   else if(document.getElementById('duedate').value==""){
        alert('Kindly fill the due date before proceeding');
    }   
    /*else if(document.getElementById('upload_document').value==""){
      alert('Kindly attach supporting document before proceeding');
    } */
    else{
         var memotype=document.querySelector('input[name="sametype"]:checked').value;
        var votehead='none';
    var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
    var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
    var amount=document.getElementById('amount').value;
 /**check if draft, delete from drafts**/ 
    var refdraft=document.getElementById('refdraft').value;
    
     variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount+"&refdraft="+refdraft+"&votehead="+votehead;
     var introductionone = introduction.replace(/&nbsp;/g,'|');
    var introductiontwo=introductionone.replace(/&#39;/g,'~');
     var introductionthree=introductiontwo.replace(/&quot;/g,'`');
    
 	var url="./controller/apis/add-memo-data.php?"+variables;
    url= encodeURI(url);
  
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introductionthree,
			dataType:"json",

			success:function(response){
                 
				  console.log(response);
				if(response.success==1){
					window.location.href="memopreview.php";
				}else{
					alert('Something went wrong');
				}
				
			}
            
		});
    }
    
}
//financial    
 else if (document.getElementById('financial').checked  == true) {
            if(document.getElementById('recipient_financials').value=="empty"){
       alert('Kindly fill the recipient before proceeding');
    }
    else if(document.getElementById('subject').value==""){
        alert('Kindly fill the subject before proceeding');
        } 
    else if(document.getElementById('datecreated').value==""){
       alert('Kindly fill the date created before proceeding');
    }
     else if((document.getElementById('urgencyvery').checked==false)&&(document.getElementById('urgencyhigh').checked==false)&&(document.getElementById('urgencymedium').checked==false)){
      alert('Kindly choose urgency before proceeding ');
        } 
   else if(document.getElementById('duedate').value==""){
       alert('Kindly fill the due date before proceeding');
    } 
      else if(document.getElementById('amount').value==""){
       alert('Kindly fill the amount before proceeding');
    }  
     /* else if(document.getElementById('votehead').value=="empty"){
       $("#validate").append('Kindly choose votehead before proceeding');
    }   */
      else if(document.getElementById('upload_document').value==""){
      alert('Kindly attach supporting document before proceeding');
    }   
     else{
          var memotype=document.querySelector('input[name="sametype"]:checked').value;
         var votehead=document.getElementById('votehead').value;
    var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
    var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
    var amount=document.getElementById('amount').value;
 /**check if draft, delete from drafts**/ 
    var refdraft=document.getElementById('refdraft').value;
    
     variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount+"&refdraft="+refdraft+"&votehead="+votehead;
     var introductionone = introduction.replace(/&nbsp;/g,'|');
    var introductiontwo=introductionone.replace(/&#39;/g,'~');
     var introductionthree=introductiontwo.replace(/&quot;/g,'`');
    
 	var url="./controller/apis/add-memo-data.php?"+variables;
    url= encodeURI(url);
  
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introductionthree,
			dataType:"json",

			success:function(response){
                 
				  console.log(response);
				if(response.success==1){
					window.location.href="memopreview.php";
				}else{
					alert('Something went wrong');
				}
				
			}
            
		});
     }
      
   }
    
    //broadcast
    else if (document.getElementById('broadcast').checked  == true) {
           if(document.getElementById('recipient_stations').value=="empty"){
      alert('Kindly fill the recipient before proceeding');
    }
    else if(document.getElementById('subject').value==""){
        alert('Kindly fill the subject before proceeding');
        } 
    else if(document.getElementById('datecreated').value==""){
       alert('Kindly fill the date created before proceeding');
    }
     else if((document.getElementById('urgencyvery').checked==false)&&(document.getElementById('urgencyhigh').checked==false)&&(document.getElementById('urgencymedium').checked==false)){
        alert('Kindly choose urgency before proceeding ');
        } 
   else if(document.getElementById('duedate').value==""){
      alert('Kindly fill the due date before proceeding');
    } 
        /*else if(document.getElementById('upload_document').value==""){
      alert('Kindly attach supporting document before proceeding');
    }*/   
        else{
            
             var memotype=document.querySelector('input[name="sametype"]:checked').value;
            var votehead="none";
    var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
    var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
    var amount=document.getElementById('amount').value;
 /**check if draft, delete from drafts**/ 
    var refdraft=document.getElementById('refdraft').value;
    
     variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount+"&refdraft="+refdraft+"&votehead="+votehead;
     var introductionone = introduction.replace(/&nbsp;/g,'|');
    var introductiontwo=introductionone.replace(/&#39;/g,'~');
     var introductionthree=introductiontwo.replace(/&quot;/g,'`');
    
 	var url="./controller/apis/add-memo-data.php?"+variables;
    url= encodeURI(url);
  
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introductionthree,
			dataType:"json",

			success:function(response){
                 
				  console.log(response);
				if(response.success==1){
					window.location.href="memopreview.php";
				}else{
					alert('Something went wrong');
				}
				
			}
            
		});
        }
        }
	
	//travel and local
	else if ((document.getElementById('travel').checked  == true)&&(document.getElementById('local').checked  == true)){
	 if(document.getElementById('recipient_emails').value=="empty"){
      alert('Kindly fill the recipient before proceeding');
    }
    else if(document.getElementById('subject').value==""){
        alert('Kindly fill the subject before proceeding');
        } 
    else if(document.getElementById('datecreated').value==""){
       alert('Kindly fill the date created before proceeding');
    }
     else if((document.getElementById('urgencyvery').checked==false)&&(document.getElementById('urgencyhigh').checked==false)&&(document.getElementById('urgencymedium').checked==false)){
        alert('Kindly choose urgency before proceeding ');
        } 
   else if(document.getElementById('duedate').value==""){
      alert('Kindly fill the due date before proceeding');
    } 
		//local travel
        else if(document.getElementById('personalno').value==""){
      alert('Kindly fill personal number before proceeding');
    } 
		 else if(document.getElementById('designation').value==""){
      alert('Kindly fill designation before proceeding');
    } 
		 else if(document.getElementById('telephone').value==""){
      alert('Kindly fill telephone number  before proceeding');
    } 
		 else if(document.getElementById('duration').value==""){
      alert('Kindly fill duration before proceeding');
    } 
		 else if(document.getElementById('startdate').value==""){
      alert('Kindly fill start date  before proceeding');
    } 
		 else if(document.getElementById('enddate').value==""){
      alert('Kindly fill end date  before proceeding');
    } 
		 else if(document.getElementById('destination').value==""){
      alert('Kindly fill destination City/County  before proceeding');
    } 
		 else if(document.getElementById('purpose').value==""){
      alert('Kindly fill reasons for travel  before proceeding');
    } 
		 else if(document.getElementById('activities').value==""){
      alert('Kindly fill activities before proceeding');
    } 
		else if(document.getElementById('delegatedofficer').value=="Select Officer"){
      alert('Kindly fill delegated officer  before proceeding');
    }  
		else if(document.getElementById('delegateddesign').value==""){
      alert('Kindly fill designation  before proceeding');
    } 
		else if(document.getElementById('components').value==""){
      alert('Kindly fill components  before proceeding');
    } 
        else{
            
             var memotype=document.querySelector('input[name="typemoja"]:checked').value;
            var votehead="none";
    var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
    var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
    var amount=document.getElementById('amount').value;
 /**check if draft, delete from drafts**/ 
    var refdraft=document.getElementById('refdraft').value;
    
			//local travel
		    var personalno=document.getElementById('personalno').value;
			var designation=document.getElementById('designation').value;
			var telephone=document.getElementById('telephone').value;
			var duration=document.getElementById('duration').value;
			var startdate=document.getElementById('startdate').value;
			var enddate=document.getElementById('enddate').value;
			var destination=document.getElementById('destination').value;
			var purpose=document.getElementById('purpose').value;
			var activities=document.getElementById('activities').value;
			var delegatedofficer=document.getElementById('delegatedofficer').value;
			var delegateddesign=document.getElementById('delegateddesign').value;
			var components=document.getElementById('components').value;
			
     variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount+"&refdraft="+refdraft+"&votehead="+votehead+"&personalno="+personalno+"&designation="+designation+"&telephone="+telephone+"&duration="+duration+"&startdate="+startdate+"&enddate="+enddate+"&destination="+destination+"&purpose="+purpose+"&activities="+activities+"&delegatedofficer="+delegatedofficer+"&delegateddesign="+delegateddesign+"&components="+components;
			
     var introductionone = introduction.replace(/&nbsp;/g,'|');
    var introductiontwo=introductionone.replace(/&#39;/g,'~');
     var introductionthree=introductiontwo.replace(/&quot;/g,'`');
    
 	var url="./controller/apis/add-memo-loctravel-data.php?"+variables;
    url= encodeURI(url);
  
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introductionthree,
			dataType:"json",

			success:function(response){
                 
				  console.log(response);
				if(response.success==1){
					window.location.href="memopreview.php";
				}else{
					alert('Something went wrong');
				}
				
			}
            
		});
        }	
	}
	
	//travel and international
	else if ((document.getElementById('travel').checked  == true)&&(document.getElementById('international').checked  == true)){
		
		if(document.getElementById('recipient_emails').value=="empty"){
      alert('Kindly fill the recipient before proceeding');
    }
    else if(document.getElementById('subject').value==""){
        alert('Kindly fill the subject before proceeding');
        } 
    else if(document.getElementById('datecreated').value==""){
       alert('Kindly fill the date created before proceeding');
    }
     else if((document.getElementById('urgencyvery').checked==false)&&(document.getElementById('urgencyhigh').checked==false)&&(document.getElementById('urgencymedium').checked==false)){
        alert('Kindly choose urgency before proceeding ');
        } 
   else if(document.getElementById('duedate').value==""){
      alert('Kindly fill the due date before proceeding');
    } 
		//international travel
        else if(document.getElementById('intpersonalno').value==""){
      alert('Kindly fill personal number before proceeding');
    } 
		 else if(document.getElementById('intdesignation').value==""){
      alert('Kindly fill designation before proceeding');
    } 
		 else if(document.getElementById('inttelephone').value==""){
      alert('Kindly fill telephone number  before proceeding');
    } 
		 else if(document.getElementById('intreasons').value==""){
      alert('Kindly fill Reasons for travel before proceeding');
    } 
		else if(document.getElementById('intcountry').value==""){
      alert('Kindly fill Country before proceeding');
    } 
		else if(document.getElementById('intcity').value==""){
      alert('Kindly fill City before proceeding');
    } 
		 else if(document.getElementById('intdepart').value==""){
      alert('Kindly fill Departure date  before proceeding');
    } 
		 else if(document.getElementById('intreturn').value==""){
      alert('Kindly fill Return date  before proceeding');
    } 
		 else if(document.getElementById('inttravelmode').value==""){
      alert('Kindly fill travel mode  before proceeding');
    } 
		 else if(document.getElementById('inthost').value==""){
      alert('Kindly fill host  before proceeding');
    } 
		 else if(document.getElementById('intsponsor').value==""){
      alert('Kindly fill Sponsor(s) before proceeding');
    } 
		 else if(document.getElementById('intsubsistence').value==""){
      alert('Kindly fill subsistence before proceeding');
    } 
		else if(document.getElementById('intsponsortwo').value==""){
      alert('Kindly fill sponsor  before proceeding');
    } 
		else if(document.getElementById('intaccomodation').value==""){
      alert('Kindly fill accomodation  before proceeding');
    } 
		else if(document.getElementById('intparticipation').value==""){
      alert('Kindly fill participation  before proceeding');
    } 
		else if(document.getElementById('inttravelexp').value==""){
      alert('Kindly fill travel expenses  before proceeding');
    } 
		else if(document.getElementById('inttotal').value==""){
      alert('Kindly fill total expenses  before proceeding');
    } 
	else if(document.getElementById('intobjs').value==""){
      alert('Kindly fill objectives  before proceeding');
    } 
		else if(document.getElementById('intbenefits').value==""){
      alert('Kindly fill benefits before proceeding');
    } 
        else{
            
             var memotype=document.querySelector('input[name="typemoja"]:checked').value;
            var votehead="none";
    var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
    var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
    var amount=document.getElementById('amount').value;
 /**check if draft, delete from drafts**/ 
    var refdraft=document.getElementById('refdraft').value;
    
			//international travel
		    var intpersonalno=document.getElementById('intpersonalno').value;
			var intdesignation=document.getElementById('intdesignation').value;
			var inttelephone=document.getElementById('inttelephone').value;
			var intreasons=document.getElementById('intreasons').value;
			var intcountry=document.getElementById('intcountry').value;
			var intcity=document.getElementById('intcity').value;
			var intdepart=document.getElementById('intdepart').value;
			var intreturn=document.getElementById('intreturn').value;
			var inttravelmode=document.getElementById('inttravelmode').value;
			var inthost=document.getElementById('inthost').value;
			var intsponsor=document.getElementById('intsponsor').value;
			var intsubsistence=document.getElementById('intsubsistence').value;
			var intsponsortwo=document.getElementById('intsponsortwo').value;
			var intaccomodation=document.getElementById('intaccomodation').value;
			var intparticipation=document.getElementById('intparticipation').value;
			var inttravelexp=document.getElementById('inttravelexp').value;
			var inttotal=document.getElementById('inttotal').value;
			var intobjs=document.getElementById('intobjs').value;
			var intbenefits=document.getElementById('intbenefits').value;
			
     variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount+"&refdraft="+refdraft+"&votehead="+votehead+"&intpersonalno="+intpersonalno+"&intdesignation="+intdesignation+"&inttelephone="+inttelephone+"&intreasons="+intreasons+"&intcountry="+intcountry+"&intcity="+intcity+"&intdepart="+intdepart+"&intreturn="+intreturn+"&inttravelmode="+inttravelmode+"&inthost="+inthost+"&intsponsor="+intsponsor+"&intsubsistence="+intsubsistence+"&intsponsortwo="+intsponsortwo+"&intaccomodation="+intaccomodation+"&intparticipation="+intparticipation+"&inttravelexp="+inttravelexp+"&inttotal="+inttotal+"&intobjs="+intobjs+"&intbenefits="+intbenefits;
			
     var introductionone = introduction.replace(/&nbsp;/g,'|');
    var introductiontwo=introductionone.replace(/&#39;/g,'~');
     var introductionthree=introductiontwo.replace(/&quot;/g,'`');
    
 	var url="./controller/apis/add-memo-inttravel-data.php?"+variables;
    url= encodeURI(url);
  
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introductionthree,
			dataType:"json",

			success:function(response){
                 
				  console.log(response);
				if(response.success==1){
					window.location.href="memopreview.php";
				}else{
					alert('Something went wrong');
				}
				
			}
            
		});
        }
		
	}
	
    else if((document.getElementById('communication').checked==false)&&(document.getElementById('financial').checked==false)&&(document.getElementById('broadcast').checked==false)&&(document.getElementById('travel').checked==false)){
          alert('Kindly choose memo type before proceeding');
    }
   
    
}


/**SAVE TO DRAFT TABLE**/
function add_draft_data(){
    
     $("#validate").append('Kindly choose memotype and urgency before saving as draft');
    
     var memotype=document.querySelector('input[name="sametype"]:checked').value;
     var urgency=document.querySelector('input[name="urgency"]:checked').value;
    var prevmemo=document.getElementById('prevmemo').value;
	var subject=document.getElementById('subject').value;
	var datecreated=document.getElementById('datecreated').value;
	var duedate=document.getElementById('duedate').value;
    var introduction=CKEDITOR.instances.introduction.getData();
     var amount=document.getElementById('amount').value;
 
  
    
 variables="memotype="+memotype+"&urgency="+urgency+"&prevmemo="+prevmemo+"&subject="+subject+"&datecreated="+datecreated+"&duedate="+duedate+"&amount="+amount;//+"&introduction="+introduction;
  //var re = new RegExp(String.fromCharCode(160), "g");
    introduction = introduction.replace(/&nbsp;/g,' ');
     //  alert(introduction);
    
        
	var url="./controller/apis/add-memo-draft-data.php?"+variables;
    url= encodeURI(url);
  //   url=
    //encodeURIComponent(url));
    //alert(introduction);
    
		$.ajax({
			type:"POST",
			url:url,
             data:'introduction='+introduction,
			dataType:"json",

			success:function(response){
                console.log(response);
				if(response.success==1){
					window.location.href="indexnonfinancialdrafts.php";
				}else{
					console.log('error while adding memo');
				}
				
			}
		});

}




/*Upload the Documents*/

function uploadDoc(){

	
	var uploader=document.getElementById('requestor').value;	
	var url='./controller/apis/documents/upload.php?req='+uploader;

	var doc=document.querySelector('[type=file]').files;

	const formData=new FormData();

	for(let i=0;i<doc.length;i++){
		let file = doc[i];

		formData.append('files[]',file);
	}

	fetch(url,{
		method: 'POST',
		body: formData,
	}).then(response=>response.json()).then(data=>{

		// var output='';

		// for(var i: data.file_names){
		// 	output += data.file_names[i].file_name+',';
		// }

		// $('#name_of_documents').html('<input type="text" class="form-control"'+
		// 	'name="names_of_docs" id="names_of_docs" >');

		for(var i in data.file_names){
			console.log(data.file_names[i]);
			$("#data_of_uploaded_documents").append(data.file_names[i]);
		}
		
	 });
	
}


/****ESCALATION****/


function add_escalation_data(){
    
    
     	//var email=document.getElementById('escalatethis').value;
       var referenceno=document.getElementById('referencing').value;
	variables="referenceno="+referenceno;

	var url="./controller/apis/add-escalation-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
                if(response.success==1){
					  alert('Memo successfully escalated');
                    location.reload();
                    //$("#recipient_data").append('Escalation Successful');
				}else{
						//$("#recipient_cc_data").append('Memo successfully escalated');
                        alert('Memo successfully escalated');
                    location.reload();
				}
					
        
			}
		});
    
}

function add_forward(){
	var email=document.getElementById('forwardthis').value;
       var referenceno=document.getElementById('referencing').value;
	variables="email="+email+"&referenceno="+referenceno;

	var url="./controller/apis/add-temp-forward.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
					$("#recipient_forward").append(response);
        
			}
		});
}

function add_forward_data(){
    
       var referenceno=document.getElementById('referencing').value;
	variables="referenceno="+referenceno;

	var url="./controller/apis/add-forward-data.php?"+variables;
		$.ajax({
			type:"POST",
			url:url,
			dataType:"text",

			success:function(response){
                if(response.success==1){
					  alert('Memo successfully fowarded'); 
                     location.reload();
                    //$("#recipient_forward").append('Memo successfully fowarded');
				}else{
						alert('Memo successfully fowarded');
                       location.reload();
                    //$("#recipient_forward").append('Memo successfully fowarded');
				}
					
        
			}
		});
    
}



