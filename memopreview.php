<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E-Memo</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/dropzone/dist/dropzone.css">
    <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
       <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    
  <script src="ckeditor/adapters/jquery.js"></script>
     <script src="ckeditor/ckeditor.js"></script>
     <script src="jquery-3.0.0.js"></script>
    <!-- App styles -->
    <link rel="stylesheet" href="css/app.min.css">
</head>

<body data-sa-theme="1">
<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>

            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                   <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="to-nav__notify"><i id="" class="label label-pill label-danger count"></i></br><i id="mee" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="messages.html" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownlow" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <a href="#" class="view-more">View all notifications</a>
                            </div>
                        </div>
                    </li>

                    <li class="dropdown message">
                        <a href="#" data-toggle="dropdownmessage" class="to-nav__notify"><i id="" class="label label-pill label-danger count"></i></br><i id="mee" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="messages.html" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownlow" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <a href="#" class="view-more">View all messages</a>
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
           $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        $sig_file_ext=$row['sig_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>  
                        <div class="user__info" data-toggle="dropdown">
                           
                            <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                      
                                
                               <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>
                       
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->

                        


                         </ul>
                </div>
            </aside>

    <section class="content">
        <header class="content__title">
       
        </header>
            <?php
                  
include_once 'config.php';
                    
if(isset($_POST['send'])){
   
	/*------------------------------------NORMAL MEMOS-------------------------------------*/
    //move from temp_memo holder to main nonfinancialmemos table
    $sql="INSERT INTO nonfinancialmemos (referenceno,memotype,financial_year,votehead,recepient,nature,requestor,subject,datecreated,urgency,duedate,introduction,amount,approvedamount,comments,comment_status,prevmemo,status,progress,generalstatus,generalprogress,view,availability,vh_deduct_indicator) SELECT referenceno,memotype,financial_year,votehead,recepient,nature,requestor,subject,datecreated,urgency,duedate,introduction,amount,approvedamount,comments,comment_status,prevmemo,status,progress,generalstatus,generalprogress,view,availability,vh_deduct_indicator FROM temp_memoholder WHERE requestor='$ses'";
     if($stmnt=$mysqli->prepare($sql)){
        $stmnt->execute();
         
         echo"<p style=\"color:#FC4A1A\">Memo successfully sent</p>";
       }
       else{
       echo"There was a problem encountered!";
       }
	
	/*---------------------------------------LOCAL TRAVEL MEMOS--------------------------------------*/
	//move from temp_memo holder to main localtravelmemos table
    $sql="INSERT INTO localtravelmemos (referenceno,recipient,traveltype,officer_id,personal_number,designation,telephone,duration,start_date,end_date,destination,purpose,activities,delegated_officer,delegated_officer_design,components,remarks) SELECT referenceno,recipient,traveltype,officer_id,personal_number,designation,telephone,duration,start_date,end_date,destination,purpose,activities,delegated_officer,delegated_officer_design,components,remarks FROM temp_loctravel_memoholder WHERE officer_id='$ses'";
     if($stmnt=$mysqli->prepare($sql)){
        $stmnt->execute();
         
         echo"<p style=\"color:#FC4A1A\">Memo successfully sent</p>";
       }
       else{
       echo"There was a problem encountered!";
       }
	
	/*----------------------------------------INT TRAVEL MEMOS-------------------------------*/
	
	//move from temp_memo holder to main localtravelmemos table
    $sql="INSERT INTO internationaltravelmemos (referenceno,recipient,traveltype,officer_id,personal_number,designation,telephone,reasons,country,city,departure_date,return_date,travelmode,host,sponsor,subsistence,sponsor_two,participation,accomodation,travel_expenses,total_expenses,objectives,expected_benefits,remarks) SELECT referenceno,recipient,traveltype,officer_id,personal_number,designation,telephone,reasons,country,city,departure_date,return_date,travelmode,host,sponsor,subsistence,sponsor_two,participation,accomodation,travel_expenses,total_expenses,objectives,expected_benefits,remarks FROM temp_inttravel_memoholder WHERE officer_id='$ses'";
     if($stmnt=$mysqli->prepare($sql)){
        $stmnt->execute();
         
         echo"<p style=\"color:#FC4A1A\">Memo successfully sent</p>";
       }
       else{
       echo"There was a problem encountered!";
       }
    
    
    /***SEND EMAIL TO RECEPIENTS----PUT TOGETHER ADDRESS OF THE SENDER**/
$querulist="SELECT * FROM ememo_users where user_id='$ses'";        
//execute query
if ($mysqli->real_query($querulist)) {
    //If the query was successful
    $res = $mysqli->use_result();
    while ($row = $res->fetch_assoc()) {  
            $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $hier=$row['hierarchical_level'];
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
            $add=0;
        }
        else if($position=='C.O'){
             $station=$stationsector;
            $add=1;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
               $add=2;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
              $add=3;
        }
        else if($position=='HOU'){
             $station=$stationunit;
              $add=4;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
              $add=4;
        }
        else{
             $station='County Government of Nandi';
        }
       
           $fulltitle =$position.", ".$station;
        }
     
}        
        //get subject
    $query="SELECT * FROM temp_memoholder where requestor='$ses' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
      $subject=$row['subject'];
      $referenceno=$row['referenceno'];
      $memotype=$row['memotype'];
      $votehead=$row['votehead'];
      $amount=$row["amount"];
      $urgency=$row["urgency"];
	  $memotype=$row["memotype"];
  }
}
    
    /***ONLY SEND SMS IF PRIORITY IS VERY HIGH***/
    if($urgency=='very high'){
    
    //send message
$query="SELECT * FROM temp_memoholder inner join ememo_users on temp_memoholder.recepient=ememo_users.user_id where requestor='$ses'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
     $myarray=$row['phone'];
      $message="$fulltitle  Sent a new memo: '$subject'. Follow up from the Nandi County E-Memo system";
     $newarray=array('message'=>$message,'recipients'=>$myarray);
   
             
    $ch = curl_init();
curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-Type:application/json', 'ApiKey:061be78ebe6941ceb5fb3ee01ed767af'));
curl_setopt($ch, CURLOPT_URL, "http://api.sematime.com/v1/1538372262372/messages");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($newarray));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$server_output = curl_exec($ch);
curl_close($ch);
 
        }
     
}
    }
    else{ //just send a mail
         //send message
$query="SELECT * FROM temp_memoholder inner join ememo_users on temp_memoholder.recepient=ememo_users.user_id where requestor='$ses'";
  //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Sent a new memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}      
        
    }
    
    /******************************SEND MESSAGE to CECM**********************************/
    //select the user 
    $hieradd=$hier+$add;
$query="SELECT * FROM ememo_users where department='$stationdepartment' AND hierarchical_level='$hieradd'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Sent a new memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
    
   
        
        /*****INSERT INTO LOGS*****/
 $sql1="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $action='Sent';
                $at='at';
                 $actor_nature="direct";
                $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
                $stmnt->execute();
         
            } 

 
	
	/*------------------------NORMAL MEMOS-------------------------------*/
     //delete from temp_memoholder table
    $sqltatu="DELETE FROM temp_memoholder where requestor='$ses'";
     if($stmnt=$mysqli->prepare($sqltatu)){
        $stmnt->execute();
         
       }
    
	/*------------------------LOCAL TRAVEL MEMOS-------------------------------*/
	//delete from local temp travel memoholder table
    $sqltatu="DELETE FROM temp_loctravel_memoholder where officer_id='$ses'";
     if($stmnt=$mysqli->prepare($sqltatu)){
        $stmnt->execute();
         
       }
	
	/*------------------------INT TRAVEL MEMOS-------------------------------*/
	//delete from local temp travel memoholder table
    $sqltatu="DELETE FROM temp_inttravel_memoholder where officer_id='$ses'";
     if($stmnt=$mysqli->prepare($sqltatu)){
        $stmnt->execute();
         
       }
	
	
     ?>
                                    <script type="text/javascript">
                                        window.location.href="indexnonfinancialsent.php";
                                    </script>
                                   <?php
}

        if(isset($_POST['discard'])){
             //delete from temp_memoholder table
    $sqltatu="DELETE FROM temp_memoholder where requestor='$ses'";
     if($stmnt=$mysqli->prepare($sqltatu)){
        $stmnt->execute();
         
       }
			
			  //delete from temp local travel table
    $sqltatu="DELETE FROM temp_loctravel_memoholder where officer_id='$ses'";
     if($stmnt=$mysqli->prepare($sqltatu)){
        $stmnt->execute();
         
       }
    
     ?>
                                    <script type="text/javascript">
                                        window.location.href="nonfinancialmemocreation.php";
                                    </script>
                                   <?php
        }
        ?>

        
        <div class="card">
            <div id="whi" class="card-body">
                
                <!--DISPLAYING THE MEMO-->
                   <?php 
                                        
   require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    $ses=$_SESSION['karibu'];
    
				//determine the memo type
$query="SELECT * FROM temp_memoholder WHERE requestor='$ses'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
$urgency=$row['urgency'];
$memotype=$row['memotype'];	
	$datecreated=$row['datecreated'];
	if($urgency=='very high'){
            $urgency='Icon_red';
        }
        else if($urgency=='high'){
              $urgency='Icon_orange';
        }
        else{
              $urgency='Icon_blue';
        }
}}
                
				
		/**--------------------------------------IF MEMO IS A LOCAL TRAVEL REQUEST--------------------------------------**/	
				
				
				
				if($memotype=='local'){
					
					//get Travel details from the travel table 	and inner join with ememo users to get applicants name and signature
				
$query="SELECT * FROM temp_loctravel_memoholder inner join ememo_users on temp_loctravel_memoholder.officer_id=ememo_users.user_id WHERE officer_id='$ses'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	$fname=$row['fname'];
	$mname=$row['mname'];
	$lname=$row['lname'];
	$officer_id=$row['officer_id'];
	$sig_file_ext=$row['sig_file_ext'];
    $personalno=$row['personal_number'];	
	$designation=$row['designation'];	
	$telephone=$row['telephone'];	
	$duration=$row['duration'];	
	$startdate=$row['start_date'];	
	$enddate=$row['end_date'];	
	$destination=$row['destination'];	
	$purpose=$row['purpose'];
	$activities=$row['activities'];	
	$delegatedofficer=$row['delegated_officer'];	
	$delegatedofficerdesign=$row['delegated_officer_design'];	
	$components=$row['components'];	
}}
				
				//get the delegated officer name and signature
$query="SELECT * FROM ememo_users WHERE user_id='$delegatedofficer'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
$fnamedelegated=$row['fname'];	
	$mnamedelegated=$row['mname'];	
	$lnamedelegated=$row['lname'];
	$sig_file_extdelegated=$row['sig_file_ext'];
}}
					
					
					
					 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgency.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-family:times;font-size:18px;padding:15;\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-size:15px;font-family:times;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;font-family:times;padding-top:50px;font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
                             
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;text-align:center;font-family:times;font-size:18px;margin-left:60px;\"><b>OFFICE OF THE DEPUTY GOVERNOR</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-weight:1000;text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
				
				
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">To be completed in Duplicate: Original to be retained by applicant and Duplicate by HRM</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART 1</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">1. Name of Officer: <b> $fname $mname $lname</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">2. Personal Number:<b> $personalno</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">3. Designation:  <b>$designation</b>  Tel:  <b>$telephone</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">4. Duration:  <b>$duration</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">5. Start Date:  <b>$startdate</b>  End Date:  <b>$enddate</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">6. Country/City of Destination:  <b>$destination</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">Purpose/Reason for request of travel:</div>";

					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">$purpose</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature of Applicant: <img height='60' width='60' src='img/signatures/$officer_id$sig_file_ext'/></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART II-HANDING OVER</div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">7. The following assignments/duties/responsibilities will be handled by: </div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">ACTIVITIES/DUTIES AND RESPONSIBILITIES</div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\"><b>$activities</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Name of the Officer:  <b>$fnamedelegated $mnamedelegated $lnamedelegated</b> </div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Designation:  <b>$delegatedofficerdesign</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Component:  <b>$components</b></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature:  <img height='60' width='60' src='img/signatures/$delegatedofficer$sig_file_extdelegated'/></div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">8. SUPERVISOR/HEAD OF DEPARTMENT (Where applicable)</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature:     Date:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">9. COUNTY SECRETARY</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Status:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks (If any): </div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">10. H.E. THE DEPUTY GOVERNOR</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Status:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Remarks (If any): </div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature: Date:</div>";
					echo "<p></p>";     
                         echo "<p style=\"color:white;\">.</p>"; 
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
					
					
				echo "<p style=\"text-align:center;font-size:14px;\"><i>#TRANSFORMING NANDI</i></p>";	
					
					
					
					
					
                                       
					
				}
				
				else if($memotype=='international'){
					
					
					//get Travel details from the travel table 	and inner join with ememo users to get applicants name and signature
				
$query="SELECT * FROM temp_inttravel_memoholder inner join ememo_users on temp_inttravel_memoholder.officer_id=ememo_users.user_id WHERE officer_id='$ses'";    
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
while ($row = $res->fetch_assoc()) {
	$fname=$row['fname'];
	$mname=$row['mname'];
	$lname=$row['lname'];
	$officer_id=$row['officer_id'];
	$sig_file_ext=$row['sig_file_ext'];
    $intpersonalno=$row['personal_number'];	
	$intdesignation=$row['designation'];	
	$inttelephone=$row['telephone'];	
	$reasons=$row['reasons'];	
	$country=$row['country'];
	$city=$row['city'];
	$departdate=$row['departure_date'];
	$returndate=$row['return_date'];
	$travelmode=$row['travelmode'];
	$host=$row['host'];
	$sponsor=$row['sponsor'];
	$subsistence=$row['subsistence'];
	$sponsortwo=$row['sponsor_two'];
	$participation=$row['participation'];
	$accomodation=$row['accomodation'];	
	$travelexpenses=$row['travel_expenses'];
	$totalexpenses=$row['total_expenses'];	
	$objectives=$row['objectives'];	
	$benefits=$row['expected_benefits'];	
}}
				
			
					
					
					 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgency.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-family:times;font-size:18px;padding:15;\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-size:15px;font-family:times;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;font-family:times;padding-top:50px;font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
                             
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;text-align:center;font-family:times;font-size:18px;margin-left:60px;\"><b>OFFICE OF THE GOVERNOR</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-weight:1000;text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
				
				
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">(To  be  completed  in  quadruplicate:  Original to  be  retained  by  applicant,  Duplicate by 
processing Division, Triplicate by Head of Department and Quadruplicate for  retention 
by D/HRM)</div>"; 
					
					echo "</br>";
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART I – PERSONAL DETAILS:</div>"; 
					echo "</br>";
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">1. Name of Officer Travelling: <b> $fname $mname $lname</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">2. Personal Number:<b> $intpersonalno</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">3. Signature: <img height='60' width='60' src='img/signatures/$officer_id$sig_file_ext'/>  Date:  <b>$datecreated</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">3. Designation:  <b>$intdesignation</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">3. Department:  <b>$stationdepartment</b>  Tel:  <b>$inttelephone</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">4. Sector:  <b>$stationsector</b></div>"; 
					
					 
					echo "</br>";
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART II – TRAVEL DETAILS:</div>"; 
					echo "</br>";
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">6. Reason(s) for travel:  <b>$reasons</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">5. Country:  <b>$country</b>  City:  <b>$city</b></div>"; 
					
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Date and time of departure: <b>$departdate</b></div>"; 
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Date and time of return: <b>$returndate</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Mode of Travel (by air indicate class of travel): <b>$travelmode</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Person(s) Organization/Institution hosting the 
Meeting/Conference/Seminar/Workshop/Training: <b>$host</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Sponsor(s) of the Meeting/Conference/Seminar/Workshop/etc: <b>$sponsor</b></div>"; 
					echo "</br>";
					
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART III – COST OF THE TRIP AND SPONSOSRSHIP:</div>"; 
					echo "</br>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Subsistence:<b>$subsistence</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Sponsor:<b>$sponsor</b></div>"; 
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Accomodation:<b>$accomodation</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Participation:<b>$participation</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Travel expenses:<b>$travelexpenses</b></div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\">Total expenses:<b>$totalexpenses</b></div>"; 
					
					echo "</br>";
					 echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART IV – OBJECTIVES OF THE
MEETING/CONFERENCE/SEMINAR/WORKSHOP/TRAINING ETC:</div>"; 
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\"><b>$objectives</b></div>"; 
					echo "</br>";
				echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART V – EXPECTED BENEFITS OF THE 
MEETING/CONFERENCE/SEMINAR/WORKSHOP/TRAINING ETC:</div>"; 	
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;\"><b>$benefits</b></div>"; 
					echo "</br>";
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">PART VI – RECOMMENDATION/APPROVAL:</div>"; 	
					
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">A. H.E. THE DEPUTY GOVERNOR</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Recommendation:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature:     Date:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Reasons:</div>";
					
					echo "</br>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:800;\">B. H.E. THE GOVERNOR</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Approved/Not Approved:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Signature:     Date:</div>";
					
					echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">Reasons:</div>";
					
					
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
					
					
				echo "<p style=\"text-align:center;font-size:14px;\"><i>#TRANSFORMING NANDI</i></p>";	
					
					
					
					
					
                                       
					
					
				}
				
				//-----------------------------------------NON TRAVEL MEMO-------------------------------------------//
				else{
				
                                /*FIRST QUERY TO SELECT AND FILL IN THE SENDER/REQUESTOR*/
      $queryone="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.requestor = ememo_users.user_id WHERE requestor='$ses'";    
                                //execute query
if ($db->real_query($queryone)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $reqmemotype=$row["memotype"];
         $reqnature=$row["nature"];
         $reqfname=$row["fname"];
         $reqmname=$row["mname"];
         $reqlname=$row["lname"];
         $reqposition=$row["position"];
         $reqdepartment=$row["department"];
         $reqsector=$row["sector"];
         $reqdirectorate=$row["directorate"];
         $reqsection=$row["section"];
         $requnit=$row["unit"];
         $requser_id=$row["user_id"];
        $reqsig_file_ext=$row['sig_file_ext'];
         $requrgency=$row["urgency"];
        if($requrgency=='very high'){
            $urgency='Icon_red';
        }
        else if($requrgency=='high'){
              $urgency='Icon_orange';
        }
        else{
              $urgency='Icon_blue';
        }
        
         if($reqposition=='C.E.C.M'){
            $reqstation=$reqdepartment;
             $prefix="DEPARTMENT OF";
             $suffix=$reqstation;
        }
        else if($reqposition=='C.O'){
             $reqstation=$reqsector;
              $prefix=$reqstation;
             $suffix="SECTOR";
        }
         else if($reqposition=='DIR'){
             $reqstation=$reqdirectorate;
              $prefix= $reqstation;
             $suffix="DIRECTORATE";
        }
         else if($reqposition=='D.DIR'){
             $reqstation=$reqsection;
              $prefix= $reqstation;
             $suffix="SECTION";
        }
        else if($reqposition=='HOU'){
             $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
         else if($reqposition=='STAFF'){
            $reqstation=$requnit;
              $prefix=$reqstation;
              $suffix="UNIT";
        }
        else{
             $reqstation='County Government of Nandi';
        }
   
    }
  
    echo "<p style=\"margin-left:800px;margin-top:-10px;\"><b></p>";
    
}
                                
                                    
              /*SECOND QUERY TO SELECT AND FILL IN THE RECEIVER/RECIPIENT*/
                //check if it is broadcast
                if($reqmemotype=='broadcast'){
                    $recposition='';
                    $recstation=$reqnature;
                }
                //if not
                else{
      $querytwo="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.recepient = ememo_users.user_id WHERE requestor='$ses' AND nature='direct'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $recposition=$row["position"];
         $recdepartment=$row["department"];
        $recsector=$row["sector"];
        $recdirectorate=$row["directorate"];
        $recsection=$row["section"];
        $recunit=$row["unit"];
        $recnature=$row["nature"];
      
         if($recposition=='C.E.C.M'){
            $recstation=$recdepartment;
        }
        else if($recposition=='C.O'){
             $recstation=$recsector;
        }
         else if($recposition=='DIR'){
             $recstation=$recdirectorate;
        }
         else if($recposition=='D.DIR'){
             $recstation=$recsection;
        }
         else if($recposition=='HOU'){
             $recstation=$recunit;
        }
        else if($recposition=='STAFF'){
             $recstation=$recunit;
        }
        else{
             $recstation='County Government of Nandi';
        }
    }
}
                }
                                   
                                
 /***MAIN QUERY FOR THE MEMOBODY***/                               
$query="SELECT * FROM temp_memoholder WHERE requestor='$ses' LIMIT 1";                      
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $referenceno=$row["referenceno"];
        $d=$row["datecreated"];
        $introduction= $row["introduction"];  //str_ireplace('x', '&nbsp;',$row["introduction"]);
        $genstat=$row["generalstatus"];
        $prevmemo=$row["prevmemo"];
        $subject=$row["subject"];
        $datecreated=date("jS M, Y", strtotime($d));
        /*stamping approval status*/
    if($genstat=="Approved"){
        $stamp="approved";
    }
        else if($genstat=="Rejected"){
            $stamp="rejected";
        }
        else{
            $stamp="null";
        }
        
        
    }
}
                                     
                                   
else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  width=\"25\" height=\"25\" src=\"img/$urgency.png\"/>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;text-align:center;font-family:times;font-size:18px;padding:15;\"><b>COUNTY GOVERNMENT OF NANDI</b></td>
    </tr>
    </table>
    </div>";
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-15px;\">
     <table border=0 class=\"table\">
  
    <tr>
      <tr>
    <th style=\"width:313px;font-size:15px;font-family:times;padding-top:50px;\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TELEPHONE: 0535252355<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@nandi.go.ke<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website: www.nandi.go.ke</b></th>
    <th style=\"width:313px;text-align:center;\"><img width=\"120\" height=\"120\" src=\"img/nandiicon.png\"/></th>
    <th style=\"width:313px;text-align:left;font-family:times;padding-top:50px;font-size:15px;\"><b>P.O. BOX 802-30300<br>KAPSABET.</b></th>
    </tr>
    </tr>
    </table>
    </div>";   
                             
                                
     echo "<div class=\"table-responsive\" style=\"margin-left:25px;margin-right:25px;width:940px;margin-top:-10px;\">
     <table border=0 class=\"table\" width=\"500\">
    <tr>
    <td style=\"width:940px;text-transform:uppercase;text-align:center;font-family:times;font-size:18px;margin-left:60px;\"><b>$prefix $suffix</b></td>
    </tr>
    </table>
    </div>";                               
     echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-40px;\">
     <table border=0 class=\"table\">
    <tr>
    <td style=\"width:940px;font-weight:1000;text-align:center;font-size:20px;\"><hr style=\"height:1.6px;border:none;color:#000;background-color:#000;\"></td>
    </tr>
    </table>
    </div>";      
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-20px;text-transform:uppercase;\">
     <table border=0 class=\"table\">
  
    <tr>
    <th width=\"470px;\"><b>TO: $recposition $recstation</b></th>
    <th style=\"width:470px;text-align:left;\"><b>$referenceno</b></th>
    </tr>
    </table>
    </div>";
                                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\"><b>FROM: $reqposition $reqstation</b></th>
    <th style=\"width:370px;text-align:left;\"><b>DATE: $datecreated</b></th>
    </tr>
    </table>
     </div>";
                          
                 /**determine whether thr will be displayed**/
            $query ="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.recepient = ememo_users.user_id WHERE nature='Through' AND requestor=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $ses);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hideth=""; 
                  
                }
            else{
                $hideth="hidden";   
            }
        }
                  /****FILL IN Through below from****/           
  $querytwo="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.recepient = ememo_users.user_id WHERE requestor='$ses' AND nature='Through'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
         $recpositionthr=$row["position"];
         $recdepartmentthr=$row["department"];
        $recsectorthr=$row["sector"];
        $recdirectoratethr=$row["directorate"];
        $recsectionthr=$row["section"];
        $recunitthr=$row["unit"];
        
        if($recpositionthr=='C.E.C.M'){
            $recstationthr=$recdepartmentthr;
        }
        else if($recpositionthr=='C.O'){
             $recstationthr=$recsectorthr;
        }
         else if($recpositionthr=='DIR'){
             $recstationthr=$recdirectoratethr;
        }
         else if($recpositionthr=='D.DIR'){
             $recstationthr=$recsectionthr;
        }
        else if($recpositionthr=='HOU'){
             $recstationthr=$recunitthr;
        }
        else if($recpositionthr=='STAFF'){
             $recstationthr=$recunitthr;
        }
        else{
             $recstationthr='County Government of Nandi';
        }
                
        echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;margin-top:-30px;text-transform:uppercase;\">
    <table border=0 class=\"table\">
    <tr>
    <th width=\"370px;\" $hideth><b>THROUGH: $recpositionthr $recstationthr</b></th>
    </tr>
    </table>
     </div>";
        
    }
 
}      
                
                
    echo "<div class=\"table-responsive\" style=\"margin-left:55px;margin-right:55px;\">
    <table border=0 class=\"table\">
    <tr>
    <td style=\"text-transform:uppercase;font-weight:500;font-family:Segoe UI;\"><b><u>RE: $subject</u></b></td>
    </tr>
    </table>
     </div>";
                      echo "<div class=\"table-responsive\" style=\"margin-left:75px;margin-right:75px;font-size:14px;font-weight:200;\">$introduction</div>";     
                                        //.str_ireplace(' ', '&nbsp;',$introduction).
                     
                                             
                                        
                                        
     echo "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:55px;\">
    <table border=0 cellpadding=\"10\">
     <tr>
     <th><img  width=\"60\" height=\"60\" src=\"img/signatures/$requser_id$reqsig_file_ext\"/></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqfname $reqmname $reqlname</b></th>
     </tr>
     <tr>
     <th style=\"text-transform: uppercase;\"><b>$reqposition $reqstation</b></th>
     </tr>
     </table>
      </div>";
                                
                  
   
             
                       
                                        /**determine whether cc will be displayed**/
            $query ="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.recepient = ememo_users.user_id WHERE nature='Cc' AND requestor=?";   
        if($stmt = $db->prepare($query)){
                $stmt->bind_param('s', $ses);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                     $hidecc=""; 
                  
                }
            else{
                $hidecc="hidden";   
            }
        }
         
                                       
                
       echo  "<div class=\"table-responsive\" style=\"margin-left:65px;margin-right:75px;\">
     <table border=0 cellpadding=\"10\">
     <tr>
     <th $hidecc>Cc</th>
     </tr>
     
     </table>
      </div>";                         
                                        
         /****FILL IN CCs at the bottom****/           
   $querytwo="SELECT * FROM temp_memoholder Inner Join ememo_users on temp_memoholder.recepient = ememo_users.user_id WHERE requestor='$ses' AND nature='Cc'";    
                                //execute query
if ($db->real_query($querytwo)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
       
        $recpositioncc=$row["position"];
         $recdepartmentcc=$row["department"];
        $recsectorcc=$row["sector"];
        $recdirectoratecc=$row["directorate"];
        $recsectioncc=$row["section"];
        $recunitcc=$row["unit"];
        
        if($recpositioncc=='C.E.C.M'){
            $recstationcc=$recdepartmentcc;
        }
        else if($recpositioncc=='C.O'){
             $recstationcc=$recsectorcc;
        }
         else if($recpositioncc=='DIR'){
             $recstationcc=$recdirectoratecc;
        }
         else if($recpositioncc=='D.DIR'){
             $recstationcc=$recsectioncc;
        }
        else if($recpositioncc=='HOU'){
             $recstationcc=$recunitcc;
        }
        else if($recpositioncc=='STAFF'){
             $recstationcc=$recunitcc;
        }
        else{
             $recstationcc='County Government of Nandi';
        }
     
     
      
        echo "<p style=\"text-transform:uppercase;margin:-5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$recpositioncc .", ".$recstationcc."</p>"."<br>";
    }
}                    
                         echo "<p></p>";     
                         echo "<p style=\"color:white;\">.</p>"; 
                         echo "<p style=\"color:white;\">.</p>";
                         echo "<p style=\"color:white;\">.</p>"; 
                
      echo "<p style=\"text-align:center;\"><i>#TRANSFORMING NANDI</i></p>";                          
                    
				}
                                
$db->close();

                                        
                                        ?>
                       
                        </div>
                      </div><br/>
 <form action="memopreview.php" method="post">&nbsp;&nbsp;&nbsp;
     <div class="row quick-stats">
                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="memcreationbtns">
                          <button type="submit" class="btn btn-info" name="send"><i class="zmdi zmdi-mail-send"></i>&nbsp;Send Memo</button>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="memcreationbtns">
                            <div class="quick-stats__info">
                     <button type="submit" class="btn btn-danger" name="discard"><i class="zmdi zmdi-delete"></i>&nbsp;Discard Memo</button>
                            </div>
                        </div>
                    </div>

                </div>
        </form>

          <div class="row quick-stats">
              
                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="memcreationbtns">
                    
                            <a href="editmemo.php"><button type="submit" class="btn btn-outline-warning"><i class="zmdi zmdi-edit"></i>&nbsp;Edit Memo</button></a>
                        </div>
                    </div>

                  
                </div>
           
         
                <br/><br/><br/><br/><br/>
            </div>
        </div>
        
        

      
        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
    </section>
</main>

<script>
function futa(){
  //  $('#whi').hide();
 
}
    
</script>
<!-- Javascript -->
<!-- Vendors -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>
<script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
    
        <script src="vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>

<!-- Charts and maps-->
<script src="demo/js/flot-charts/curved-line.js"></script>
<script src="demo/js/flot-charts/line.js"></script>
<script src="demo/js/flot-charts/dynamic.js"></script>
<script src="demo/js/flot-charts/chart-tooltips.js"></script>
<script src="demo/js/other-charts.js"></script>
<script src="demo/js/jqvmap.js"></script>

<!-- App functions and actions -->
<script src="js/app.min.js"></script>
<script src="js/event-controller.js"></script>

    <!-- Demo -->

                              
        <script type="text/javascript">
        
            
            /*lost*/
        
            

            /*--------------------------------------
                Bootstrap Notify Notifications
            ---------------------------------------*/
            function notify(from, align, icon, type, animIn, animOut){
                $.notify({
                    icon: icon,
                    title: 'Bootstrap Notify',
                    message: 'Turning standard Bootstrap alerts into awesome notifications',
                    url: ''
                },{
                    element: 'body',
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: from,
                        align: align
                    },
                    offset: {
                        x: 20,
                        y: 20
                    },
                    spacing: 10,
                    z_index: 1031,
                    delay: 2500,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: false,
                    animate: {
                        enter: animIn,
                        exit: animOut
                    },
                    template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                                    '<span data-notify="icon"></span> ' +
                                    '<span data-notify="title">{1}</span> ' +
                                    '<span data-notify="message">{2}</span>' +
                                    '<div class="progress" data-notify="progressbar">' +
                                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                    '</div>' +
                                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                    '<button type="button" aria-hidden="true" data-notify="dismiss" class="close"><span>×</span></button>' +
                                '</div>'
                });
            }

            $('.notifications-demo > .btn').click(function(e){
                e.preventDefault();
                var nFrom = $(this).attr('data-from');
                var nAlign = $(this).attr('data-align');
                var nIcons = $(this).attr('data-icon');
                var nType = $(this).attr('data-type');
                var nAnimIn = $(this).attr('data-animation-in');
                var nAnimOut = $(this).attr('data-animation-out');

                notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
            });


            /*--------------------------------------
                Sweet Alert Dialogs
            ---------------------------------------*/

            // Basic
            $('#sa-basic').click(function(){
                swal({
                    title: "Here's a message!",
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Success Message
            $('#sa-success').click(function(){
                swal({
                    title: 'Good job!',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'success',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Success Message
            $('#sa-info').click(function(){
                swal({
                    title: 'Information!',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'info',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Warning Message
            $('#sa-warning').click(function(){
                swal({
                    title: 'Not a good sign...',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Question Message
            $('#sa-question').click(function(){
                swal({
                    title: 'Hmm.. what did you say?',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis',
                    type: 'question',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                })
            });

            // Warning Message with function
            $('#sa-funtion').click(function(){
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover this imaginary file!',
                    type: 'warning',
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonClass: 'btn btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                }).then(function(){
                    swal({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover this imaginary file!',
                        type: 'success',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-light',
                        background: 'rgba(0, 0, 0, 0.96)'
                    });
                });
            });

            // Custom Image
            $('#sa-image').click(function(){
                swal({
                    title: 'Sweet!',
                    text: "Here's a custom image.",
                    imageUrl: 'demo/img/thumbs-up.png',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-light',
                    confirmButtonText: 'Super!',
                    background: 'rgba(0, 0, 0, 0.96)'
                });
            });

            // Auto Close Timer
            $('#sa-timer').click(function(){
                swal({
                    title: 'Auto close alert!',
                    text: 'I will close in 2 seconds.',
                    timer: 2000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                });
            });
        </script>

</body>
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>