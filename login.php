<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    
    <script src="js/jquery-3.0.0.js"></script>

    
    </head>

    <body data-sa-theme="1">
      
        <div class="login" style="background-color:#ecebec;">
  <div class="login__block active">
<img width="80" height="80" src="img/nanditransold.png"/>
            <!-- Login -->
            <div class="login__block active" id="cbmemcreation">
                <h id="title">E-Memo</h>
                <div class="login__block__header">
                    <i id="meelogin" class="zmdi zmdi-account-circle"></i>
                    Sign in
<p id="uchi"></p>
                    <div class="actions actions--inverse login__block__actions">
                        <div class="dropdown">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-register" href="#"></a>
                                <a class="dropdown-item" data-sa-action="login-switch" data-sa-target="#l-forget-password" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="login__block__body">
                    <form action="login.php" method="post">
                        <div id="logindiv">
                    <div class="form-group">  
                       
                        <input  type="text" name="phone" class="form-control text-center" placeholder="Phone Number">
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control text-center" placeholder="Password">
                    </div>
                     <button class="btn btn-dark btn--icon" name="submit" ><i class="zmdi zmdi-key" onclick=""></i></button>
                    </div>
                        
                         
                        </form>
                    
                   
                    
                    
                    
                    
                    
                    
                    
<?php
include_once 'config.php';
     if(isset($_POST['submit'])){
         //before anything else unset the stubborn sessions
         unset($_SESSION['infilref']);
          unset($_SESSION['dirtyses']);
          unset($_SESSION['karibu']);
         
         
        $phone=$_POST['phone'];
        $password=$_POST['password'];
         
          //encrypting the password
        $salt='#!@*%';
        $pepper='*-#$QW';
        $password_1=hash('sha512',$salt.$password.$pepper);
         
 
           $query ="SELECT user_id, phone, password FROM ememo_users WHERE phone=? LIMIT 1";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $phone);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                         $stmt->bind_result($user_id, $phone, $db_password);
                         $stmt->fetch();
                    //if the password match
                    if(($password_1 == $db_password)){
                           //ASSIGN SESSION (-1 TO plus one later)
                          $_SESSION['welcome']=$user_id-1;
                        
                        //generate otp
                        	$otp = rand(100000,999999);
                        
                        //INSERT OTP in Database
                        
                         $sql="INSERT into otp_pass(otp,is_expired)VALUES(?,0)";
                        if($stmnt=$mysqli->prepare($sql)){
                            $stmnt->bind_param('s',$otp);
                            $stmnt->execute();
                            }
						
     //set sms parameters     
      $message="Your One Time Password(OTP) is $otp";                    
      $myarray=$phone;
     $newarray=array('message'=>$message,'recipients'=>$myarray);
   
                    //SEND USER text Message with OTP
    $ch = curl_init();
curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-Type:application/json', 'ApiKey:061be78ebe6941ceb5fb3ee01ed767af'));
curl_setopt($ch, CURLOPT_URL, "ttp://api.sematime.com/v1/1538372262372/messages");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($newarray));
//curl_setopt($ch, CURLOPT_POSTFIELDS, {"message":"hello world":"recipients":$p});
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$server_output = curl_exec($ch);
curl_close($ch);
                    
                    //REDIRECT user to OTP LOG IN
                        
                      
                    ?>
                                    <script type="text/javascript">
                                       
                                         window.location.href="login_otp.php";
                                     
                                    </script>
                                   <?php
                   //to remove error below index page                         
                  $_SESSION['infilref']="";           
       }
         else{
                        echo "<p style=\"color:#FC4A1A\">Access denied!, You entered the wrong password!</p>";
                    
                    }
                }
           else {
                    echo "<p style=\"color:#FC4A1A\">Credentials not registered</p>";
        }
        }
        else{
            throw new runtimeexception("Failed to execute query".$mysqli->error);
        }
     }
          
     
    ?>
                </div>
            </div>
            </div>
            </div>


        <!-- Javascript -->
           <script>
            
              
            </script>
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:42:46 GMT -->
</html>