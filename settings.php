<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">    
     <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendors/bower_components/flatpickr/dist/flatpickr.min.css" />
   <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="jquery-3.0.0.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    
    <script type="text/javascript">
     $(document).ready(function(){
    $('#votehead').hide();
         
            $('#directorategroup').hide();
            $('#sectiongroup').hide();
            $('#unitgroup').hide();
            $('#directoratebay').hide();
             $('#sectionbay').hide();
             $('#unitbay').hide();
         $('#fordir').hide();
         $('#forco').hide();
         $('#fordirview').hide();
         $('#createstaff').hide();
         //$('#profile').hide();
         $('#viewstaff').hide();
         $('#stationcreation').hide();
         
         $('#forcoview').hide();
          $('#posco').hide();
          $('#posdir').hide();
         
         $('#removedir').hide();
         $('#removesection').hide();
         $('#removeunit').hide();
         $('#removeuser').hide();
		 
		 $('.staffonly').hide();
     });
    </script>
    
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>


            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                              <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        <?php         
                                      require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
         $position=$row["position"];
		$staffpos=$row["staffposition"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row['section'];
        $stationunit=$row['unit'];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
                                     $jobdesc=$row['jobdesc'];
                                     $workstatus=$row['workstatus'];
                                     $phone=$row['phone'];
                                     $idnum=$row['idnum'];
                                     $pfno=$row['pfno'];
                                     $gender=$row['gender'];
                                     $acting=$row['acting'];
                                     $empstatus=$row['empstatus'];
                                     $ward=$row['ward'];
                                     $dob=$row['dob'];
                                     $email=$row['email'];
                                     $subcounty=$row['subcounty'];
                                     $designation=$row['designation'];
        
        
        
       if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
      
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>      
                        <div class="user__info" data-toggle="dropdown">
                           <?php
                            
                            echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                       
                              <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                        
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                       
                        
                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->
                      
                        <li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>


                         </ul>
                </div>
            </aside>

            <section class="content">
              
        <!-- ----------------------------------------TABBED ACTIVITY, TAB LINKS----------------------------------------- -->
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'mp')">Manage Profile</button>
  <button class="tablinks" onclick="openCity(event, 'mod')">Manage Office Delegation</button>
  <button class="tablinks" onclick="openCity(event, 'me')">Manage Employees</button>
     <button class="tablinks" onclick="openCity(event, 'mdsu')">Manage Directorates/Sections/Units</button>
     <button class="tablinks" onclick="openCity(event, 'mvhs')">Manage Vote Heads</button>
</div>

<!----------------------------------------                    MANAGE PROFILE                          ------------------------------------- -->
<div id="mp" class="tabcontent">
    
                    
           <div class="row quick-stats">   
			   
			                    
        <!----------------------------------------------------Profile View and Edit-------------------------------------------------------------->   
                    
                    <div class="col-sm-6 col-md-12" id="profile">
       <p id="profiletext">Profile</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                <p>This is your current profile in the system</p> </br>
                              <div class="row quick-stats">
                    <div class="col-sm-6 col-md-12">
                <div id="qstatspia" class="quick-stats__item" style="background-color:#ecebec;">
                     
                               <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                           <?php  echo "<img width=\"120\" height=\"120\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";  ?>
                               <h6 id="basicstats">Position: </h6><h id="profiletext"><?php echo $position." ".$station?></h><br/>
                                <h6 id="basicstats">Work Status: </h6><h id="profiletext"><?php echo $workstatus ?></h><br/>
                                
								<form action="settings.php" method="post" class="staffonly">
									 <h6 id="basicstats">Specific STAFF position: </h6>
									 <input style="color:black;" type="text" name="staffposition" value="<?php echo $staffpos ?>"required/><br/></br>
									
							<button type="submit" class="btn btn btn-sm" name="editstaffpos"><i class="zmdi zmdi-edit"></i>Update</button>
									<!--STAFF updates his own staff position-->
									<?php
	                                 require("./_connect.php");
								//connect to db
							$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
								 if(isset($_POST['editstaffpos'])){
	                                 $staffposi=$_POST['staffposition']; 
									 
							$sql = "UPDATE ememo_users SET staffposition='$staffposi' WHERE user_id='$ses'";
                             if($stmnt=$mysqli->prepare($sql)){
                              $stmnt->execute();
                            }
									 
								 }
	
										?>
									
								</form>
								<br/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                            <h6 id="basicstats">Name: </h6><h id="profiletext\"><?php echo $fname." ".$mname." ".$lname ?></h><br/>
                                <h6 id="basicstats\">Gender: </h6><h id="profiletext"><?php echo $gender ?></h><br/>
                                <h6 id="basicstats">D.O.B: </h6><h id="profiletext"><?php echo $dob ?></h><br/>
                                <h6 id="basicstats">ID: </h6><h6 id="profiletext"><?php echo $idnum ?></h6><br/>
                                <h6 id="basicstats">Email: </h6><h6 id="profiletext"><?php echo $email ?></h6><br/>
                                <h6 id="basicstats">Acting: </h6><h6 id="profiletext"><?php echo $acting ?></h6><br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                               <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                              <h6 id="basicstats">PF. No: </h6><h6 id="profiletext"><?php echo $pfno ?></h6><br/>
                                <h6 id="basicstats">Job Description: </h6><h6 id="profiletext"><?php echo $jobdesc ?></h6><br/>
                                 <h6 id="basicstats">Employment Status: </h6><h6 id="profiletext"><?php echo $empstatus ?></h6><br/>
                                  <h6 id="basicstats">Designation:</h6><h6 id="profiletext"> <?php echo $designation ?></h6>
                                   <h6 id="basicstats">Ward:</h6><h6 id="profiletext"><?php echo $ward ?></h6><br/>
                                  <h6 id="basicstats">Sub County:</h6><h6 id="profiletext"> <?php echo $subcounty ?></h6><br/>
                            </div>
                        </div>
                                   <form action="editprofile.php?nomad=<?php echo $ses;?>" method="post">
                                    <button id="fichaview" type="submit" class="btn btn-danger" name="edit"><i class="zmdi zmdi-edit"></i>&nbsp;Edit Profile</button></br><br/>
                                   </form>
                    </div>
                           
                        </div>
                        
                    </div>

                </div>
 
                            </div>
                           
                        </div>
                    </div>
			   
			   
			   
			  <!----------------------------------------------CHANGE PASSWORD---------------------------------------------> 
   <div class="col-sm-6 col-md-6">
       <p id="profiletext">Change password</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="settings.php">
             </br>
             
    
        <input type="password" name="password" class="form-control text-center" placeholder="Current Password" size=20  required/></br></br>
      <input type="password" name="newpassword" class="form-control text-center" placeholder="New Password" size=30  required/></br></br>
        <input type="password" name="cnewpassword" class="form-control text-center" placeholder="Confirm New Password" size=30  required/></br></br>
    
             
            <input type="submit" name="save"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;change&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
    
        </form>

 <?php
 require("./_connect.php");

//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
     if(isset($_POST['save'])){
        $password=$_POST['password'];
        $newpassword=$_POST['newpassword'];
        $cnewpassword=$_POST['cnewpassword'];
         
          //encrypting the password
        $salt='#!@*%';
        $pepper='*-#$QW';
        $password_1=hash('sha512',$salt.$password.$pepper);
         
 
           $query ="SELECT user_id, password FROM ememo_users WHERE user_id=? LIMIT 1";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $ses);
                $stmt->execute();
                $stmt->store_result();
            //To check if the row exists
                if($stmt->num_rows>=1){
                         $stmt->bind_result($db_user_id, $db_password);
                         $stmt->fetch();
                    //if the password match
                    if(($password_1 == $db_password)){
                        
                        if(($newpassword==$cnewpassword)){
                          //encrypting the password
                          $salt='#!@*%';
                          $pepper='*-#$QW';
                          $newestpass=hash('sha512',$salt.$newpassword.$pepper);
                            
                            $sql = "UPDATE ememo_users SET password='$newestpass' WHERE user_id='$ses'";
                            
                             if($stmnt=$mysqli->prepare($sql)){
                              $stmnt->execute();
                           ?>
                        <script>
                           alert('password successfully changed');
                            </script>
                           <?php
                                  
                            }
                            else{
                                echo"an error occurred querrying the database";
                            }
                        }
                            else{
                                
                          ?>
                        <script>
                           alert('passwords do not match!');
                            </script>
                           <?php
                            }
                        
                     
                       
       }
         else{
              echo "<p style=\"color:#FC4A1A\">Access denied!, You entered the wrong password!!</p>";           
           
                    
                    }
                }
           else {
                    echo "Credentials not registered";
        }
        }
        else{
            throw new runtimeexception("Failed to execute query".$mysqli->error);
        }
     }
        
     
    ?>
    
                         
                            </div>

                        </div>
                    </div>

<!----------------------------------------------------Upload profile picture-------------------------------------------------------------->     

                    <div class="col-sm-6 col-md-6">
                        <p id="profiletext">Upload profile picture</p>
                        <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                
                         <form action="settings.php" enctype="multipart/form-data" method="post">
                             <input type="file" class="btn btn-info" name="photo"/></br></br></br></br>
                               <input type="submit" name="upload"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Upload&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
                                </form>
                                
                                
                                <?php
                                if(isset($_POST['upload'])){
                                    
                       	$filename = $_FILES["photo"]["name"];
	$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
	$file_ext = substr($filename, strripos($filename, '.')); // get file name
	$filesize = $_FILES["photo"]["size"];
	$allowed_file_types = array('.png','.jpeg','.jpg');	

	if (in_array($file_ext,$allowed_file_types) && ($filesize < 20000000))
	{	
		// Rename file
		$newfilename =$ses. $file_ext;
		if (file_exists("upload/" . $newfilename))
		{
			// file already exists error
			echo "You have already uploaded this file.";
		}
		else
		{		
			move_uploaded_file($_FILES["photo"]["tmp_name"], "img/profilepics/" . $newfilename);
          
				 ?>
                        <script>
                           alert('File uploaded successfully');
                            </script>
                           <?php
          //save the file extension along side ememo_user record
            $sql1="UPDATE ememo_users SET dp_file_ext='$file_ext' WHERE user_id='$ses'";
           if($stmnt=$mysqli->prepare($sql1)){
            $stmnt->execute();      
     }  
		}
	}
	elseif (empty($file_basename))
	{	
		// file selection error
		echo "Please select a file to upload.";
	} 
	elseif ($filesize > 20000000)
	{	
		// file size error
		echo "The file you are trying to upload is too large.";
	}
	else
	{
		// file type error
		echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
		unlink($_FILES["file"]["tmp_name"]);
	}

                                }
                                ?>
                                
                            </div>

                        </div>
                        
                    </div>
<!----------------------------------------------------------Upload Signature----------------------------------------------------------->

        <div class="col-sm-6 col-md-6">
                        <p id="profiletext">Upload Signature</p>
                        <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                
                         <form action="settings.php" enctype="multipart/form-data" method="post">
                             <input type="file" class="btn btn-danger" name="photo"/></br></br></br></br>
                               <input type="submit" name="uploadsig"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Upload&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
                                </form>
                                
                                
                                <?php
                                if(isset($_POST['uploadsig'])){
                                    
                       	$filename = $_FILES["photo"]["name"];
	$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
	$file_ext = substr($filename, strripos($filename, '.')); // get file name
	$filesize = $_FILES["photo"]["size"];
	$allowed_file_types = array('.png','.jpeg','.jpg');	

	if (in_array($file_ext,$allowed_file_types) && ($filesize < 20000000))
	{	
		// Rename file
		$newfilename =$ses. $file_ext;
		if (file_exists("upload/" . $newfilename))
		{
			// file already exists error
			echo "You have already uploaded this file.";
		}
		else
		{		
			move_uploaded_file($_FILES["photo"]["tmp_name"], "img/signatures/" . $newfilename);
           
			 ?>
                        <script>
                           alert('File uploaded successfully');
                            </script>
                           <?php	
          //save the file extension along side ememo_user record
            $sql1="UPDATE ememo_users SET sig_file_ext='$file_ext' WHERE user_id='$ses'";
           if($stmnt=$mysqli->prepare($sql1)){
            $stmnt->execute();      
     }  
		}
	}
	elseif (empty($file_basename))
	{	
		// file selection error
		echo "Please select a file to upload.";
	} 
	elseif ($filesize > 20000000)
	{	
		// file size error
		echo "The file you are trying to upload is too large.";
	}
	else
	{
		// file type error
		echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
		unlink($_FILES["file"]["tmp_name"]);
	}

                                }
                                ?>
                                
                            </div>

                        </div>
                        
                    </div>
</div>
</div>


<!----------------------------------------           MANAGE OFFICE DELEGATION              ------------------------------------- -->

<div id="mod" class="tabcontent">
   <!---------------------------------------Delegate your office duties to another staff------------------------------------------------->               
     <div class="row quick-stats">     

                               <div class="col-lg-6">
                                    <p id="profiletext">Delegate your office duties to another staff</p>
                        <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                    <?php
                                include('./controller/class.php');
                             ?> 
                               <form action="settings.php" method="post">
                               <select name="firstrecepient" class="select2" id="recipient_emails" onchange="add_delegate()">
                                <option value="null">Choose a staff</option>
                                 <?php

                                  echo App::get_recipient_emails();

                               ?>
                            </select><br/><br/><br/>
                                  <p id="recipient_data" style="color:#FC4A1A"></p>
     <br/>
                        
                                      <button class="btn btn-success btn--icon " name="delegate"><i class="zmdi zmdi-key"></i></button>   <br/> <br/>
                                 </form>
                                  <?php
                                require("./_connect.php");
//connect to db
$conn = new mysqli($db_host,$db_user, $db_password, $db_name); 
                                   if(isset($_POST['delegate'])){
                                  $delegate_id=$_POST['firstrecepient'];
                                
                                //get the position and station of the delegated office
                                $query="SELECT * FROM ememo_users where user_id='$delegate_id'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $position=$row["position"];
                                         $stationdepartment=$row["department"];
                                        $stationsector=$row["sector"];
                                        $stationdirectorate=$row["directorate"];
                                        $stationsection=$row['section'];
                                        $stationunit=$row['unit'];
        

                                        if($position=='C.E.C.M'){
                                            $station=$stationdepartment;
                                        }
                                        else if($position=='C.O'){
                                             $station=$stationsector;
                                        }
                                         else if($position=='DIR'){
                                             $station=$stationdirectorate;
                                        }
                                          else if($position=='D.DIR'){
                                             $station=$stationsection;
                                        }
                                        else if($position=='HOU'){
                                             $station=$stationunit;
                                        }
                                        else if($position=='STAFF'){
                                             $station=$stationunit;
                                        }
                                        else{
                                             $station='County Government of Nandi';
                                        }
                                      $delegated_office=$position.", ".$station;
                                          
                                        }

                                }
                                //get the position and station of the delegating office
                                $query="SELECT * FROM ememo_users where user_id='$ses'";
                                //execute query
                                if ($conn->real_query($query)) {
                                    //If the query was successful
                                    $res = $conn->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $position=$row["position"];
                                         $stationdepartment=$row["department"];
                                        $stationsector=$row["sector"];
                                        $stationdirectorate=$row["directorate"];

                                        if($position=='C.E.C.M'){
                                            $station=$stationdepartment;
                                        }
                                        else if($position=='C.O'){
                                             $station=$stationsector;
                                        }
                                         else if($position=='DIR'){
                                             $station=$stationdirectorate;
                                        }
                                         else if($position=='D.DIR'){
                                             $station=$stationdirectorate;
                                        }
                                        else{
                                             $station='County Government of Nandi';
                                        }
                                    
                                      $delegating_office=$position.", ".$station;
                                    
                                        }

                                }
                              
                              
                                    $sqlogs="insert into delegates(delegate_id,delegated_by,delegated_office,delegating_office)VALUES(?,?,?,?)";
                                    if($stmnt=$mysqli->prepare($sqlogs)){
                                     $delegated_by=$_SESSION['karibu'];
                                       $stmnt->bind_param('ssss',$delegate_id,$delegated_by,$delegated_office,$delegating_office);
                                        $stmnt->execute();
                                       
                                         ?>
                        <script>
                           alert('Duties successfully delegated');
                            </script>
                           <?php
                                        
                                    }
                                 }
                                  ?>
                            </div>
                        </div>
                    </div>
               
<!---------------------------------------Staff you have delegated your office duties to.------------------------------------------------->        

                               <div class="col-lg-6">
                                    <p id="profiletext">Staff you have delegated your office duties to.(Click on the icon to revoke the  rights)</p>
                        <div class="card">
                            <div class="card-body" id="cbmemcreation">
                             <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="user_datadeleg" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                            </div>
                        </div>
                    </div>

</div>
</div>

<!----------------------------------------           MANAGE EMPLOYEES             ------------------------------------- -->
        
<div id="me" class="tabcontent">
      <div class="row quick-stats">  
          <!----------------------------------------------------------Create STAFF--------------------------------------------------------------->
 <div class="col-sm-6 col-md-6" id="createstaff">
       <p id="profiletext">Create Staff</p>
                       <div class="card">
                
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="settings.php">
                                       <div class="form-group">
             <input type="text" name="fname" class="form-control text-center" placeholder="First Name" required>
                    </div>
                        <div class="form-group">
                        <input type="text" name="mname" class="form-control text-center" placeholder="Middle Name" required>
                    </div>
                        <div class="form-group">
                        <input type="text" name="lname" class="form-control text-center" placeholder="Last Name" required>
                    </div>
                    
                        <div class="form-group">
                        <input type="text" name="phone" class="form-control text-center" placeholder="Phone Number" required>
                    </div>
                                       <div class="form-group">
                        <input type="text" name="pfno" class="form-control text-center" placeholder="PF No." required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="idnum" class="form-control text-center" placeholder="ID Number" required>
                    </div>
                                      
                         <div class="form-group">
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-teal"><input type="radio" name="gender" autocomplete="off" value="male" required></label> </br>Male &nbsp;&nbsp;&nbsp;
                        <label class="btn bg-cyan"><input type="radio" name="gender" autocomplete="off" value="female"></label></br>Female&nbsp;&nbsp;&nbsp;       
                         </div>
                        </div>
                           
                    <div class="form-group">
                        <input type="text" name="designation" class="form-control text-center" placeholder="Designation" required>
                    </div>
                           <div class="form-group">
                       <select name="empstatus" class="select2">
                               <option value="null">Choose employment status</option>
                                <option value="internship">Internship</option>
                                <option value="contract">Contract</option>
                                <option value="permanent">Permanent</option>
                               </select>
                    </div>
                 
                               <div class="form-group">  
                        <select name="position" class="select2" onChange="hideFields(this.value);">
                                <option value="null">Select Position</option>
                                <option value="<?php
                                               if($position=='DIR'){
                                               echo "null";
                                                   $text="--";
                                               }
                                               else if($position=='C.O'){
                                                   echo "DIR";
                                                   $text="Director";
                                               }
                                               ?>"><?php echo $text;?></option>
                                <option value="D.DIR">Deputy Director</option>
                                <option value="HOU">Head of Unit</option>
                                <option value="STAFF">Other Staff</option>
                               
                     </select>

                    </div>
                           
                         
                        
                        <div class="form-group" id="actinggroup"> 
                         <label class="custom-control custom-checkbox">
                                <input type="checkbox" name="acting" class="custom-control-input" value="Ag">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Acting</span>
                            </label>
                          
                                <input type="checkbox" name="acting" value="A" hidden="true">
                          </div>
                           
                           <div class="form-group">
                        <textarea name="jobdesc" class="form-control text-center" placeholder="Job Description" required></textarea>
                    </div>
                        
                            <div class="form-group">
                            <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                        <div class="form-group">
                                            <input type="text" name="dob" class="form-control datetime-picker" placeholder="Pick Date of Birth">
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                           </div>
                           
                           <div class="form-group">
                        <input type="text" name="email" class="form-control text-center" placeholder="Email(must be @nandicounty.go.ke or @nandi.go.ke)" required>
                    </div> 
                           
                    
                  
                           
                           <div class="form-group">
                        <input type="text" name="subcounty" class="form-control text-center" placeholder="Sub County where stationed(Optional)">
                    </div> 
                           
                           <div class="form-group">
                        <input type="text" name="ward" class="form-control text-center" placeholder="Ward(Optional)">
                    </div> 
                           
                          <div class="form-group">
                       <select name="workstatus" class="select2">
                               <option value="null">Choose Work status</option>
                             <option value="active">Active</option>
                                <option value="onleave">OnLeave</option>
                                <option value="outofstation">Outofstation</option>
                                <option value="inactive">Inactive</option>
                               </select>
                    </div>  
                           <?php
                           
                            $query="SELECT * from sectors_list WHERE sector='$stationsector' ORDER BY id DESC";
                        //execute query
                        if ($mysqli->real_query($query)) {
                            //If the query was successful
                            $res = $mysqli->use_result();
                            while ($row = $res->fetch_assoc()) {
                                $sector_id=$row["sector_id"];
                               

                            }
                        }
                           
                           ?>
                    <div class="form-group" id="directorategroup">  
                        <select name="directorate" class="select2" onChange="getSection(this.value);">
                                <option value="">Select Directorate</option>
                               	<?php
                            include_once 'connectdb.php';
                             
                            if($position=='DIR'){
                                 $query="SELECT * from directorates_list WHERE directorate='$stationdirectorate'";
                        //execute query
                        if ($dbhandle->real_query($query)) {
                            //If the query was successful
                            $res = $dbhandle->use_result();
                            while ($row = $res->fetch_assoc()) {
                                $directorate_id=$row["directorate_id"];
                               

                            }
                        }
                                  $sql1="SELECT * FROM directorates_list where directorate_id='$directorate_id'";
                            }
                            else{
                            $sql1="SELECT * FROM directorates_list where sector_id='$sector_id'";
                            }
                            $results=$dbhandle->query($sql1); 
                            while($rs=$results->fetch_assoc()) { 
                            ?>
                            <option value="<?php echo $rs["directorate_id"]; ?>"><?php echo $rs["directorate"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>

                    </div>
                        <div class="form-group" id="sectiongroup">  
                        <select name="section" id="section" class="select2" onChange="getUnit(this.value);">
                            <option value="">Select Section</option>
                            
                        </select>

                    </div>
                        <div class="form-group" id="unitgroup">  
                        <select name="unit" id="unit" class="select2">
                                <option value="">Select Unit</option>
                               
                            </select>

                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control text-center" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name ="confirmpass" class="form-control text-center" placeholder="Confirm Password" required>
                    </div>
                        
                
                        
                <input type="submit" name="savestaff"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
                        </form>

<?php
require("./_connect.php");

//connect to db
$mysqli= new mysqli($db_host,$db_user, $db_password, $db_name); 
include_once 'co_admin/user.php';
//create an instance of DB_con class, an object
$con= new DB_connect();
//insert data
if(isset($_POST['savestaff'])){
    $fname=$_POST['fname'];
    $mname=$_POST['mname'];
    $lname=$_POST['lname'];
    $phone=$_POST['phone'];
    $pfno=$_POST['pfno'];
    $idnum=$_POST['idnum'];
    $gender=$_POST['gender'];
    $designation=$_POST['designation'];
     $empstatus=$_POST['empstatus'];
    $jobdesc=$_POST['jobdesc'];
     $dob=$_POST['dob'];
    $email=$_POST['email'];
    $subcounty=$_POST['subcounty'];
     $ward=$_POST['ward'];
    $workstatus=$_POST['workstatus'];
    $position=$_POST['position']; 
    $department=$stationdepartment;
    $sector=$stationsector;
    $directorate=$_POST['directorate'];
    $section=$_POST['section'];
    $unit=$_POST['unit'];
    $password=$_POST['password'];
    $confirmpass=$_POST['confirmpass'];
    
   if (isset($_POST["acting"])) {
       $acting=$_POST['acting'];
}else{  
    $acting="";
}
    //checking if passwords match
    if($password==$confirmpass){
        
         //get the directoratename
           if($_POST['directorate']==''){
                       $directoratename=''; 
                    }
        else{
         
           $query2="SELECT * FROM directorates_list WHERE directorate_id='$directorate'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $directoratename=$row['directorate'];
         }
              
            }
        }
        
		  //get the sectionname
         if($_POST['section']==''){
                       $sectionname=''; 
                    }
        else{
            
           $query2="SELECT * FROM sections_list WHERE section_id='$section'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                   
                    $sectionname=$row['section'];
            
         }
              
            }
    }
        
        
        
        
       
        //making sure info is saved 
        if($con->insertAdmin($mysqli,$fname,$mname,$lname,$phone,$pfno,$idnum,$gender,$designation,$empstatus,$jobdesc,$dob,$email,$subcounty,$ward,$workstatus,$position,$acting,$department,$sector,$directoratename,$sectionname,$unit,$password)== true){
            
               $con->set_sign($mysqli);
        }
        
   
        
    }
    else{
         ?>
<script>
alert("please make sure you match the passwords, try again!!");
</script>
<?php
       
    }
    
    
}

    

?>
 
    
                         
                            </div>

                        </div>
                    </div>

<!---------------------------------------------------REMOVE USER--------------------------------------> 
<div class="col-sm-6 col-md-6" id="removeuser">
                         <?php
if($position=='C.O'){
    
    $removed="user_datausers";
    
}
else if($position=='DIR'){
      $removed="user_datausers";          
}
else{
    $removed="";
}

?>
                     <p id="profiletext">Remove/Edit User</p>
                     <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="<?php echo $removed ?>" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                    </div> 

<!--------------------------------------------------VIEW YOUR STAFF------------------------------------------------------------->

   <div class="col-sm-6 col-md-12" id="viewstaff">
       <p id="profiletext">View Staff belonging to your Sector</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                <p>Choose the station you would like to view staff from</p> </br>
                                <div class="form-group" id='forcoview'>
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-red"><input type="radio" name="stationtype" autocomplete="off" value="directorate" onchange="getDirectorateStaff(this.value);"></label> </br>Directorate &nbsp;&nbsp;&nbsp;
                        <label class="btn bg-cyan"><input type="radio" name="stationtype" autocomplete="off" value="section" onchange="getSectionStaff(this.value);"></label></br>Section&nbsp;&nbsp;&nbsp;
                                  <label class="btn bg-teal"><input type="radio" name="stationtype" autocomplete="off" value="unit" onchange="getUnitStaff(this.value);"></label></br>Unit&nbsp;&nbsp;&nbsp; 
                         </div>
                        </div>
<!--for the director-->
   <div class="form-group" id='fordirview'>
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-cyan"><input type="radio" name="stationtype" autocomplete="off" value="section" onchange="getSectionStaff(this.value);"></label></br>Section&nbsp;&nbsp;&nbsp;
                                  <label class="btn bg-teal"><input type="radio" name="stationtype" autocomplete="off" value="unit" onchange="getUnitStaff(this.value);"></label></br>Unit&nbsp;&nbsp;&nbsp; 
                         </div>
                        </div>
                             <div id="directoratestaff"></div>
 
                            </div>
                           
                        </div>
                    </div>

    </div>

</div>         

<!---------------------------------------- MANAGE DIRECTORATES/SECTIONS/UNITS--------------------------------->
        
<div id="mdsu" class="tabcontent">
      <div class="row quick-stats"> 
          <!--------------------------------------------------Create Directorate/Section/Unit----------------------------------------------------------->
  <div class="col-sm-6 col-md-6" id="stationcreation">
       <p id="profiletext">Create Directorate/Section/Unit</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  
             </br>
             <p>Choose the station you would like to create. NOTE: You can only create a Section under an existing Directorate and a Unit under an existing Section</p> </br>
                                <div class="form-group" id='forco'>
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-red"><input type="radio" name="stationtype" autocomplete="off" value="directorate"></label> </br>Directorate &nbsp;&nbsp;&nbsp;
                        <label class="btn bg-cyan"><input type="radio" name="stationtype" autocomplete="off" value="section"></label></br>Section&nbsp;&nbsp;&nbsp;
                                  <label class="btn bg-teal"><input type="radio" name="stationtype" autocomplete="off" value="unit"></label></br>Unit&nbsp;&nbsp;&nbsp; 
                         </div>
                        </div>
<!--for the director-->
<div class="form-group" id='fordir'>
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-cyan"><input type="radio" name="stationtype" autocomplete="off" value="section"></label></br>Section&nbsp;&nbsp;&nbsp;
                                  <label class="btn bg-teal"><input type="radio" name="stationtype" autocomplete="off" value="unit"></label></br>Unit&nbsp;&nbsp;&nbsp; 
                         </div>
                        </div>
    <form method="post" action="settings.php"  id="directoratebay">
           <div class="form-group">
        <input type="text" name="directorate" class="form-control text-center" placeholder="Enter name of Directorate"  required/></br></br>
        <input type="text" name="directorate_id" class="form-control text-center" placeholder="Enter Code(in the format XYZ, xyz being initials of directorates name)"  required/></br></br>
              </div>
<input type="submit" name="createdir"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
</form>
<form method="post" action="settings.php"  id="sectionbay">
    
                           <select name="directorate" class="select2">
                                           <option value="null">Select Directorate to insert Section</option>
                                         <?php
                                            require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
                               
     $query="SELECT * from sectors_list WHERE sector='$stationsector' ORDER BY id DESC";
                    
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $sector_id=$row["sector_id"];
       
    }
}
 if($position=='DIR'){
    $query="SELECT * from directorates_list WHERE directorate='$stationdirectorate' ORDER BY id DESC";
     }
     else{                              
 $query="SELECT * from directorates_list WHERE sector_id='$sector_id' ORDER BY id DESC";
     }
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $directorate=$row["directorate"];
        $directorate_id=$row["directorate_id"];
       
                               echo "<option value=\"$directorate_id\">$directorate</option>";
    }
}

$db->close();
?>                                      
                                
    
</select></br></br>
    
<div class="form-group" >
        <input type="text" name="section" class="form-control text-center" placeholder="Enter name of Section"  required/></br></br>
        <input type="text" name="section_id" class="form-control text-center" placeholder="Enter Code(in the format XYZ, xyz being initials of sections name)"  required/></br></br>
              </div>
<input type="submit" name="createsec"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
</form>
<form method="post" action="settings.php" id="unitbay">
    
      <select name="section" class="select2">
                                           <option value="empty">Select Section to insert Unit</option>
                                         <?php
                                            require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
     $query="SELECT * from sectors_list WHERE sector='$stationsector' ORDER BY id DESC";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $sector_id=$row["sector_id"];
       
    }
}
          $db = new mysqli($db_host,$db_user, $db_password, $db_name); 
     $query="SELECT * from directorates_list WHERE directorate='$stationdirectorate' ORDER BY id DESC";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $directorate_id=$row["directorate_id"];
       
    }
}
          
                if($position=='DIR'){
                                    $query="SELECT * from sections_list WHERE directorate_id='$directorate_id' ORDER BY id DESC";
                               }
                               else{               
 $query="SELECT * from sections_list WHERE sector_id='$sector_id' ORDER BY id DESC";
                               }
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $section=$row["section"];
        $section_id=$row["section_id"];
       
                               echo "<option value=\"$section_id\">$section</option>";
    }
}

$db->close();
?>                                      
                                
    
</select></br></br>
    
    
    
<div class="form-group">
        <input type="text" name="unit" class="form-control text-center" placeholder="Enter name of Unit"  required/></br></br>
        <input type="text" name="unit_id" class="form-control text-center" placeholder="Enter Code(in the format XYZ, xyz being initials of units name)"  required/></br></br>
              </div>
    
             
            <input type="submit" name="createduni"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
    
        </form>

 <?php
 require("./_connect.php");

//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
     if(isset($_POST['createdir'])){
   
         $directorate=$_POST["directorate"];
          $directorate_id="DIR".$_POST["directorate_id"];
        
         
         //get departmen_id and sector id of the creating C.O
                      $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($mysqli->real_query($query)) {
                                    //If the query was successful
                                    $res = $mysqli->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $department_id=$row["department_id"];
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
      
         
         /**insert **/
         $sqlogs="insert into directorates_list(directorate,department_id,sector_id,directorate_id)VALUES(?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
   $stmnt->bind_param('ssss',$directorate,$department_id,$sector_id,$directorate_id);
    $stmnt->execute();
    ?>
<script>
   alert('Station successfully created');
    </script>
   <?php
}
         
         
     }

if(isset($_POST['createsec'])){
    
         $section=$_POST["section"];
          $section_id="SEC".$_POST["section_id"];
         $directorate=$_POST["directorate"];
        
         
         //get departmen_id and sector id of the creating C.O
                      $query="SELECT * FROM directorates_list where directorate_id='$directorate'";
                                //execute query
                                if ($mysqli->real_query($query)) {
                                    //If the query was successful
                                    $res = $mysqli->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $department_id=$row["department_id"];
                                         $sector_id=$row["sector_id"];
                                         $directorate_id=$row["directorate_id"];
                                        
                                }
                                }
      
         
         /**insert **/
         $sqlogs="insert into sections_list(section,department_id,sector_id,directorate_id,section_id)VALUES(?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
   $stmnt->bind_param('sssss',$section,$department_id,$sector_id,$directorate_id,$section_id);
    $stmnt->execute();
    ?>
<script>
   alert('Station successfully created');
    </script>
   <?php
}
          
    
}

if(isset($_POST['createduni'])){
            $unit=$_POST["unit"];
          $unit_id="UNI".$_POST["unit_id"];
         $section=$_POST["section"];
        
         
         //get departmen_id and sector id of the creating C.O
                      $query="SELECT * FROM sections_list where section_id='$section'";
                                //execute query
                                if ($mysqli->real_query($query)) {
                                    //If the query was successful
                                    $res = $mysqli->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $department_id=$row["department_id"];
                                         $sector_id=$row["sector_id"];
                                         $directorate_id=$row["directorate_id"];
                                        $section_id=$row["section_id"];
                                        
                                }
                                }
      
         
         /**insert **/
         $sqlogs="insert into units_list(unit,department_id,sector_id,directorate_id,section_id,unit_id)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
   $stmnt->bind_param('ssssss',$unit,$department_id,$sector_id,$directorate_id,$section_id,$unit_id);
    $stmnt->execute();
    ?>
<script>
   alert('Station successfully created');
    </script>
   <?php
}
 
    
}
     
    ?>
    
                         
                            </div>

                        </div>
                    </div>

<!---------------------------------------------------REMOVE DIRECTORATE--------------------------------------> 



                <div class="col-sm-6 col-md-6" id="removedir">
                     <?php
if($position=='C.O'){
    
    $removea="user_datadir";
    
}
else if($position=='DIR'){
      $removea="user_datadir";          
}
else{
    $removea="";
}

?>
       <p id="profiletext">Remove Directorate</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="<?php echo $removea?>" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                     </div> 

<!---------------------------------------------------REMOVE SECTION--------------------------------------> 
   <div class="col-sm-6 col-md-6" id="removesection">
                        <?php
if($position=='C.O'){
    
    $removeb="user_datasection";
    
}
else if($position=='DIR'){
      $removeb="user_datasection";          
}
else{
    $removeb="";
}

?>
                     <p id="profiletext">Remove Section</p>
                     <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="<?php echo $removeb ?>" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                    </div> 


<!---------------------------------------------------REMOVE UNIT--------------------------------------> 
<div class="col-sm-6 col-md-6" id="removeunit">
                        <?php
if($position=='C.O'){
    
    $removec="user_dataunit";
    
}
else if($position=='DIR'){
      $removec="user_dataunit";          
}
else{
    $removec="";
}

?>
                     <p id="profiletext">Remove Unit</p>
                     <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <div class="container box">
   
   <div class="table-responsive">
   <br />
    
    <br />
    <div id="alert_message"></div>
    <table id="<?php echo $removec ?>" class="table">
     <thead>
      <tr>
       <th></th>
       <th></th>
      </tr>
     </thead>
     
    </table>
   </div>
  </div>
                         
                            </div>

                        </div>
                    </div> 


    </div>
</div>

<!---------------------------------------- MANAGE VOTE HEADS--------------------------------->

<div id="mvhs" class="tabcontent">
      <div class="row quick-stats"> 
          
          <!----------------------------------------------CREATE VOTEHEAD-------------------------------------------------------------->
                     
              <div class="col-sm-6 col-md-6" id="votehead">
       <p id="profiletext">Create Vote Head</p>
                       <div class="card">
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="settings.php">
             </br>
             
      <div class="form-group">
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-cyan"><input type="radio" name="vhtype" autocomplete="off" value="development" required></label></br>Development&nbsp;&nbsp;&nbsp;
                                  <label class="btn bg-teal"><input type="radio" name="vhtype" autocomplete="off" value="recurrent" required></label></br>Recurrent&nbsp;&nbsp;&nbsp; 
                         </div>
                        </div>
        <input type="text" name="code" class="form-control text-center" placeholder="District/Location Code"  required/></br></br>
      <input type="text" name="item_source" class="form-control text-center" placeholder="Item-Source" size=30  required/></br></br>
        <input type="text" name="title_and_details" class="form-control text-center" placeholder="Title and Details" size=30  required/></br></br>
 <input type="number" name="printedest" class="form-control text-center" placeholder="Printed Estimates" size=30  required/></br></br> <input type="number" name="approvedest" class="form-control text-center" placeholder="Approved Estimates" size=30  required/></br></br>
    
             
            <input type="submit" name="create"  class="btn btn-info"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
    
        </form>

 <?php
  require("./_connect.php");
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
     if(isset($_POST['create'])){
         $type=$_POST["vhtype"];
          $code=$_POST["code"];
          $item_source=$_POST["item_source"];
          $title_and_details=$_POST["title_and_details"];
          $printedest=$_POST["printedest"];
          $approvedest=$_POST["approvedest"];
		 $used=0;
         
         //get department and sector id
                      $query="SELECT * FROM sectors_list where sector='$stationsector'";
                                //execute query
                                if ($mysqli->real_query($query)) {
                                    //If the query was successful
                                    $res = $mysqli->use_result();
                                while ($row = $res->fetch_assoc()) {
                                        $department_id=$row["department_id"];
                                         $sector_id=$row["sector_id"];
                                        
                                }
                                }
         
         
         
          /**insert vote head**/
         $sqlogs="insert into vote_heads(Code,Item_source,Title_and_details,Type,Printed_estimates,Approved_estimates,Used,Balance,department_id,sector_id)VALUES(?,?,?,?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
  $stmnt->bind_param('ssssssssss',$code,$item_source,$title_and_details,$type,$printedest,$approvedest,$used,$printedest,$department_id,$sector_id);
    $stmnt->execute();
                        ?>
                        <script>
                           alert('Vote head successfully created');
                            </script>
                           <?php
   
   
}
         
     }
        
     
    ?>
    
                         
                            </div>

                        </div>
                    </div>
    </div>
</div>


                <div class="row quick-stats">
   
                    
                    
                    
                    
                    
                    
                    
                    
   
  
                       

          <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> 












                    </div>
 

        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
            </section>
<script>
/******************TABBED ACTIVITY************************/
function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}       

</script>

<script>    
       
             function hideFields(val){
               if(val=='DIR'){
                       $('#directorategroup').show();
                    $('#sectiongroup').hide();
                       $('#unitgroup').hide();
                 }
                 else if(val=="D.DIR"){
                        $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').hide();
             
                 }
                 else if(val=="HOU"){
                         $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').show();
                 }
                  else if(val=="STAFF"){
                         $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').show();
                 }
                 else{
                       $('#directorategroup').hide();
                    $('#sectiongroup').hide();
                       $('#unitgroup').hide();
                 }
             }
                
function getSection(val) {
	$.ajax({
	type: "POST",
	url: "co_admin/get_section.php",
	data:'directorate_id='+val,
	success: function(data){
		$("#section").html(data);
	}
	});
}
          
function getUnit(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_unit.php",
	data:'section_id='+val,
	success: function(data){
		$("#unit").html(data);
	}
	});  
}                

       /***to view staff***/
    function getDirectorateStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_directoratestaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       function getSectionStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_sectionstaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       function getUnitStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_unitstaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       
</script>



<script>
 
$("#stationcreation").mouseover(function() {
       var stationtype=document.querySelector('input[name="stationtype"]:checked').value;
      if(stationtype=='directorate'){
        $("#directoratebay").show(); 
           $("#sectionbay").hide(); 
           $("#unitbay").hide(); 
      }
    else if(stationtype=='section'){
        
           $("#directoratebay").hide(); 
          $("#sectionbay").show();
        $("#unitbay").hide(); 
    }
     else if(stationtype=='unit'){
          $("#directoratebay").hide(); 
          $("#sectionbay").hide(); 
          $("#unitbay").show(); 
     }
});

</script>


  <script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datadeleg').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"co_admin/release_acc/fetch.php",
     type:"POST"
    }
   });
  }
  
 
  
  $(document).on('click', '.delete', function(){
   var iddeleg = $(this).attr("iddeleg");
 
    $.ajax({
     url:"co_admin/release_acc/delete.php",
     method:"POST",
     data:{iddeleg:iddeleg},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datadeleg').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
</script>

<script>
function add_delegate(){
   
    $("#recipient_data").append("If you are sure you want to delegate your duties to the staff you just selected click the icon below");
}


</script>
      

<!--CHATS-->
<script>
$(document).ready(function(){
 var pos='<?php echo $position?>';
    if(pos=='C.O'){
        $('#votehead').show();
        $('#stationcreation').show();
        $('#createstaff').show();
        $('#forco').show();
        $('#forcoview').show();
         $('#viewstaff').show();
         $('#posco').show();
          $('#removedir').show();
        $('#removesection').show();
        $('#removeunit').show();
          $('#removeuser').show();
    }
    else if(pos=='DIR'){
        $('#profile').show();
        $('#stationcreation').show();
        $('#createstaff').show();
        $('#fordir').show();
        $('#fordirview').show();
        $('#viewstaff').show();
         $('#posdir').show();
        $('#removesection').show();
        $('#removeunit').show();
         $('#removeuser').show();
        
    }
    else if(pos=='D.DIR'){
         $('#profile').show();
    }
    else if(pos=='HOU'){
         $('#profile').show();
    }
    else if(pos=='STAFF'){
         $('#profile').show();
		$('.staffonly').show();
    }
    
});
</script>        

<!--delete-->
<script type="text/javascript" language="javascript">
    //------------------------------------fetch and delete directorate
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datadir').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"co_admin/display_deletestations/fetchdir.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var iddir = $(this).attr("iddir");

    $.ajax({
     url:"co_admin/display_deletestations/deletedir.php",
     method:"POST",
     data:{iddir:iddir},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datadir').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
     
    //------------------------------------fetch and delete section
    $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datasection').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"co_admin/display_deletestations/fetchsection.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var idsection = $(this).attr("idsection");
 
    $.ajax({
     url:"co_admin/display_deletestations/deletesection.php",
     method:"POST",
     data:{idsection:idsection},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datasection').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
    
     //------------------------------------fetch and delete unit
    $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_dataunit').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"co_admin/display_deletestations/fetchunit.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var idunit = $(this).attr("idunit");
 
    $.ajax({
     url:"co_admin/display_deletestations/deleteunit.php",
     method:"POST",
     data:{idunit:idunit},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_dataunit').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });

    
    
      //------------------------------------fetch and delete user
    $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_datausers').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"co_admin/display_deletestations/fetchusers.php",
     type:"POST"
    }
   });
  }
   $(document).on('click', '.delete', function(){
   var idusers = $(this).attr("idusers");
 
    $.ajax({
     url:"co_admin/display_deletestations/deleteusers.php",
     method:"POST",
     data:{idusers:idusers},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_datausers').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
    
    
    
</script>


<!--PREVENT FORM RESUBMISSIOM-->
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>



<!--------LOG USER OUT AFTER 5 MINUTES OF INACTIVITY----------->
<script>


</script>



<!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="vendors/bower_components/flatpickr/dist/flatpickr.min.js"></script>
        <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

   <script src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

       
        <script src="js/rChat.js"></script>
        <script src="js/rChatSent.js"></script>

        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>