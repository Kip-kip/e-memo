<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>E-Memo</title>

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">    
     <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendors/bower_components/flatpickr/dist/flatpickr.min.css" />
   <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="jquery-3.0.0.js"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
    
    <script type="text/javascript">
     $(document).ready(function(){
    $('#votehead').hide();
         
            $('#directorategroup').hide();
            $('#sectiongroup').hide();
            $('#unitgroup').hide();
            $('#directoratebay').hide();
             $('#sectionbay').hide();
             $('#unitbay').hide();
     });
    </script>
    
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>


            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                              <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
            <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        <?php         
                                      require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
        $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row['section'];
        $stationunit=$row['unit'];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
      
      
              
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>      
                        <div class="user__info" data-toggle="dropdown">
                           <?php
                            
                            echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                       
                              <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>

                        
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                       
                        
                       
                      
                        <li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>


                         </ul>
                </div>
            </aside>

            <section class="content">
              
<!----------------------------GET USER DETAILS TO EDIT---------------------------->
 <?php
  require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name);                
//RECEIVED MESSAGE    
$user=$_GET['nomad'];
$query="SELECT * FROM ememo_users where user_id='$user'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
        
                                    $userposition=$row["position"];
                                    $acting=$row["acting"];
                                     $originaldepartment=$row["department"];
                                    $originalsector=$row["sector"];
                                    $originaldirectorate=$row["directorate"];
                                    $originalsection=$row['section'];
                                    $originalunit=$row['unit'];
                                    $user_id=$row['user_id'];
                                     $fname=$row['fname'];
                                     $mname=$row['mname'];
                                     $lname=$row['lname'];
        
                                     $jobdesc=$row['jobdesc'];
                                     $workstatus=$row['workstatus'];
                                     $phone=$row['phone'];
                                     $idnum=$row['idnum'];
                                     $pfno=$row['pfno'];
                                     $gender=$row['gender'];
                                     $empstatus=$row['empstatus'];
                                     $ward=$row['ward'];
                                     $dob=$row['dob'];
                                     $email=$row['email'];
                                     $subcounty=$row['subcounty'];
                                     $designation=$row['designation'];
        
        
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>      
                
                
             
                
                <div class="row quick-stats">
                    
        
<!----------------------------------------------------------Update profile--------------------------------------------------------------->
 <div class="col-sm-6 col-md-12" id="createstaff">
	 
	  <a href ="settings.php"><button class="btn btn-dark btn--icon"><i class="zmdi zmdi-arrow-back"></i></button></a>
	 
	 
       <p id="profiletext">Update Profile</p>
                       <div class="card">
                
                            <div class="card-body"  id="cbmemcreation">
                                  <form method="post" action="editprofile.php?nomad=<?php echo $_GET['nomad'];?>">
                                      <!--hidden field for user's id and stations-->
                                      <input type="text" name="person" class="form-control text-center" placeholder="" value="<?php echo $user_id; ?>" hidden>
									  <input type="text" name="originaldepartment" class="form-control text-center" placeholder="" value="<?php echo $originaldepartment; ?>" hidden>
									  <input type="text" name="originalsector" class="form-control text-center" placeholder="" value="<?php echo $originalsector; ?>" hidden>
									   <input type="text" name="originaldirectorate" class="form-control text-center" placeholder="" value="<?php echo $originaldirectorate; ?>" hidden>
									   <input type="text" name="originalsection" class="form-control text-center" placeholder="" value="<?php echo $originalsection; ?>" hidden>
									  <input type="text" name="originalunit" class="form-control text-center" placeholder="" value="<?php echo $originalunit; ?>" hidden>
             <div class="form-group">
				 <label>First Name:</label>
                        <input type="text" name="fname" class="form-control text-center" placeholder="First Name" value="<?php echo $fname; ?>" required>
                    </div>
                                      
                        <div class="form-group">
							<label>Second Name:</label>
                        <input type="text" name="mname" class="form-control text-center" placeholder="Middle Name" value="<?php echo $mname; ?>" required>
                    </div>
                                      
                        <div class="form-group">
							<label>Last Name:</label>
                        <input type="text" name="lname" class="form-control text-center" placeholder="Last Name" value="<?php echo $lname; ?>" required>
                    </div>
                    
                        <div class="form-group">
							<label>Phone NO:</label>
                        <input type="text" name="phone" class="form-control text-center" placeholder="Phone Number" value="<?php echo $phone; ?>"  required>
                    </div>
                                      
                         <div class="form-group">
							 <label>PF NO:</label>
                        <input type="text" name="pfno" class="form-control text-center" placeholder="PF No." value="<?php echo $pfno; ?>"  required>
                    </div>
                    <div class="form-group">
						<label>ID NO:</label>
                        <input type="text" name="idnum" class="form-control text-center" placeholder="ID Number"  value="<?php echo $idnum; ?>" required>
                    </div>
                                      
                    <div class="form-group">
                        <div class="btn-group btn-group--colors" data-toggle="buttons">
                        <label class="btn bg-teal <?php if($gender=='male'){echo "active";}else{ echo "null";}?>"><input type="radio" name="gender" value="male"></label> </br>Male &nbsp;&nbsp;&nbsp;
                        <label class="btn bg-cyan <?php if($gender=='female'){echo "active";}else{ echo "null";}?>"><input type="radio" name="gender" autocomplete="on" value="female"></label></br>Female&nbsp;&nbsp;&nbsp;       
                         </div>
                           </div>
                           
                           
                    <div class="form-group">
						<label>Designation:</label>
                        <input type="text" name="designation" class="form-control text-center" placeholder="Designation" value="<?php echo $designation; ?>" required>
                    </div>   
                           
                    <div class="form-group">
						<label>Employment Status:</label>
                       <select name="empstatus" class="select2" required>
                           <!--autoload-->
                               <option value="null">Choose employment status</option>
                                <option  value="internship" <?php if($empstatus=='internship'){echo "selected";}else{ echo "null";}?>>Internship</option>
                                <option   value="contract" <?php if($empstatus=='contract'){echo "selected";}else{ echo "null";}?>>Contract</option>
                                <option value="permanent" <?php if($empstatus=='permanent'){echo "selected";}else{ echo "null";}?>>Permanent</option>
                               </select>
                    </div>
                    
                            <div class="form-group" <?php 
                                 
                                 if(($position=='HOU') OR ($position=='STAFF') OR ($position=='D.DIR') OR ($position=='C.E.C.M') OR ($position=='D.C.S') OR ($position=='C.S') OR ($position=='D.G') OR ($position=='GO')){
                                               echo "hidden";
                        
                                               }
                                
                                 ?>>  
								<label>Position:</label>
                        <select name="position" class="select2" onChange="hideFields(this.value);">
							
                                <option value="null">Select Position</option>
                                <option value="DIR" <?php if($userposition=='DIR'){echo "selected";}else{ echo "null";}?>>Director</option>
                                <option value="D.DIR" <?php if($userposition=='D.DIR'){echo "selected";}else{ echo "null";}?>>Deputy Director</option>
                                <option value="HOU" <?php if($userposition=='HOU'){echo "selected";}else{ echo "null";}?>>Head of Unit</option>
                                <option value="STAFF" <?php if($userposition=='STAFF'){echo "selected";}else{ echo "null";}?>>Other Staff</option>
                               
                     </select>

                    </div>
                        
                             <div class="form-group" id="actinggroup"> 
                         <label class="custom-control custom-checkbox">
                                <input type="checkbox" name="acting" class="custom-control-input" value="Ag"<?php if($acting=='Ag'){echo "checked";}else{ echo "null";}?>>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Acting</span>
                            </label>
                          
                               <input type="checkbox" name="acting" value="A" hidden="true">
                                
                            
                         </div>
                           
                    <div class="form-group">
						<label>Job Description:</label>
                        <textarea name="jobdesc" class="form-control text-center" placeholder="Job Description" required><?php echo $jobdesc; ?></textarea>
                    </div>
                        
                            <div class="form-group">
                            <div class="input-group">
								<label>DOB:</label>
                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                        <div class="form-group">
                                            <input type="text" name="dob" class="form-control datetime-picker" placeholder="Pick Date of Birth" value="<?php echo $dob; ?>"  required>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                           </div>
                           
                           <div class="form-group">
							   <label>Email:</label>
                        <input type="text" name="email" class="form-control text-center" placeholder="Email(must be @nandicounty.go.ke or @nandi.go.ke)" value="<?php echo $email; ?>"  required>
                    </div> 
                           
                    
                  
                           
                           <div class="form-group">
							   <label>Sub County:</label>
                        <input type="text" name="subcounty" class="form-control text-center" placeholder="Sub County where stationed(Optional)" value="<?php echo $subcounty; ?>" >
                    </div> 
                           
                           <div class="form-group">
							   <label>Ward:</label>
                        <input type="text" name="ward" class="form-control text-center" placeholder="Ward(Optional)" value="<?php echo $ward; ?>" >
                    </div> 
                           
                          <div class="form-group">
							  <label>Work Status:</label>
                       <select name="workstatus" class="select2" required>
                               <option value="null">Choose Work status</option>
                               <option value="active" <?php if($workstatus=='active'){echo "selected";}else{ echo "null";}?>>Active</option>
                                <option value="onleave" <?php if($workstatus=='onleave'){echo "selected";}else{ echo "null";}?>>OnLeave</option>
                                <option value="outofstation" <?php if($workstatus=='outofstation'){echo "selected";}else{ echo "null";}?>>Outofstation</option>
                                <option value="inactive" <?php if($workstatus=='inactive'){echo "selected";}else{ echo "null";}?>>Inactive</option>
                               </select>
                    </div>  
                             <?php
                           include_once 'connectdb.php';
                            $query="SELECT * from sectors_list WHERE sector='$stationsector' ORDER BY id DESC";
                        //execute query
                        if ($dbhandle->real_query($query)) {
                            //If the query was successful
                            $res = $dbhandle->use_result();
                            while ($row = $res->fetch_assoc()) {
                                $sector_id=$row["sector_id"];
                               

                            }
                        }
                           
                           ?>
                    <div class="form-group" id="directorategroup">  
                        <select name="directorate" class="select2" onChange="getSection(this.value);">
                                <option value="">Select Directorate</option>
                               	<?php
                            include_once 'connectdb.php';
                             
                            if($position=='DIR'){
                                 $query="SELECT * from directorates_list WHERE directorate='$stationdirectorate'";
                        //execute query
                        if ($dbhandle->real_query($query)) {
                            //If the query was successful
                            $res = $dbhandle->use_result();
                            while ($row = $res->fetch_assoc()) {
                                $directorate_id=$row["directorate_id"];
                               

                            }
                        }
                                  $sql1="SELECT * FROM directorates_list where directorate_id='$directorate_id'";
                            }
                            else{
                            $sql1="SELECT * FROM directorates_list where sector_id='$sector_id'";
                            }
                            $results=$dbhandle->query($sql1); 
                            while($rs=$results->fetch_assoc()) { 
                            ?>
                            <option value="<?php echo $rs["directorate_id"]; ?>"><?php echo $rs["directorate"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>

                    </div>
                        <div class="form-group" id="sectiongroup">  
                        <select name="section" id="section" class="select2" onChange="getUnit(this.value);">
                            <option value="">Select Section</option>
                            
                        </select>

                    </div>
                        <div class="form-group" id="unitgroup">  
                        <select name="unit" id="unit" class="select2">
                                <option value="">Select Unit</option>
                               
                            </select>

                    </div>
                           
                           
                   
                       <input type="submit" name="update"  class="btn btn-danger"  value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
    
        </form>
       
<?php
    
 require("./_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
//insert data
if(isset($_POST['update'])){
    $person=$_POST['person'];
	$pos=$_POST["position"];
    $fname=$_POST['fname'];
    $mname=$_POST['mname'];
    $lname=$_POST['lname'];
    $phone=$_POST['phone'];
	//original stations
    $originaldirectorate=$_POST['originaldirectorate'];
    $originalsection=$_POST['originalsection'];
    $originalunit=$_POST['originalunit'];
	//entered stations
	$directorate=$_POST['directorate'];
    $section=$_POST['section'];
    $unit=$_POST['unit'];
	
    $pfno=$_POST['pfno'];
    $idnum=$_POST['idnum'];
    $gender=$_POST['gender'];
    $designation=$_POST['designation'];
    $empstatus=$_POST['empstatus'];
    $jobdesc=$_POST['jobdesc'];
     $dob=$_POST['dob'];
    $email=$_POST['email'];
    $subcounty=$_POST['subcounty'];
     $ward=$_POST['ward'];
    $workstatus=$_POST['workstatus'];
    
    
   if (isset($_POST["acting"])) {
       $acting=$_POST['acting'];
}else{  
    $acting="";
}
	
	
	
	/*****--------------DIRECTOR IS BEING EDITED-------------*/
	if($pos=='DIR'){
		//check whether there is change in directorate or no
		if($directorate==''){
			//use the original directorate but first confirm that it is there
			if($originaldirectorate==''){
				?>
	 <script>alert('You cannot update profile without choosing directorate')</script>
				<?php
			}
			else{
				//assign directorate name to original directorate
				$directoratename=$originaldirectorate;
				/***UPLOAD HERE***/
				$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
				
			}
			
		}
		else{
		//however if there is change use the new entered field but get its name first
		$query2="SELECT * FROM directorates_list WHERE directorate_id='$directorate'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $directoratename=$row['directorate'];
         }
              
            }
			/***UPLOAD HERE***/
			//set hierarchical_level
			 $hierarchical_level=2;
			
			$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname',hierarchical_level='$hierarchical_level' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
			
		}
		
	}
	
		/*****--------------DEPUTY DIRECTOR IS BEING EDITED-------------*/
	else if($pos=='D.DIR'){
		//check whether there is change in directorate or no
		if($section==''){
			//use the section directorate but first confirm that it is there
			if($originalsection==''){
				?>
	 <script>alert('You cannot update profile without choosing section')</script>
				<?php
			}
			else{
				//assign directorate name to original directorate
				$directoratename=$originaldirectorate;
				$sectionname=$originalsection;
				/***UPLOAD HERE***/
				$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
				
			}
			
		}
		else{
		//however if there is change use the new entered field but get its name first
			$directoratename=$originaldirectorate;
			//get directorate name
		$query2="SELECT * FROM directorates_list WHERE directorate_id='$directorate'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $directoratename=$row['directorate'];
         }
              
            }
			//get section name
		$query2="SELECT * FROM sections_list WHERE section_id='$section'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $sectionname=$row['section'];
         }
              
            }
			/***UPLOAD HERE***/
			//set hierarchical_level
			 $hierarchical_level=1;
			
			$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname',hierarchical_level='$hierarchical_level' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
			
		}
		
	}
	
	
	/*****--------------HOU OR STAFF IS BEING EDITED-------------*/
	else if(($pos=='HOU') OR ($pos=='STAFF')){
		//check whether there is change in directorate or no
		if($unit==''){
			//use the original directorate but first confirm that it is there
			if($originalunit==''){
				?>
	 <script>alert('You cannot update profile without choosing unit')</script>
				<?php
			}
			else{
				//assign directorate name to original directorate
				$directoratename=$originaldirectorate;
				$sectionname=$originalsection;
				$unitname=$originalunit;
				/***UPLOAD HERE***/
				$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
				
			}
			
		}
		else{
		//however if there is change use the new entered field but get its name first
			//get directorate name
		$query2="SELECT * FROM directorates_list WHERE directorate_id='$directorate'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $directoratename=$row['directorate'];
         }
              
            }
			//get section name
		$query2="SELECT * FROM sections_list WHERE section_id='$section'";
            if ($mysqli->real_query($query2)) {
                $res = $mysqli->use_result();
                while ($row = $res->fetch_assoc()) {
                    $sectionname=$row['section'];
         }
              
            }
	
				$unitname=$unit;
		
			/***UPLOAD HERE***/
			//set hierarchical_level
			 $hierarchical_level=0;
			
			$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$pos',directorate='$directoratename',section='$sectionname',unit='$unitname',hierarchical_level='$hierarchical_level' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
			
		}
		
	}
	
	//anyone else is editing their profile
	else{
		
		       $deptname=$originaldepartment;
		        $sectorname=$originalsector;
				$directoratename=$originaldirectorate;
				$sectionname=$originalsection;
		        $position=$userposition;
				/***UPLOAD HERE***/
				$sql="UPDATE ememo_users SET fname='$fname',mname='$mname',lname='$lname',phone='$phone',acting='$acting',pfno='$pfno',idnum='$idnum',gender='$gender',designation='$designation',empstatus='$empstatus',jobdesc='$jobdesc',dob='$dob',email='$email',subcounty='$subcounty',ward='$ward',workstatus='$workstatus',position='$position',department='$deptname',sector='$sectorname',directorate='$directoratename',section='$sectionname',unit='$unitname' WHERE user_id='$person'";
            if($stmnt=$mysqli->prepare($sql)){
   
        $stmnt->execute(); 
                ?>
     <script>
     alert("Profile Successfully Updated");
           window.location.href="settings.php";
     </script>
     <?php
                
        } 
		
	}
	
	
	
}
?>
    

 
    
                         
                            </div>

                        </div>
                    </div>

                    </div>
 

        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
            </section>
   <script>            
             
             function hideFields(val){
               if(val=='DIR'){
                       $('#directorategroup').show();
                    $('#sectiongroup').hide();
                       $('#unitgroup').hide();
                 }
                 else if(val=="D.DIR"){
                        $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').hide();
             
                 }
                 else if(val=="HOU"){
                         $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').show();
                 }
                  else if(val=="STAFF"){
                         $('#directorategroup').show();
                       $('#sectiongroup').show();
                       $('#unitgroup').show();
                 }
                 else{
                       $('#directorategroup').hide();
                    $('#sectiongroup').hide();
                       $('#unitgroup').hide();
                 }
             }
                
                
function getSection(val) {
	$.ajax({
	type: "POST",
	url: "co_admin/get_section.php",
	data:'directorate_id='+val,
	success: function(data){
		$("#section").html(data);
	}
	});
}
          
function getUnit(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_unit.php",
	data:'section_id='+val,
	success: function(data){
		$("#unit").html(data);
	}
	});  
}                

       /***to view staff***/
    function getDirectorateStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_directoratestaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       function getSectionStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_sectionstaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       function getUnitStaff(val){
  	$.ajax({
	type: "POST",
	url: "co_admin/get_unitstaff.php",
	data:'section_id='+val,
	success: function(data){
		$("#directoratestaff").html(data);
	}
	});  
}   
       
</script>

<script>
 
$("#stationcreation").mouseover(function() {
       var stationtype=document.querySelector('input[name="stationtype"]:checked').value;
      if(stationtype=='directorate'){
        $("#directoratebay").show(); 
           $("#sectionbay").hide(); 
           $("#unitbay").hide(); 
      }
    else if(stationtype=='section'){
        
           $("#directoratebay").hide(); 
          $("#sectionbay").show();
        $("#unitbay").hide(); 
    }
     else if(stationtype=='unit'){
          $("#directoratebay").hide(); 
          $("#sectionbay").hide(); 
          $("#unitbay").show(); 
     }
});

</script>


  <script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"release_acc/fetch.php",
     type:"POST"
    }
   });
  }
  
 
  
  $(document).on('click', '.delete', function(){
   var id = $(this).attr("id");
 
    $.ajax({
     url:"release_acc/delete.php",
     method:"POST",
     data:{id:id},
     success:function(data){
      $('#alert_message').html('<div class="">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 1000);
   
  });
 });
</script>

<script>
function add_delegate(){
   
    $("#recipient_data").append("If you are sure you want to delegate your duties to the staff you just selected click the icon below");
}


</script>
    

<script>
$(document).ready(function(){
 var pos='<?php echo $position?>';
    if(pos='C.O'){
        $('#votehead').show();
    }
});
</script>  




<!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/flatpickr/dist/flatpickr.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- Charts and maps-->
        <script src="demo/js/flot-charts/curved-line.js"></script>
        <script src="demo/js/flot-charts/line.js"></script>
        <script src="demo/js/flot-charts/dynamic.js"></script>
        <script src="demo/js/flot-charts/chart-tooltips.js"></script>
        <script src="demo/js/other-charts.js"></script>
        <script src="demo/js/jqvmap.js"></script>

   <script src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

        <!-- App functions and actions -->
        <script src="js/app.min.js"></script>

       
        <script src="js/rChat.js"></script>
        <script src="js/rChatSent.js"></script>

        
    </body>

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>