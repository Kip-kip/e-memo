<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:29:20 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E-Memo</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="vendors/bower_components/dropzone/dist/dropzone.css">
    <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
    <link rel="stylesheet" href="vendors/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
      <link rel="stylesheet" href="vendors/bower_components/flatpickr/dist/flatpickr.min.css" />
       <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
    
      <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="push.js"></script>
    
  <script src="ckeditor/adapters/jquery.js"></script>
     <script src="ckeditor/ckeditor.js"></script>
     <script src="jquery-3.0.0.js"></script>
     <script>
                $(document).ready(function(){
                   $('#hide').hide();
                    $('#hidevote').hide();
                    $('#broadcastrec').hide();
                     $('#financialrec').hide();
					$('#localrec').hide();
					$('#intrec').hide();
                    $('#staff').hide();
					$('#travelmemolocal').hide();
					$('#travelmemoint').hide();
					$('#travelmemotype').hide();
            
    
                });
            </script>
    <!-- App styles -->
    <link rel="stylesheet" href="css/app.min.css">
</head>

<body data-sa-theme="1">
<main class="main">
    <div class="page-loader">
        <div class="page-loader__spinner">
            <svg viewBox="25 25 50 50">
                <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>

            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
                    <i class="zmdi zmdi-menu"></i>
                </div>

                <div class="logo hidden-sm-down">
                     <h1 id="dash"><img width="60" height="60" src="img/nanditrans.png"/>E-MEMO NCG</h1>
                </div>

                <form class="search">
                    <div class="search__inner">
                        <input type="text" class="search__text" placeholder="Search for people, files, documents...">
                        <i id="mee" class="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
                    </div>
                </form>

                                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdownotif">
                        <a href="#" data-toggle="dropdown" class="nav__notify"><i id="" class="label label-pill label-danger countnotif"></i></br><i id="meenotif" class="zmdi zmdi-notifications"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Notifications

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownotif" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                                <!--a href="#" class="view-more">View all notifications</a-->
                            </div>
                        </div>
                    </li>

                    <li class="dropdownmes">
                        <a href="#" data-toggle="dropdown" class="nav__mes"><i id="" class="label label-pill label-danger countmes"></i></br><i id="meemes" class="zmdi zmdi-email"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                            <div class="dropdown-header">
                                Messages

                                <div class="actions">
                                    <a href="" class="actions__item zmdi zmdi-plus"></a>
                                </div>
                            </div>

                            <div class="listview listview--hover">
                                <a href="#" class="listview__item">

                                    <div class="listview__content">
                                        
                                        
                                        
                                        <div id="dropdownmes" class="listview__heading">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </a>


                               
                            </div>
                        </div>
                    </li>

               


                  
                </ul>

                <div class="clock hidden-md-down" id="saa">
                    <div class="time">
                        <span class="hours"></span>
                        <span class="min"></span>
                        <span class="sec"></span>
                    </div>
                </div>
            </header>
              <aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                                      <?php         
                                require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    
//RECEIVED MESSAGE    
$ses= $_SESSION['karibu'];
$query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $position=$row["position"];
         $stationdepartment=$row["department"];
        $stationsector=$row["sector"];
        $stationdirectorate=$row["directorate"];
        $stationsection=$row["section"];
        $stationunit=$row["unit"];
         $fname=$row['fname'];
         $mname=$row['mname'];
         $lname=$row['lname'];
        $dp_file_ext=$row['dp_file_ext'];
        
        if($position=='C.E.C.M'){
            $station=$stationdepartment;
        }
        else if($position=='C.O'){
             $station=$stationsector;
        }
         else if($position=='DIR'){
             $station=$stationdirectorate;
        }
         else if($position=='D.DIR'){
             $station=$stationsection;
        }
        else if($position=='HOU'){
             $station=$stationunit;
        }
        else if($position=='STAFF'){
             $station=$stationunit;
        }
        else{
             $station='County Government of Nandi';
        }
       
       
        }
     
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
 
              
$db->close();                  
                 ?>  
                        <div class="user__info" data-toggle="dropdown">
                             <?php
                            
                             echo "<img class=\"user__img\" src=\"img/profilepics/$ses$dp_file_ext\" alt=\"\">";
                            ?>
                            <div>
                       
                                
                                <div class="user__name"> <?php
echo $fname.'&nbsp;'.$mname.'&nbsp;'.$lname;
?></div>
                                <div class="user__email"> <?php
echo $position.'&nbsp;'.$station;
?></div>
                            </div>
                        </div>
                      
                    </div>

                    <ul class="navigation">
                       
                       
                         <li class="navigation__active"><a href="index.php"><i id="mee" class="zmdi zmdi-view-dashboard"></i> Dashboard</a></li>
                            
                          <li class="navigation__sub @@variantsactive">
                            <a href="#"><i id="mee" class="zmdi zmdi-receipt"></i> Memo Management</a>

                            <ul>
                                 <li class="@@sidebaractive"><a href="indexnonfinancialreceived.php">Received Requests</a></li>
                                <li class="@@boxedactive"><a href="indexnonfinancialsent.php">Sent Requests</a></li>
                                <li class="@@hiddensidebarboxedactive"><a href="indexnonfinancialdrafts.php">Memos in Draft</a></ul>
                        </li>
                        

                        <li class="@@typeactive">
                          <a href="nonfinancialmemocreation.php"><i id="mee" class="zmdi zmdi-collection-plus"></i>Create memo</a>
                        </li>

                     
                         <!--li class="@@typeactive">
                          <a href="indexchats.php"><i id="mee" class="zmdi zmdi-comments"></i>Chat</a>
                        </li-->
<li class="@@typeactive">
                            <a href="settings.php"><i id="mee" class="zmdi zmdi-settings"></i>Settings</a>
                        </li>
                         </ul>
                </div>
            </aside>

    <section id="fichayote" class="content">
        <header class="content__title">
            <h1 id="dash">MEMO FORM</h1>
            <small id="profiletext">Create a New Memo</small>
        </header>
        
        <form action="nonfinancialmemocreation.php" method="post" >
         
        <div class="card">
            <div class="card-body" id="cbmemcreation">
				
                <div class="row">
                <div class="col-sm-6 col-md-4">
                    
                        <div class="form-group">

                            <label>Memo Type.</label>

                        </div>
                    </div>
                          <div class="col-sm-6 col-md-8">
                              <!--hide financial for staff-->
                              <?php
                            if(($position=='STAFF') OR ($position=='HOU')){
                                $camou="hidden";
                                $word="";
                            }
                              else{
                                  $camou="";
                                  $word="Financial";
                              }

                                  ?>
                              
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                             <div class="btn-group btn-group--colors" data-toggle="buttons" onclick="toa()">
                                <label class="btn bg-cyan"><input type="radio" name="sametype" id="communication" value="communication"></label> </br>Communication &nbsp;&nbsp;&nbsp;
                                <label class="btn bg-purple"<?php echo $camou;?>><input type="radio" name="sametype" id="financial" value="financial"></label></br><?php echo $word;?>&nbsp;&nbsp;&nbsp;
                              <label class="btn bg-danger"><input type="radio" name="sametype" id="broadcast" value="broadcast"></label></br>BroadCast&nbsp;&nbsp;&nbsp;
						     <label class="btn bg-teal"><input type="radio" name="sametype" id="travel" value="travel"></label></br>Travel&nbsp;&nbsp;&nbsp;
   
                                    </div>
                              
 
                                        
                                       </div>
                                </div>
                             
                        </div>
           
            
                        </div>
              
                   
                </div>

      <div class="row" id="travelmemotype">
                <div class="col-sm-6 col-md-4">
                    
                        <div class="form-group">

                            <label>Travel Memo Type.</label>

                        </div>
                    </div>
                          <div class="col-sm-6 col-md-8">
                         <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                             <div class="btn-group btn-group--colors" data-toggle="buttons">
                               <label class="btn"><input type="radio" name="typemoja" id="local" value="local"></label></br>Local&nbsp;&nbsp;&nbsp;
						     <label class="btn"><input type="radio" name="typemoja" id="international" value="international"></label></br>International&nbsp;&nbsp;&nbsp;
   
                                    </div>
                              
 
                                        
                                       </div>
                                </div>
                             
                        </div>
           
            
                        </div>
              
                   
                </div>


              
                <div  id="normalrec" class="row">
                    <div class="col-sm-6 col-md-4">
                        
                        
                       <?php
                                include('./controller/class.php');
                             ?>  

                        <div class="form-group">

                            <label>To.</label>

                        </div>
                    </div>
                    
                    
                    <div class="col-sm-6 col-md-8">
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <select name="firstrecepient" class="select2" id="recipient_emails" onchange="add_email()">
                                <option value="null">Recipient</option>
                                 <?php

                                  echo App::get_recipient_emails();

                               ?>
                            </select>

                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_data" class="row"></div>
                        </div>
                        </div>
                
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>


          <div id="broadcastrec" class="row">
                    <div class="col-sm-6 col-md-4">
                        
                   
                        <div class="form-group">

                            <label>To.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                  <select name="stationrecepient" class="select2" id="recipient_stations" onchange="add_station()">
                                <option value="empty">Recipient</option>
                                  <?php

                                  echo App::get_recipient_station();

                               ?>
                            </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_station" class="row"></div>
                        </div>
                        </div>
                
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
           <div id="financialrec" class="row">
                    <div class="col-sm-6 col-md-4">
                        
                   
                        <div class="form-group">

                            <label>To.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                  <select name="stationrecepient" class="select2" id="recipient_financials" onchange="add_financial()">
                                <option value="empty">Recipient</option>
                                  <?php

                                  echo App::get_recipient_financial();

                               ?>
                            </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_financial" class="row"></div>
                        </div>
                        </div>
                
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>

<!---Local Travel--->
							 <div id="localrec" class="row">
                    <div class="col-sm-6 col-md-4">
                        
                   
                        <div class="form-group">

                            <label>To.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                  <select name="stationrecepient" class="select2" id="recipient_loctravels" onchange="add_loctravel()">
                                <option value="empty">Recipient</option>
                                  <?php

                                  echo App::get_recipient_loctravel();

                               ?>
                            </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_loctravel" class="row"></div>
                        </div>
                        </div>
                
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>

<!---International Travel--->
							 <div id="intrec" class="row">
                    <div class="col-sm-6 col-md-4">
                        
                   
                        <div class="form-group">

                            <label>To.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                     <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                  <select name="stationrecepient" class="select2" id="recipient_inttravels" onchange="add_inttravel()">
                                <option value="empty">Recipient</option>
                                  <?php

                                  echo App::get_recipient_inttravel();

                               ?>
                            </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_inttravel" class="row"></div>
                        </div>
                        </div>
                
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>




                  <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Through.(Optional)</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                          <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <select name="firstrecepient" class="select2" id="recipient_cc_emails" onchange="add_cc_email()">
                                <option value="void">Recipient</option>
                                 <?php

                                  echo App::get_recipient_emails();

                               ?>
                            </select>

                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_cc_data" class="row"></div>
                        </div>
                      </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
                
               <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>From.</label>
                            
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="form-group">
<input type="text" name="requestor" id="requestor" class="form-control" value="<?php echo $_SESSION['karibu']; ?>" hidden>
                            <input type="text" name="requestor" class="form-control" value=" <?php echo $fname.' ';echo $mname.' '; echo $lname; echo ' ('.$position.', '; echo $station.')'; ?>" disabled>

                            
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>

          
                <div class="row" onclick="filter_finance()">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Subject.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="form-group">

                            <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter the subject of the memo" required>
                            
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>

                
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Date created.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                       
                         <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                        <div class="form-group">
                                            <input type="text" name="datecreated" class="form-control datetime-picker" id="datecreated" placeholder="Pick a date & time" required>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                      
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
                
                    <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Urgency.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                       
                            <div class="btn-group btn-group--colors" data-toggle="buttons">
                              
                                <label class="btn bg-red"><input type="radio" name="urgency" id="urgencyvery" autocomplete="off" value="very high"></label> </br>Very High &nbsp;&nbsp;&nbsp;
                                <label class="btn bg-orange"><input type="radio" name="urgency" id="urgencyhigh" autocomplete="off" value="high"></label></br>High&nbsp;&nbsp;&nbsp;
                                <label class="btn bg-blue"><input type="radio" name="urgency" id="urgencymedium" autocomplete="off" value="medium"></label></br>Medium&nbsp;&nbsp;&nbsp;
                            </div>
                      
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
                
            
                
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Due date.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                       
                         <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                        <div class="form-group">
                                            <input type="text" name="duedate" class="form-control datetime-picker" id="duedate" placeholder="Pick a date & time" required>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                      
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
<p class="ckeditor"></p>
              
                <div id="memobody" class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Memo Body(create your memo in the editor provided below).</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-12">
     
                        <div class="form-group">

                           
                          <textarea name="introduction" id="introduction" class="ckeditor">
                            
                            </textarea>
                    <script>
                         CKEDITOR.replace('introduction');
                        
                    </script>

                      
                        </div>
                    </div>
                    

                </div>

<!------------------------------------------------------LOCAL TRAVEL FORM--------------------------------------------------->
<div id="travelmemolocal">
<p style="color:#FC4A1A">To be completed in Duplicate: Original to be retained by applicant and Duplicate by HRM</p>
<p style="color:#FC4A1A">PART &#8544;</p>
 <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>1: Name of Officer:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="officername" class="form-control" id="officername" value=" <?php echo $fname.' ';echo $mname.' '; echo $lname;?>" disabled>
                        </div>
                    </div>
                    
</div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>2: Personal Number:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="personalno" class="form-control" id="personalno" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>3: Designation:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="designation" class="form-control" id="designation" placeholder="Enter Designation">
							
                        </div>
                    </div>
	<div class="col-sm-6 col-md-2">
                        <div class="form-group">

                            <label>Tel:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="telephone" class="form-control" id="telephone" placeholder="Enter Telephone Number">
							
                        </div>
                    </div>
	
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>4: Duration:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="duration" class="form-control" id="duration" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>5: Start Date:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="startdate" class="form-control datetime-picker" id="startdate" placeholder="Enter Start Date">
							
                        </div>
                    </div>
	<div class="col-sm-6 col-md-2">
                        <div class="form-group">

                            <label>End Date:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="enddate" class="form-control datetime-picker" id="enddate" placeholder="Enter End Date">
							
                        </div>
                    </div>
	
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>6: County/City of Destination:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="destination" class="form-control" id="destination" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                  
                    <div class="col-sm-6 col-md-12">
     
                        <div class="form-group">
			
							<textarea class="form-control" name="purpose" rows="5" id="purpose" placeholder="Enter reason for requesting travel"></textarea>
							
                        </div>
                    </div>
                    

                </div>
<p style="color:#FC4A1A">PART &#8545;- HANDING OVER</p>

<p>7. The following assignments/duties/responsibilities will be handled by:</p>

<p><b>ACTIVITIES/DUTIES AND RESPONSIBILITIES</b></p>
<div class="row">
 <div class="col-sm-6 col-md-12">
	 
	   <div class="form-group">
		   
<textarea class="form-control" name="activities" rows="5" id="activities" placeholder="List and number the Activities/duties here"></textarea>
		   
		    
</div>
</div>
</div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Name of the Officer:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <select name="delegatedofficer" class="select2" id="delegatedofficer">
                                           <option value="empty">Select Officer</option>
                                         <?php
                                            require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
 $query="SELECT * from ememo_users WHERE position!='GO' AND position!='D.G' AND position!='C.S' AND position!='D.C.S' ORDER BY user_id DESC ";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
		$formuser_id=$row["user_id"];
        $formfname=$row["fname"];
        $formmname=$row["mname"];
        $formlname=$row['lname'];
        
                               echo     "<option value=\"$formuser_id\">&nbsp$formfname&nbsp$formmname &nbsp$formlname</option>";
    }
}

$db->close();
?>                                      
                                
    
</select>
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Designation:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="delegateddesign" class="form-control" id="delegateddesign" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Components:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="components" class="form-control" id="components" placeholder="">
							
                        </div>
                    </div>
                    

                </div>
</div>


<!---------------------------------------------------INT'NATIONAL TRAVEL FORM---------------------------------------------->

<div id="travelmemoint">
<p style="color:#FC4A1A">(To  be  completed  in  quadruplicate:  Original to  be  retained  by  applicant,  Duplicate by 
processing Division, Triplicate by Head of Department and Quadruplicate for  retention 
by D/HRM)</p>
<p style="color:#FC4A1A">PART &#8544; – PERSONAL DETAILS</p>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>1: Name of Officer:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intofficername" class="form-control" id="intofficername" value=" <?php echo $fname.' ';echo $mname.' '; echo $lname;?>" disabled>
                        </div>
                    </div>
                    
</div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>2: Personal Number:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intpersonalno" class="form-control" id="intpersonalno" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>3: Designation:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="intdesignation" class="form-control" id="intdesignation" placeholder="Enter Designation">
							
                        </div>
                    </div>
	<div class="col-sm-6 col-md-2">
                        <div class="form-group">

                            <label>Tel:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="inttelephone" class="form-control" id="inttelephone" placeholder="Enter Telephone Number">
							
                        </div>
                    </div>
	
                    

                </div>

<p style="color:#FC4A1A">PART &#8545; – TRAVEL DETAILS</p>

<div class="row">
 <div class="col-sm-6 col-md-12">
	 
	   <div class="form-group">
		   
<textarea class="form-control" name="intreasons" rows="5" id="intreasons" placeholder="Reason(s) for travel"></textarea>
		   
		    
</div>
</div>
</div>
<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>2: Country:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="intcountry" class="form-control" id="intcountry" placeholder="Enter Country">
							
                        </div>
                    </div>
	<div class="col-sm-6 col-md-2">
                        <div class="form-group">

                            <label>City:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
     
                        <div class="form-group">
							
 <input type="text" name="intcity" class="form-control" id="intcity" placeholder="Enter City">
							
                        </div>
                    </div>
	
                    

                </div>
<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>3: Date and time of departure:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intdepart" class="form-control datetime-picker" id="intdepart" placeholder="Pick departure date and time">
							
                        </div>
                    </div>
                    

                </div>
<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>4: Date and time of return:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intreturn" class="form-control datetime-picker" id="intreturn" placeholder="Pick return date and time">
							
                        </div>
                    </div>
                    

                </div>


<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>5: Mode of travel (By air indicate class of travel):</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="inttravelmode" class="form-control" id="inttravelmode" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>6: Person(s) Organization/Institution hosting the 
Meeting/Conference/Seminar/Workshop/Training:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="inthost" class="form-control" id="inthost" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>7: Sponsor(s) of the Meeting/Conference/Seminar/Workshop/etc:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intsponsor" class="form-control" id="intsponsor" placeholder="">
							
                        </div>
                    </div>
                    

                </div>






<p style="color:#FC4A1A">PART &#8546; – COST OF THE TRIP AND SPONSORSHIP</p>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(a): Subsistence:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intsubsistence" class="form-control" id="intsubsistence" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(b): Sponsor:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intsponsortwo" class="form-control" id="intsponsortwo" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(c): Accomodation:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intaccomodation" class="form-control" id="intaccomodation" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(d): Participation:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="text" name="intparticipation" class="form-control" id="intparticipation" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(e): Travel Expenses:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="number" name="inttravelexp" class="form-control" id="inttravelexp" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>(f): Total Expenses:</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
     
                        <div class="form-group">
							
 <input type="number" name="inttotal" class="form-control" id="inttotal" placeholder="">
							
                        </div>
                    </div>
                    

                </div>

<p style="color:#FC4A1A">PART &#8547; – OBJECTIVES OF THE
MEETING/CONFERENCE/SEMINAR/WORKSHOP/TRAINING ETC</p>

<div class="row">
                  
                    <div class="col-sm-6 col-md-12">
     
                        <div class="form-group">
			
							<textarea class="form-control" name="intobjs" rows="5" id="intobjs" placeholder="Enter Objectives of the Meeting"></textarea>
							
                        </div>
                    </div>
                    

                </div>



<p style="color:#FC4A1A">PART &#8548; – EXPECTED BENEFITS OF THE 
MEETING/CONFERENCE/SEMINAR/WORKSHOP/TRAINING ETC</p>

<div class="row">
                  
                    <div class="col-sm-6 col-md-12">
     
                        <div class="form-group">
			
							<textarea class="form-control" name="intbenefits" rows="5" id="intbenefits" placeholder="Enter expected benefits of meeting"></textarea>
							
                        </div>
                    </div>
                    

                </div>

</div>


<!--------------------------------------------------CONTINUATION------------------------------------------->
  <div class="row" id="hide">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Total Amount of funds requested.</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="form-group">
                              <input type="number" name="amount" id="amount" class="form-control text-center" placeholder="Enter amount, example: 350,000">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          
                        </div>
                    </div>


                </div>


 <div class="row" id="hidevote">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label> Assign to vote head</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                      
                          <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                       <select name="votehead" class="select2" id="votehead" onchange="checkVoteHead(this.value)">
                                           <option value="none">Select vote head</option>
                                            <?php
                                            require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
                 //get the sector of requestor
               $query="SELECT * FROM ememo_users where user_id='$ses'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $sector=$row["sector"];
      
    }
}
                 
                 $query="SELECT * FROM sectors_list where sector='$sector'";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $sector_id=$row["sector_id"];
      
    }
}
 $query="SELECT * from vote_heads where sector_id='$sector_id' ORDER BY id DESC";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $item_source=$row["Item_source"];
        $title=$row["Title_and_details"];
        $balance=$row["Balance"];
        
        
                               echo     "<option value=\"$item_source\">$title&nbsp;&nbsp; Balance:&nbsp;kshs&nbsp;".number_format($balance)."</option>";
    }
}

$db->close();
?>                   
    
</select>

                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_cc_data" class="row"></div>
                        </div>
                      </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>



                  <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Supporting document(optional).</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="form-group">
                            <input type="file" class="btn btn-info" name="upload_document" id="upload_document" onchange="uploadDoc()" multiple></input>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <div id="data_of_uploaded_documents" class="row"></div>
                        </div>
                    </div>


                </div>
                
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">

                            <label>Refer to an existing previous Memo(optional)</label>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                      
                          <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                       <select name="memattach" class="select2" id="prevmemo">
                                           <option value="empty">Select memo</option>
                                         <?php
                                            require("./_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
 $query="SELECT * from nonfinancialmemos WHERE nature='direct' ORDER BY id DESC ";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();
    while ($row = $res->fetch_assoc()) {
        $subject=$row["subject"];
        $referenceno=$row["referenceno"];
        $date=$row['datecreated'];
        
                               echo     "<option value=\"$referenceno\">Subject:&nbsp$subject&nbsp&nbsp&nbsp&nbsp&nbsp$referenceno &nbsp&nbsp&nbsp&nbspDate:&nbsp$date</option>";
    }
}

$db->close();
?>                                      
                                
    
</select>

                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                             
                        </div>
                        <div class="row">
                            <div id="recipient_cc_data" class="row"></div>
                        </div>
                      </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                        </div>
                    </div>


                </div>
              
                          </br>

               
              <input type="text" id="refdraft" value="love" hidden></input>
                            
                                 
    
           
           <div class="row quick-stats">
                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="reener">
                        
                          <button type="button" class="btn btn-info" name="" onclick='add_data()'><i class="zmdi zmdi-eye"></i>&nbsp;Preview Memo  </button>
                        
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6">
                        <div class="quick-stats__item" id="reener">
                            <div class="quick-stats__info">
                     <button type="button" class="btn btn-danger" name="submit" onclick='add_draft_data()'><i class="zmdi zmdi-edit"></i>&nbsp;Save as Draft
                        </button>
                            </div>
                        </div>
                    </div>

                </div>
<p id="validate" style="color:#FC4A1A" hidden></p>
</div>
                    

      

            </div>
            
        </div>
  
         

        </form>   

      
        <footer class="footer hidden-xs-down">
            <p>© Kisa Software Enterprise 2017. All rights reserved.</p>

        </footer>
    </section>
</main>

<!--NOTIFICATIONS-->

<script>
    /**HIDE AND UNHIDE COMPONENTS BASED ON MEMOTYPE**/
$("#fichayote").mouseover(function() {
       var memotype=document.querySelector('input[name="sametype"]:checked').value;
	
	if(memotype=='broadcast'){
        $('#broadcastrec').show();
         $('#normalrec').hide();
        $('#financialrec').hide();
		$('#localrec').hide();
		$('#intrec').hide();
         $('#hide').hide();
       $('#hidevote').hide();
		
		$('#memobody').show();
		
		document.getElementById('local').checked = false;
		document.getElementById('international').checked = false;
		
		$('#travelmemotype').hide(); 
		 $('#travelmemolocal').hide();
		 $('#travelmemoint').hide();
    }
    else if(memotype=='communication'){
      $('#broadcastrec').hide();
          $('#financialrec').hide();
          $('#normalrec').show();
		$('#localrec').hide();
		$('#intrec').hide();
		
         $('#hide').hide();
        $('#hidevote').hide();
		
		$('#memobody').show();
		
		document.getElementById('local').checked = false;
		document.getElementById('international').checked = false;
		
		$('#travelmemotype').hide(); 
		 $('#travelmemolocal').hide();
		 $('#travelmemoint').hide();
    }
     else if(memotype=='financial'){
      $('#broadcastrec').hide();
          $('#normalrec').hide();
           $('#financialrec').show();
		 $('#localrec').hide();
		$('#intrec').hide();
		 
           $('#hide').show();
		 
		 $('#memobody').show();
		 
		 document.getElementById('local').checked = false;
		document.getElementById('international').checked = false;
		 
		$('#travelmemotype').hide(); 
		 $('#travelmemolocal').hide();
		 $('#travelmemoint').hide();
         var position='<?php echo $position ?>';
          if(position=='C.O'){
         $('#hidevote').show();
    }
    else{
         $('#hidevote').hide();
         }
    }
	else if(memotype=='travel'){
		$('#hidevote').hide();
		$('#hide').hide();
		
		$('#localrec').hide();
		$('#intrec').hide();
		
		
		 $('#memobody').hide();
		$('#travelmemo').show();
		$('#travelmemotype').show();
	}
	

	
});
  
  
  
    
</script>


<!--Travel Memo Type-->
<script>
$("#fichayote").mouseover(function() {
var travelmemotype=document.querySelector('input[name="typemoja"]:checked').value;
	
	if(travelmemotype=='local'){
		$('#broadcastrec').hide();
          $('#normalrec').hide();
           $('#financialrec').hide();
		$('#localrec').show();
		$('#intrec').hide();
		
		$('#travelmemolocal').show();
		$('#travelmemoint').hide();
	}
	else if(travelmemotype=='international'){
		$('#broadcastrec').hide();
          $('#normalrec').hide();
           $('#financialrec').hide();
		$('#intrec').show();
		$('#localrec').hide();
	
		
		$('#travelmemoint').show();
		$('#travelmemolocal').hide();
	}

});
</script>




<script>

$(document).ready(function(){
var position='<?php echo $position ?>';
 if(position=='HOU'){
        $('#nostaff').hide();
         $('#staff').show();
         
    }
    else if(position=='STAFF'){
       $('#nostaff').hide();
        $('#staff').show();
      
    }
});

</script>




<script type="text/javascript">
function checkVoteHead(val){
    
    var amount=document.getElementById('amount').value;
    var vh =val;
  $.ajax({
    method: "POST",
    url: "checkvotehead/checkvotehead.php",
    data: {
        amount:amount,
      vh:vh
    },
    cache: false
  }).done(function(data){
    $("#validate").html(data);
  });
    
}
</script>




<!--HIDE Form IF SESSION IS NOT REGISTRY-->
<script>
    
var session = "<?php 
  echo $ses ?>";
  
  if(session==71){
      $('#fichayote').hide();
  }
</script>

<!--------LOG USER OUT AFTER 5 MINUTES OF INACTIVITY----------->
<script>

  
</script>


<!-- Javascript -->
<!-- Vendors -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<script src="vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="vendors/bower_components/dropzone/dist/min/dropzone.min.js"></script>
<script src="vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
    
        <script src="vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="vendors/bower_components/flatpickr/dist/flatpickr.min.js"></script>

<!-- Charts and maps-->
<script src="demo/js/flot-charts/curved-line.js"></script>
<script src="demo/js/flot-charts/line.js"></script>
<script src="demo/js/flot-charts/dynamic.js"></script>
<script src="demo/js/flot-charts/chart-tooltips.js"></script>
<script src="demo/js/other-charts.js"></script>
<script src="demo/js/jqvmap.js"></script>

<!-- App functions and actions -->
<script src="js/app.min.js"></script>
<script src="js/event-controller.js"></script>

    <!---------------VALIDATION------------------->
    

</body>
    
<!-- Mirrored from byrushan.com/projects/super-admin/app/2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Dec 2017 17:30:43 GMT -->
</html>