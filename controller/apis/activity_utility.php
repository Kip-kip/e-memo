<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
include_once '../../config.php';

class DB_connect{

    
public function updateProg(){
  
}
    
    
    
    public function setPersonnalStatus($mysqli,$ses,$referenceno){
         //SELECT ALL FOR SINGLE RECEPIENT ROW TO UPDATE STATUS
  $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && recepient='$ses'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $progress=$row["progress"];   
        $status=$row["status"];
    }
}
        if($progress>=100){
            //ensure that if it is already rejected it remains rejected   
            if(($status!=='Rejected')&&($status!=='Approved')&&($status!=='Closed')&&($status!=='Escalated')&&($status!=='Forwarded')&&($status!=='Noted')){
            $sql3="UPDATE nonfinancialmemos SET status='Approved' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql3)){
   
        $stmnt->execute();      
           }  
            }
        }
         else if($progress<=0){
             $sql4="UPDATE nonfinancialmemos SET status='Inactive' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql4)){
   
        $stmnt->execute();      
     }  
        }
        else{
            $sql5="UPDATE nonfinancialmemos SET status='Pending' WHERE referenceno='$referenceno' && recepient='$ses'";
          if($stmnt=$mysqli->prepare($sql5)){
   
        $stmnt->execute();      
}  
        }
    
    
    }
    
    public function generateGeneralProgress($mysqli,$referenceno){
        
                  
$abc="SELECT sum(progress) as c FROM nonfinancialmemos where referenceno='$referenceno' AND nature!='Cc'";
  $result1=mysqli_query($mysqli,$abc);
    if($result1) {
 while($row=mysqli_fetch_assoc($result1)) {
        $totalprogress=$row['c'];
    
      }     

}
          
          //ACQUIRE TOTAL NUMBER OF ROWS CONTAINING REFERENCE NO
          
          $def="SELECT count(*) as f FROM nonfinancialmemos where referenceno='$referenceno' AND nature!='Cc'";

          $result2=mysqli_query($mysqli,$def);
        if($result2) {
            while($row=mysqli_fetch_assoc($result2)) {
            $totalcolumns=$row['f'];
    
      }     

}
          
          
 //DO THE CALCULATION TO HAVE A MUTUAL GENERAL PROGRESS         
          
          $percentage=100;
          
          $calcprogress=($totalprogress/($totalcolumns*$percentage)*$percentage);
        //  echo $calcprogress;
          
          
      
                    //UPDATE GENERAL PROGRESS
      $sql8="UPDATE nonfinancialmemos SET generalprogress='$calcprogress' WHERE referenceno='$referenceno'";
     if($stmnt=$mysqli->prepare($sql8)){
   
        $stmnt->execute();      
}  
    
    
    }
    
    
    public function setGeneralStatus($mysqli,$referenceno){
    
     //SELECT ALL FOR ALL RECEPIENTS ROW TO UPDATE GENERAL STATUS
  $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $generalprogress= $row["generalprogress"];   
    }
    }
        
        //find out what main recepient said and set as general status
         $query="SELECT * FROM nonfinancialmemos WHERE referenceno='$referenceno' && nature='direct'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
    while ($row = $res->fetch_assoc()) {
         $status= $row["status"];   
    }
    }
        
        if($generalprogress>=100){
            
            
            $sql3="UPDATE nonfinancialmemos SET generalstatus='$status' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql3)){
   
        $stmnt->execute();      
}  
        }
        else if($generalprogress<=0){
             $sql4="UPDATE nonfinancialmemos SET generalstatus='Inactive' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql4)){
   
        $stmnt->execute();      
     }  
        }
        else{
             $sql5="UPDATE nonfinancialmemos SET generalstatus='Pending' WHERE referenceno='$referenceno'";
          if($stmnt=$mysqli->prepare($sql5)){
   
        $stmnt->execute();      
}  
        }
    }

   
   
    
    
    
   
    
    public function setEscalateLogs($mysqli,$referenceno,$subject,$ses){
        
     /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                       $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Escalated';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Escalated';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }


   //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Escalated memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
         //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Escalated memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}



    
    }
    
    
    
    
    public function setForwardLogs($mysqli,$referenceno,$subject,$ses){
        
      /*****INSERT INTO LOGS AS WELL*****/    
    $query1="SELECT * FROM nonfinancialmemos Inner Join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id WHERE recepient='$ses' && referenceno='$referenceno'";
    //execute query
if ($mysqli->real_query($query1)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         
                        $position=$row['position'];
                  $department=$row['department'];
                  $sector=$row['sector'];
                  $directorate=$row['directorate'];
                $section=$row["section"];
                $unit=$row["unit"];
                 $memotype=$row['memotype'];
                 $nature=$row['nature'];
                 
                  /*get station*/
           if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
        else if($position=='HOU'){
             $station=$unit;
        }
        else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
       /*get address*/
         $fulltitle=$position.",".$station;
         
         }
   
}
       
//indicate nature of of actor in log table
        if($nature=='direct'){
         //create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,actor_nature,action,at)VALUES(?,?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Forwarded';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
    $actor_nature="direct";
   $stmnt->bind_param('sssssss',$referenceno,$subject,$ses,$fulltitle,$actor_nature,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }
        else{

//create and execute a query
$sqlogs="insert into logs(referenceno,subject,actor_id,actor_recepient,action,at)VALUES(?,?,?,?,?,?)";
if($stmnt=$mysqli->prepare($sqlogs)){
    //execute
        $action='Forwarded';
        $at='at';
     //  $d =date("Y-m-d H:i:s");
   $subject='Memo:'.$subject;
   $stmnt->bind_param('ssssss',$referenceno,$subject,$ses,$fulltitle,$action,$at);
    $stmnt->execute();
    $message="Successfully updated";
   
}
        }

 //send message to all recepients
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.recepient=ememo_users.user_id where referenceno='$referenceno'";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Forwarded memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}
        //send message to requestor
$query="SELECT * FROM nonfinancialmemos inner join ememo_users on nonfinancialmemos.requestor=ememo_users.user_id where referenceno='$referenceno' LIMIT 1";
//execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
  while ($row = $res->fetch_assoc()) {
 $email_subject="Memo Activity Alert!";      	
$email_body = "$fulltitle Forwarded memo '$subject'. Follow up from the Nandi County E-Memo system";
$headers = "From: info@nandi.go.ke\n";  
mail($row['email'],$email_subject,$email_body,$headers);
   
        }
     
}

    
    
    }
    
    
    

}

 
    
    
    
?>