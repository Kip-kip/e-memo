<?php


error_reporting(E_ALL);
ini_set('display_errors',1);
include_once '../../config.php';

class DB_connect{
    
    //-----------------------------------------------------D.DIR INITIATING--------------------------------------------------------------//
    
    
     //*** CC TO REQUESTOR'S DIR WHEN REQUESTOR IS D.DIR***//                 
      public function ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
                   $query1="SELECT * FROM ememo_users WHERE directorate='$reqdirectorate' AND hierarchical_level='$reqhierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO RECIPIENT'S DIR WHEN REcipient IS D.DIR***//                   
      public function ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
                   $query1="SELECT * FROM ememo_users WHERE directorate='$recdirectorate' AND hierarchical_level='$rechierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
    //*** CC TO REQUESTOR'S CO WHEN REQUESTOR IS D.DIR***//                  
      public function ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
                   $query1="SELECT * FROM ememo_users WHERE sector='$reqsector' AND hierarchical_level='$reqhierarchical_level'+2";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO RECIPIENT'S CO WHEN Recipient IS D.DIR***//                    
      public function ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
                   $query1="SELECT * FROM ememo_users WHERE sector='$recsector' AND hierarchical_level='$rechierarchical_level'+2";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO REQUESTOR'S CECM WHEN REQUESTOR IS D.DIR***//                    
      public function ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
           $query1="SELECT * FROM ememo_users WHERE department='$reqdepartment' AND hierarchical_level='$reqhierarchical_level'+3";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO RECIPIENT'S CECM WHEN Recipirnt IS D.DIR***//                 
      public function ccToRecCECMRecisDDIR($mysqli,$recdepartment,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
           $query1="SELECT * FROM ememo_users WHERE department='$recdepartment' AND hierarchical_level='$rechierarchical_level'+3";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
    
       //-------------------------------------------------------------------DIR INITIATING------------------------------------------------------------//
    
   
    
    //*** CC TO REQUESTOR'S CO WHEN REQUESTOR IS DIR***//                   
      public function ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
                   $query1="SELECT * FROM ememo_users WHERE sector='$reqsector' AND hierarchical_level='$reqhierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO RECIPIENT'S CO WHEN Recipient IS DIR***//                   
      public function ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
            $query1="SELECT * FROM ememo_users WHERE sector='$recsector' AND hierarchical_level='$rechierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     //*** CC TO REQUESTOR'S CECM WHEN REQUESTOR IS DIR***//                   
      public function ccToReqCECMReqisDIR($mysqli,$reqdepartment,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
            $query1="SELECT * FROM ememo_users WHERE department='$reqdepartment' AND hierarchical_level='$reqhierarchical_level'+2";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
     
      //*** CC TO RECIPIENT'S CECM WHEN REcepient IS DIR***//                   
      public function ccToRecCECMRecisDIR($mysqli,$recdepartment,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
            $query1="SELECT * FROM ememo_users WHERE department='$recdepartment' AND hierarchical_level='$rechierarchical_level'+2";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
    
      //-------------------------------------------------------------------CO INITIATING------------------------------------------------------------//
    
    
       //*** CC TO RECIPIENT'S CECM WHEN REquestor IS CO***//                   
      public function ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level){
         $ses=$_SESSION["karibu"];
            $query1="SELECT * FROM ememo_users WHERE department='$reqdepartment' AND hierarchical_level='$reqhierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
      //*** CC TO RECIPIENT'S CECM WHEN RECIPIENT IS CO***//                   
      public function ccToRecCECMRecisCO($mysqli,$recdepartment,$rechierarchical_level){
         $ses=$_SESSION["karibu"];
            $query1="SELECT * FROM ememo_users WHERE department='$recdepartment' AND hierarchical_level='$rechierarchical_level'+1";
                   if ($mysqli->real_query($query1)) {
                       $res = $mysqli->use_result();
                       while ($row = $res->fetch_assoc()) {
         
                           $user_id=$row["user_id"];
                           $recipient_nature='Cc';
                           $author=$ses;
                            
                       } 
                   }
            //create and execute query
            $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

    
    }
    
    
    
    /*-----------------------------------------------------STATION MEETING CALLING----------------------------------------------*/
    
    
    
               
      
}
?>