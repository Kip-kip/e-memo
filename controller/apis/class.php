<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
session_start();
if(!isset($_SESSION['karibu'])){
    header("Location:login.php");
}

include('./db.php');
class App{

	public static function get_recipient_emails(){
		global $db;

		$data=$db->GetArray("SELECT * from ememo_users");

		foreach($data as $row){?>
			<option value="<?php echo $row["position"];?>"><?php echo $row["position"]; ?></option>
		<?php }
	}
  
	public static function add_temp_data($email){
         $ses=$_SESSION["karibu"];
	//original
        global $db;
		$data=array();
        
        $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php  
                    $recposition=$row['position'];
                    $recdepartment=$row['department'];
                    $recsector=$row['sector'];
                    $recdirectorate=$row['directorate'];
                    $recsection=$row['section'];
                    $recunit=$row['unit'];
                    $rechierarchical_level=$row['hierarchical_level']; 
                                
         if($recposition=='C.E.C.M'){
            $recstation=$recdepartment;
        }
        else if($recposition=='C.O'){
             $recstation=$recsector;
        }
         else if($recposition=='DIR'){
             $recstation=$recdirectorate;
        }
         else if($recposition=='D.DIR'){
             $recstation=$recsection;
        }
            else if($recposition=='HOU'){
             $recstation=$recunit;
        }
            else if($recposition=='STAFF'){
             $recstation=$recunit;
        }
        else{
             $recstation='County Government of Nandi';
        }
                                
                                
?>
      <?php }
        ?><?php
         include_once '../../config.php';
              $query ="SELECT * from temp_storage WHERE author=?";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $ses);
                $stmt->execute();
                $stmt->store_result();
            //If there exists a recepient in the nfm table who is set to be affected
                if($stmt->num_rows>=1){
                 require("../../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name);
                    //first delete
           $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$db->prepare($sql8)){
       $stmnt->execute();      
          }   
                    
                    //then insert
                    $query="SELECT * FROM ememo_users WHERE user_id='$email'";
                        if ($db->real_query($query)) {
                       $res = $db->use_result();
                       while ($row = $res->fetch_assoc()) {
                           $user_id=$row["user_id"];
                           $recipient_nature='direct';
                           $author=$ses;
                       } 
                   }
                    
                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
            if($stmnt=$mysqli->prepare($sql1)){
                $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                $stmnt->execute();
         
            }

                  } 
              
            //if there are no duplicates, proceed as normal]
    else{   
          ?><?php  
		$data['user_id']=$email;
        $data['recipient_nature']="direct";
        $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');
?><?php
    }}
        ?><?php
        //original
        
        /**********MAJOR LOGIC FOR MEMO STRUCTURE---SPANS LONGER*********/
       
        /**Get Requestor's Position, Sector and Department**/    
        
        ?>
        <?php
          require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
        
        $ses=$_SESSION["karibu"];
      $query="SELECT * FROM ememo_users WHERE user_id='$ses'";
    //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
     while ($row = $res->fetch_assoc()) {
         $reqposition=$row["position"];
         $reqdepartment=$row["department"];
         $reqsector=$row["sector"];
         $reqdirectorate=$row["directorate"];
         $reqsection=$row["section"];
         $reqhierarchical_level=$row["hierarchical_level"];
         }
   
}
        
include_once 'structure_utility.php';
//create an instance of DB_con class, an object
$con= new DB_connect();
         
          /////-----/////***** REQUESTOR IS DEPUTY DIRECTOR *****/////-----/////
        if($reqposition=="D.DIR"){  //check if requestor is a director(YES)   
    
    /***** D.DIR ---> D.DIR *****/
    if($recposition=="D.DIR"){  //check if recipient is a director as well()       
        if($reqdirectorate==$recdirectorate){  //check if they are from the same directorate(yes)    
          //none       
        }  
        else{ //if they do not share directorate    
            if($reqsector==$recsector){ //check if the share sector(to MIS)
                //cc to requestor's DIR
                          if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                //cc to recepients's DIR
                       if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    } 
            }  
            else{ //if the sectors are different   
                if($reqdepartment==$recdepartment){//check if they share department (to ABC)                
                      //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                      //cc to recipient's CO
                             if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){    } 
                     //cc to recipient's DIR
                             if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    } 
                    
                }
                else{//if departments are different(to XYZ)
                //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to requestor's CECM
                             if($con->ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
                    //cc to recipient's CECM
                            if($con->ccToRecCECMRecisDDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                      //cc to recipient's CO
                             if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){    } 
                     //cc to recipient's DIR
                             if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    } 
                }
            }
        }
    }
               /***** D.DIR ---> DIR *****/
            else if($recposition=="DIR"){               
            if($reqdirectorate==$recdirectorate){  //check if they are from the same directorate(yes)    
          //none       
              }  
            else{ //if they do not share directorate    
            if($reqsector==$recsector){ //check if the share sector(to Infrastructure)
                //cc to requestor's DIR
                          if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
            }  
            else{ //if the sectors are different   
                if($reqdepartment==$recdepartment){//check if they share department (to Administration)                
                      //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                      //cc to recipient's CO
                             if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){    } 
                    
                }
                else{//if departments are different(to CDVS)
                //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to requestor's CECM
                             if($con->ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
                    //cc to recipient's CECM
                            if($con->ccToRecCECMRecisDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                      //cc to recipient's CO
                             if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){    } 
                   
                }
            }
        }     
                
            }
    

    /***** D.DIR ---> C.O *****/
    else if($recposition=="C.O"){  // if the recipient is a C.O
        if($reqsector==$recsector){ //if they are under same sector
            //cc to Requestor's DIR
               if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
        }
        else{
            if($reqdepartment==$recdepartment){ //if they share department
               //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    }  
            }
            else{ //if departments are different
                //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to requestor's CECM
                             if($con->ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
                    //cc to recipient's CECM
                            if($con->ccToRecCECMRecisCO($mysqli,$recdepartment,$rechierarchical_level)){    }  
            }        
        }     
    }
    
    
    /*****D. DIR ---> C.E.C.M *****/
    else if($recposition=="C.E.C.M"){ //if recipient is CECM
        
        if($reqdepartment==$recdepartment){
                    //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    }  
        }
        else{ //if different departments
            //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to requestor's CECM
                             if($con->ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
        }
        
    }
    
    
    /***** D.DIR ---> EXECUTIVES *****/
    else if(($recposition=="C.O.F") OR($recposition=="D.C.S") OR ($recposition=="C.S") OR ($recposition=="D.G") OR ($recposition=="GO")){
             //cc to requestor's DIR
                              if($con->ccToReqDIRReqisDDIR($mysqli,$reqdirectorate,$reqhierarchical_level)){    } 
                     //cc to requestor's CO
                             if($con->ccToReqCOReqisDDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to requestor's CECM
                             if($con->ccToReqCECMReqisDDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
    } 
    

}
        
        
        
        
        
        
        
        
        
        
        
        /////-----/////***** REQUESTOR IS DIRECTOR *****/////-----/////
        
if($reqposition=="DIR"){  //check if requestor is a director(YES)   
    
     /***** DIR ---> D.DIR *****/
    if($recposition=="D.DIR"){  //check if recipient is a director as well(yes)       
        if($reqdirectorate==$recdirectorate){  //check if they are from the same directorate(yes)    
           //none
        }  
        else{ //if they do not share directorate    
            if($reqsector==$recsector){ //check if the share sector(to MIS)
                
                //cc to recepients's DIR
                       if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    } 
                  
            }  
            else{ //if the sectors are different   
                if($reqdepartment==$recdepartment){//check if the departments are same(to ABC)
                    //cc to Requestor's CO
                       if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to Recipient's CO
                      if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){    } 
                    //cc to Recipients DIR
                      if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    } 
                }
                else{//different departments(XYZ)
                    //cc to Requestor's CO
                       if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                    //cc to Requestors's CECM
                       if($con->ccToReqCECMReqisDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
                    //cc to Recipient's CECM
                     if($con->ccToRecCECMRecisDDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                    //cc to Recipient's CO
                     if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){    }
                    //cc to Recipient's DIR
                     if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }
                       
                }
}
        }
    }
    
    /***** DIR ---> DIR *****/
    else if($recposition=="DIR"){  //check if recipient is a director as well(yes)       
        if($reqsector==$recsector){  //check if they are from the same sector(yes)    
            //none  
        }  
        else{ //if they do not share sector    
            if($reqdepartment==$recdepartment){ //check if the share department(to Administration)
                //cc to Requestor's CO
                          if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                //cc to Recepients's CO
                       if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){    } 
              
            }  
            else{ //if the departments are different   (to CDVS)
                   //cc to Requestor's CO
                   if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                //cc to Requestor's CECM
                   if($con->ccToReqCECMReqisDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
                  //cc to Recepients's CECM
                   if($con->ccToRecCECMRecisDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                  //cc to Recepients's CO
                   if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){    } 
            }
        }
    }
    

    /***** DIRECTOR ---> C.O *****/
    else if($recposition=="C.O"){  // if the recipient is a C.O
        if($reqsector==$recsector){ //if they are under same sector
            //none
        }
        else{
            if($reqdepartment==$recdepartment){ //if they share department(to CO administration)
                //cc to Requestor's CO
                if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
               
            }
            else{ //if departments are different(ALF)
                //cc to Requestor's C.O
                  if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
                
            }        
        }     
    }
    
    
    /***** DIRECTOR ---> C.E.C.M *****/
    else if($recposition=="C.E.C.M"){ //if recipient is CECM
        
        if($reqdepartment==$recdepartment){
            //cc to Requestors CO
              if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    } 
        }
        else{ //if different departments
            //cc to Requestor's C.O
             if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    }    
            //cc to Requestor's C.E.C.M
              if($con->ccToReqCECMReqisDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
        }
        
    }
    
    
    /***** DIRECTOR ---> EXECUTIVES *****/
    else if(($recposition=="C.O.F") OR($recposition=="D.C.S") OR ($recposition=="C.S") OR ($recposition=="D.G") OR ($recposition=="GO")){
              //cc to Requestor's C.O
             if($con->ccToReqCOReqisDIR($mysqli,$reqsector,$reqhierarchical_level)){    }    
            //cc to Requestor's C.E.C.M
              if($con->ccToReqCECMReqisDIR($mysqli,$reqdepartment,$reqhierarchical_level)){    } 
    } 
    

}
        
        
            /////-----/////***** REQUESTOR IS C.O *****/////-----/////       
else if($reqposition=="C.O"){  //check if requestor is a C.O(YES)   
    
     /***** C.O ---> D.DIR *****/
    if($recposition=="D.DIR"){  //check if recipient is a DEPUTY director(yes)    
         if($reqsector==$recsector){ //check if they share sector
             //cc Recepients DIR
               if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }   
         }
        else{
            if($reqdepartment==$recdepartment){//if they share departments(ABC)
                //cc Recipient's DIR
                  if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }   
                //cc Recipient's CO
                  if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){  }  
            }
            else{ //different departments(xyz)
                //cc Requestor's C.E.C.M
                 if($con->ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level)){  }  
                //cc Recipients C.E.C.M
                 if($con->ccToRecCECMRecisDDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                 //cc Recipient's CO
                  if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){  }    
                  //cc Recipient's DIR
                  if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }   
               
            }
        }         
    }  
    /***** C.O ---> DIRECTOR *****/
   else if($recposition=="DIR"){  //check if recipient is a director(yes)    
         if($reqsector==$recsector){ //check if they share sector
             //none
         }
        else{
            if($reqdepartment==$recdepartment){//if they share departments
                //cc Recipient's C.O
                    if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){  }     
              
            }
            else{ //different departments
                //cc Requestor's C.E.C.M
                if($con->ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level)){  }  
                //cc Recipients C.E.C.M
                 if($con->ccToRecCECMRecisDIR($mysqli,$recdepartment,$rechierarchical_level)){    } 
                //cc Recipients C.O
                 if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){  }   
            }
        }         
    }
    
    /***** C.O ---> C.O *****/
    else if($recposition=="C.O"){ //check if recipient is CO(yes)
        if($reqdepartment==$recdepartment){ //check if they share department
          //none
        }
        else{
           //cc Requestor's C.E.C.M
                if($con->ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level)){  }  
                //cc Recipients C.E.C.M
                 if($con->ccToRecCECMRecisCO($mysqli,$recdepartment,$rechierarchical_level)){  } 
    }
    }
    
    /***** C.O ---> C.E.C.M *****/
    else if($recposition=="C.E.C.M"){ //recipient is CECM yes
        if($reqdepartment==$recdepartment){ //if they share department
          //none
        }
        else{ //different departments
            //cc Requestor's C.E.C.M
            if($con->ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level)){  }  
        }
    }
    
    /***** C.O ---> EXECUTIVES *****/
    else if(($recposition=="C.O.F") OR($recposition=="D.C.S") OR ($recposition=="C.S") OR ($recposition=="D.G") OR ($recposition=="GO")){
           //cc to Requestor's C.E.C.M
       if($con->ccToReqCECMReqisCO($mysqli,$reqdepartment,$reqhierarchical_level)){  }    
    }
    }
        

    
    
        /////-----/////***** REQUESTOR IS C.E.C.M *****/////-----/////
 else if($reqposition=="C.E.C.M"){
       /***** C.E.C.M ---> DIRECTOR *****/
    if($recposition=="D.DIR"){  //check if recipient is a director(yes)    

        if($reqdepartment==$recdepartment){  //if they share department
             //cc Recipient's CO
                  if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){  } 
            //cc Recipient's DIR
                  if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }   
             
        }
        else{ //if departments are different(XYZ)
            //cc to Recipient's C.E.C.M
            if($con->ccToRecCECMRecisDDIR($mysqli,$recdepartment,$rechierarchical_level)){  }    
             //cc Recipient's CO
                  if($con->ccToRecCORecisDDIR($mysqli,$recsector,$rechierarchical_level)){  } 
            //cc Recipient's DIR
                  if($con->ccToRecDIRRecisDDIR($mysqli,$recdirectorate,$rechierarchical_level)){    }    
        }
    }   
         /***** C.E.C.M ---> DIRECTOR *****/
    else if($recposition=="DIR"){  //check if recipient is a director(yes)    

        if($reqdepartment==$recdepartment){  //if they share department
              //cc Recipient's CO
                  if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){  } 
        }
        else{ //if departments are different
          //cc to Recipient's C.E.C.M
            if($con->ccToRecCECMRecisDIR($mysqli,$recdepartment,$rechierarchical_level)){  }    
             //cc Recipient's CO
                  if($con->ccToRecCORecisDIR($mysqli,$recsector,$rechierarchical_level)){  } 
        }
    }   
      /***** C.E.C.M ---> C.O *****/
    else if($recposition=="C.O"){ //check if recipient is CO(yes)
         if($reqdepartment==$recdepartment){  //if they share department
            //none
     }
        else{ //if departments are different
           //cc to Recipient's C.E.C.M
            if($con->ccToRecCECMRecisCO($mysqli,$recdepartment,$rechierarchical_level)){  } 
        }
    
    }
     
     /***** C.E.C.M ---> C.E.C.M *****/
    else if($recposition=="C.E.C.M"){ //recipient is CECM yes
        //ALLOWED
    }
     
     /***** C.E.C.M ---> EXECUTIVES *****/
    else if(($recposition=="C.O.F") OR($recposition=="D.C.S") OR ($recposition=="C.S") OR ($recposition=="D.G") OR ($recposition=="GO")){
        //ALLOWED
    }
     
 }
        
                    
?>
      <?php       
           
/*****PART OF THE ORIGINAL*****/
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                ".$recposition.", ".$recstation."
                            </div>
";
        
        
//part of the original
        
        	
        	
	}
      //ADD station DATA TO TEMP_STORAGE
      public static function add_temp_station($email){
     
        ?>
        <?php
          $ses=$_SESSION["karibu"];
          require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
          
         if($email=="allstaff"){
              $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();      
      }
             $station="All staff";
               $stationtype="NCG";
             
             //select all users from database and insert to temp_storage
                 $coabc="SELECT * FROM ememo_users WHERE user_id!=1";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                  
                                                }
                                                }
             
                                                    //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                        $recipient_nature="ALL STAFF";
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
             
             
                             //delete the sending user
                             $sql8="DELETE FROM temp_storage WHERE user_id='$ses'";
                              if($stmnt=$mysqli->prepare($sql8)){
                               $stmnt->execute();      
                              }
         }
         
          else if($email=="allcos"){
              
             $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();      
      }
               $station="C.Os";
               $stationtype="All sectors";
              
            
               //select all cos from ememo users and insert to temp_storage
                 $coabc="SELECT * FROM ememo_users WHERE position='C.O'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                   
                                                }
                                                }
              
               //insert Ciro
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                        $recipient_nature="ALL CHIEF OFFICERS";
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
              
              
              //get senders info
               $coabc="SELECT * FROM ememo_users WHERE user_id='$ses'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    $sector=$row['sector'];
                                                    $position=$row['position'];
                                                }
                                                }
              
              
              //if the sendin user is a DIR 
              if($position=='DIR'){
              //cc the requestor CO
               $coabc="SELECT * FROM ememo_users WHERE position='C.O' AND sector='$sector'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                   
                                                     $sql="UPDATE temp_storage SET recipient_nature='Through' WHERE user_id='$user_id'";
                                                        if($stmnt=$mysqli->prepare($sql)){
                                                        $stmnt->execute();      
                                                        }    

                                                }
                                                }
              }
              else if($position=='C.O'){
                  //delete the sender from temp_storage to avoid sending to self
                    $sql8="DELETE FROM temp_storage WHERE user_id='$ses'";
                      if($stmnt=$mysqli->prepare($sql8)){
                       $stmnt->execute();      
                      }
              }
              else{
                  
                  
              }
              
              
          }
           else if($email=="allcecs"){
              $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();      
      }
               $station="C.E.Cs";
               $stationtype="All Departments";
              
            
               //select all cecms from ememo users and insert to temp_storage
                 $coabc="SELECT * FROM ememo_users WHERE position='C.E.C.M'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                  
                                                }
                                                }
               
               
                 //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                        $recipient_nature="ALL C.E.Cs";
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                    
               
               
               
              
              //get senders info
               $coabc="SELECT * FROM ememo_users WHERE user_id='$ses'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    $department=$row['department'];
                                                    $position=$row['position'];
                                                }
                                                }
              
              
              //if the sendin user is a C.O
              if($position=='C.O'){
              //cc the requestor CECM
               $coabc="SELECT * FROM ememo_users WHERE position='C.E.C.M' AND department='$department'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                   
                                                     $sql="UPDATE temp_storage SET recipient_nature='Through' WHERE user_id='$user_id'";
                                                        if($stmnt=$mysqli->prepare($sql)){
                                                        $stmnt->execute();      
                                                        }    

                                                }
                                                }
              }
              else if($position=='C.E.C.M'){
                  //delete the sender from temp_storage to avoid sending to self
                    $sql8="DELETE FROM temp_storage WHERE user_id='$ses'";
                      if($stmnt=$mysqli->prepare($sql8)){
                       $stmnt->execute();      
                      }
              }
              else{
                  
                  
              }
              
          }
          else{
              
              $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();    
      }
                 $juke="SELECT * FROM broadcast WHERE id='$email'";
                                    $result1=mysqli_query($mysqli,$juke);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                    
                                                   $station=$row['station'];
                                                   $stationtype=$row['stationtype'];
                                                }}
          
          
          if($stationtype=='Department'){
                 $coabc="SELECT * FROM ememo_users WHERE department='$station'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                  
                                                }     

                                            }
              
                //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                        $recipient_nature=$station." ".$stationtype;
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }

                                        
          }
          else if($stationtype=='Sector'){
                $coabc="SELECT * FROM ememo_users WHERE sector='$station'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                    
                                                }     

                                            }
               //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                         $recipient_nature=$station." ".$stationtype;
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                         
          }
          else if($stationtype=='directorate'){
                 $coabc="SELECT * FROM ememo_users WHERE directorate='$station'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                    

                                                }     

                                            }
               //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                         $recipient_nature=$station." ".$stationtype;
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                        
          }
          else if($stationtype=='section'){
               $coabc="SELECT * FROM ememo_users WHERE section='$station'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                   
                                                }
                                                }
                //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                        $recipient_nature=$station." ".$stationtype;
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
              
          }
          else if($stationtype=='unit'){
                        $coabc="SELECT * FROM ememo_users WHERE unit='$station'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='direct';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
                                                   
                                                }
                                                }
                //insert Cairo
                                                    $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $user_id=1;
                                                         $recipient_nature=$station." ".$stationtype;
                                                        $author=$ses;
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }
              
          }
          
              
          
          }

                    
?>
      <?php       
           
/*****PART OF THE ORIGINAL*****/
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                ".$station.", ".$stationtype."
                            </div>
";
        
        
//part of the original
        
      }
    
      //ADD financial DATA TO TEMP_STORAGE
      public static function add_temp_financial($email){
           $ses=$_SESSION["karibu"];
          	global $db;
          
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
            $section=$row['section'];
            $unit=$row['unit'];
          
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
            
              require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
            //delete from the table first
            $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();    
      }
?>
      <?php }
		$data['user_id']=$email;
        $data['recipient_nature']="direct";
        $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');


             ?>
        <?php
          require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
          //get the current user info
         $coabc="SELECT * FROM ememo_users WHERE user_id='$ses'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $curposition=$row['position'];
                                                }
                                                }
               
          //if the user is C.O then dont cc the c.o finance
          
          if($curposition!=='C.O'){
                 $coabc="SELECT * FROM ememo_users WHERE sector='Finance' AND hierarchical_level=3";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='Cc';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }

                                                }     

                                            }
          }
              else{
                  
              }
                                        
          ?>
      <?php   
        
          
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
          
      }
    
	
	
	  //ADD local travel DATA TO TEMP_STORAGE
      public static function add_temp_loctravel($email){
           $ses=$_SESSION["karibu"];
          	global $db;
          
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
            $section=$row['section'];
            $unit=$row['unit'];
          
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
            
              require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
            //delete from the table first
            $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();    
      }
?>
      <?php }
		$data['user_id']=$email;
        $data['recipient_nature']="direct";
        $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');


             ?>
        <?php
          require("../../_connect.php");

               
          // Go through the COunty Secretary
          
                 $coabc="SELECT * FROM ememo_users WHERE position='C.S'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='Through';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }

                                                }     

                                            }
          
                                        
          ?>
      <?php   
        
          
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
          
      }
	
	
	 //ADD local travel DATA TO TEMP_STORAGE
      public static function add_temp_inttravel($email){
           $ses=$_SESSION["karibu"];
          	global $db;
          
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
            $section=$row['section'];
            $unit=$row['unit'];
          
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
            
              require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name); 
            //delete from the table first
            $sql8="DELETE FROM temp_storage WHERE author='$ses'";
      if($stmnt=$mysqli->prepare($sql8)){
       $stmnt->execute();    
      }
?>
      <?php }
		$data['user_id']=$email;
        $data['recipient_nature']="direct";
        $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');


             ?>
        <?php
          require("../../_connect.php");

               
          // Go through the Deputy Governor
          
                 $coabc="SELECT * FROM ememo_users WHERE position='D.G'";
                                    $result1=mysqli_query($mysqli,$coabc);
                                            if($result1) {
                                                while($row=mysqli_fetch_assoc($result1)) {
                                                   $user_id=$row['user_id'];
                                                    $recipient_nature='Through';
                                                    $author=$ses;
                                                       $sql1="insert into temp_storage(user_id,recipient_nature,author)VALUES(?,?,?)";
                                                    if($stmnt=$mysqli->prepare($sql1)){
                                                        $stmnt->bind_param('sss',$user_id,$recipient_nature,$author);
                                                        $stmnt->execute();

                                                    }

                                                }     

                                            }
          
                                        
          ?>
      <?php   
        
          
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
          
      }
	
	
	
	
    
       //ADD FORWARD DATA TO TEMP_STORAGE
      public static function add_forward_data($email){
           $ses=$_SESSION["karibu"];
          	global $db;
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
            $section=$row['section'];
            $unit=$row['unit'];
          
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
                    
?>
      <?php }
		$data['user_id']=$email;
        $data['recipient_nature']="direct";
           $data['author']=$ses;
     
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');

          
		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_single_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
          
      }
    
    
    
    
    //ADD CC DATA TO TEMP_STORAGE
    
    public static function add_temp_cc_data($email){
         $ses=$_SESSION["karibu"];
		global $db;
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
             $section=$row['section'];
            $unit=$row['unit'];
            /*  $name= $row['fname'];
                 $salutation=$row['mname'];
                */
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
        else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
                    
?>
      <?php }
        
        ?><?php
         include_once '../../config.php';
              $query ="SELECT * from temp_storage WHERE author='$ses' AND user_id=?";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $email);
                $stmt->execute();
                $stmt->store_result();
            //If there exists a recepient in the nfm table who is set to be affected
                if($stmt->num_rows>=1){
                    //select all from temp_storage in order to check nature
                      require("../../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name);
                      $query19="SELECT * FROM temp_storage where user_id='$email' AND author='$ses'";
if ($db->real_query($query19)) {
    $res = $db->use_result();
 while ($row = $res->fetch_assoc()) {
         $nature=$row["recipient_nature"];}}
                    
                    //if nature is Direct replace the one in nfmt with
                  if($nature=='Cc'){
                      //update the nature to direct
           $sql8="UPDATE temp_storage SET recipient_nature='Through' WHERE user_id='$email' AND author='$ses'";
      if($stmnt=$db->prepare($sql8)){
       $stmnt->execute();      
          }   
                  } 
                    else{//dont affect
                        
                    }
                    
              
            }
            //if there are no duplicates, proceed as normal]
    else{   
          ?><?php  
		$data['user_id']=$email;
        $data['recipient_nature']="Through";
        $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');
?><?php
    }}
        ?><?php

		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_singlethr_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
        
        
	}
    
    
     public static function add_temp_ccf_data($email){
          $ses=$_SESSION["karibu"];
		global $db;
		$data=array();
         $data=$db->GetArray("SELECT * from ememo_users where user_id='$email'");
        
        foreach($data as $row) {?>
        <?php
              $position=$row['position'];
             $department=$row['department'];
             $sector=$row['sector'];
             $directorate=$row['directorate'];
            $section=$row['section'];
            $unit=$row['unit'];
            /*  $name= $row['fname'];
                 $salutation=$row['mname'];
                */
             if($position=='C.E.C.M'){
            $station=$department;
        }
        else if($position=='C.O'){
             $station=$sector;
        }
         else if($position=='DIR'){
             $station=$directorate;
        }
         else if($position=='D.DIR'){
             $station=$section;
        }
            else if($position=='HOU'){
             $station=$unit;
        }
            else if($position=='STAFF'){
             $station=$unit;
        }
        else{
             $station='County Government of Nandi';
        }
                    
?>
      <?php }
		$data['user_id']=$email;
        $data['recipient_nature']="Through";
       $data['author']=$ses;
        
        
		$db->AutoExecute('temp_storage',$data, 'INSERT');


		return "<div class='col-sm-12 alert alert-info alert-dismissible fade show' >
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close' id=".$email." onclick='delete_singlethr_data(this.id)'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                 ".$position.", ".$station."
                            </div>
";
     
        
        
	}
    
    
	public static function single_temp_delete($email){
       $ses=$_SESSION["karibu"];
		global $db;

		$db->Execute("DELETE from temp_storage WHERE (recipient_nature='Cc' AND author='$ses') OR (recipient_nature='direct' AND author='$ses') ");
		return 'email has been removed';

	}
    public static function singlethr_temp_delete($email){
       $ses=$_SESSION["karibu"];
		global $db;

		$db->Execute("DELETE from temp_storage WHERE user_id='$email' AND author='$ses'");
		return 'email has been removed';

	}
	
	public static function delete_all_temp(){
         $ses=$_SESSION["karibu"];
		global $db;

		$db->Execute("DELETE from temp_storage WHERE author='$ses'");
	}

	public static function get_all_temp_data(){
		global $db;

		$data=$db->GetArray("SELECT email from temp_storage");

		?>
		<div class="row">
		<?php
		foreach($data as $row){
		?>
			<div class="col-sm-4 alert alert-info alert-dismissible fade show" >
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?php echo $row['email']; ?>
                            </div>

		<?php
		}
		?> 
		</div>
		<?php 
	}

    
    
	public static function add_memo_data($subject,$datecreated,$duedate,$introduction,$amount,$from,$memotype,$urgency,$prevmemo,$refdraft,$votehead){
		global $db;
		$temp_data=$db->GetArray("SELECT * from temp_storage WHERE author='$from'");
		//$ref_no=App::produce_reference_no();
      
        	 //open ?>
        	 <?php
           require("../../_connect.php");
//connect to db
$diblo = new mysqli($db_host,$db_user, $db_password, $db_name);
        //Delete from drafts table. Just in case if it was a draft 
        $sql="DELETE FROM nonfinancialdrafts WHERE referenceno='$refdraft'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
         //step 1: get department of the sender
$query1="SELECT * FROM ememo_users where user_id='$from'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department'];
            $sect=$row['sector'];
            $dir=$row['directorate'];
} }    
        //GET DEPARTMENT ID
        $query1="SELECT * FROM departments_list where department='$dept'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department_id'];
             $dept_id = substr($dept, 1);
} }    
        //GET SECTOR ID
 $query1="SELECT * FROM sectors_list where sector='$sect'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $sect=$row['sector_id'];
             $sect_id = substr($sect, 1);
} }    
        //GET DIRECTORATE  ID
      $query1="SELECT * FROM directorates_list where directorate='$dir'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dir=$row['directorate_id'];
             $dir_id = substr($dir, 3);
} }   
        
         //step 2: get counter of that particular department
    $query2="SELECT * FROM referenceno_counter";
if ($diblo->real_query($query2)) {
    $res = $diblo->use_result();
  while ($row = $res->fetch_assoc()) {
            $count=$row['counter'];
}  } 
  
      //department
if(($dept!=='') AND ($sect=='') AND ($dir=='')){
        $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
         $ref_no="REF:NCG/$dept_id/$leadcounter/19-20";   
        //step 3: update the counter
        //include_once 'config.php';
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
}
        //sector
        else if(($dept!=='') AND ($sect!=='')AND($dir=='')) {
                $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$leadcounter/19-20";
        //step 3: update the counter
        //include_once 'config.php';
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
        //directorate
        else if(($dept!=='') AND ($sect!=='')AND($dir!=='')){
               $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$dir_id/$leadcounter/19-20";
        //step 3: update the counter
        //include_once 'config.php';
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
	
        /*****CHECK TO ENSURE THERE IS NO DATA WITH REQUESTOR IN TEMP_MEMOHOLDER****/
        
       include_once '../../config.php';
              $query ="SELECT * from temp_memoholder WHERE requestor=?";  
        if($stmt = $mysqli->prepare($query)){
                $stmt->bind_param('s', $from);
                $stmt->execute();
                $stmt->store_result();
            //If there exists a recepient in the nfm table who is set to be affected
                if($stmt->num_rows>=1){
                    //delete from the temp_memoholder
            $sql8="DELETE FROM temp_memoholder WHERE requestor='$from'";
      if($stmnt=$diblo->prepare($sql8)){
       $stmnt->execute();      
          }
                 //close   ?><?php
                    foreach ($temp_data as $row) {  
				$data=array();
			$data['recepient']=$row['user_id'];
            $data['nature']=$row['recipient_nature'];
			$data['referenceno']=$ref_no;
            $data['memotype']=$memotype;
            $data['financial_year']="2019-2020"; 
            $data['votehead']=$votehead;
            $data['urgency']=$urgency;
            $data['prevmemo']=$prevmemo;
			$data['requestor']=$from;
			$data['subject']=$subject;
            $data['datecreated']=$datecreated;
			$data['duedate']=$duedate;
			$data['introduction']=$introduction;
            $data['amount']=$amount;
            $data['approvedamount']=0;
            $data['comments']="";
            $data['comment_status']=0;
            $data['progress']=0;
            $data['status']='Inactive';
            $data['generalstatus']='Inactive';
            $data['generalprogress']=0;
            $data['view']=0;
            $data['availability']="";
            $data['vh_deduct_indicator']=0;

			$db->AutoExecute('temp_memoholder',$data,'INSERT');
                       //open  ?><?php
         require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
                        
    $query="SELECT * FROM temp_memoholder WHERE requestor='$from'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $introduction=$row["introduction"];   
       $replacedone=str_ireplace('|', '&nbsp;',$introduction);
        $replacedtwo=str_ireplace('~', '&#39;',$replacedone);
         $replacedthree=str_ireplace('`', '&quot;',$replacedtwo);
    }
}
                        
                        
   $sqli="UPDATE temp_memoholder SET introduction='$replacedthree'";
     if($stmnt=$mysqli->prepare($sqli)){
   
        $stmnt->execute(); 
}  
                        
                    //close    ?><?php

        
		}
    //open	?><?php
                    
                   
                  }
            else{
             //close   ?><?php
                	foreach ($temp_data as $row) {  
				$data=array();
			$data['recepient']=$row['user_id'];
            $data['nature']=$row['recipient_nature'];
			$data['referenceno']=$ref_no;
            $data['memotype']=$memotype;
            $data['financial_year']="2019-2020"; 
            $data['votehead']=$votehead;
            $data['urgency']=$urgency;
            $data['prevmemo']=$prevmemo;
			$data['requestor']=$from;
			$data['subject']=$subject;
            $data['datecreated']=$datecreated;
			$data['duedate']=$duedate;
			$data['introduction']=$introduction;
            $data['amount']=$amount;
            $data['approvedamount']=0;
            $data['comments']="";
            $data['comment_status']=0;
            $data['progress']=0;
            $data['status']='Inactive';
            $data['generalstatus']='Inactive';
            $data['generalprogress']=0;
            $data['view']=0;
            $data['availability']="";
            $data['vh_deduct_indicator']=0;

			$db->AutoExecute('temp_memoholder',$data,'INSERT');
        	
                //open         ?><?php
         require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);    
                        
    $query="SELECT * FROM temp_memoholder WHERE requestor='$from'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();

    while ($row = $res->fetch_assoc()) {
         $introduction=$row["introduction"];   
       $replacedone=str_ireplace('|', '&nbsp;',$introduction);
        $replacedtwo=str_ireplace('~', '&#39;',$replacedone);
         $replacedthree=str_ireplace('`', '&quot;',$replacedtwo);
    }
}
                        
                        
   $sqli="UPDATE temp_memoholder SET introduction='$replacedthree'";
     if($stmnt=$mysqli->prepare($sqli)){
   
        $stmnt->execute(); 
}  
                        
                //close        ?><?php

                        
		}
    
          //open      ?><?php
                
            }
        }
           //close
        
		
		
		
        App::update_document_directory($ref_no,$from);
        
		
		$response['success']=1;
	
	
		
       //open 	 ?>
        	 <?php
         
 //connect to db
  require("../../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
       
/**TRUNCATE THE TEMP_STORAGE AFTER USE**/
   $sql="DELETE FROM temp_storage WHERE author='$from'";
     if($stmnt=$db->prepare($sql)){
   
        $stmnt->execute(); 
}  
    

		//close       ?>
			
        	<?php
	

		
		return json_encode($response);	
        
        //redirect to preview page
    
	}
	
	/*****--------------------SAVE TO TEMP LOCAL TRAVEL MEMO HOLDER TABLE------------------*****/
	public static function add_memo_loctravel_data($subject,$datecreated,$duedate,$introduction,$amount,$from,$memotype,$urgency,$prevmemo,$refdraft,$votehead,$personalno,$designation,$telephone,$duration,$startdate,$enddate,$destination,$purpose,$activities,$delegatedofficer,$delegateddesign,$components){
		global $db;
		$temp_data=$db->GetArray("SELECT * from temp_storage WHERE author='$from'");
		//$ref_no=App::produce_reference_no();
      
      //open  	 ?>
        	 <?php
           require("../../_connect.php");
//connect to db
$diblo = new mysqli($db_host,$db_user, $db_password, $db_name);
        //Delete from drafts table. Just in case if it was a draft lol
        $sql="DELETE FROM nonfinancialdrafts WHERE referenceno='$refdraft'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
         //step 1: get department of the sender
$query1="SELECT * FROM ememo_users where user_id='$from'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department'];
            $sect=$row['sector'];
            $dir=$row['directorate'];
} }    
        //GET DEPARTMENT ID
        $query1="SELECT * FROM departments_list where department='$dept'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department_id'];
             $dept_id = substr($dept, 1);
} }    
        //GET SECTOR ID
 $query1="SELECT * FROM sectors_list where sector='$sect'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $sect=$row['sector_id'];
             $sect_id = substr($sect, 1);
} }    
        //GET DIRECTORATE  ID
      $query1="SELECT * FROM directorates_list where directorate='$dir'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dir=$row['directorate_id'];
             $dir_id = substr($dir, 3);
} }   
        
         //step 2: get counter
    $query2="SELECT * FROM referenceno_counter";
if ($diblo->real_query($query2)) {
    $res = $diblo->use_result();
  while ($row = $res->fetch_assoc()) {
            $count=$row['counter'];
}  } 
  
      //department
if(($dept!=='') AND ($sect=='') AND ($dir=='')){
        $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
         $ref_no="REF:NCG/$dept_id/$leadcounter/19-20";   
        //step 3: update the counter
        //include_once 'config.php';
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
}
        //sector
        else if(($dept!=='') AND ($sect!=='')AND($dir=='')) {
                $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$leadcounter/19-20";
        //step 3: update the counter
      
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
        //directorate
        else if(($dept!=='') AND ($sect!=='')AND($dir!=='')){
               $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$dir_id/$leadcounter/19-20";
			
        //step 3: update the counter
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
	
		
        /****delete from the temp_memoholder where the Requestor is current user****/
                
            $sql8="DELETE FROM temp_memoholder WHERE requestor='$from'";
      if($stmnt=$diblo->prepare($sql8)){
       $stmnt->execute();      
          }
		
           //close         ?><?php
		
		
		//INSERT INTO TEMP_MEMO HOLDER
                    foreach ($temp_data as $row) {  
				$data=array();
			$data['recepient']=$row['user_id'];
            $data['nature']=$row['recipient_nature'];
			$data['referenceno']=$ref_no;
            $data['memotype']=$memotype;
            $data['financial_year']="2019-2020"; 
            $data['votehead']=$votehead;
            $data['urgency']=$urgency;
            $data['prevmemo']=$prevmemo;
			$data['requestor']=$from;
			$data['subject']=$subject;
            $data['datecreated']=$datecreated;
			$data['duedate']=$duedate;
			$data['introduction']=$introduction;
            $data['amount']=$amount;
            $data['approvedamount']=0;
            $data['comments']="";
            $data['comment_status']=0;
            $data['progress']=0;
            $data['status']='Inactive';
            $data['generalstatus']='Inactive';
            $data['generalprogress']=0;
            $data['view']=0;
            $data['availability']="";
            $data['vh_deduct_indicator']=0;

			$db->AutoExecute('temp_memoholder',$data,'INSERT');
      //open                  ?><?php
         require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
                        
    $query="SELECT * FROM temp_memoholder WHERE requestor='$from'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
    while ($row = $res->fetch_assoc()) {
         $introduction=$row["introduction"];   
       $replacedone=str_ireplace('|', '&nbsp;',$introduction);
        $replacedtwo=str_ireplace('~', '&#39;',$replacedone);
         $replacedthree=str_ireplace('`', '&quot;',$replacedtwo);
    }
}
                        
                        
   $sqli="UPDATE temp_memoholder SET introduction='$replacedthree'";
     if($stmnt=$mysqli->prepare($sqli)){   
       $stmnt->execute(); 
}  
                        
       //close                 ?><?php

        
		}
            
		
		/***DELETE  ALL FROm TRAVEL TEMP MEMO HOLDER WHERE OFFICER_ID is current USER***/
		 //open ?><?php
		
		 require("../../_connect.php");
		$sql8="DELETE FROM temp_loctravel_memoholder WHERE officer_id='$from'";
      if($stmnt=$diblo->prepare($sql8)){
       $stmnt->execute();      
          }
		  
		 //close ?><?php
		
		//INSERT INTO LOCAL TRAVEL TEMP MEMO HOLDER
                    foreach ($temp_data as $row) {  
			$data=array();
			$data['referenceno']=$ref_no;
			$data['recipient']=$row['user_id'];
            $data['traveltype']=$memotype;
			$data['officer_id']=$from;
            $data['personal_number']=$personalno;
            $data['designation']=$designation; 
            $data['telephone']=$telephone;                    
            $data['duration']=$duration;
            $data['start_date']=$startdate;
			$data['end_date']=$enddate;
			$data['destination']=$destination;
			$data['purpose']=$purpose;
			$data['activities']=$activities;
            $data['delegated_officer']=$delegatedofficer;
            $data['delegated_officer_design']=$delegateddesign;
            $data['components']=$components;

			$db->AutoExecute('temp_loctravel_memoholder',$data,'INSERT');
    

        
		}
		
		
		
		
		
        
		
        App::update_document_directory($ref_no,$from);
        
		
		$response['success']=1;
	
	
		
//open        	 ?>
        	 <?php
         
 //connect to db
  require("../../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
       
/**TRUNCATE THE TEMP_STORAGE AFTER USE**/
   $sql="DELETE FROM temp_storage WHERE author='$from'";
     if($stmnt=$db->prepare($sql)){
   
        $stmnt->execute(); 
}  
    
    

//close   ?>
			
        	<?php
	

		
		return json_encode($response);	
        
        //redirect to preview page
    
	}
	
	
	/*****--------------------SAVE TO TEMP INTERNATIONAL TRAVEL MEMO HOLDER TABLE------------------*****/
	public static function add_memo_inttravel_data($subject,$datecreated,$duedate,$introduction,$amount,$from,$memotype,$urgency,$prevmemo,$refdraft,$votehead,$intpersonalno,$intdesignation,$inttelephone,$intreasons,$intcountry,$intcity,$intdepart,$intreturn,$inttravelmode,$inthost,$intsponsor,$intsubsistence,$intsponsortwo,$intaccomodation,$intparticipation,$inttravelexp,$inttotal,$intobjs,$intbenefits){
		global $db;
		$temp_data=$db->GetArray("SELECT * from temp_storage WHERE author='$from'");
		//$ref_no=App::produce_reference_no();
      
      //open  	 ?>
        	 <?php
           require("../../_connect.php");
//connect to db
$diblo = new mysqli($db_host,$db_user, $db_password, $db_name);
        //Delete from drafts table. Just in case if it was a draft lol
        $sql="DELETE FROM nonfinancialdrafts WHERE referenceno='$refdraft'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
         //step 1: get department of the sender
$query1="SELECT * FROM ememo_users where user_id='$from'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department'];
            $sect=$row['sector'];
            $dir=$row['directorate'];
} }    
        //GET DEPARTMENT ID
        $query1="SELECT * FROM departments_list where department='$dept'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dept=$row['department_id'];
             $dept_id = substr($dept, 1);
} }    
        //GET SECTOR ID
 $query1="SELECT * FROM sectors_list where sector='$sect'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $sect=$row['sector_id'];
             $sect_id = substr($sect, 1);
} }    
        //GET DIRECTORATE  ID
      $query1="SELECT * FROM directorates_list where directorate='$dir'";
if ($diblo->real_query($query1)) {
    $res = $diblo->use_result();
    while ($row = $res->fetch_assoc()) {
            $dir=$row['directorate_id'];
             $dir_id = substr($dir, 3);
} }   
        
         //step 2: get counter
    $query2="SELECT * FROM referenceno_counter";
if ($diblo->real_query($query2)) {
    $res = $diblo->use_result();
  while ($row = $res->fetch_assoc()) {
            $count=$row['counter'];
}  } 
  
      //department
if(($dept!=='') AND ($sect=='') AND ($dir=='')){
        $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
         $ref_no="REF:NCG/$dept_id/$leadcounter/19-20";   
        //step 3: update the counter
        //include_once 'config.php';
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        
}
        //sector
        else if(($dept!=='') AND ($sect!=='')AND($dir=='')) {
                $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$leadcounter/19-20";
        //step 3: update the counter
      
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
        //directorate
        else if(($dept!=='') AND ($sect!=='')AND($dir!=='')){
               $counter=$count+1;
                 $leadcounter=sprintf("%04d", $counter); 
               $ref_no="REF:NCG/$dept_id/$sect_id/$dir_id/$leadcounter/19-20";
			
        //step 3: update the counter
   $sql="UPDATE referenceno_counter SET counter='$counter'";
                    if($stmnt=$diblo->prepare($sql)){
                    $stmnt->execute();      
                    }  
        }
	
		
        /****delete from the temp_memoholder where the Requestor is current user****/
                
            $sql8="DELETE FROM temp_memoholder WHERE requestor='$from'";
      if($stmnt=$diblo->prepare($sql8)){
       $stmnt->execute();      
          }
		
           //close         ?><?php
		
		
		//INSERT INTO TEMP_MEMO HOLDER
                    foreach ($temp_data as $row) {  
				$data=array();
			$data['recepient']=$row['user_id'];
            $data['nature']=$row['recipient_nature'];
			$data['referenceno']=$ref_no;
            $data['memotype']=$memotype;
            $data['financial_year']="2019-2020"; 
            $data['votehead']=$votehead;
            $data['urgency']=$urgency;
            $data['prevmemo']=$prevmemo;
			$data['requestor']=$from;
			$data['subject']=$subject;
            $data['datecreated']=$datecreated;
			$data['duedate']=$duedate;
			$data['introduction']=$introduction;
            $data['amount']=$amount;
            $data['approvedamount']=0;
            $data['comments']="";
            $data['comment_status']=0;
            $data['progress']=0;
            $data['status']='Inactive';
            $data['generalstatus']='Inactive';
            $data['generalprogress']=0;
            $data['view']=0;
            $data['availability']="";
            $data['vh_deduct_indicator']=0;

			$db->AutoExecute('temp_memoholder',$data,'INSERT');
      //open                  ?><?php
         require("../../_connect.php");
//connect to db
$mysqli = new mysqli($db_host,$db_user, $db_password, $db_name);
                        
    $query="SELECT * FROM temp_memoholder WHERE requestor='$from'";
        //execute query
if ($mysqli->real_query($query)) {
    //If the query was successful
    $res = $mysqli->use_result();
    while ($row = $res->fetch_assoc()) {
         $introduction=$row["introduction"];   
       $replacedone=str_ireplace('|', '&nbsp;',$introduction);
        $replacedtwo=str_ireplace('~', '&#39;',$replacedone);
         $replacedthree=str_ireplace('`', '&quot;',$replacedtwo);
    }
}
                        
                        
   $sqli="UPDATE temp_memoholder SET introduction='$replacedthree'";
     if($stmnt=$mysqli->prepare($sqli)){   
       $stmnt->execute(); 
}  
                        
       //close                 ?><?php

        
		}
            
		
		/***DELETE  ALL FROm TRAVEL TEMP MEMO HOLDER WHERE OFFICER_ID is current USER***/
		 //open ?><?php
		
		 require("../../_connect.php");
		$sql8="DELETE FROM temp_inttravel_memoholder WHERE officer_id='$from'";
      if($stmnt=$diblo->prepare($sql8)){
       $stmnt->execute();      
          }
		  
		 //close ?><?php
		
		//INSERT INTO INTERNATIONAL TRAVEL TEMP MEMO HOLDER
                    foreach ($temp_data as $row) {  
			$data=array();
			$data['referenceno']=$ref_no;
			$data['recipient']=$row['user_id'];
            $data['traveltype']=$memotype;
			$data['officer_id']=$from;
            $data['personal_number']=$intpersonalno;
            $data['designation']=$intdesignation; 
            $data['telephone']=$inttelephone;                    
            $data['reasons']=$intreasons;
			$data['country']=$intcountry;
			$data['city']=$intcity;
			$data['departure_date']=$intdepart;
			$data['return_date']=$intreturn;
			$data['travelmode']=$inttravelmode;
			$data['host']=$inthost;
			$data['sponsor']=$intsponsor;
            $data['subsistence']=$intsubsistence;
			$data['sponsortwo']=$intsponsortwo;
			$data['participation']=$intparticipation;
			$data['accomodation']=$intaccomodation;
            $data['travel_expenses']=$inttravelexp;
			$data['total_expenses']=$inttotal;
			$data['objectives']=$intobjs;
			$data['expected_benefits']=$intbenefits;
           

			$db->AutoExecute('temp_inttravel_memoholder',$data,'INSERT');
    

        
		}
		
		
		
		
		
        
		
        App::update_document_directory($ref_no,$from);
        
		
		$response['success']=1;
	
	
		
//open        	 ?>
        	 <?php
         
 //connect to db
  require("../../_connect.php");
//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
       
/**TRUNCATE THE TEMP_STORAGE AFTER USE**/
   $sql="DELETE FROM temp_storage WHERE author='$from'";
     if($stmnt=$db->prepare($sql)){
   
        $stmnt->execute(); 
}  
    
    

//close   ?>
			
        	<?php
	

		
		return json_encode($response);	
        
        //redirect to preview page
    
	}
	
	
	
	
	
    
    /**SAVE TO DRAFT TABLE**/
    
	public static function add_memo_draft_data($subject,$datecreated,$duedate,$introduction,$amount,$from,$memotype,$urgency,$prevmemo){
      $ses=$_SESSION["karibu"];
 

		global $db;
		$temp_data=$db->GetArray("SELECT * from temp_storage WHERE author='$from'");
		$ref_no=App::produce_reference_no();
       
		foreach ($temp_data as $row) {
				$data=array();
			$data['recepient']=$row['user_id'];
            $data['nature']=$row['recipient_nature'];
			$data['referenceno']=$ref_no;
            $data['memotype']=$memotype;
            $data['financial_year']="2019-2020"; 
            $data['votehead']="";
            $data['urgency']=$urgency;
            $data['prevmemo']=$prevmemo;
			$data['requestor']=$from;
			$data['subject']=$subject;
            $data['datecreated']=$datecreated;
			$data['duedate']=$duedate;
			$data['introduction']=$introduction;
            $data['amount']=$amount;
            $data['approvedamount']=0;
            $data['comments']="";
            $data['comment_status']=0;
            $data['progress']=0;
            $data['status']='Inactive';
            $data['generalstatus']='Inactive';
            $data['generalprogress']=0;
            $data['view']=0;
            $data['availability']="";
            $data['vh_deduct_indicator']=0;
            

			$db->AutoExecute('nonfinancialdrafts',$data,'INSERT');
			
		}
        
        	 
		
		  
//die($introduction);
        
          App::update_document_directory($ref_no,$from);
        
		App::delete_all_temp();
		
		$response['success']=1;
	//	$response['message']=$introduction;

		return json_encode($response);

        	



	}
    
    /**++**/

   public static function reference_no($length){
		  //  $substring_name=substr($username,0,3);
		    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    $clen   = strlen( $chars )-1;
		    $id  = '';

		    for ($i = 0; $i < $length; $i++) {
		            $id .= $chars[mt_rand(0,$clen)];
		    }
		 //   return ($substring_name."".$id);
		    return $id;
  }

  public static function check_refrence_number($reference){
  		global $db;

  		$get_ref=$db->GetOne("SELECT * FROM nonfinancialmemos WHERE referenceno='$reference'");

  		if(!$get_ref){
  			return false;
  		}else{
  			return true;
  		}
      
  }

  public static function produce_reference_no(){

  		$ref_no;
  		for ($i=0; $i <100 ; $i++) { 
  			$ref_no=App::reference_no(8);
  			if(!App::check_refrence_number($ref_no)){
  				break;
  			}
  		}
   

  		return $ref_no;
  }
    
     public static function update_document_directory($ref_no,$uploader){
		 
         	global $db;
  			$data=array();
			$data["ref_id"]=$ref_no;
			$data["active_status"]="1";

			$where_clause="uploaded_by='".$uploader."'AND active_status='".$uploader."'";
			$db->AutoExecute('tb_document_directory',$data, 'UPDATE',$where_clause);
  }
   
	
}
?>