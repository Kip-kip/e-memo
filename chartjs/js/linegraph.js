$(document).ready(function(){
  $.ajax({
    url : "http://localhost/e-memo/chartjs/memototal.php",
    type : "GET",
    success : function(data){
      console.log(data);

      var objectives = [];
      var memocount = [];
     
      objectives.push("Healthcare","Infrastructure","Sports","Education","Internal affairs","Agriculture","Technology",
"Staff training","Seminars");

        
     
        
      for(var i in data) {
        memocount.push(data[i].memocount);   
           }

      var chartdata = {
        labels: objectives,
        datasets: [
          {
            label: "Number of Memos",
            fill: true,
            lineTension: 0.3,
            backgroundColor: "#818181",
            borderColor: "#818181",
            pointHoverBackgroundColor: "#818181",
            pointHoverBorderColor: "#818181",
            data: memocount
          },
         
        ]
      };

      var ctx = $("#mycanvas");

      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata
      });
    },
    error : function(data) {

    }
  });
});



































$(document).ready(function(){
  $.ajax({
    url :"http://localhost/e-memo/chartjs/memobreakdown.php",
    type : "GET",
    success : function(data){
      console.log(data);

      var objectives = [];
      var approved = [];
      var rejected = [];
      var pending = [];

      for(var i in data) {
        objectives.push(data[i].objectives);
         approved.push(data[i].approved);
          rejected.push(data[i].rejected);
          pending.push(data[i].pending);
           }

      var chartdata = {
        labels: objectives,
        datasets: [
          {
            label: "Approved Memos",
            fill: true,
            lineTension: 0.2,
            backgroundColor: "#63726f",
            borderColor: "#0c7609",
            pointHoverBackgroundColor: "#0c7609",
            pointHoverBorderColor: "#0c7609",
            data: approved
          },
            {
            label: "Rejected Memos",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#ff1f19",
            borderColor: "#ff1f19",
            pointHoverBackgroundColor: "#ff1f19",
            pointHoverBorderColor: "#ff1f19",
            data: rejected
          },
            {
            label: "Pending Memos",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#63726f",
            borderColor: "#63726f",
            pointHoverBackgroundColor: "#63726f",
            pointHoverBorderColor: "#63726f",
            data: pending
          }
         
        ]
      };

      var ctx = $("#mycanvas");

      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata
      });
    },
    error : function(data) {

    }
  });
});