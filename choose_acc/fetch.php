<?php
session_start();
if(!isset($_SESSION['welcome'])){
    header("Location:login.php");
}
$ses=$_SESSION['welcome']+1;
//fetch.php
include_once '../config.php';
$columns = array('delegating_office');

$query = "SELECT * FROM delegates ";

$query .= ' WHERE  delegate_id LIKE "%'.$ses.'%" ';

if(isset($_POST["order"]))
{
 /*$query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' 
 ';*/
}
else
{
 $query .= 'ORDER BY id DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($mysqli, $query));

$result = mysqli_query($mysqli, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<div  data-id="'.$row["id"].'" data-column="delegating_office">' . $row["delegating_office"] . '</div>';
 $sub_array[] = '<button type="button" name="access" class="btn btn-success btn-sm access" id="'.$row["id"].'"><i class="zmdi zmdi-key"></i> access</button>';
 $data[] = $sub_array;
}

function get_all_data($mysqli)
{
 $query = "SELECT * FROM delegates";
 $result = mysqli_query($mysqli, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($mysqli),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
