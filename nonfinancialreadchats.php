<?php
session_start();
if(isset($_SESSION["karibu"])){
 if((time() - $_SESSION['last_time']) > 600) // Time in Seconds --10 Minutes
 {
session_destroy();
header('Location:login.php');
 }
 else{
 $_SESSION['last_time'] = time();
 }
}
else
{
 header('Location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    
    
      <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">

        <!-- App styles -->
        <link rel="stylesheet" href="css/app.min.css">
  </head>
    <body data-sa-theme="1">       

    	
<?php
 
require("./_connect.php");

//connect to db
$db = new mysqli($db_host,$db_user, $db_password, $db_name); 
if ($db->connect_errno) {
    //if the connection to the db failed
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
    if(isset($_POST["otherperson"])){
        
        $ses=$_SESSION['karibu'];
        $other = trim(mysqli_real_escape_string($db,$_POST["otherperson"]));
$query="SELECT * FROM chats WHERE receiver='$ses' AND sender='$other' OR receiver='$other' AND sender='$ses' ORDER BY id DESC";
//execute query
if ($db->real_query($query)) {
    //If the query was successful
    $res = $db->use_result();

    while ($row = $res->fetch_assoc()) {
         $id=$row["id"];
        $sender=$row["sender"];
        $receiver=$row["receiver"];
        $message=$row["message"];
        $time=$row['time'];
              echo  "<tr>";
        if($receiver!=$ses){
             echo "<td><div class=\"messages__item\">
                                    <div class=\"messages__details\">
                                        <p> $message</p>
                                        <small><i class=\"zmdi zmdi-time\"></i>$time</small>
                                    </div>
                                </div>
                                </td>";
                echo "</tr>";
      }
        else{
              echo "<td><div style=\"margin-left:170px;\"class=\"messages__item--right\">
                                    <div class=\"messages__details\">
                                        <p> $message</p>
                                        <small><i class=\"zmdi zmdi-time\"></i>$time</small>
                                    </div>
                                </div>
                                </td>";
                echo "</tr>";
        
        }
     
    }
}else{
    //If the query was NOT successful
    echo "An error occured";
    echo $db->errno;
}
        
            
    }
           
$db->close();
?>
</tbody>
    </table>
        <!--/div>
            </div-->
        <!-- Javascript -->
        <!-- Vendors -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

        <!-- Vendors: Data tables -->
        <script src="vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

        <!-- App functions and actions -->
<script src="js/app.min.js"></script>

        </body>
    
</html>